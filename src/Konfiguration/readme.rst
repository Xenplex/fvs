Konfiguration
==============

**FVS** arbeitet mit einer Konfigurationsdatei *konfig.cfg* um die Daten zur
Datenbankverbindung abzuspeichern und wieder aufrufen zu können.

.. automodule:: Konfiguration.konfig
   :members:
