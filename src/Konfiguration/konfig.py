# -*- coding: utf-8 -*-

# Ritchie Flick
# T3IFAN 2012/2013 Abschlussprojekt
# LN
# 'Feuerwehrverwaltungssoftware - FVS'
# 'src/Konfiguration/konfig.py'

# Importieren von Python Modulen
import configparser


def speichern_konfig(dbAdresse, dbPort, dbName, dbBenutzer, dbPasswort):
    """ *Abspeichern der Konfiguration in konfig.cfg*

    **Parameter**

    - dbAdresse: Die IP Adresse der Datenbank (Bsp. 127.0.0.1)
    - dbPort: Die Port Nummer auf dem die Datenbank läuft
    - dbName: Der Name der Datenbank
    - dbBenutzer: Der Benutzer der Datenbank
    - dbPasswort: Das Passwort des Benutzers

    **Rückgabewerte**

    - False: Wird zurückgegeben falls das Abspeichern fehlgeschlagen ist

    """
    konfig = configparser.ConfigParser()
    konfig.read('konfig.cfg')
    konfig['Einstellungen'] = {
        'Datenbankadresse': dbAdresse,
        'Datenbankport': dbPort,
        'Datenbankname': dbName,
        'Datenbankbenutzer': dbBenutzer,
        'Datenbankpasswort': dbPasswort
    }
    try:
        with open('konfig.cfg', 'w') as konfigdatei:
            konfig.write(konfigdatei)
    except IOError:
        return False


def laden_konfig():
    """ *Die Konfiguration wird aus konfig.cfg geladen*

    **Rückgabewerte**

    - einstellungen:

      - dbAdresse: Die IP Adresse der Datenbank (Bsp. 127.0.0.1)
      - dbPort: Die Port Nummer auf dem die Datenbank läuft
      - dbName: Der Name der Datenbank
      - dbBenutzer: Der Benutzer der Datenbank
      - dbPasswort: Das Passwort des Benutzers

    """
    try:
        konfig = configparser.ConfigParser()
        konfig.read('konfig.cfg')
        dbAdresse = konfig['Einstellungen']['Datenbankadresse']
        dbPort = konfig['Einstellungen']['Datenbankport']
        dbName = konfig['Einstellungen']['Datenbankname']
        dbBenutzer = konfig['Einstellungen']['Datenbankbenutzer']
        dbPasswort = konfig['Einstellungen']['Datenbankpasswort']
        einstellungen = (dbAdresse, dbPort, dbName, dbBenutzer, dbPasswort)
        return einstellungen
    except:
        return "error"
