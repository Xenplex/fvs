# -*- coding: utf-8 *-*

# Ritchie Flick
# T3IFAN 2012/2013 Abschlussprojekt
# LN
# 'Feuerwehrverwaltungssoftware - FVS'
# 'src/setup.py'

# Importieren von Python Modulen
import sys
from cx_Freeze import setup, Executable


# Packete werden angegeben welche speziell eingebunden werden müssen und die
# nicht eingebunden werden sollen
build_exe_options = {"packages": ["os", "PySide.QtNetwork"],
                     "excludes": ["tkinter"]
                     }

# Es muss ein Unterschied gemacht werden zwischen dem Windows OS und den Linux
# Distributionen
basis = None
if sys.platform == "win32":
    basis = "Win32GUI"

# Meta-Daten
setup(name="FVS",
      options={"build_exe": build_exe_options},
      executables=[Executable("fvs.py", base=basis)])
