# Ritchie Flick
# T3IFAN 2012/2013 Abschlussprojekt
# LN
# 'Feuerwehrverwaltungssoftware - FVS'
# 'src/UI_Aufbau/login.py'

# Importieren von Python Modulen
import sys

# Importieren von PySide Modulen
from PySide import QtCore
from PySide import QtDeclarative
from PySide.QtGui import QIcon

# Importieren von FVS Modulen
from Konfiguration import konfig
from Datenbank import db_verbindung
from Datenbank import sql_anweisungen

# Importieren der benötigten Kontroller
from Kontroller import button
from Kontroller import operationen


class QMLUI(QtDeclarative.QDeclarativeView):
    """ *Hauptklasse der grafischen Benutzeroberfläche des Login*

    **Beschreibung**

    Der Benutzer muss sich über das Loginfenster im System anmelden und
    abhängig davon mit welchem Benutzernamen er sich anmeldet, wird er in den
    entsprechenden Benutzerbereich weitergeleitet.
    Wenn noch keine Konfigurationsdatei erstellt worden ist, wird automatisch
    der Benutzer ins Konfigurationsfenster weitergeleitet.
    """

    # Initialisierung der Qt Signale
    konfigurieren_fenster_signal = QtCore.Signal()
    anmelde_daten_setzen_signal = QtCore.Signal()
    konfig_daten_signal = QtCore.Signal()
    zum_login_signal = QtCore.Signal()
    fehler_anmeldung_signal = QtCore.Signal()
    fehler_verbindung_signal = QtCore.Signal()
    verbindung_ok_signal = QtCore.Signal()

    def __init__(self):
        super(QMLUI, self).__init__()

        # Festlegung welche Funktionen von welchen Buttons aufgerufen werden
        button_komponenten_funktionen = {
            "btnAnmelden": self.anmelde_daten_setzen,
            "btnAbbrechen": self.abbrechen,
            "btnKonfig": self.konfigurieren,
            "btnAbspeichern": self.konfig_daten,
            "btnKonfigAbbrechen": self.zum_login
        }

        # Festlegung welche Funktionen von welchen Operationssignalen
        # werden sollen aufgerufen
        operationen_funktionen = {
            "anmelde_daten": self.anmelden,
            "konfig_daten": self.konfig_abspeichern
        }

        # Kontroller für die QMLUI werden eingerichtet
        button_kontroller = button.Kontroller(self,
                                              button_komponenten_funktionen)
        operationen_kontroller = operationen.Kontroller(self,
                                                        operationen_funktionen)

        # Attribute/Kontroller auf welche die QMLUI zugreifen kann
        self.kontext = self.rootContext()
        self.kontext.setContextProperty('button_kontroller', button_kontroller)
        self.kontext.setContextProperty('operationen_kontroller',
                                        operationen_kontroller)

        # Einstellungen laden
        # Die möglichen Einstellungen müssen bevor das Fenster aufgerufen wird
        # gesetzt sein, obwohl erst später erst kontrolliert wird ob das
        # lesen der Konfigurationsdatei erfolgreich war
        einstellungen = self.einstellungen_laden()

        # QMLUI wird geladen
        self.setWindowTitle("FVS - Benutzeranmeldung")
        self.setWindowIcon(QIcon("./icon.png"))
        self.setSource(QtCore.QUrl.fromLocalFile(
            './QMLUI/Login.qml'))
        self.setResizeMode(QtDeclarative.QDeclarativeView.SizeRootObjectToView)

        # QMLUI Attribute werden Python zugänglich gemacht
        self.ui_objekt = self.rootObject()

        # Slots der QMLUI
        self.konfigurieren_fenster_signal.connect(
            self.ui_objekt.konfigurieren_fenster_qml)
        self.anmelde_daten_setzen_signal.connect(
            self.ui_objekt.anmelde_daten_setzen_qml)
        self.konfig_daten_signal.connect(
            self.ui_objekt.konfig_speichern_qml)
        self.zum_login_signal.connect(
            self.ui_objekt.zum_login_qml)
        self.fehler_anmeldung_signal.connect(
            self.ui_objekt.fehler_anmeldung_qml)
        self.fehler_verbindung_signal.connect(
            self.ui_objekt.fehler_verbindung_qml)
        self.verbindung_ok_signal.connect(
            self.ui_objekt.verbindung_ok_qml)

        # Instanzvariablen
        self.fehler_verbindung = False

        # Testen ob eine Konfigurationsdatei existiert
        if einstellungen == "error":
            self.konfigurieren()
        else:
            self.verbinden(einstellungen)

    def einstellungen_laden(self):
        """ *Die Einstellungen werden aus der konfig.cfg Datei ausgelesen* """
        einstellungen = konfig.laden_konfig()
        try:
            self.kontext.setContextProperty('db_adresse', einstellungen[0])
            self.kontext.setContextProperty('db_port', einstellungen[1])
            self.kontext.setContextProperty('db_name', einstellungen[2])
            self.kontext.setContextProperty('db_benutzer', einstellungen[3])
            self.kontext.setContextProperty('db_passwort', einstellungen[4])
            return einstellungen
        except:
            pass

    def verbinden(self, einstellungen):
        """ *Verbindung zur Datenbank wird hergestellt *

        **Beschreibung**

        Die Verbindung zur Datenbank wird hergestellt mit den Parametern welche
        aus der *konfig.cfg* Datei ausgelesen wurden. War der Verbindungsaufbau
        nicht erfolgreich, wird der Benutzer zum Überprüfen der Konfiguration
        zum Konfigurationsfenster weitergeleitet.

        **Parameter**

        - einstellungen: Array welches die einzelnen Parameter für die
          Verbindung zur Datenbank enthält

        """
        # Verbindung zur Datenbank aufbauen
        try:
            self.datenbank_verbindung = db_verbindung.DB_Verbindung(
                einstellungen)
            self.fehler_verbindung = False
            self.verbindung_ok_signal.emit()
        except:
            self.fehler_verbindung = True
            self.konfigurieren()

    def konfigurieren(self):
        """ *Das Konfigurationsfenster wird geöffnet* """
        # Fehlermeldung falls die Verbindung zur Datenbank vorher
        # fehlgeschlagen war
        if self.fehler_verbindung:
            self.fehler_verbindung_signal.emit()

        self.konfigurieren_fenster_signal.emit()

    def anmelden(self):
        """ *Der Benutzer wird im System angemeldet*

        **Beschreibung**

        Der Benutzer wird mit seinem angegebenem Benutzernamen und
        Benutzerpasswort im System angemeldet. Abhängig davon welcher
        Benutzername angegeben wurde, wird der Benutzer in seinen jeweiligen
        Bereich weitergeleitet.

        +------------------------+----------------+-------------------------+
        | Benutzername           | Passwort       | Benutzerbereich         |
        +========================+================+=========================+
        | admin                  | admin          | Administrator           |
        +------------------------+----------------+-------------------------+
        | inventarist            | inventarist    | Inventarist             |
        +------------------------+----------------+-------------------------+
        | kommandant             | kommandant     | Inventarist, Kommandant |
        |                        |                | Maschinist, Sekretär    |
        +------------------------+----------------+-------------------------+
        | maschinist             | maschinist     | Maschinist              |
        +------------------------+----------------+-------------------------+
        | sekretaer              | sekretaer      | Sekretär                |
        +------------------------+----------------+-------------------------+
        """
        # Sicherstellen dass eine Datenbankverbindung besteht
        try:
            self.verbinden(self.einstellungen_laden())
            self.datenbank_verbindung
        except AttributeError:
            self.fehler_verbindung = True
            self.konfigurieren()
            return

        benutzer = self.ui_objekt.property('edtLogin')
        passwort = self.ui_objekt.property('edtPasswort')
        sql_anweisung = sql_anweisungen.sql(
            tabelle='tblBenutzer',
            select='idBenutzernummer',
            filtern='dtLogin="%s" AND dtPasswort="%s"' % (benutzer,
                                                          passwort))
        sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
        if not sql_resultate:
            self.fehler_anmeldung_signal.emit()
        elif benutzer == "admin":
            from UI_Aufbau import admin
            Admin = admin.QMLUI()
            Admin.setWindowState(QtCore.Qt.WindowMaximized)
            Admin.show()
            self.hide()
        elif benutzer == "kommandant":
            from UI_Aufbau import kommandant
            Kommandant = kommandant.QMLUI()
            Kommandant.setWindowState(QtCore.Qt.WindowMaximized)
            Kommandant.show()
            self.hide()
        elif benutzer == "sekretaer":
            from UI_Aufbau import sekretaer
            Sekretaer = sekretaer.QMLUI()
            Sekretaer.setWindowState(QtCore.Qt.WindowMaximized)
            Sekretaer.show()
            self.hide()
        elif benutzer == "inventarist":
            from UI_Aufbau import inventarist
            Inventarist = inventarist.QMLUI()
            Inventarist.setWindowState(QtCore.Qt.WindowMaximized)
            Inventarist.show()
            self.hide()
        elif benutzer == "maschinist":
            from UI_Aufbau import maschinist
            Maschinist = maschinist.QMLUI()
            Maschinist.setWindowState(QtCore.Qt.WindowMaximized)
            Maschinist.show()
            self.hide()

    def abbrechen(self):
        """ *FVS wird geschlossen* """
        sys.exit()

    def anmelde_daten_setzen(self):
        """ *Auslesen der Anmeldedaten* """
        self.anmelde_daten_setzen_signal.emit()

    def konfig_daten(self):
        """ *Die Konfigurationsdaten werden ausgelesen* """
        self.konfig_daten_signal.emit()

    def konfig_abspeichern(self):
        """ *Die Konfigurationsdaten werden in die konfig.cfg abgespeichert*
        """
        adresse = self.ui_objekt.property('edtAdresse')
        port = self.ui_objekt.property('edtPort')
        name = self.ui_objekt.property('edtName')
        benutzer = self.ui_objekt.property('edtBenutzer')
        passwort = self.ui_objekt.property('edtKonfigPasswort')
        konfig.speichern_konfig(dbAdresse=adresse, dbPort=port, dbName=name,
                                dbBenutzer=benutzer, dbPasswort=passwort)
        self.zum_login_signal.emit()

    def zum_login(self):
        """ *Der Benutzer wird zurück zum Anmeldefenster geleitet* """
        self.zum_login_signal.emit()
