# -*- coding: utf-8 -*-

# Ritchie Flick
# T3IFAN 2012/2013 Abschlussprojekt
# LN
# 'Feuerwehrverwaltungssoftware - FVS'
# 'src/UI_Aufbau/sekretaer.py'

# Importieren von Python Modulen
from time import strptime, strftime

# Importieren von PySide Modulen
from PySide import QtCore
from PySide import QtDeclarative
from PySide.QtGui import QIcon

# Importieren von FVS Modulen
from Konfiguration import konfig
from Datenbank import db_verbindung
from Datenbank import sql_anweisungen
from Daten_Modelle import verpacken_modelisieren
from Daten_Objekte import ereignis_objekt
from Daten_Objekte import inventarliste_objekt
from Daten_Objekte import mitglied_objekt
from Daten_Objekte import fahrzeug_objekt
from Daten_Objekte import protokoll_objekt
from Logik import protokoll_logik
from Logik import bericht_erstellung

# Importieren der benötigten Kontroller
from Kontroller import listen
from Kontroller import button
from Kontroller import operationen


class QMLUI(QtDeclarative.QDeclarativeView):
    """ *Hauptklasse der grafischen Benutzeroberfläche des Sekretärs*

    **Beschreibung**

    Die **QMLUI** (grafische Benutzeroberfläche) des Sekretärs wird
    aufgebaut, die Verbindung zur Datenbank wird aufgebaut und alle nötigen
    Signale/Slots und Funktionen zum arbeiten mit der **QMLUI** werden
    implementiert.
    """

    # Initialisierung der Qt Signale
    protokoll_verwaltung_signal = QtCore.Signal()
    protokoll_info_setzen_signal = QtCore.Signal()
    zum_hauptmenu_signal = QtCore.Signal()
    ereignis_verwaltung_signal = QtCore.Signal()
    ereignisInfo_signal = QtCore.Signal()
    einsatz_verwaltung_signal = QtCore.Signal()
    einsatzInfo_signal = QtCore.Signal()
    protokoll_info_abspeichern_signal = QtCore.Signal()
    abspeichern_ereignis_signal = QtCore.Signal()

    def __init__(self):
        super(QMLUI, self).__init__()
        # Verbindung zur Datenbank aufbauen
        try:
            einstellungen = konfig.laden_konfig()
            self.datenbank_verbindung = db_verbindung.DB_Verbindung(
                einstellungen)
        except:
            print("Es konnte keine Datenbankverbindung hergestellt werden")

        # Festlegung welche Listen welche Funktionen aufrufen sollen
        listen_komponenten_funktionen = {
            "lstProtokolle": [True, self.protokoll_info_setzen],
            "cmbEreignisAuswahl": [True, self.auswahlSchlusselEreignis],
            "cmbInventarAuswahl": [True, self.auswahlSchlusselInventar],
            "cmbMitgliedAuswahl": [True, self.auswahlSchlusselMitglied],
            "cmbFahrzeugAuswahl": [True, self.auswahlSchlusselFahrzeug],
            "lstReferenzen": [True, self.referenzSchlussel],
            "lstEreignisse": [True, self.ereignisInfo],
            "lstTeilnehmer": [True, self.teilnehmerSchlussel],
            "cmbTeilnehmerAuswahl": [True, self.teilnehmerAuswahl],
            "lstEinsatz": [True, self.einsatzInfo]
        }

        # Festlegung welche Buttons welche Funktionen aufrufen sollen
        button_komponenten_funktionen = {
            "btnProtokollVerwaltung": self.protokoll_verwaltung,
            "btnZumHauptmenu": self.zum_hauptmenu_signal.emit,
            "btnEreignisHinzu": self.ereignis_hinzu,
            "btnInventarHinzu": self.inventar_hinzu,
            "btnMitgliedHinzu": self.mitglied_hinzu,
            "btnFahrzeugHinzu": self.fahrzeug_hinzu,
            "btnEntfernen": self.referenz_entfernen,
            "btnEreignisVerwaltung": self.ereignis_verwaltung,
            "btnTeilnehmerEntfernen": self.teilnehmer_entfernen,
            "btnTeilnehmerHinzu": self.teilnehmer_hinzu,
            "btnEinsatzberichtVerwaltung": self.einsatz_verwaltung,
            "btnNeuProtokoll": self.neu_protokoll,
            "btnEntfernenProtokoll": self.entfernen_protokoll,
            "btnProtokollAbspeichern": self.protokoll_speichern,
            "btnProtokollBericht": self.protokoll_bericht,
            "btnNeuEreignis": self.neu_ereignis,
            "btnEntfernenEreignis": self.entfernen_ereignis,
            "btnEreignisAbspeichern": self.abspeichern_ereignis
        }

        # Festlegung welche Funktionen von welchen Operationssignalen
        # werden sollen aufgerufen
        operationen_funktionen = {
            "protokoll_variablen_gesetzt": self.protokoll_info_db,
            "ereignis_daten": self.ereignis_in_db
        }

        # Kontroller für die QMLUI werden eingerichtet
        listen_kontroller = listen.Kontroller(self,
                                              listen_komponenten_funktionen)
        button_kontroller = button.Kontroller(self,
                                              button_komponenten_funktionen)
        operationen_kontroller = operationen.Kontroller(self,
                                                        operationen_funktionen)

        # Attribute/Kontroller auf welche die QML UI zugreifen kann
        self.kontext = self.rootContext()
        self.kontext.setContextProperty('listen_kontroller', listen_kontroller)
        self.kontext.setContextProperty('button_kontroller', button_kontroller)
        self.kontext.setContextProperty('operationen_kontroller',
                                        operationen_kontroller)

        # QMLUI wird geladen
        self.setWindowTitle("FVS - Sekretär")
        self.setWindowIcon(QIcon("./icon.png"))
        self.setSource(QtCore.QUrl.fromLocalFile(
            './QMLUI/Sekretaer.qml'))
        self.setResizeMode(QtDeclarative.QDeclarativeView.SizeRootObjectToView)

        # QMLUI Attribute werden zugänglich gemacht
        self.ui_objekt = self.rootObject()

        # Slots der QMLUI
        self.protokoll_verwaltung_signal.connect(
            self.ui_objekt.protokoll_verwaltung_qml)
        self.protokoll_info_setzen_signal.connect(
            self.ui_objekt.protokoll_info_setzen_qml)
        self.zum_hauptmenu_signal.connect(
            self.ui_objekt.zum_hauptmenu_qml)
        self.ereignis_verwaltung_signal.connect(
            self.ui_objekt.ereignis_verwaltung_qml)
        self.ereignisInfo_signal.connect(
            self.ui_objekt.ereignisInfo_qml)
        self.einsatz_verwaltung_signal.connect(
            self.ui_objekt.einsatz_verwaltung_qml)
        self.einsatzInfo_signal.connect(
            self.ui_objekt.einsatzInfo_qml)
        self.protokoll_info_abspeichern_signal.connect(
            self.ui_objekt.protokoll_info_abspeichern_qml)
        self.abspeichern_ereignis_signal.connect(
            self.ui_objekt.abspeichern_ereignis_qml)

    def protokoll_verwaltung(self, state_wechseln=True):
        """ *Das Menü zur Protokollverwaltung wird aufgebaut*

        **Beschreibung**

        Das Menü zur Verwaltung der Protokolle wird aufgebaut. Die Hauptauswahl
        für Protokolle wird aufgebaut und Listen von

        - Fahrzeugen
        - Mitglieder
        - Inventargegenstände
        - Ereignisse

        werden aufgebaut, welche in *AuswahlListen* (ComboBox) gesetzt werden,
        mit welchen Referenzen gesetzt werden können.

        **Parameter**

        - state_wechseln: *Boolean* Angabe ob nur die einzelnen Listen neu
          erstellt werden sollen oder auch der *State* der **QMLUI** geändert
          werden soll.

        """
        # Hilfsvariable um herauszufinden ob ein Fehler aufgetreten ist
        fehler = False
        # Eine Liste aller Protokolle wird erstellt zur Hauptauswahl
        sql_anweisung = sql_anweisungen.sql(tabelle="tblProtokolle")
        try:
            sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
        except:
            self.kontext.setContextProperty('protokoll_liste', None)
            fehler = True
        datensatz = []
        if sql_resultate:
            for eintrag in sql_resultate:
                try:
                    datensatz.append(protokoll_objekt.Protokoll(
                        schlussel=eintrag[0],
                        bezeichnung=eintrag[1], datum=eintrag[2],
                        inhalt=eintrag[3]))
                except:
                    datensatz.append(protokoll_objekt.Protokoll(None))
        datenliste = verpacken_modelisieren.list_verpackung_modelisieren(
            datensatz)
        # Kontrollieren ob nicht vorher ein Fehler aufgetreten war
        if not fehler:
            self.kontext.setContextProperty('protokoll_liste', datenliste)
        else:
            fehler = False

        # Eine Liste aller Ereignisse zum setzen als Referenzen wird erstellt
        sql_resultate = None
        sql_anweisung = sql_anweisungen.sql(tabelle="tblEreignis")
        try:
            sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
        except:
            self.kontext.setContextProperty('ereignis_liste',
                                            'Keine Ereignisse')
            fehler = True
        datensatz = []
        if sql_resultate:
            for eintrag in sql_resultate:
                try:
                    datensatz.append(ereignis_objekt.Ereignis(
                        schlussel=eintrag[0], typ=eintrag[1],
                        startdatum=eintrag[2], startzeit=eintrag[3],
                        enddatum=eintrag[4], endzeit=eintrag[5],
                        bezeichnung=eintrag[6]))
                except:
                    datensatz.append(ereignis_objekt.Ereignis(None))
        datenliste = verpacken_modelisieren.list_verpackung_modelisieren(
            datensatz)
        # Kontrollieren ob nicht vorher ein Fehler aufgetreten war
        if not fehler:
            self.kontext.setContextProperty('ereignis_liste', datenliste)
        else:
            fehler = False

        # Eine Liste aller Inventargegenstände zum setzen als Refernzen wird
        # erstellt
        sql_anweisung = sql_anweisungen.sql(
            select="idInventarnummer, dtBezeichnung, dtKategorie, dtKnappheit",
            tabelle="tblInventar")
        try:
            sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
        except:
            self.kontext.setContextProperty('inventar_liste', None)
            fehler = True
        datensatz = []
        if sql_resultate:
            for eintrag in sql_resultate:
                try:
                    datensatz.append(inventarliste_objekt.Inventarliste(
                        schlussel=eintrag[0], bezeichnung=eintrag[1],
                        kategorie=eintrag[2], knappheit=eintrag[3]))
                except:
                    datensatz.append(inventarliste_objekt.Inventarliste(None))
        datenliste = verpacken_modelisieren.list_verpackung_modelisieren(
            datensatz)
        # Kontrollieren ob nicht vorher ein Fehler aufgetreten war
        if not fehler:
            self.kontext.setContextProperty('inventar_liste', datenliste)
        else:
            fehler = False

        # Eine Liste aller Mitglieder zum setzen als Referenzen wird erstellt
        sql_anweisung = sql_anweisungen.sql(
            tabelle="tblMitglied",
            select="idStammlistennummer, dtName, dtVorname")
        try:
            sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
        except:
            self.kontext.setContextProperty('mitglied_liste', None)
            fehler = True
        datensatz = []
        if sql_resultate:
            for eintrag in sql_resultate:
                try:
                    datensatz.append(mitglied_objekt.Mitglied(
                        schlussel=eintrag[0],
                        name=eintrag[1], vorname=eintrag[2]))
                except:
                    datensatz.append(mitglied_objekt.Mitglied(None))
        datenliste = verpacken_modelisieren.list_verpackung_modelisieren(
            datensatz)
        # Kontrollieren ob nicht vorher ein Fehler aufgetreten war
        if not fehler:
            self.kontext.setContextProperty('mitglied_liste', datenliste)
        else:
            fehler = False

        # Eine Liste aller Fahrzeuge zum setzen als Referenzen wird erstellt
        sql_anweisung = sql_anweisungen.sql(tabelle='tblFahrzeug')
        try:
            sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
        except:
            self.kontext.setContextProperty('fahrzeug_liste', None)
            fehler = True
        datensatz = []
        # Instanz Variablen für Fahrzeug() und FahrzeugTyp() werden gesetzt
        if sql_resultate:
            for eintrag in sql_resultate:
                try:
                    datensatz.append(fahrzeug_objekt.Fahrzeug(
                        schlussel=eintrag[0],
                        kennzeichen=eintrag[1], bezeichnung=eintrag[2],
                        besatzung=eintrag[3], technisches_problem=eintrag[4],
                        kommentar=eintrag[5], tuv=eintrag[6],
                        werkstatt=eintrag[7], typ=eintrag[8]))
                except:
                    datensatz.append(fahrzeug_objekt.Fahrzeug(None))
        datenliste = verpacken_modelisieren.list_verpackung_modelisieren(
            datensatz)
        # Kontrollieren ob nicht vorher ein Fehler aufgetreten war
        if not fehler:
            self.kontext.setContextProperty('fahrzeug_liste', datenliste)

        if state_wechseln:
            self.protokoll_verwaltung_signal.emit()

    def protokoll_info_setzen(self, protokoll):
        """ *Die Daten des ausgewählten Protokolls werden gesetzt*

        **Beschreibung**

        Die Informationen eines Protokolls werden gesetzt und der gesamte
        **Datensatz** des jeweiligen Protokolls wird in eine Klassenvariable
        gespeichert, zur späteren weiter verwendung.

        **Parameter**

        - protokoll: Der **datensatz** des in der Liste ausgewählten Protokolls

        """
        self.protokoll = protokoll
        self.schlussel = protokoll.datensatz.schlussel
        datum = protokoll.datensatz.datum
        bezeichnung = protokoll.datensatz.bezeichnung
        try:
            referenzen = protokoll_logik.referenzenListe(
                self.datenbank_verbindung, self.schlussel)
            datenliste = verpacken_modelisieren.list_verpackung_modelisieren(
                referenzen)
            self.kontext.setContextProperty('protokoll_referenzen', datenliste)
        except:
            self.kontext.setContextProperty('protokoll_referenzen', None)
        inhalt = protokoll.datensatz.inhalt
        self.kontext.setContextProperty('protokoll_datum', str(datum))
        self.kontext.setContextProperty('protokoll_inhalt', inhalt)
        self.kontext.setContextProperty('protokoll_bezeichnung_info',
                                        bezeichnung)
        self.protokoll_info_setzen_signal.emit()

    def auswahlSchlusselEreignis(self, packung):
        """ *Der Schlüssel des ausgewählten Ereignisses wird gesetzt*

        **Beschreibung**

        Der Schlüssel des zuletzt ausgewählten Ereignisses wird gesetzt.

        **Parameter**

        - packung: **datensatz** des ausgewählten Ereignisses in der
          Auswahliste

        """
        self.schlusselEreignis = packung.datensatz.schlussel

    def auswahlSchlusselInventar(self, packung):
        """ *Der Schlüssel des ausgewählten Inventargegenstandes wird gesetzt*

        **Parameter**

        - packung: Verpackter Datensatz eines Inventargegenstandes

        """
        self.schlusselInventar = packung.datensatz.schlussel

    def auswahlSchlusselMitglied(self, packung):
        """ *Der Schlüssel des ausgewählten Mitgliedes wird gesetzt*

        **Parameter**

        - packung: Verpackter Datensatz eines Inventargegenstandes

        """
        self.schlusselMitglied = packung.datensatz.schlussel

    def auswahlSchlusselFahrzeug(self, packung):
        """ *Der Schlüssel des ausgewählten Fahrzeuges wird gesetzt*

        **Parameter**

        - packung: Verpackter Datensatz eines Inventargegenstandes

        """
        self.schlusselFahrzeug = packung.datensatz.schlussel

    def ereignis_hinzu(self):
        """ *Ein neues Ereignis wird als Referenz hinzugefügt* """
        sql_anweisung = sql_anweisungen.hinzufugen(
            tabelle='tblEreignis_Protokoll_Referenzen',
            felder='fiProtokoll, fiEreignisnummerProtokoll',
            werte='%s, %s' % (self.schlussel, self.schlusselEreignis))
        self.datenbank_verbindung.execute(sql_anweisung, True)
        self.protokoll_info_setzen(self.protokoll)

    def inventar_hinzu(self):
        """ *Ein neuer Inventargegenstand wird als Referenz hinzugefügt* """
        sql_anweisung = sql_anweisungen.hinzufugen(
            tabelle='tblInventar_Protokoll_Referenzen',
            felder='fiProtokollInventar, fiInventarnummerProtokoll',
            werte='%s, %s' % (self.schlussel, self.schlusselInventar))
        self.datenbank_verbindung.execute(sql_anweisung, True)
        self.protokoll_info_setzen(self.protokoll)

    def mitglied_hinzu(self):
        """ *Ein neues Mitglied wird als Referenz hinzugefügt* """
        sql_anweisung = sql_anweisungen.hinzufugen(
            tabelle='tblMitglied_Protokoll_Referenzen',
            felder='fiProtokollMitglied, fiStammlistennummerProtokoll',
            werte='%s, %s' % (self.schlussel, self.schlusselMitglied))
        self.datenbank_verbindung.execute(sql_anweisung, True)
        self.protokoll_info_setzen(self.protokoll)

    def fahrzeug_hinzu(self):
        """ *Ein neues Fahrzeug wird als Referenz hinzugefügt* """
        sql_anweisung = sql_anweisungen.hinzufugen(
            tabelle='tblFahrzeug_Protokoll_Referenzen',
            felder='fiProtokollFahrzeug, fiFahrzeugNummerProtokoll',
            werte='%s, %s' % (self.schlussel, self.schlusselFahrzeug))
        self.datenbank_verbindung.execute(sql_anweisung, True)
        self.protokoll_info_setzen(self.protokoll)

    def referenzSchlussel(self, packung):
        """ *Der Schlussel der letzten ausgewählten Referenz wird gesetzt*

        **Parameter**

        - packung: Verpackter Datensatz eines Referenz

        """
        self.referenz = packung.datensatz.schlussel
        self.referenzTabelle = packung.datensatz.referenzTabelle

    def referenz_entfernen(self):
        """ *Die momentan ausgewählte Referenz wird gelöscht* """
        # Inventarreferenz
        if self.referenzTabelle == 1:
            sql_anweisung = sql_anweisungen.entfernen(
                tabelle='tblInventar_Protokoll_Referenzen',
                filtern='(fiProtokollInventar = %s) AND '
                '(fiInventarnummerProtokoll = %s)' % (self.schlussel,
                                                      self.referenz))
        # Ereignisreferenz
        elif self.referenzTabelle == 2:
            sql_anweisung = sql_anweisungen.entfernen(
                tabelle='tblEreignis_Protokoll_Referenzen',
                filtern='fiProtokoll =%s AND fiEreignisnummerProtokoll =%s' % (
                    self.schlussel, self.referenz))

        # Mitgliedreferenz
        elif self.referenzTabelle == 3:
            sql_anweisung = sql_anweisungen.entfernen(
                tabelle='tblMitglied_Protokoll_Referenzen',
                filtern='fiProtokollMitglied = %s AND '
                'fiStammlistennummerProtokoll = %s' % (self.schlussel,
                                                       self.referenz))
        # Fahrzeugreferenz
        elif self.referenzTabelle == 4:
            sql_anweisung = sql_anweisungen.entfernen(
                tabelle='tblFahrzeug_Protokoll_Referenzen',
                filtern='fiProtokollFahrzeug = %s AND '
                'fiFahrzeugNummerProtokoll = %s' % (self.schlussel,
                                                    self.referenz))
        self.datenbank_verbindung.execute(sql_anweisung, True)
        self.protokoll_info_setzen(self.protokoll)

    def ereignis_verwaltung(self, einsatz=False):
        """ *Das Menü zur Vewaltung von Ereignissen wird aufgerufen*

        **Parameter**

        - einsatz: Wenn *einsatz* gesetzt ist, werden nur Ereignisse welche als
          Einsatz markiert wurden angezeigt

        """
        # Hilfsvariable um herauszufinden ob ein Fehler aufgetreten ist
        fehler = False
        # Ereignis Typen
        ereignis_typen = ['Fest', 'Lehrgang', 'Medizinische Kontrolle',
                          'Einsatz', 'Generalversammlung', 'Übung',
                          'Sonstiges']
        datensatz = []
        for eintrag in ereignis_typen:
            datensatz.append(ereignis_objekt.EreignisTyp(
                typ=eintrag))
        datenliste = verpacken_modelisieren.list_verpackung_modelisieren(
            datensatz)
        self.kontext.setContextProperty('ereignis_typen', datenliste)

        # Einsatz Typen
        einsatz_typen = ['Feuer', 'Verkehrssunfall', 'Technischer Einsatz']
        datensatz = []
        for eintrag in einsatz_typen:
            datensatz.append(ereignis_objekt.EreignisTyp(
                typ=eintrag))
        datenliste = verpacken_modelisieren.list_verpackung_modelisieren(
            datensatz)
        self.kontext.setContextProperty('einsatz_typen', datenliste)

        # Liste aller Mitglieder
        sql_anweisung = sql_anweisungen.sql(
            tabelle='tblMitglied',
            select='idStammlistennummer, dtName, dtVorname')
        try:
            sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
        except:
            self.kontext.setContextProperty('mitglied_liste', None)
            fehler = True
        datensatz = []
        if sql_resultate:
            for eintrag in sql_resultate:
                try:
                    datensatz.append(mitglied_objekt.Mitglied(
                        schlussel=eintrag[0], name=eintrag[1], vorname=eintrag[2]))
                except:
                    datensatz.append(mitglied_objekt.Mitglied(None))
        datenliste = verpacken_modelisieren.list_verpackung_modelisieren(
            datensatz)
        if not fehler:
            self.kontext.setContextProperty('mitglied_liste', datenliste)

        # Falls es sich um einen Einsatzbericht handelt
        if einsatz:
            # Liste aller Einsätze
            sql_anweisung = sql_anweisungen.sql(
                tabelle='tblEreignis',
                filtern='dtEreignisTyp="Einsatz"')
        else:
            # Liste aller Ereignisse
            sql_anweisung = sql_anweisungen.sql(tabelle='tblEreignis')
        try:
            fehler = False
            sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
        except:
            self.kontext.setContextProperty('ereignis_liste', None)
            fehler = True
        datensatz = []
        if sql_resultate:
            for eintrag in sql_resultate:
                try:
                    datensatz.append(ereignis_objekt.Ereignis(
                        schlussel=eintrag[0],
                        typ=eintrag[1], startdatum=eintrag[2],
                        startzeit=eintrag[3], enddatum=eintrag[4],
                        endzeit=eintrag[5], bezeichnung=eintrag[6]))
                except:
                    datensatz.append(ereignis_objekt.Ereignis(None))
            datenliste = verpacken_modelisieren.list_verpackung_modelisieren(
                datensatz)
            self.kontext.setContextProperty('ereignis_liste', datenliste)
        else:
            self.kontext.setContextProperty('ereignis_liste', None)

        # Kontrolle in welchem Menü sich der Benutzer befindet
        if einsatz:
            self.einsatz_verwaltung_signal.emit()
        else:
            self.ereignis_verwaltung_signal.emit()

    def ereignisInfo(self, packung, einsatz=False):
        """ *Die Informationen eines Ereignisses werden gesetzt*

        **Parameter**

        - packung: Verpackter Datensatz eines Ereignisses
        - einsatz: Wenn *einsatz* auf True steht, werden weitere Informationen
          eines Einsatzes ausgelesen

        """
        # Hilfsvariable um herauszufinden ob ein Fehler aufgetreten ist
        fehler = False
        self.packung = packung
        self.ereignisSchlussel = packung.datensatz.schlussel
        self.kontext.setContextProperty('ereignis_bezeichnung',
                                        packung.datensatz.bezeichnung)
        self.kontext.setContextProperty('ereignis_typ_t',
                                        packung.datensatz.typ)
        self.kontext.setContextProperty('ereignis_startdatum',
                                        str(packung.datensatz.startdatum))
        self.kontext.setContextProperty('ereignis_startzeit',
                                        str(packung.datensatz.startzeit))
        self.kontext.setContextProperty('ereignis_enddatum',
                                        str(packung.datensatz.enddatum))
        self.kontext.setContextProperty('ereignis_endzeit',
                                        str(packung.datensatz.endzeit))

        # Liste aller Mitglieder welche an einem Ereignis teilgenommen haben
        sql_anweisung = sql_anweisungen.sql(
            tabelle='tblTeilgenommen, tblMitglied',
            select="idStammlistennummer, dtName, dtVorname",
            filtern="fiStammlistennummerTeilnahme=idStammlistennummer AND "
            "fiEreignisnummer=%s" % (self.ereignisSchlussel))
        try:
            sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
        except:
            self.kontext.setContextProperty('teilnahme_liste', None)
            fehler = True
        datensatz = []
        if sql_resultate:
            for eintrag in sql_resultate:
                try:
                    datensatz.append(mitglied_objekt.Mitglied(
                        schlussel=eintrag[0],
                        name=eintrag[1], vorname=eintrag[2]))
                except:
                    datensatz.append(mitglied_objekt.Mitglied(None))
            datenliste = verpacken_modelisieren.list_verpackung_modelisieren(
                datensatz)
            self.kontext.setContextProperty('teilnahme_liste', datenliste)
        else:
            self.kontext.setContextProperty('teilnahme_liste', None)

        if einsatz:
            sql_anweisung = sql_anweisungen.sql(
                tabelle='tblEinsatzbericht',
                select='*', filtern='fiEreignisnummerEinsatz=%s' % (
                    self.ereignisSchlussel))
            try:
                fehler = False
                eintrag = self.datenbank_verbindung.execute(sql_anweisung)
            except:
                self.kontext.setContextProperty('einsatz_einsatztyp', None)
                self.kontext.setContextProperty('einsatz_ortschaft', None)
                self.kontext.setContextProperty('einsatz_adresse', None)
                self.kontext.setContextProperty('einsatz_postleitzahl', None)
                self.kontext.setContextProperty('betreff_name', None)
                self.kontext.setContextProperty('betreff_vorname', None)
                self.einsatzInfo_signal.emit()
                fehler = True
            if not fehler:
                self.kontext.setContextProperty('einsatz_einsatztyp',
                                                eintrag[0][1])
                self.kontext.setContextProperty('einsatz_ortschaft',
                                                eintrag[0][2])
                self.kontext.setContextProperty('einsatz_adresse',
                                                eintrag[0][3])
                self.kontext.setContextProperty('einsatz_postleitzahl',
                                                eintrag[0][4])
                self.kontext.setContextProperty('betreff_name',
                                                eintrag[0][5])
                self.kontext.setContextProperty('betreff_vorname',
                                                eintrag[0][6])
                self.einsatzInfo_signal.emit()
        self.ereignisInfo_signal.emit()

    def teilnehmerSchlussel(self, packung):
        """ *Der Schlüssel des ausgewählten Teilnehmers wird gesetzt*

        **Parameter**

        - packung: Verpackter Datensatz eines Mitglieds

        """
        self.schlusselTeilnehmer = packung.datensatz.schlussel

    def teilnehmer_entfernen(self):
        """ *Der ausgewählte Teilnehmer wird entfernt* """
        sql_anweisung = sql_anweisungen.entfernen(
            tabelle='tblTeilgenommen',
            filtern='fiStammlistennummerTeilnahme=%s' %
            self.schlusselTeilnehmer)
        self.datenbank_verbindung.execute(sql_anweisung, True)
        self.ereignisInfo(self.packung)

    def teilnehmer_hinzu(self):
        """ *Der ausgewählte Teilnehmer wird der Liste hinzugefügt* """
        sql_anweisung = sql_anweisungen.hinzufugen(
            tabelle='tblTeilgenommen',
            felder='fiStammlistennummerTeilnahme, fiEreignisnummer',
            werte='%s, %s' % (self.schlusselTeilnehmerAuswahl,
                              self.ereignisSchlussel))
        self.datenbank_verbindung.execute(sql_anweisung, True)
        self.ereignisInfo(self.packung)

    def teilnehmerAuswahl(self, packung):
        """ *Der Schlüssel des für einen neuen Teilnehmer wird gesetzt*

        **Parameter**

        - packung: Verpackter Datensatz eines Mitglieds

        """
        self.schlusselTeilnehmerAuswahl = packung.datensatz.schlussel

    def einsatz_verwaltung(self):
        """ *Die Einsatzverwaltung wird aufgerufen* """
        self.ereignis_verwaltung(einsatz=True)
        self.einsatz_verwaltung_signal.emit()

    def einsatzInfo(self, packung):
        """ *Die Informationen eines Einsatzes werden gesetzt*

        **Parameter**

        - packung: Verpackter Datensatz eines Ereignisses
        """
        self.ereignisInfo(packung, einsatz=True)

    def neu_protokoll(self):
        """ *Ein neues Protokoll wird in der Datenbank erstellt* """
        sql_anweisung = sql_anweisungen.hinzufugen(tabelle='tblProtokolle')
        self.datenbank_verbindung.execute(sql_anweisung, True)
        self.protokoll_verwaltung()

    def entfernen_protokoll(self):
        """ *Ausgewähltes Protokoll wird aus der Datenbank gelöscht* """
        sql_anweisung = sql_anweisungen.entfernen(
            tabelle='tblProtokolle',
            filtern="idProtokoll=%s" % self.schlussel)
        self.datenbank_verbindung.execute(sql_anweisung, True)
        self.protokoll_verwaltung()

    def protokoll_speichern(self):
        """ *Der Vorgang zum Abspeichern der Protokolldaten wird vorbereitet*
        """
        self.protokoll_info_abspeichern_signal.emit()

    def protokoll_info_db(self):
        """ *Die Informationen eines Protokolls werden in die DB gespeichert*

        **Beschreibung**

        Der Python Unterbau geht die Daten des ausgewählten Protokolls
        aus der **QMLUI** auslesen und speichert diese Daten in die Datenbank
        ab.
        """
        # Attribute aus der QMLUI werden abgefragt
        datum = self.ui_objekt.property('edtProtokollDatum')
        bezeichnung = self.ui_objekt.property('edtProtokollBezeichnung')
        inhalt = self.ui_objekt.property('edtProtokollInhalt')

        # Falls ein Fehler während der Konvertierung auftritt
        try:
            datum = strptime(datum, "%d-%m-%Y")
            datum = strftime("%Y-%m-%d", datum)
        except ValueError:
            pass

        # String für die SQL Anweisung wird zusammengesetzt
        feld_akt = 'dtBezeichnungProtokoll = "%s", ' % bezeichnung
        feld_akt = feld_akt + 'dtErstellungsdatum = "%s", ' % str(datum)
        feld_akt = feld_akt + 'dtProtokollInhalt = "%s"' % inhalt

        # SQL Anweisung durchführen
        sql_anweisung = sql_anweisungen.aktualisieren(
            tabelle='tblProtokolle',
            aktualisierung=feld_akt, filtern='idProtokoll=%s' % self.schlussel)
        self.datenbank_verbindung.execute(sql_anweisung, True)
        self.protokoll_verwaltung(state_wechseln=False)

    def protokoll_bericht(self):
        """ *Ein Bericht des aktuellen Protokolls wird erstellt*

        **Beschreibung**

        Ein Bericht wird aus dem aktuellen Protokoll erstellt. Als erstes wird
        der eigentliche Inhalt des Protokolls erstellt und anschließend wird
        die Liste aller Referenzen ausgelesen und die Informationen der
        Referenzen werden gesetzt.
        """
        # Hauptinformationen setzen
        bezeichnung = self.protokoll.datensatz.bezeichnung
        datum = self.protokoll.datensatz.datum
        inhalt = self.protokoll.datensatz.inhalt
        bericht = ""
        bericht = bericht + '<p class="bericht">\n'
        bericht = bericht + '<h2>Protokoll Bericht vom %s</h2>\n' % datum
        bericht = bericht + '<h3>Bezeichnung: %s</h3>' % bezeichnung
        bericht = bericht + '<h3>Inhalt</h3><br />'
        bericht = bericht + inhalt
        bericht = bericht + '<h3>Gesetzte Referenzen</h3>'

        # Inventar Referenzen setzen
        bericht = bericht + '<h4>Inventargegenstände</h4>'
        sql_anweisung = sql_anweisungen.sql(
            tabelle='tblInventar_Protokoll_Referenzen',
            select='fiInventarnummerProtokoll',
            filtern='fiProtokollInventar=%s' % self.schlussel
        )
        try:
            sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
        except:
            pass
        if sql_resultate:
            for schlussel in sql_resultate:
                sql_anweisung = sql_anweisungen.sql(
                    tabelle='tblInventar',
                    filtern='idInventarnummer=%s' % str(schlussel[0]))
                try:
                    inventar_info = self.datenbank_verbindung.execute(
                        sql_anweisung)
                except:
                    pass
                for inhalt in inventar_info:
                    if inhalt[1]:
                        bericht = bericht + '<b>Bezeichnung</b>: '
                        bericht = bericht + inhalt[1] + '<br />'
                    if inhalt[2]:
                        bericht = bericht + '<b>Größe</b>: '
                        bericht = bericht + inhalt[2] + '<br />'
                    if inhalt[3]:
                        bericht = bericht + '<b>Zustand</b>: '
                        bericht = bericht + inhalt[3] + '<br />'
                    if inhalt[4]:
                        bericht = bericht + '<b>Kategorie</b>: '
                        bericht = bericht + inhalt[4] + '<br />'
                    if inhalt[5]:
                        datum = inhalt[5]
                        try:
                            datum = datum.strftime("%d-%m-%Y")
                        except ValueError:
                            pass
                        bericht = bericht + '<b>Letzte Kontrolle</b>: ' + str(
                            datum) + '<br />'
                    bericht = bericht + '<br />'
        else:
            bericht = bericht + '<i>Keine Inventargegenstände Referenzen</i>'

        # Ereignis Referenzen setzen
        sql_resultate = None   # keine Resultate von vorher
        bericht = bericht + '<h4>Ereignisse</h4>'
        sql_anweisung = sql_anweisungen.sql(
            tabelle='tblEreignis_Protokoll_Referenzen',
            select='fiEreignisnummerProtokoll',
            filtern='fiProtokoll=%s' % self.schlussel)
        try:
            sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
        except:
            pass
        if sql_resultate:
            for schlussel in sql_resultate:
                sql_anweisung = sql_anweisungen.sql(
                    tabelle='tblEreignis',
                    filtern='idEreignisnummer=%s' % str(schlussel[0]))
                try:
                    ereignis_info = self.datenbank_verbindung.execute(
                        sql_anweisung)
                except:
                    pass
                for inhalt in ereignis_info:
                    if inhalt[6]:
                        bericht = bericht + '<b>Bezeichnung</b>: '
                        bericht = bericht + inhalt[6] + '<br />'
                    if inhalt[1]:
                        bericht = bericht + '<b>Ereignis</b>: '
                        bericht = bericht + inhalt[1] + '<br />'
                    if inhalt[2]:
                        datum = inhalt[2]
                        try:
                            datum = datum.strftime("%d-%m-%Y")
                        except ValueError:
                            pass
                        bericht = bericht + '<b>Start Datum</b>: ' + str(
                            datum) + '<br />'
                    if inhalt[3]:
                        bericht = bericht + '<b>Start Zeit</b>: ' + str(
                            inhalt[3]) + '<br />'
                    if inhalt[4]:
                        datum = inhalt[4]
                        try:
                            datum = datum.strftime("%d-%m-%Y")
                        except ValueError:
                            pass
                        bericht = bericht + '<b>End Datum</b>: ' + str(
                            datum) + '<br />'
                    if inhalt[5]:
                        bericht = bericht + '<b>End Zeit</b>: ' + str(
                            inhalt[5]) + '<br />'
                    bericht = bericht + '<br />'
        else:
            bericht = bericht + '<i>Keine Ereignis Referenzen</i>'

        # Mitglied Referenzen setzen
        sql_resultate = None
        bericht = bericht + '<h4>Mitglieder</h4>'
        sql_anweisung = sql_anweisungen.sql(
            tabelle='tblMitglied_Protokoll_Referenzen',
            select='fiStammlistennummerProtokoll',
            filtern='fiProtokollMitglied=%s' % self.schlussel)
        try:
            sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
        except:
            pass
        if sql_resultate:
            for schlussel in sql_resultate:
                sql_anweisung = sql_anweisungen.sql(
                    tabelle='tblMitglied',
                    filtern='idStammlistennummer=%s' % str(schlussel[0]))
                try:
                    mitglied_info = self.datenbank_verbindung.execute(
                        sql_anweisung)
                except:
                    pass
                for inhalt in mitglied_info:
                    if inhalt[1]:
                        bericht = bericht + '<b>Name</b>: '
                        bericht = bericht + inhalt[1] + '<br />'
                    if inhalt[2]:
                        bericht = bericht + '<b>Vorname</b>: '
                        bericht = bericht + inhalt[2] + '<br />'
                    if inhalt[3]:
                        bericht = bericht + '<b>SozialversicherungNr</b>: '
                        bericht = bericht + str(inhalt[3]) + '<br />'
                    if inhalt[4]:
                        datum = inhalt[4]
                        try:
                            datum = datum.strftime("%d-%m-%Y")
                        except ValueError:
                            pass
                        bericht = bericht + '<b>Geburtsdatum</b>: ' + str(
                            datum) + '<br />'
                    if inhalt[5]:
                        bericht = bericht + '<b>Mobiltelefon</b>: ' + str(
                            inhalt[5]) + '<br />'
                    if inhalt[6]:
                        bericht = bericht + '<b>Email</b>: '
                        bericht = bericht + inhalt[6] + '<br />'
                    if inhalt[7]:
                        bericht = bericht + '<b>Telefonnummer</b>: ' + str(
                            inhalt[7]) + '<br />'
                    if inhalt[8]:
                        bericht = bericht + '<b>Ortschaft</b>: '
                        bericht = bericht + inhalt[8] + '<br />'
                    if inhalt[9]:
                        bericht = bericht + '<b>Adresse</b>: '
                        bericht = bericht + inhalt[9] + '<br />'
                    if inhalt[10]:
                        bericht = bericht + '<b>Postleitzahl</b>: ' + str(
                            inhalt[10]) + '<br />'
                    if inhalt[11]:
                        bericht = bericht + '<b>Lehrgangsstunden</b>: ' + str(
                            inhalt[11]) + '<br />'
                    if inhalt[12]:
                        if inhalt[12] == 0:
                            bericht = bericht + '<b>APTEinsatz</b>: NEIN<br />'
                        else:
                            bericht = bericht + '<b>APTEinsatz</b>: JA<br/>'
                    if inhalt[13]:
                        if inhalt[13] == 0:
                            bericht = bericht + '<b>APTAtemschutz</b>: JA<br />'
                        else:
                            bericht = bericht+'<b>APTAtemschutz</b>: NEIN<br />'
                    if inhalt[14]:
                        datum = inhalt[4]
                        try:
                            datum = datum.strftime("%d-%m-%Y")
                        except ValueError:
                            pass
                        bericht = bericht + '<b>Med. Kontrolle</b>: ' + str(
                            datum) + '<br />'
                    bericht = bericht + '<br />'
        else:
            bericht = bericht + '<i>Keine Mitglieder Referenzen</i>'

        # Fahrzeug Referenz setzen
        sql_resultate = None
        bericht = bericht + '<h4>Fahrzeuge</h4>'
        sql_anweisung = sql_anweisungen.sql(
            tabelle='tblFahrzeug_Protokoll_Referenzen',
            select='fiFahrzeugNummerProtokoll',
            filtern='fiProtokollFahrzeug=%s' % self.schlussel)
        try:
            sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
        except:
            pass
        if sql_resultate:
            for schlussel in sql_resultate:
                sql_anweisung = sql_anweisungen.sql(
                    tabelle='tblFahrzeug',
                    filtern='idFahrzeugNummer=%s' % str(schlussel[0]))
                try:
                    fahrzeug_info = self.datenbank_verbindung.execute(
                        sql_anweisung)
                except:
                    pass
                for inhalt in fahrzeug_info:
                    if inhalt[1]:
                        bericht = bericht + '<b>Kennzeichen</b>: '
                        bericht = bericht + inhalt[1] + '<br />'
                    if inhalt[2]:
                        bericht = bericht + '<b>Bezeichnung</b>: '
                        bericht = bericht + inhalt[2] + '<br />'
                    if inhalt[3]:
                        bericht = bericht + '<b>Max. Besatzung</b>: ' + str(
                            inhalt[3]) + '<br />'
                    if inhalt[4]:
                        if inhalt[4] == 0:
                            bericht = bericht+'<b>Techn. Problem</b>: NEIN<br/>'
                        else:
                            bericht = bericht+'<b>Techn. Problem</b>: JA<br />'
                    if inhalt[5]:
                        bericht = bericht + '<b>Techn. Kommentar</b>'
                        bericht = bericht + inhalt[5] + '<br />'
                    if inhalt[6]:
                        datum = inhalt[6]
                        try:
                            datum = datum.strftime("%d-%m-%Y")
                        except ValueError:
                            pass
                        bericht = bericht + '<b>TÜV Kontrolle</b>: ' + str(
                            datum) + '<br />'
                    if inhalt[7]:
                        datum = inhalt[7]
                        try:
                            datum = datum.strftime("%d-%m-%Y")
                        except ValueError:
                            pass
                        bericht = bericht+'<b>Werkstatt Kontrolle</b>: ' + str(
                            datum) + '<br />'
                    if inhalt[8]:
                        bericht = bericht + '<b>Typ</b>: '
                        bericht = bericht + inhalt[8] + '<br/>'
                    bericht = bericht + '<br />'
        else:
            bericht = bericht + '<i>Keine Fahrzeug Referenzen</i>'

        # Der Bericht wird erstellt
        bericht_erstellung.html_bericht(daten=bericht, benutzer='Sekretär',
                                        datei_name=bezeichnung)

    def neu_ereignis(self):
        """ *Ein neues Ereignis wird erstellt* """
        sql_anweisung = sql_anweisungen.hinzufugen(tabelle="tblEreignis")
        self.datenbank_verbindung.execute(sql_anweisung, True)
        einsatz = self.ui_objekt.property('einsatz_verwaltung')
        if einsatz:
            schlussel = self.datenbank_verbindung.letzter_erstellter_schlussel()
            sql_anweisung = sql_anweisungen.hinzufugen(
                tabelle="tblEinsatzbericht",
                felder='fiEreignisnummerEinsatz',
                werte='%s' % schlussel)
            self.datenbank_verbindung.execute(sql_anweisung, True)
        self.ereignis_verwaltung()

    def entfernen_ereignis(self):
        """ *Das aktuell ausgewählte Ereignis entfernen* """
        sql_anweisung = sql_anweisungen.entfernen(
            tabelle="tblEreignis",
            filtern='idEreignisnummer=%s' % self.ereignisSchlussel)
        self.datenbank_verbindung.execute(sql_anweisung, True)
        self.ereignis_verwaltung()

    def abspeichern_ereignis(self):
        """ *Die Informationen des aktuellen Ereignis werden abgespeichert*

        **Beschreibung**

        Abhängig davon in welchem State sich die **QMLUI** befindet, werden die
        zusätzlichen Informationen für einen Einsatz mit abgespeichert.
        """
        self.abspeichern_ereignis_signal.emit()

    def ereignis_in_db(self):
        """ *Die Informationen eines Ereignisses werden in die DB gespeichert*

        **Beschreibung**

        Der Python Unterbau geht die Daten des ausgewählten Ereignisses
        aus der **QMLUI** auslesen und speichert diese Daten in die Datenbank
        ab.
        """
        # Attribute aus der QMLUI werden abgefragt
        bezeichnung = self.ui_objekt.property('edtEreignisBezeichnung')
        typ = self.ui_objekt.property('edtEreignisTyp')
        startdatum = self.ui_objekt.property('edtEreignisStartdatum')

        # Das Datum wird in das richtige Format für die Datenbank gespeichert
        try:
            startdatum = strptime(startdatum, "%d-%m-%Y")
            startdatum = strftime("%Y-%m-%d", startdatum)
        except ValueError:
            pass

        startzeit = self.ui_objekt.property('edtEreignisStartzeit')
        enddatum = self.ui_objekt.property('edtEreignisEnddatum')

        try:
            enddatum = strptime(enddatum, "%d-%m-%Y")
            enddatum = strftime("%Y-%m-%d", enddatum)
        except ValueError:
            pass

        endzeit = self.ui_objekt.property('edtEreignisEndzeit')

        # String für die SQL Anweisung wird zusammengesetzt
        feld_akt = 'dtBezeichnungEreignis = "%s", ' % bezeichnung
        feld_akt = feld_akt + 'dtEreignisTyp = "%s", ' % typ
        feld_akt = feld_akt + 'dtStartdatum = "%s", ' % str(startdatum)
        feld_akt = feld_akt + 'dtStartzeit = "%s", ' % startzeit
        feld_akt = feld_akt + 'dtEnddatum = "%s", ' % str(enddatum)
        feld_akt = feld_akt + 'dtEndzeit = "%s" ' % endzeit

        # SQL Anweisung durchführen
        sql_anweisung = sql_anweisungen.aktualisieren(
            tabelle='tblEreignis',
            aktualisierung=feld_akt,
            filtern='idEreignisnummer=%s' % self.ereignisSchlussel)
        self.datenbank_verbindung.execute(sql_anweisung, True)

        # Falls gegeben, Einsatz Informationen abspeichern
        einsatz = self.ui_objekt.property('einsatzVerwaltung')
        if einsatz:
            einsatztyp = self.ui_objekt.property('edtEinsatzTyp')
            ortschaft = self.ui_objekt.property('edtOrtschaft')
            adresse = self.ui_objekt.property('edtAdresse')
            postleitzahl = self.ui_objekt.property('edtPostleitzahl')
            name = self.ui_objekt.property('edtBetreffenderName')
            vorname = self.ui_objekt.property('edtBetreffenderVorname')

            # String für die SQL Anweisung wird zusammengesetzt
            feld_akt = 'dtEinsatzTyp = "%s", ' % einsatztyp
            feld_akt = feld_akt + 'dtOrtschaftEinsatz = "%s", ' % ortschaft
            feld_akt = feld_akt + 'dtAdresseEinsatz = "%s", ' % adresse
            feld_akt = feld_akt + 'dtPostleitzahlEinsatz="%s", ' % postleitzahl
            feld_akt = feld_akt + 'dtNameGeschädigter = "%s", ' % name
            feld_akt = feld_akt + 'dtVornameGeschädigter = "%s" ' % vorname

            # SQL Anweisung durchführen
            sql_anweisung = sql_anweisungen.aktualisieren(
                tabelle='tblEinsatzbericht',
                aktualisierung=feld_akt,
                filtern='fiEreignisnummerEinsatz=%s' % self.ereignisSchlussel)
            self.datenbank_verbindung.execute(sql_anweisung, True)
            self.ereignis_verwaltung(einsatz=True)
        else:
            self.ereignis_verwaltung()
