# -*- coding: utf-8 -*-

# Ritchie Flick
# T3IFAN 2012/2013 Abschlussprojekt
# LN
# 'Feuerwehrverwaltungssoftware - FVS'
# 'src/UI_Aufbau/admin.py'

# Importieren von PySide Modulen
from PySide import QtCore
from PySide import QtDeclarative
from PySide.QtGui import QIcon

# Importieren von FVS Modulen
from Konfiguration import konfig
from Datenbank import admin_db_verbindung
from Daten_Modelle import verpacken_modelisieren
from Daten_Objekte import db_tabellen_objekt

# Importieren der benötigten Kontroller
from Kontroller import button
from Kontroller import listen


class QMLUI(QtDeclarative.QDeclarativeView):
    """ *Hauptklasse der grafischen Benutzeroberfläche des Admins*

    **Beschreibung**

    Die **QMLUI** (grafische Benutzeroberfläche) des Admins wird
    aufgebaut, die Verbindung zur Datenbank wird aufgebaut und alle nötigen
    Signale/Slots und Funktionen zum arbeiten mit der **QMLUI** werden
    implementiert.
    """

    # Initialisierung der Qt Signale
    tabelle_kontrollieren_signal = QtCore.Signal()

    def __init__(self):
        super(QMLUI, self).__init__()
        adminDB = admin_db_verbindung
        # Verbindung zur Datenbank aufbauen
        try:
            einstellungen = konfig.laden_konfig()
            self.datenbank_verbindung = adminDB.Admin_DB_Verbindung(
                einstellungen)
        except:
            print("Es konnte keine Datenbankverbindung hergestellt werden")

        # Festlegung welche Funktionen von welchen Buttons aufgerufen werden
        button_komponenten_funktionen = {
            "btnTabelleKontrolle": self.tabelle_kontrollieren,
            "btnTabelleOptimieren": self.tabelle_optimieren,
            "btnTabelleReparieren": self.tabelle_reparieren
        }

        # Festlegung welche Funktionen von welchen Listen aufgerufen werden
        listen_komponenten_funktionen = {
            "lstTabellen": [True, self.tabellen_name_setzen]
        }

        # Kontroller für die QMLUI werden eingerichtet
        button_kontroller = button.Kontroller(self,
                                              button_komponenten_funktionen)
        listen_kontroller = listen.Kontroller(self,
                                              listen_komponenten_funktionen)

        # Attribute/Kontroller auf welche die QMLUI zugreifen kann
        self.kontext = self.rootContext()
        self.kontext.setContextProperty('button_kontroller', button_kontroller)
        self.kontext.setContextProperty('listen_kontroller', listen_kontroller)

        # Aufrufen der Startmethoden
        self.admin_bereich_aufbau()

        # QMLUI wird geladen
        self.setWindowTitle("FVS - Admin")
        self.setWindowIcon(QIcon("./icon.png"))
        self.setSource(QtCore.QUrl.fromLocalFile(
            './QMLUI/Admin.qml'))
        self.setResizeMode(QtDeclarative.QDeclarativeView.SizeRootObjectToView)

        # QMLUI Attribute werden Python zugänglich gemacht
        self.ui_objekt = self.rootObject()

        # Slots der QMLUI
        self.tabelle_kontrollieren_signal.connect(
            self.ui_objekt.tabelle_kontrollieren_qml)

    def admin_bereich_aufbau(self):
        """ *Der Admin Bereich wird aufgebaut*

        **Beschreibung**

        Eine Liste mit allen Tabellen in der Datenbank wird erstellt.
        """
        tabellen_liste = self.datenbank_verbindung.liste_aller_tabellen()
        datensatz = []
        for eintrag in tabellen_liste:
            datensatz.append(db_tabellen_objekt.DBTabelle(tabelle=eintrag[0]))
        datenliste = verpacken_modelisieren.list_verpackung_modelisieren(
            datensatz)
        self.kontext.setContextProperty('tabellen_liste', datenliste)

    def tabelle_kontrollieren(self):
        """ *Ausgewählte Tabelle wird auf Fehler kontrolliert* """
        resultat = self.datenbank_verbindung.tabelle_kontrolle(
            tabelle=self.tabellen_name)
        print(resultat)
        self.kontext.setContextProperty('kontrolle', resultat[0][3])
        self.tabelle_kontrollieren_signal.emit()

    def tabellen_name_setzen(self, packung):
        """ *Der Name der ausgewählten Tabelle wird gesetzt* """
        self.tabellen_name = packung.datensatz.tabelle

    def tabelle_optimieren(self):
        """ *Ausgewählte Tabelle wird optimiert* """
        self.datenbank_verbindung.tabelle_optimieren(
            tabelle=self.tabellen_name)

    def tabelle_reparieren(self):
        """ *Ausgewählte Tabelle wird repariert* """
        self.datenbank_verbindung.tabelle_reparieren(
            tabelle=self.tabellen_name)
