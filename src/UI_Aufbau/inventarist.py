# -*- coding: utf-8 -*-

# Ritchie Flick
# T3IFAN 2012/2013 Abschlussprojekt
# LN
# 'Feuerwehrverwaltungssoftware - FVS'
# 'src/UI_Aufbau/inventarist.py'

# Importieren von Python Modulen
from time import strptime, strftime

# Importieren von PySide Modulen
from PySide import QtCore
from PySide import QtDeclarative
from PySide.QtGui import QIcon

# Importieren von FVS Modulen
from Konfiguration import konfig
from Datenbank import db_verbindung
from Datenbank import sql_anweisungen
from Daten_Modelle import verpacken_modelisieren
from Daten_Objekte import inventarliste_objekt
from Daten_Objekte import schlauche_objekt
from Daten_Objekte import mitglied_objekt
from Logik import bericht_erstellung

# Importieren der benötigten Kontroller
from Kontroller import listen
from Kontroller import button
from Kontroller import operationen
from Kontroller import jaNeinAuswahl


class QMLUI(QtDeclarative.QDeclarativeView):
    """ *Hauptklasse der grafischen Benutzeroberfläche des Inventaristen*

    **Beschreibung**

    Die **QMLUI** (grafische Benutzeroberfläche) des Inventaristen wird
    aufgebaut, die Verbindung zur Datenbank wird aufgebaut und alle nötigen
    Signale/Slots und Funktionen zum arbeiten mit der **QMLUI** werden
    implementiert.
    """

    # Qt Signale
    sonstige_liste_setzen_signal = QtCore.Signal()
    sonstige_informationen_setzen_signal = QtCore.Signal()
    zum_hauptmenu_signal = QtCore.Signal()
    schlauch_liste_setzen_signal = QtCore.Signal()
    aktueller_schlauch_info_setzen_signal = QtCore.Signal()
    neu_schlauch_anzahl_abfragen_signal = QtCore.Signal()
    atemschutz_liste_setzen_signal = QtCore.Signal()
    atemschutz_info_setzen_signal = QtCore.Signal()
    atemschutz_info_abfragen_signal = QtCore.Signal()
    kleider_gegenstaende_setzen_signal = QtCore.Signal()
    kleidung_info_setzen_signal = QtCore.Signal()
    kleidung_info_abfragen_signal = QtCore.Signal()
    kommunikation_gegenstaende_setzen_signal = QtCore.Signal()
    kommunikation_info_setzen_signal = QtCore.Signal()
    kommunikation_info_abfragen_signal = QtCore.Signal()
    sonstiges_abspeichern_signal = QtCore.Signal()

    def __init__(self):
        super(QMLUI, self).__init__()
        # Verbindung zur Datenbank wird aufgebaut
        try:
            einstellungen = konfig.laden_konfig()
            self.datenbank_verbindung = db_verbindung.DB_Verbindung(
                einstellungen)
        except:
            print("Es konnte keine Datenbankverbindung hergestellt werden")

        listen_komponenten_funktionen = {
            "lstSonstigeGegenstaende": [True,
                                        self.sonstige_informationen_setzen],

            "lstSchlauchGegenstaende": [True,
                                        self.aktueller_schlauch_info_setzen],

            "lstAtemschutzAuflistung": [True, self.atemschutz_info_setzen],
            "lstKleiderAuflistung": [True, self.kleider_info_setzen],

            "lstKommunikationAuflistung": [True,
                                           self.kommunikation_info_setzen]
        }

        button_komponenten_funktionen = {
            "btnSonstigeVerwaltung": self.sonstige_gegenstaende_liste,

            "btnZumHauptmenu": self.zum_hauptmenu,

            "btnSchlauchVerwaltung": self.schlauch_gegenstaende_liste,

            "btnSchlauchBericht": self.schlauch_bericht_erstellen,

            "btnEinzelnSchlauchEntfernen": self.einzeln_schlauch_entfernen,

            "btnAktuelleSchlauchListeEntfernen":
            self.aktuel_schlauch_liste_entf,

            "btnSchlaucheHinzufugen": self.neu_schlauch_anzahl_abfragen,

            "btnAtemschutzVerwaltung": self.atemschutz_liste_setzen,

            "btnAtemschutzKontrollenBericht":
            self.atemschutz_kontrolle_bericht,

            "btnAtemschutzBeschaedigtBericht":
            self.atemschutz_beschaedigt_bericht,

            "btnNeuAtemschutz": self.neu_atemschutz,

            "btnEntfernenAtemschutz": self.atemschutz_entfernen,

            "btnAktualisierenAtemschutz": self.atemschutz_aktualisieren,

            "btnKleiderVerwaltung": self.kleider_verwaltung,

            "btnKleiderNeu": self.kleider_neu,

            "btnKleiderEntfernen": self.kleider_entfernen,

            "btnKleiderAktualisieren": self.kleider_aktualisieren,

            "btnKommunikationVerwaltung": self.kommunikation_verwaltung,

            "btnKommunikationNeu": self.kommunikation_neu,

            "btnKommunikationEntfernen": self.kommunikation_entfernen,

            "btnKommunikationAktualisieren": self.kommunikation_aktualisieren,
            "btnNeuSonstiges": self.sonstiges_neu,
            "btnEntfernenSonstiges": self.sonstiges_entfernen,
            "btnAbspeichernSonstiges": self.sonstiges_abspeichern
        }

        jaNeinAuswahl_funktionen = {
            "chkTypA": self.typA_wechsel,
            "chkTypB": self.typB_wechsel,
            "chkTypC": self.typC_wechsel,
            "chkTypD": self.typD_wechsel,
            "chkZustand": self.zustand_wechsel,
            "chkAtemschutzBeschaedigtAnzeigen": self.atemschutz_liste_setzen
        }

        operationen_funktionen = {
            "schlauch_anzahl_gesetzt": self.schlauche_hinzufugen,
            "atemschutz_info_abgefragt": self.atemschutz_abspeichern,
            "kleidung_info_abgefragt": self.kleidung_abspeichern,
            "kommuni_info_abgefragt": self.kommunikation_abspeichern,
            "sonstige_info_abgefragt": self.sonstiges_in_db
        }

        # Kontroller für die QMLUI werden eingerichtet
        listen_kontroller = listen.Kontroller(self,
                                              listen_komponenten_funktionen)
        button_kontroller = button.Kontroller(self,
                                              button_komponenten_funktionen)
        jaNein_kontroller = jaNeinAuswahl.Kontroller(self,
                                                     jaNeinAuswahl_funktionen)
        operationen_kontroller = operationen.Kontroller(self,
                                                        operationen_funktionen)

        # Attribute/Kontroller auf welche die QML UI zugreifen kann
        self.kontext = self.rootContext()
        self.kontext.setContextProperty('listen_kontroller', listen_kontroller)
        self.kontext.setContextProperty('button_kontroller', button_kontroller)
        self.kontext.setContextProperty('jaNeinAuswahl_kontroller',
                                        jaNein_kontroller)
        self.kontext.setContextProperty('operationen_kontroller',
                                        operationen_kontroller)

        # Informationen setzen
        self.auswahl_listen_aufbauen()
        self.schlauch_typ_auswahliste_aufbauen()

        # QMLUI wird geladen
        self.setWindowTitle("FVS - Maschinist")
        self.setWindowIcon(QIcon("./icon.png"))
        self.setSource(QtCore.QUrl.fromLocalFile(
            './QMLUI/Inventarist.qml'))
        self.setResizeMode(QtDeclarative.QDeclarativeView.SizeRootObjectToView)

        # QMLUI Attribute werden zugänglich gemacht
        self.ui_objekt = self.rootObject()

        # Slots der QMLUI
        self.sonstige_liste_setzen_signal.connect(
            self.ui_objekt.sonstige_liste_setzen_qml)
        self.sonstige_informationen_setzen_signal.connect(
            self.ui_objekt.sonstige_informationen_setzen_qml)
        self.zum_hauptmenu_signal.connect(
            self.ui_objekt.zum_hauptmenu_qml)

        self.schlauch_liste_setzen_signal.connect(
            self.ui_objekt.schlauch_liste_setzen_qml)
        self.aktueller_schlauch_info_setzen_signal.connect(
            self.ui_objekt.aktueller_schlauch_info_setzen_qml)
        self.neu_schlauch_anzahl_abfragen_signal.connect(
            self.ui_objekt.neu_schlauch_anzahl_abfragen_qml)

        self.atemschutz_liste_setzen_signal.connect(
            self.ui_objekt.atemschutz_liste_setzen_qml)
        self.atemschutz_info_setzen_signal.connect(
            self.ui_objekt.atemschutz_info_setzen_qml)
        self.atemschutz_info_abfragen_signal.connect(
            self.ui_objekt.atemschutz_info_abfragen_qml)

        self.kleider_gegenstaende_setzen_signal.connect(
            self.ui_objekt.kleider_gegenstaende_setzen_qml)
        self.kleidung_info_setzen_signal.connect(
            self.ui_objekt.kleidung_info_setzen_qml)
        self.kleidung_info_abfragen_signal.connect(
            self.ui_objekt.kleidung_info_abfragen_qml)

        self.kommunikation_gegenstaende_setzen_signal.connect(
            self.ui_objekt.kommunikation_gegenstaende_setzen_qml)
        self.kommunikation_info_setzen_signal.connect(
            self.ui_objekt.kommunikation_info_setzen_qml)
        self.kommunikation_info_abfragen_signal.connect(
            self.ui_objekt.kommunikation_info_abfragen_qml)

        self.sonstiges_abspeichern_signal.connect(
            self.ui_objekt.sonstiges_abspeichern_qml)

        # Instanzvariablen
        self.inventarID = None
        self.inventarKategorie = None
        self.typAStatus = 'unchecked'
        self.typBStatus = 'unchecked'
        self.typCStatus = 'unchecked'
        self.typDStatus = 'unchecked'
        self.zustandStatus = 'unchecked'

    def sonstige_gegenstaende_liste(self):
        """ *Liste sonstiger Inventargegenstände wird generiert*

        **Beschreibung**

        Eine Liste aller Inventargegenständen welche nicht in eine der
        Hauptkategorien passen wird generiert.
        """
        sql_anweisung = sql_anweisungen.sql(
            select="idInventarnummer, dtBezeichnung, dtKategorie, "
            "dtKnappheit, dtGrosse, dtZustand, "
            "dtKontrollDatum, fiStammlistennummerAusrustung, "
            "COUNT(dtBezeichnung) AS 'Anzahl'",
            tabelle="tblInventar",
            filtern="dtKategorie NOT IN('Schlauche', 'Atemschutz', "
            "'Kommunikation', 'Kleider', 'Schaummittel', 'Maschinerie', "
            "'1.Hilfe')", gruppieren="dtKategorie, dtBezeichnung")
        try:
            sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
        except:
            self.kontext.setContextProperty('sonstige_gegenstaende', None)
        datensatz = []
        if sql_resultate:
            for eintrag in sql_resultate:
                try:
                    datensatz.append(inventarliste_objekt.Inventarliste(
                        schlussel=eintrag[0], bezeichnung=eintrag[1],
                        kategorie=eintrag[2], knappheit=eintrag[3],
                        anzahl=eintrag[8], groesse=eintrag[4],
                        zustand=eintrag[5], kontrolle=eintrag[6],
                        mitglied=eintrag[7]))
                except:
                    datensatz.append(inventarliste_objekt.Inventarliste(None))
                datenliste = verpacken_modelisieren.list_verpackung_modelisieren(
                    datensatz)
                self.kontext.setContextProperty('sonstige_gegenstaende',
                                                datenliste)
        else:
            self.kontext.setContextProperty('sonstige_gegenstaende', None)
        self.sonstige_liste_setzen_signal.emit()

    def auswahl_listen_aufbauen(self):
        """ *Die verschiedenen AuswahlListe Komponenten aufbauen* """
        groessen_liste = ['L', 'S', 'XL', 'XXL']
        zustand_liste = ['Beschädigt', 'Schlecht', 'Mittel', 'Gut']
        kategorie_liste = ['Schlauche', 'Atemschutz', 'Kommunikation',
                           'Kleider', 'Schaummittel', 'Maschinerie',
                           '1. Hilfe']
        datensatz_groessen = []
        datensatz_zustand = []
        datensatz_kategorie = []
        for eintrag in groessen_liste:
            datensatz_groessen.append(inventarliste_objekt.Groesse(
                groesse=eintrag))
        for eintrag in zustand_liste:
            datensatz_zustand.append(inventarliste_objekt.Zustand(
                zustand=eintrag))
        for eintrag in kategorie_liste:
            datensatz_kategorie.append(inventarliste_objekt.Kategorie(
                kategorie=eintrag))
        groessen = verpacken_modelisieren.list_verpackung_modelisieren(
            datensatz_groessen)
        zustaende = verpacken_modelisieren.list_verpackung_modelisieren(
            datensatz_zustand)
        kategorien = verpacken_modelisieren.list_verpackung_modelisieren(
            datensatz_kategorie)
        self.kontext.setContextProperty('groessen_auswahl', groessen)
        self.kontext.setContextProperty('zustaende_auswahl', zustaende)
        self.kontext.setContextProperty('kategorien_auswahl', kategorien)

    def sonstige_informationen_setzen(self, gegenstand):
        """ *Informationen eines sonstitgen Gegenstandes werden gesetzt*

        **Parameter**

        - gegenstand: Verpackter Datensatz eines sonstigen Gegenstandes

        """
        self.inventarID = gegenstand.datensatz.schlussel
        self.inventarKategorie = gegenstand.datensatz.kategorie
        self.kontext.setContextProperty('inventar_bezeichnung',
                                        gegenstand.datensatz.bezeichnung)
        self.kontext.setContextProperty('inventar_kategorie',
                                        gegenstand.datensatz.kategorie)
        self.kontext.setContextProperty('inventar_knappheit',
                                        gegenstand.datensatz.knappheit)
        self.kontext.setContextProperty('inventar_anzahl',
                                        gegenstand.datensatz.anzahl)

        # Es wird kontrolliert welche Attribute gesetzt sind und welche leer
        # sind. Ist ein Attribut gesetzt, wird dessen Inhalt in der QMLUI
        # gesetzt
        if gegenstand.datensatz.groesse:
            self.kontext.setContextProperty('inventar_groesse',
                                            gegenstand.datensatz.groesse)
        else:
            self.kontext.setContextProperty('inventar_groesse', None)
        if gegenstand.datensatz.zustand:
            self.kontext.setContextProperty('inventar_zustand',
                                            gegenstand.datensatz.zustand)
        else:
            self.kontext.setContextProperty('inventar_zustand', None)
        if gegenstand.datensatz.kontrolle:
            self.kontext.setContextProperty('inventar_kontrolle',
                                            str(gegenstand.datensatz.kontrolle))
        else:
            self.kontext.setContextProperty('inventar_kontrolle', None)
        self.sonstige_informationen_setzen_signal.emit()

    def zum_hauptmenu(self):
        """ *Der Benutzer geht zurück zum Hauptmenü* """
        self.zum_hauptmenu_signal.emit()

    def schlauch_gegenstaende_liste(self):
        """ *Die Schlauch Verwaltung wird aufgerufen* """
        try:
            # Anzahl aller A Schläuche
            sql_anweisung = sql_anweisungen.sql(
                tabelle='tblInventar',
                select='COUNT(dtBezeichnung) AS "Anzahl"',
                filtern='dtBezeichnung = "A Schlauch"')
            sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
            a_schlauch_anzahl = sql_resultate[0][0]
        except:
            a_schlauch_anzahl = 0

        try:
            # Anzahl aller B Schläuche
            sql_anweisung = sql_anweisungen.sql(
                tabelle='tblInventar',
                select='COUNT(dtBezeichnung) AS "Anzahl"',
                filtern='dtBezeichnung = "B Schlauch"')
            sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
            b_schlauch_anzahl = sql_resultate[0][0]
        except:
            b_schlauch_anzahl = 0

        try:
            # Anzahl aller C Schläuche
            sql_anweisung = sql_anweisungen.sql(
                tabelle='tblInventar',
                select='COUNT(dtBezeichnung) AS "Anzahl"',
                filtern='dtBezeichnung = "C Schlauch"')
            sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
            c_schlauch_anzahl = sql_resultate[0][0]
        except:
            c_schlauch_anzahl = 0

        try:
            # Anzahl aller D Schläuche
            sql_anweisung = sql_anweisungen.sql(
                tabelle='tblInventar',
                select='COUNT(dtBezeichnung) AS "Anzahl"',
                filtern='dtBezeichnung = "D Schlauch"')
            sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
            d_schlauch_anzahl = sql_resultate[0][0]
        except:
            d_schlauch_anzahl = 0

        try:
            # Anzahl aller beschädigter Schläuche
            sql_anweisung = sql_anweisungen.sql(
                tabelle='tblInventar',
                select='COUNT(dtzustand) AS "Anzahl"',
                filtern='dtKategorie = "Schlauche" AND dtZustand="beschädigt"')
            sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
            beschaedigt_schlauch_anzahl = sql_resultate[0][0]
        except:
            beschaedigt_schlauch_anzahl = 0

        # Attribute der QMLUI zur Verfügung stellen
        self.kontext.setContextProperty('a_schlauch_anzahl',
                                        a_schlauch_anzahl)
        self.kontext.setContextProperty('b_schlauch_anzahl',
                                        b_schlauch_anzahl)
        self.kontext.setContextProperty('c_schlauch_anzahl',
                                        c_schlauch_anzahl)
        self.kontext.setContextProperty('d_schlauch_anzahl',
                                        d_schlauch_anzahl)
        self.kontext.setContextProperty('beschaedigt_schlauch_anzahl',
                                        beschaedigt_schlauch_anzahl)

        # Anzahl der beschädigten Schläuche (Variable wird später im Programm
        # benötigt)
        self.beschaedigt_schlauch_anzahl = beschaedigt_schlauch_anzahl

        # Liste aller Schläuche wird erstellt
        sql_anweisung = sql_anweisungen.sql(select='*', tabelle='tblInventar',
                                            filtern='dtKategorie ="Schlauche"',
                                            sortieren='dtBezeichnung')
        try:
            sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
        except:
            self.kontext.setContextProperty('schlauch_gegenstaende', None)
            self.schlauch_liste_setzen_signal.emit()
            return
        datensatz = []
        if sql_resultate:
            for eintrag in sql_resultate:
                try:
                    datensatz.append(inventarliste_objekt.Inventarliste(
                        schlussel=eintrag[0], bezeichnung=eintrag[1],
                        zustand=eintrag[3], kategorie=eintrag[4],
                        knappheit=eintrag[6]))
                except:
                    datensatz.append(inventarliste_objekt.Inventarliste(None))
            schlauche = verpacken_modelisieren.list_verpackung_modelisieren(
                datensatz)
            self.kontext.setContextProperty('schlauch_gegenstaende', schlauche)
            self.aktuelle_schlauch_liste = datensatz
        else:
            self.kontext.setContextProperty('schlauch_gegenstaende', None)
        self.schlauch_liste_setzen_signal.emit()

    def typA_wechsel(self, status):
        """ *Wird aufgerufen wenn der Status von chkTypA wechselt*

        **Parameter**

        - status: Der aktuelle Status von **chkTypA**

        """
        self.typAStatus = status
        self.schlauch_filter()

    def typB_wechsel(self, status):
        """ *Wird aufgerufen wenn der Status von chkTypB wechselt*

        **Parameter**

        - status: Der aktuelle Status von **chkTypB**

        """
        self.typBStatus = status
        self.schlauch_filter()

    def typC_wechsel(self, status):
        """ *Wird aufgerufen wenn der Status von chkTypC wechselt*

        **Parameter**

        - status: Der aktuelle Status von **chkTypC**

        """
        self.typCStatus = status
        self.schlauch_filter()

    def typD_wechsel(self, status):
        """ *Wird aufgerufen wenn der Status von chkTypD wechselt*

        **Parameter**

        - status: Der aktuelle Status von **chkTypD**

        """
        self.typDStatus = status
        self.schlauch_filter()

    def zustand_wechsel(self, status):
        """ *Wird aufgerufen wenn der Status von chkZustand wechselt*

        **Parameter**

        - status: Der aktuelle Status von **chkZustand**

        """
        self.zustandStatus = status
        self.schlauch_filter()

    def schlauch_filter(self):
        """ *Filtern der Schlauch Liste*

        **Beschreibung**

        Abhängig davon welche **JaNeinAuswahl** ausgewählt sind:

        - chkTypA
        - chkTypB
        - chkTypC
        - chkTypD
        - chkZustand,

        wird die Liste der verschiedenen Schläuche nach dieser Auswahl
        gefiltert.
        """
        filtern_nach = ""
        if self.typAStatus == "checked":
            filtern_nach = filtern_nach + "'A Schlauch'"
        if self.typBStatus == "checked":
            if filtern_nach:
                filtern_nach = filtern_nach + ", 'B Schlauch'"
            else:
                filtern_nach = filtern_nach + "'B Schlauch'"
        if self.typCStatus == "checked":
            if filtern_nach:
                filtern_nach = filtern_nach + ", 'C Schlauch'"
            else:
                filtern_nach = filtern_nach + "'C Schlauch'"
        if self.typDStatus == "checked":
            if filtern_nach:
                filtern_nach = filtern_nach + ", 'D Schlauch'"
            else:
                filtern_nach = filtern_nach + "'D Schlauch'"
        if self.zustandStatus == "checked" and filtern_nach == '':
            sql_anweisung = sql_anweisungen.sql(
                tabelle='tblInventar',
                select='*',
                filtern='dtZustand="beschädigt" AND dtKategorie="Schlauche"',
                sortieren='dtBezeichnung')
        elif self.zustandStatus == "checked":
            sql_anweisung = sql_anweisungen.sql(
                tabelle='tblInventar',
                select='*',
                filtern='dtZustand="beschädigt" AND dtBezeichnung IN(%s)' %
                (filtern_nach),
                sortieren='dtBezeichnung')
        else:
            sql_anweisung = sql_anweisungen.sql(
                tabelle='tblInventar',
                select='*',
                filtern='dtBezeichnung IN(%s)' % (filtern_nach),
                sortieren='dtBezeichnung')
        try:
            if filtern_nach == "" and self.zustandStatus == "unchecked":
                self.schlauch_gegenstaende_liste()
                return
            sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
        except:
            pass
        datensatz = []
        if sql_resultate:
            for eintrag in sql_resultate:
                try:
                    datensatz.append(inventarliste_objekt.Inventarliste(
                        schlussel=eintrag[0], bezeichnung=eintrag[1],
                        zustand=eintrag[3], kategorie=eintrag[4],
                        knappheit=eintrag[6]))
                except:
                    datensatz.append(inventarliste_objekt.Inventarliste(None))
            schlauche = verpacken_modelisieren.list_verpackung_modelisieren(
                datensatz)
            self.kontext.setContextProperty('schlauch_gegenstaende', schlauche)
        else:
            self.kontext.setContextProperty('schlauch_gegenstaende', None)
        self.aktuelle_schlauch_liste = datensatz
        self.schlauch_liste_setzen_signal.emit()

    def aktueller_schlauch_info_setzen(self, schlauch):
        """ *Der Zustand des aktuellen Schlauches wird gesetzt*

        **Parameter**

        - schlauch: Verpackter Datensatz eines Schlauchs

        """
        self.schlauch_schlussel = schlauch.datensatz.schlussel
        self.kontext.setContextProperty('schlauch_bezeichnung',
                                        schlauch.datensatz.bezeichnung)
        self.kontext.setContextProperty('schlauch_zustand',
                                        schlauch.datensatz.zustand)
        self.aktueller_schlauch_info_setzen_signal.emit()

    def schlauch_bericht_erstellen(self):
        """ *Ein Bericht über alle beschädigten Schläuche wird erstellt* """
        bericht = "Es sind %s beschädigte Schläuche im Inventar enthalten" % (
            self.beschaedigt_schlauch_anzahl)
        bericht_erstellung.html_bericht(daten=bericht, benutzer="Inventarist")

    def einzeln_schlauch_entfernen(self):
        """ *Der aktuelle Schlauch wird entfernt* """
        sql_anweisung = sql_anweisungen.entfernen(
            tabelle='tblInventar',
            filtern='idInventarnummer=%s' % self.schlauch_schlussel)
        self.datenbank_verbindung.execute(sql_anweisung, True)
        self.schlauch_gegenstaende_liste()

    def aktuel_schlauch_liste_entf(self):
        """ *Die aktuelle Liste von Schläuchen wird entfernt* """
        for schlauch in self.aktuelle_schlauch_liste:
            sql_anweisung = sql_anweisungen.entfernen(
                tabelle='tblInventar',
                filtern='idInventarnummer=%s' % schlauch.schlussel)
            self.datenbank_verbindung.execute(sql_anweisung, True)
        self.schlauch_gegenstaende_liste()

    def schlauche_hinzufugen(self):
        """ *Neue Schläuche werden der DB hinzugefügt* """
        anzahl = self.ui_objekt.property('edtSchlauchHinzuAnzahl')
        schlauch_typ = self.ui_objekt.property('cmbSchlauchTypAuswahl')
        sql_anweisung = sql_anweisungen.hinzufugen(
            tabelle='tblInventar',
            felder='dtBezeichnung, dtKategorie',
            werte='"' + schlauch_typ + '"' + ', "Schlauche"')
        for i in range(anzahl):
            self.datenbank_verbindung.execute(sql_anweisung)
        self.datenbank_verbindung.commit()
        self.schlauch_gegenstaende_liste()

    def neu_schlauch_anzahl_abfragen(self):
        """ *Die Anzahl an neuen Schläuchen wird abgefragt* """
        self.neu_schlauch_anzahl_abfragen_signal.emit()

    def schlauch_typ_auswahliste_aufbauen(self):
        """ *AuswahlListe von Schlauch Typs wird aufgebaut* """
        schlauch_typ_liste = ['A Schlauch', 'B Schlauch', 'C Schlauch',
                              'D Schlauch']
        datensatz = []
        for eintrag in schlauch_typ_liste:
            datensatz.append(schlauche_objekt.SchlauchTypListe(
                typ=eintrag))
        datenliste = verpacken_modelisieren.list_verpackung_modelisieren(
            datensatz)
        self.kontext.setContextProperty('schlauch_typ_liste', datenliste)

    def atemschutz_liste_setzen(self, status=''):
        """ *Die Verwaltung zum Atemschutz wird aufgebaut*

        **Parameter**

        - status: Gibt an ob die Liste nach beschädigten Geräten gefiltert
          werden soll oder nicht
        """
        if status == "checked":
            beschaedigt_filter = True
        else:
            beschaedigt_filter = False
        if beschaedigt_filter:
            # SQL Anweisung zum Anzeigen von nur beschädigten Atemschutzgeräten
            sql_anweisung = sql_anweisungen.sql(
                tabelle='tblInventar',
                filtern='dtKategorie="Atemschutz" AND dtzustand="beschädigt"',
                select='*',
                sortieren='dtBezeichnung')
        else:
            # SQL Anweisung für alle Atemschutzgeräte
            sql_anweisung = sql_anweisungen.sql(
                tabelle='tblInventar',
                filtern='dtKategorie="Atemschutz"', select='*',
                sortieren='dtBezeichnung')
        try:
            sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
        except:
            pass
        datensatz = []
        if sql_resultate:
            for eintrag in sql_resultate:
                datensatz.append(inventarliste_objekt.Inventarliste(
                    schlussel=eintrag[0], bezeichnung=eintrag[1],
                    zustand=eintrag[3], kategorie=eintrag[4],
                    kontrolle=eintrag[5], knappheit=eintrag[6],
                    mitglied=eintrag[7]))
            datenliste = verpacken_modelisieren.list_verpackung_modelisieren(
                datensatz)
            self.kontext.setContextProperty('atemschutz_auflistung',
                                            datenliste)
        else:
            self.kontext.setContextProperty('atemschutz_auflistung', None)

        sql_anweisung = sql_anweisungen.sql(tabelle='tblInventar',
                                            filtern='dtKategorie="Atemschutz"',
                                            select='*',
                                            sortieren='dtKontrollDatum')
        try:
            sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
        except:
            pass
        datensatz = []
        if sql_resultate:
            for eintrag in sql_resultate:
                try:
                    datensatz.append(inventarliste_objekt.Inventarliste(
                        schlussel=eintrag[0], bezeichnung=eintrag[1],
                        zustand=eintrag[3], kategorie=eintrag[4],
                        kontrolle=eintrag[5], knappheit=eintrag[6],
                        mitglied=eintrag[7], kontrolle_anzeigen=True))
                except:
                    datensatz.append(inventarliste_objekt.Inventarliste(None))
            datenliste = verpacken_modelisieren.list_verpackung_modelisieren(
                datensatz)
            self.atemschutz_kontrolle_liste = datensatz
            self.kontext.setContextProperty('atemschutz_kontrolle_liste_model',
                                            datenliste)
        else:
            self.atemschutz_kontrolle_liste = None
            self.kontext.setContextProperty('atemschutz_kontrolle_liste_model',
                                            None)

        self.atemschutz_liste_setzen_signal.emit()

    def atemschutz_kontrolle_bericht(self):
        """ *Berichterstellung über die Kontrollen der Atemschutzgeräte* """
        bericht = ""
        try:
            for atemschutz in self.atemschutz_kontrolle_liste:
                bericht = bericht + '<p class="bericht">'
                bericht = bericht + atemschutz.bezeichnung + "<br \>"
                bericht = bericht + str(atemschutz.kontrolle) + "<br \>"
                bericht = bericht + "</p>"
        except:
            bericht = "<i>Keine Meldungen verfügbar</i>"
        bericht_erstellung.html_bericht(daten=bericht,
                                        benutzer="Inventarist")

    def atemschutz_beschaedigt_bericht(self):
        """ *Berichterstellung über beschädigte Atemschutzgeräte* """
        sql_anweisung = sql_anweisungen.sql(
            tabelle='tblInventar',
            filtern='dtKategorie="Atemschutz" AND dtZustand="beschädigt"',
            select='*',
            sortieren='dtBezeichnung')
        try:
            sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
        except:
            pass
        datensatz = []
        if sql_resultate:
            for eintrag in sql_resultate:
                try:
                    datensatz.append(inventarliste_objekt.Inventarliste(
                        schlussel=eintrag[0], bezeichnung=eintrag[1],
                        zustand=eintrag[3], kategorie=eintrag[4],
                        kontrolle=eintrag[5], knappheit=eintrag[6],
                        mitglied=eintrag[7]))
                except:
                    datensatz.append(inventarliste_objekt.Inventarliste(None))
        bericht = ""
        try:
            for atemschutz in datensatz:
                bericht = bericht + '<p class="bericht">'
                bericht = bericht + atemschutz.bezeichnung + "<br \>"
                bericht = bericht + atemschutz.zustand + "<br \>"
                bericht = bericht + "</p>"
        except:
            bericht = "<i>Keine Meldungen verfügbar</i>"
        bericht_erstellung.html_bericht(daten=bericht,
                                        benutzer="Inventarist")

    def atemschutz_info_setzen(self, atemschutz):
        """*Die Informationen des momentanen Atemschutzgerätes wird gesetzt*"""
        self.kontext.setContextProperty('atemschutz_bezeichnung_wert',
                                        atemschutz.datensatz.bezeichnung)
        self.kontext.setContextProperty('atemschutz_kontrolle_wert',
                                        str(atemschutz.datensatz.kontrolle))
        self.kontext.setContextProperty('atemschutz_zustand_wert',
                                        atemschutz.datensatz.zustand)
        self.atemschutz_info_setzen_signal.emit()
        self.atemschutz_schlussel = atemschutz.datensatz.schlussel

    def neu_atemschutz(self):
        """ *Erstellung eines neuen Atemschutzgerätes* """
        sql_anweisung = sql_anweisungen.hinzufugen(tabelle='tblInventar',
                                                   felder='dtKategorie',
                                                   werte='"Atemschutz"')
        self.datenbank_verbindung.execute(sql_anweisung)
        letzter_schussel = self.datenbank_verbindung.letzter_erstellter_schlussel
        self.atemschutz_schlussel = letzter_schussel()
        self.atemschutz_liste_setzen()

    def atemschutz_entfernen(self):
        """ *Löschen eines Atemschutzgerätes* """
        sql_anweisung = sql_anweisungen.entfernen(
            tabelle='tblInventar',
            filtern='idInventarnummer=%s' % self.atemschutz_schlussel)
        self.datenbank_verbindung.execute(sql_anweisung, True)
        self.atemschutz_liste_setzen()

    def atemschutz_aktualisieren(self):
        """ *Signal zum abfragen der Daten aus der QMLUI* """
        self.atemschutz_info_abfragen_signal.emit()

    def atemschutz_abspeichern(self):
        """ *Die Daten eines Atemschutzgerätes werden abgespeichert* """
        bezeichnung = self.ui_objekt.property('edtAtemschutzBezeichnung')
        kontrolle = self.ui_objekt.property('edtAtemschutzKontrolle')

        try:
            kontrolle = strptime(kontrolle, "%d-%m-%Y")
            kontrolle = strftime("%Y-%m-%d", kontrolle)
        except ValueError:
            pass

        zustand = self.ui_objekt.property('edtAtemschutzZustand')
        sql_anweisung = sql_anweisungen.aktualisieren(
            tabelle='tblInventar',
            aktualisierung='dtBezeichnung = "%s",dtKontrollDatum = "%s",'
            'dtZustand = "%s"' % (bezeichnung, kontrolle, zustand),
            filtern='idInventarnummer=%s' % self.atemschutz_schlussel)
        self.datenbank_verbindung.execute(sql_anweisung, True)
        self.atemschutz_liste_setzen()

    def kleider_verwaltung(self):
        """ *Die Verwaltung der Kleider wird aufgerufen* """
        # Alle Kleider werden aufgelistet
        sql_anweisung = sql_anweisungen.sql(tabelle='tblInventar',
                                            select='*',
                                            filtern='dtKategorie="Kleider"')
        try:
            sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
        except:
            self.kontext.setContextProperty('kleider_auflistung', None)
        datensatz = []
        if sql_resultate:
            for eintrag in sql_resultate:
                try:
                    datensatz.append(inventarliste_objekt.Inventarliste(
                        schlussel=eintrag[0], bezeichnung=eintrag[1],
                        groesse=eintrag[2], zustand=eintrag[3],
                        kategorie=eintrag[4], knappheit=eintrag[6],
                        mitglied=eintrag[7]))
                except:
                    datensatz.append(inventarliste_objekt.Inventarliste(None))
            datenliste = verpacken_modelisieren.list_verpackung_modelisieren(
                datensatz)
            self.kontext.setContextProperty('kleider_auflistung',
                                            datenliste)
        else:
            self.kontext.setContextProperty('kleider_auflistung', None)

        # Kategorien für die Kleider werden aufgelistet
        sql_anweisung = sql_anweisungen.sql(tabelle='tblInventar',
                                            select='DISTINCT(dtBezeichnung)',
                                            filtern='dtKategorie="Kleider"')
        try:
            sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
        except:
            self.kontext.setContextProperty('kleider_gruppierung', None)
        datensatz = []
        if sql_resultate:
            for eintrag in sql_resultate:
                try:
                    datensatz.append(inventarliste_objekt.Kleidung(
                        art=eintrag[0]))
                except:
                    datensatz.append(inventarliste_objekt.Kleidung(None))
            datenliste = verpacken_modelisieren.list_verpackung_modelisieren(
                datensatz)
            self.kontext.setContextProperty('kleider_gruppierung',
                                            datenliste)
        else:
            self.kontext.setContextProperty('kleider_gruppierung', None)

        # Auflistung aller Mitglieder
        sql_anweisung = sql_anweisungen.sql(tabelle='tblMitglied', select='*')
        try:
            sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
        except:
            self.kontext.setContextProperty('kleider_mitglieder', None)
        datensatz = []
        if sql_resultate:
            for eintrag in sql_resultate:
                try:
                    datensatz.append(mitglied_objekt.Mitglied(
                        schlussel=eintrag[0], name=eintrag[1],
                        vorname=eintrag[2]))
                except:
                    datensatz.append(mitglied_objekt.Mitglied(None))
            datenliste = verpacken_modelisieren.list_verpackung_modelisieren(
                datensatz)
            self.kontext.setContextProperty('kleider_mitglieder',
                                            datenliste)
        else:
            self.kontext.setContextProperty('kleider_mitglieder', None)
        self.kleider_gegenstaende_setzen_signal.emit()

    def kleider_info_setzen(self, kleidung):
        """ *Die Daten der Kleidung wird gesetzt* """
        self.kontext.setContextProperty('kleidung_bezeichnung_wert',
                                        kleidung.datensatz.bezeichnung)
        self.kontext.setContextProperty('kleidung_groesse_wert',
                                        kleidung.datensatz.groesse)
        self.kontext.setContextProperty('kleidung_zustand_wert',
                                        kleidung.datensatz.zustand)
        self.kontext.setContextProperty('kleidung_mitglied_wert',
                                        kleidung.datensatz.mitglied)
        self.kleidung_schlussel = kleidung.datensatz.schlussel
        self.kleidung_info_setzen_signal.emit()

    def kleider_neu(self):
        """ *Ein neuer Kleidungsgegenstand wird erstellt* """
        sql_anweisung = sql_anweisungen.hinzufugen(
            tabelle='tblInventar', felder='dtKategorie', werte='"Kleider"')
        try:
            self.datenbank_verbindung.execute(sql_anweisung)
            letzter_schussel = self.datenbank_verbindung.letzter_erstellter_schlussel
            self.kleidung_schlussel = letzter_schussel()
        except:
            pass
        self.kleider_verwaltung()

    def kleider_entfernen(self):
        """ *Ein Kleidungsgegenstand wird entfernt* """
        try:
            sql_anweisung = sql_anweisungen.entfernen(
                tabelle='tblInventar',
                filtern='idInventarnummer=%s' % self.kleidung_schlussel)
            self.datenbank_verbindung.execute(sql_anweisung, True)
        except:
            return
        self.kleider_verwaltung()

    def kleider_aktualisieren(self):
        """ *Die Daten der Kleidung werden abgefragt* """
        self.kleidung_info_abfragen_signal.emit()

    def kleidung_abspeichern(self):
        """ *Die Daten der Kleidung werden abgespeichert* """
        bezeichnung = self.ui_objekt.property('edtKleidungBezeichnung')
        groesse = self.ui_objekt.property('edtKleidungGroesse')
        zustand = self.ui_objekt.property('edtKleidungZustand')
        mitglied = '"' + self.ui_objekt.property('edtKleidungMitglied') + '"'
        if mitglied == '"kein Mitglied"':
            mitglied = 'NULL'
        try:
            sql_anweisung = sql_anweisungen.aktualisieren(
                tabelle='tblInventar',
                aktualisierung='dtBezeichnung = "%s", dtGrosse = "%s", '
                'dtZustand = "%s", fiStammlistennummerAusrustung = %s' %
                (bezeichnung, groesse, zustand, mitglied),
                filtern='idInventarnummer=%s' % self.kleidung_schlussel)
            self.datenbank_verbindung.execute(sql_anweisung, True)
        except:
            return
        self.kleider_verwaltung()

    def kommunikation_verwaltung(self):
        """ *Die Kommunikationsverwaltung wird aufgerufen* """
        # Auflistung aller Kommunikationsgeräte
        sql_anweisung = sql_anweisungen.sql(
            tabelle='tblInventar',
            select='*',
            filtern='dtKategorie="Kommunikation"')
        try:
            sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
        except:
            self.kontext.setContextProperty('kommunikation_auflistung', None)
        datensatz = []
        if sql_resultate:
            for eintrag in sql_resultate:
                try:
                    datensatz.append(inventarliste_objekt.Inventarliste(
                        schlussel=eintrag[0], bezeichnung=eintrag[1],
                        zustand=eintrag[3], kategorie=eintrag[4],
                        knappheit=eintrag[6], mitglied=eintrag[7]))
                except:
                    datensatz.append(inventarliste_objekt.Inventarliste(None))
            datenliste = verpacken_modelisieren.list_verpackung_modelisieren(
                datensatz)
            self.kontext.setContextProperty('kommunikation_auflistung',
                                            datenliste)
        else:
            self.kontext.setContextProperty('kommunikation_auflistung', None)

        # Auflistung aller Mitglieder
        sql_anweisung = sql_anweisungen.sql(tabelle='tblMitglied', select='*')
        try:
            sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
        except:
            self.kontext.setContextProperty('kommunikation_mitglieder', None)
        datensatz = []
        if sql_resultate:
            for eintrag in sql_resultate:
                try:
                    datensatz.append(mitglied_objekt.Mitglied(
                        schlussel=eintrag[0], name=eintrag[1],
                        vorname=eintrag[2]))
                except:
                    datensatz.append(mitglied_objekt.Mitglied(None))
            datenliste = verpacken_modelisieren.list_verpackung_modelisieren(
                datensatz)
            self.kontext.setContextProperty('kommunikation_mitglieder',
                                            datenliste)
        else:
            self.kontext.setContextProperty('kommunikation_mitglieder', None)

        self.kommunikation_gegenstaende_setzen_signal.emit()

    def kommunikation_info_setzen(self, kommunikation):
        """ *Die Daten eines Kommunikationsgerätes werden gesetzt*

        **Parameter**

        - kommunikation: Verpackter Datensatz eines Kommunikationsgerätes

        """
        self.kontext.setContextProperty('kommunikation_bezeichnung_wert',
                                        kommunikation.datensatz.bezeichnung)
        self.kontext.setContextProperty('kommunikation_zustand_wert',
                                        kommunikation.datensatz.zustand)
        self.kontext.setContextProperty('kommunikation_mitglied_wert',
                                        kommunikation.datensatz.mitglied)
        self.kommunikation_schlussel = kommunikation.datensatz.schlussel
        self.kommunikation_info_setzen_signal.emit()

    def kommunikation_neu(self):
        """ *Ein neues Kommunikationsgerät wird erstellt* """
        sql_anweisung = sql_anweisungen.hinzufugen(
            tabelle='tblInventar', felder='dtKategorie',
            werte='"Kommunikation"')
        self.datenbank_verbindung.execute(sql_anweisung)
        letzter_schussel = self.datenbank_verbindung.letzter_erstellter_schlussel
        self.kommunikation_schlussel = letzter_schussel()
        self.kommunikation_verwaltung()

    def kommunikation_entfernen(self):
        """ *Ein Kommunikationsgerät wird entfernt* """
        try:
            sql_anweisung = sql_anweisungen.entfernen(
                tabelle='tblInventar',
                filtern='idInventarnummer=%s' % self.kommunikation_schlussel)
            self.datenbank_verbindung.execute(sql_anweisung, True)
        except:
            return
        self.kommunikation_verwaltung()

    def kommunikation_aktualisieren(self):
        """ *Die Daten eines Kommunikationsgerätes werden abgefragt* """
        self.kommunikation_info_abfragen_signal.emit()

    def kommunikation_abspeichern(self):
        """ *Die Daten eines Kommunikationsgerätes werden abgespeichert* """
        bezeichnung = self.ui_objekt.property('edtKommunikationBezeichnung')
        zustand = self.ui_objekt.property('edtKommunikationZustand')
        mitglied = '"' + self.ui_objekt.property('edtKommunikationMitglied') + '"'
        if mitglied == '"kein Mitglied"':
            mitglied = 'NULL'
        try:
            sql_anweisung = sql_anweisungen.aktualisieren(
                tabelle='tblInventar',
                aktualisierung='dtBezeichnung = "%s", dtZustand = "%s", '
                'fiStammlistennummerAusrustung = %s' %
                (bezeichnung, zustand, mitglied),
                filtern='idInventarnummer=%s' % self.kommunikation_schlussel)
            self.datenbank_verbindung.execute(sql_anweisung, True)
        except:
            return
        self.kommunikation_verwaltung()

    def sonstiges_neu(self):
        """ *Ein neuer sonstiger Inventargegenstand wird erstellt* """
        sql_anweisung = sql_anweisungen.hinzufugen(tabelle='tblInventar')
        self.datenbank_verbindung.execute(sql_anweisung, True)
        self.sonstige_gegenstaende_liste()

    def sonstiges_entfernen(self):
        """ *Ein sonstiger Inventargegenstand wird entfernt* """
        sql_anweisung = sql_anweisungen.entfernen(
            tabelle='tblInventar',
            filtern='idInventarnummer=%s' % self.inventarID)
        self.datenbank_verbindung.execute(sql_anweisung, True)
        self.sonstige_gegenstaende_liste()

    def sonstiges_abspeichern(self):
        """ *Die Info eines sonstigen Inventargegenstandes werden abgefragt* """
        self.sonstiges_abspeichern_signal.emit()

    def sonstiges_in_db(self):
        """ *Die Info eines sonst. Gegenstandes werden in die DB gespeichert*

        **Beschreibung**

        Der Python Unterbau geht die Daten des zuletzt ausgewählten sonstigen
        Inventargegenstandes aus der **QMLUI** auslesen und speichert diese
        Daten in die Datenbank ab.
        """
        # Attribute aus der QMLUI werden abgefragt
        bezeichnung = self.ui_objekt.property('edtSonstigeBezeichnung')
        groesse = self.ui_objekt.property('edtSonstigeGroesse')
        zustand = self.ui_objekt.property('edtSonstigeZustand')
        kategorie = self.ui_objekt.property('edtSonstigeKategorie')
        kontrolle = self.ui_objekt.property('edtSonstigeKontrolle')

        try:
            kontrolle = strptime(kontrolle, "%d-%m-%Y")
            kontrolle = strftime("%Y-%m-%d", kontrolle)
        except ValueError:
            pass

        # String für die SQL Anweisung wird zusammengesetzt
        feld_akt = 'dtBezeichnung="%s", ' % bezeichnung
        feld_akt = feld_akt + 'dtGrosse="%s", ' % groesse
        feld_akt = feld_akt + 'dtZustand="%s", ' % zustand
        feld_akt = feld_akt + 'dtKategorie="%s", ' % kategorie
        feld_akt = feld_akt + 'dtKontrollDatum="%s" ' % kontrolle

        # SQL Anweisung durchführen
        sql_anweisung = sql_anweisungen.aktualisieren(
            tabelle='tblInventar',
            aktualisierung=feld_akt,
            filtern='idInventarnummer=%s' % self.inventarID)
        self.datenbank_verbindung.execute(sql_anweisung, True)
        self.sonstige_gegenstaende_liste()
