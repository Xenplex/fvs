UI Aufbau
==========

Die **UI_Aufbau** *Python* *Module* bauen die **QMLUI** auf und setzen alle Informationen und stellen alle weiteren Funktionen/Methoden zum Gebrauch der **QMLUI** bereit. Es handelt sich um die Hauptdatein von **FVS**.

Inventarist
-------------

.. automodule:: UI_Aufbau.inventarist
   :members:

Maschinist
------------

.. automodule:: UI_Aufbau.maschinist
   :members:

Sekretär
---------

.. automodule:: UI_Aufbau.sekretaer
   :members:

Kommandant
-----------

.. automodule:: UI_Aufbau.kommandant
   :members:

Admin
------

.. automodule:: UI_Aufbau.admin
   :members:
