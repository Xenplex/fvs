# -*- coding: utf-8 -*-

# Ritchie Flick
# T3IFAN 2012/2013 Abschlussprojekt
# LN
# 'Feuerwehrverwaltungssoftware - FVS'
# 'src/UI_Aufbau/kommandant.py'

# Importieren von Python Modulen
from time import strptime, strftime
import datetime

# Importieren von PySide Modulen
from PySide import QtCore
from PySide import QtDeclarative
from PySide.QtGui import QIcon

# Importieren von FVS Modulen
from UI_Aufbau import maschinist
from UI_Aufbau import inventarist
from UI_Aufbau import sekretaer
from Konfiguration import konfig
from Datenbank import db_verbindung
from Datenbank import sql_anweisungen
from Daten_Modelle import verpacken_modelisieren
from Daten_Objekte import mitglied_objekt
from Daten_Objekte import lehrgang_objekt
from Daten_Objekte import auszeichnung_objekt
from Daten_Objekte import fuhrerschein_objekt
from Daten_Objekte import inventarliste_objekt
from Daten_Objekte import meldungen_objekt
from Logik import inventar
from Logik import mitglied_logik
from Logik import bericht_erstellung

# Importieren der benötigten Kontroller
from Kontroller import listen
from Kontroller import button
from Kontroller import operationen


class QMLUI(QtDeclarative.QDeclarativeView):
    """ *Hauptklasse der grafischen Benutzeroberfläche des Kommandanten*

    **Beschreibung**

    Die **QMLUI** (*grafische Benutzeroberfläche) des Kommandanten wird
    aufgebaut, die Verbindung zur Datenbank wird aufgebaut und alle nötigen
    Signale/Slots und Funktionen zum arbeiten mit der **QMLUI** werden
    implementiert.
    """

    # Initialisierung der Qt Signale
    kommandant_bereich_aufbau_signal = QtCore.Signal()
    mitglied_info_setzen_signal = QtCore.Signal()
    mitglied_info_abspeichern_signal = QtCore.Signal()
    ausrustung_bearbeiten_signal = QtCore.Signal()

    def __init__(self):
        super(QMLUI, self).__init__()

        # Verbindung zur Datenbank aufbauen
        try:
            einstellungen = konfig.laden_konfig()
            self.datenbank_verbindung = db_verbindung.DB_Verbindung(
                einstellungen)
        except:
            print("Es konnte keine Datenbankverbindung hergestellt werden")
            # Weiteres QMLUI Fenster erstellen und UI_Aufbau Datei erstellen,
            # importieren und aufrufen und als MessageBox verwenden

        # Festlegung welche Funktionen von welchen Listen aufgerufen werden
        listen_komponenten_funktionen = {
            "lstMitglieder": [True, self.mitglied_info_setzen],
            "lstLehrgaenge": [True, self.lehrgaenge_schlussel_setzen],
            "lstAuszeichnungen": [True, self.auszeichnungen_schlussel_setzen],
            "lstFuhrerscheine": [True, self.fuhrerscheine_schlussel_setzen],
            "cmbLehrgaengeAuswahl": [True, self.lehrgang_auswahl_setzen],
            "cmbAuszeichnungAuswahl": [True, self.auszeichnung_auswahl_setzen],
            "cmbFuhrerscheinAuswahl": [True, self.fuhrerschein_auswahl_setzen],
            "lstInventar": [True, self.inventar_schlussel_setzen],
            "lstAusrustung": [True, self.ausrustung_schlussel_setzen]
        }

        # Festlegung welche Funktionen von welchen Buttons aufgerufen werden
        button_komponenten_funktionen = {
            "btnSekretaerBereich": self.sekretaer_bereich_aufrufen,
            "btnInventarBereich": self.inventarist_bereich_aufrufen,
            "btnMaschinistBereich": self.maschinist_bereich_aufrufen,
            "btnLehrgangHinzu": self.lehrgang_hinzu,
            "btnLehrgangEntfernen": self.lehrgang_entfernen,
            "btnAuszeichnungHinzu": self.auszeichnung_hinzu,
            "btnAuszeichnungEntfernen": self.auszeichnung_entfernen,
            "btnFuhrerscheinHinzu": self.fuhrerschein_hinzu,
            "btnFuhrerscheinEntfernen": self.fuhrerschein_entfernen,
            "btnNeuMitglied": self.neues_mitglied,
            "btnEntfernenMitglied": self.entfernen_mitglied,
            "btnAktualisieren": self.mitglied_aktualisieren,
            "btnAusrustungBearbeiten": self.ausrustung_bearbeiten,
            "btnZuruckMitglied": self.mitglied_info_anzeigen,
            "btnAusrustungHinzu": self.ausrustung_hinzu,
            "btnAusrustungEntfernen": self.ausrustung_entfernen,
            "btnOverlayBericht": self.overlay_bericht
        }

        # Festlegung welche Funktionen von welchen Operationssignalen
        # werden sollen aufgerufen
        operationen_funktionen = {
            "mitglied_variablen_gesetzt": self.mitglied_info_in_db_speichern
        }

        # Kontroller für die QMLUI werden eingerichtet
        listen_kontroller = listen.Kontroller(self,
                                              listen_komponenten_funktionen)
        button_kontroller = button.Kontroller(self,
                                              button_komponenten_funktionen)
        operationen_kontroller = operationen.Kontroller(self,
                                                        operationen_funktionen)

        # Attribute/Kontroller auf welche die QMLUI zugreifen kann
        self.kontext = self.rootContext()
        self.kontext.setContextProperty('listen_kontroller', listen_kontroller)
        self.kontext.setContextProperty('button_kontroller', button_kontroller)
        self.kontext.setContextProperty('operationen_kontroller',
                                        operationen_kontroller)

        # Aufrufen der Startmethoden
        self.overlay_meldungen_aufbau()
        self.kommandant_bereich_aufbau()

        # QMLUI wird geladen
        self.setWindowTitle("FVS - Kommandant")
        self.setWindowIcon(QIcon("./icon.png"))
        self.setSource(QtCore.QUrl.fromLocalFile(
            './QMLUI/Kommandant.qml'))
        self.setResizeMode(QtDeclarative.QDeclarativeView.SizeRootObjectToView)

        # QMLUI Attribute werden Python zugänglich gemacht
        self.ui_objekt = self.rootObject()

        # Slots der QMLUI
        self.kommandant_bereich_aufbau_signal.connect(
            self.ui_objekt.kommandant_bereich_aufbau_qml)

        self.mitglied_info_setzen_signal.connect(
            self.ui_objekt.mitglied_info_setzen_qml)

        self.mitglied_info_abspeichern_signal.connect(
            self.ui_objekt.mitglied_info_abspeichern_qml)

        self.ausrustung_bearbeiten_signal.connect(
            self.ui_objekt.ausrustung_bearbeiten_qml)

    def kommandant_bereich_aufbau(self, state_wechseln=True):
        """*Die Informationen für den Bereich des Kommandanten werden gesetzt*

        **Beschreibung**

        Eine Liste aller Mitglieder welche in der Datenbank abgespeichert sind
        wird erstellt und gesetzt und die einzelnen Listen aller Lehrgänge,
        Führerscheine und Auszeichnungen für die Auswahllisten werden erstellt
        und gesetzt.

        **Parameter**

        - state_wechseln: *Boolean* Angabe ob nur die einzelnen Listen neu
          erstellt werden sollen oder auch der *State* der **QMLUI** geändert
          werden soll.

        """
        # Eine Liste aller Mitglieder in der Datenbank wird erstellt
        try:
            sql_anweisung = sql_anweisungen.sql(tabelle="tblMitglied")
            sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
            datensatz = []
            for eintrag in sql_resultate:
                datensatz.append(mitglied_objekt.Mitglied(
                    schlussel=eintrag[0],
                    name=eintrag[1], vorname=eintrag[2],
                    sozialnummer=eintrag[3], bday=eintrag[4],
                    mobiltelefon=eintrag[5], email=eintrag[6],
                    telefonnummer=eintrag[7], ortschaft=eintrag[8],
                    adresse=eintrag[9], postleitzahl=eintrag[10],
                    lehrgangsstunden=eintrag[11], apteinsatz=eintrag[12],
                    aptatemschutz=eintrag[13], mediKontrolle=eintrag[14]))
            datenliste = verpacken_modelisieren.list_verpackung_modelisieren(
                datensatz)
            self.kontext.setContextProperty('mitglieder_liste', datenliste)
        except:
            # Wenn eine Ausnahme eintritt, leere Liste setzen
            self.kontext.setContextProperty('mitglieder_liste', None)

        # Liste aller Lehrgänge wird erstellt
        try:
            sql_anweisung = sql_anweisungen.sql(tabelle='tblLehrgang')
            sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
            datensatz = []
            for eintrag in sql_resultate:
                datensatz.append(lehrgang_objekt.Lehrgang(
                    schlussel=eintrag[0]))
            datenliste = verpacken_modelisieren.list_verpackung_modelisieren(
                datensatz)
            self.kontext.setContextProperty('alle_lehrgaenge', datenliste)
        except:
            # Wenn eine Ausnahme eintritt, leere Liste setzen
            self.kontext.setContextProperty('alle_lehrgaenge', None)

        # Liste aller Auszeichnungen wird erstellt
        try:
            sql_anweisung = sql_anweisungen.sql(tabelle='tblAuszeichnung')
            sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
            datensatz = []
            for eintrag in sql_resultate:
                datensatz.append(auszeichnung_objekt.Auszeichnung(
                    auszeichnung=eintrag[0]))
            datenliste = verpacken_modelisieren.list_verpackung_modelisieren(
                datensatz)
            self.kontext.setContextProperty('alle_auszeichnungen', datenliste)
        except:
            # Wenn eine Ausnahme eintritt, leere Liste setzen
            self.kontext.setContextProperty('alle_auszeichnungen', None)

        # Liste aller Führerscheine wird erstellt
        try:
            fuehrerscheine = ['A', 'B', 'C', 'D', 'E']
            datensatz = []
            for eintrag in fuehrerscheine:
                datensatz.append(fuhrerschein_objekt.Fuhrerschein(
                    schlussel=eintrag))
            datenliste = verpacken_modelisieren.list_verpackung_modelisieren(
                datensatz)
            self.kontext.setContextProperty('alle_fuhrerscheine', datenliste)
        except:
            # Wenn eine Ausnahme eintritt, leere Liste setzen
            self.kontext.setContextProperty('alle_auszeichnungen', None)

        if state_wechseln:
            self.kommandant_bereich_aufbau_signal.emit()

    def overlay_meldungen_aufbau(self):
        """ *Die Meldungen für das Overlay werden aufgebaut*

        **Beschreibung**

        Methode welche alle benötigten/vorgesehenen Kontrollen/Tests durchführt
        und diese ins Overlay schreibt.
        """
        sql_anweisung = sql_anweisungen.sql(tabelle='tblMitglied')
        try:
            sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
        except:
            pass
        datensatz = []
        if sql_resultate:
            for eintrag in sql_resultate:
                try:
                    if mitglied_logik.medizinischeKontrolle(eintrag[14]):
                        datensatz.append(meldungen_objekt.Meldungen(
                            'Die medizinische Kontrolle für das Mitglied\n%s %s '
                            'ist abgelaufen' % (eintrag[1], eintrag[2])))
                    if mitglied_logik.restliche_stunden(eintrag[11]):
                        datensatz.append(meldungen_objekt.Meldungen(
                            'Das Mitglied %s %s hat nicht mehr\nviele freie Stunden'
                            ' für Lehrgänge übrig' % (eintrag[1], eintrag[2])))
                except:
                    datensatz.append(meldungen_objekt.Meldungen(None))
            datenliste = verpacken_modelisieren.list_verpackung_modelisieren(
                datensatz)
            self.kontext.setContextProperty('meldungen_liste', datenliste)
            self.meldungen_liste = datensatz
        else:
            self.kontext.setContextProperty('meldungen_liste', None)

    # Aufrufen der anderen Benutzerbereiche für den Kommandanten
    def sekretaer_bereich_aufrufen(self):
        """ *Der Bereich des Sekretärs wird aufgerufen* """
        Sekretaer = sekretaer.QMLUI()
        Sekretaer.setWindowState(QtCore.Qt.WindowMaximized)
        Sekretaer.show()

    def inventarist_bereich_aufrufen(self):
        """ *Der Bereich des Inventaristen wird aufgerufen* """
        Inventarist = inventarist.QMLUI()
        Inventarist.setWindowState(QtCore.Qt.WindowMaximized)
        Inventarist.show()

    def maschinist_bereich_aufrufen(self):
        """ *Der Bereich des Maschinisten wird aufgerufen* """
        Maschinist = maschinist.QMLUI()
        Maschinist.setWindowState(QtCore.Qt.WindowMaximized)
        Maschinist.show()

    def mitglied_info_setzen(self, mitglied):
        """ *Die Informationen des ausgewäglten Mitglieds werden gesetzt*

        **Parameter**

        - mitglied: Verpackter Datensatz eines Mitglieds

        """
        # Standard Mitglied Informationen
        self.mitglied = mitglied
        self.schlussel = mitglied.datensatz.schlussel
        self.kontext.setContextProperty('mitglied_info_name',
                                        mitglied.datensatz.name)
        self.kontext.setContextProperty('mitglied_info_vorname',
                                        mitglied.datensatz.vorname)
        self.kontext.setContextProperty('mitglied_info_sozialnummer',
                                        mitglied.datensatz.sozialnummer)
        self.kontext.setContextProperty('mitglied_info_bday',
                                        str(mitglied.datensatz.bday))
        self.kontext.setContextProperty('mitglied_info_mobiltelefon',
                                        mitglied.datensatz.mobiltelefon)
        self.kontext.setContextProperty('mitglied_info_email',
                                        mitglied.datensatz.email)
        self.kontext.setContextProperty('mitglied_info_telefonnummer',
                                        mitglied.datensatz.telefonnummer)
        self.kontext.setContextProperty('mitglied_info_ortschaft',
                                        mitglied.datensatz.ortschaft)
        self.kontext.setContextProperty('mitglied_info_adresse',
                                        mitglied.datensatz.adresse)
        self.kontext.setContextProperty('mitglied_info_postleitzahl',
                                        mitglied.datensatz.postleitzahl)
        self.kontext.setContextProperty('mitglied_info_lehrgangsstunden',
                                        mitglied.datensatz.lehrgangsstunden)
        self.kontext.setContextProperty('mitglied_info_medi',
                                        str(mitglied.datensatz.mediKontrolle))
        self.kontext.setContextProperty('mitglied_info_apteinsatz',
                                        mitglied.datensatz.apteinsatz)
        self.kontext.setContextProperty('mitglied_info_aptatemschutz',
                                        mitglied.datensatz.aptatemschutz)

        # Lehrgänge eines Mitglieds setzen
        try:
            sql_anweisung = sql_anweisungen.sql(
                tabelle='tblAbsolvieren Ab, tblLehrgang Leh',
                select='idLehrgang',
                filtern='Ab.fiStammlistennummerAbsolvieren=%s AND '
                'Ab.fiLehrgangLehrgang=Leh.idLehrgang' % (self.schlussel))
            sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
            datensatz = []
            for eintrag in sql_resultate:
                datensatz.append(lehrgang_objekt.Lehrgang(
                    schlussel=eintrag[0]))
            datenliste = verpacken_modelisieren.list_verpackung_modelisieren(
                datensatz)
            self.kontext.setContextProperty('mitglied_info_lehrgaenge',
                                            datenliste)
        except:
            # Wenn eine Ausnahme eintritt, leere Liste setzen
            self.kontext.setContextProperty('mitglied_info_lehrgaenge',
                                            None)

        # Auszeichnungen eines Mitglieds setzen
        try:
            sql_anweisung = sql_anweisungen.sql(
                tabelle='tblMitgliedsAuszeichnungen Mit, tblAuszeichnung Aus',
                select='idAuszeichnung',
                filtern='Mit.fiStammlistennummerAuszeichnung=%s AND '
                'Mit.fiAuszeichnung=Aus.idAuszeichnung' % (self.schlussel))
            sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
            datensatz = []
            for eintrag in sql_resultate:
                datensatz.append(auszeichnung_objekt.Auszeichnung(
                    auszeichnung=eintrag[0]))
            datenliste = verpacken_modelisieren.list_verpackung_modelisieren(
                datensatz)
            self.kontext.setContextProperty('mitglied_info_auszeichnungen',
                                            datenliste)
        except:
            # Wenn eine Ausnahme eintritt, leere Liste setzen
            self.kontext.setContextProperty('mitglied_info_auszeichnungen',
                                            None)

        # Führerscheine eines Mitglieds setzen
        try:
            sql_anweisung = sql_anweisungen.sql(
                tabelle='tblMitgliederFührerscheine',
                select='idFuehrerschein',
                filtern='fiStammlistennummerFührerschein=%s' % (
                    self.schlussel))
            sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
            datensatz = []
            for eintrag in sql_resultate:
                datensatz.append(fuhrerschein_objekt.Fuhrerschein(
                    schlussel=eintrag[0]))
            datenliste = verpacken_modelisieren.list_verpackung_modelisieren(
                datensatz)
            self.kontext.setContextProperty('mitglied_info_fuhrerscheine',
                                            datenliste)
        except:
            # Wenn eine Ausnahme eintritt, leere Liste setzen
            self.kontext.setContextProperty('mitglied_info_fuhrerscheine',
                                            None)

        self.mitglied_info_setzen_signal.emit()

    def lehrgaenge_schlussel_setzen(self, packung):
        """ *Schlüssel des ausgewählten Lehrgangs wird gesetzt*

        **Parameter**

        - packung: Verpackter Datensatz eines Lehrgangs

        """
        self.lehrgang_schlussel = packung.datensatz.schlussel

    def auszeichnungen_schlussel_setzen(self, packung):
        """ *Schlüssel der ausgewählten Auszeichnung wird gesetzt*

        **Parameter**

        - packung: Verpackter Datensatz einer Auszeichnung

        """
        self.auszeichnung_schlussel = packung.datensatz.auszeichnung

    def fuhrerscheine_schlussel_setzen(self, packung):
        """ *Schlüssel des ausgewählten Führerscheins wird gesetzt*

        **Parameter**

        - packung: Verpackter Datensatz eines Führerscheins

        """
        self.fuhrerschein_schlussel = packung.datensatz.schlussel

    def lehrgang_auswahl_setzen(self, packung):
        """ *Schlüssel des ausgewählten Lehrgangs in der Auswahliste setzen*

        **Parameter**

        - packung: Verpackter Datensatz einer Auswahl eines Lehrgangs

        """
        self.lehrgang_auswahl_schlussel = packung.datensatz.schlussel

    def auszeichnung_auswahl_setzen(self, packung):
        """*Schlüssel der ausgewählten Auszeichnung in der Auswahliste setzen*

        **Parameter**

        - packung: Verpackter Datensatz einer Auswahl einer Auszeichnung

        """
        self.auszeichnung_auswahl_schlussel = packung.datensatz.auszeichnung

    def fuhrerschein_auswahl_setzen(self, packung):
        """*Schlüssel des ausgewählten Führerschein in der Auswahliste setzen*

        **Parameter**

        - packung: Verpackter Datensatz einer Auswahl eines Führerscheins

        """
        self.fuhrerschein_auswahl_schlussel = packung.datensatz.schlussel

    def lehrgang_hinzu(self):
        """ *Ein neuer absolvierter Lehrgang einem Mitglied zuweisen*

        **Beschreibung**

        Der letzte ausgewählte Lehrgang aus der Lehrgang Auswahlliste wird
        den bereits bestehenden Lehrgängen des Mitglieds hinzugefügt.
        """
        try:
            sql_anweisung = sql_anweisungen.hinzufugen(
                tabelle='tblAbsolvieren',
                felder='fiStammlistennummerAbsolvieren, fiLehrgangLehrgang',
                werte='%s, "%s"' % (
                    self.schlussel, self.lehrgang_auswahl_schlussel))
            self.datenbank_verbindung.execute(sql_anweisung, True)
        except:
            return
        self.mitglied_info_setzen(self.mitglied)

    def lehrgang_entfernen(self):
        """ *Ein Lehrgang wird aus der Liste des Mitglieds entfernt*

        **Beschreibung**

        Der letzte ausgewählte Lehrgang aus der Liste der bereits bestehenden
        Lehrgängen des Mitglieds wird entfernt.
        """
        try:
            sql_anweisung = sql_anweisungen.entfernen(
                tabelle='tblAbsolvieren',
                filtern='fiStammlistennummerAbsolvieren=%s AND '
                'fiLehrgangLehrgang="%s"' % (self.schlussel,
                                             self.lehrgang_schlussel))
            self.datenbank_verbindung.execute(sql_anweisung, True)
        except:
            return
        self.mitglied_info_setzen(self.mitglied)

    def auszeichnung_hinzu(self):
        """ *Eine neue Auszeichnung wird einem Mitglied hinzugefügt*

        **Beschreibung**

        Die letzte ausgewählte Auszeichnung aus der Auszeichnungen Auswahlliste
        wird den bereits bestehenden Auszeichnungen des Mitglieds hinzugefügt.
        """
        try:
            sql_anweisung = sql_anweisungen.hinzufugen(
                tabelle='tblMitgliedsAuszeichnungen',
                felder='fiAuszeichnung, fiStammlistennummerAuszeichnung',
                werte='"%s", %s' % (self.auszeichnung_auswahl_schlussel,
                                    self.schlussel))
            self.datenbank_verbindung.execute(sql_anweisung, True)
        except:
            return
        self.mitglied_info_setzen(self.mitglied)

    def auszeichnung_entfernen(self):
        """ *Eine Auszeichnung wird aus der Liste des Mitglieds entfernt*

        **Beschreibung**

        Die letzte ausgewählte Auszeichnung aus der Liste der bereits
        bestehenden Auszeichnungen des Mitglieds wird entfernt.
        """
        try:
            sql_anweisung = sql_anweisungen.entfernen(
                tabelle='tblMitgliedsAuszeichnungen',
                filtern='fiStammlistennummerAuszeichnung=%s AND '
                'fiAuszeichnung="%s"' % (self.schlussel,
                                         self.auszeichnung_schlussel))
            self.datenbank_verbindung.execute(sql_anweisung, True)
        except:
            return
        self.mitglied_info_setzen(self.mitglied)

    def fuhrerschein_hinzu(self):
        """ *Ein neuer Führerschein wird einem Mitglied hinzugefügt*

        **Beschreibung**

        Der letzte ausgewählte Führerschein aus der Führerschein Auswahlliste
        wird den bereits bestehenden Führerscheinen des Mitglieds hinzugefügt.
        """
        try:
            sql_anweisung = sql_anweisungen.hinzufugen(
                tabelle='tblMitgliederFührerscheine',
                felder='fiStammlistennummerFührerschein, idFuehrerschein',
                werte='%s, "%s"' % (self.schlussel,
                                    self.fuhrerschein_auswahl_schlussel))
            self.datenbank_verbindung.execute(sql_anweisung, True)
        except:
            return
        self.mitglied_info_setzen(self.mitglied)

    def fuhrerschein_entfernen(self):
        """ *Ein Führerschein wird aus der Liste des Mitglieds entfernt*

        **Beschreibung**

        Der letzte ausgewählte Führerschein aus der Liste der bereits
        bestehenden Führerscheine des Mitglieds wird entfernt.
        """
        try:
            sql_anweisung = sql_anweisungen.entfernen(
                tabelle='tblMitgliederFührerscheine',
                filtern='fiStammlistennummerFührerschein=%s AND '
                'idFuehrerschein="%s"' % (self.schlussel,
                                          self.fuhrerschein_schlussel))
            self.datenbank_verbindung.execute(sql_anweisung, True)
        except:
            return
        self.mitglied_info_setzen(self.mitglied)

    def neues_mitglied(self):
        """ *Ein neues Mitglied soll erstellt werden*

        **Beschreibung**

        In der Datenbank wird ein neues Mitglied erstellt, bei welchem die
        3 wichtigsten Kolonnen einen Standard Wert erhalten.
        """
        sql_anweisung = sql_anweisungen.hinzufugen(
            tabelle='tblMitglied',
            felder='dtName, dtVorname, dtSozialnummer',
            werte='"Name","Vorname","000"')
        self.datenbank_verbindung.execute(sql_anweisung, True)
        self.kommandant_bereich_aufbau()

    def entfernen_mitglied(self):
        """ *Ein Mitglied soll gelöscht werden*

        **Beschreibung**

        Das zuletzt ausgewählte Mitglied wird aus der Datenbank gelöscht.
        """
        try:
            sql_anweisung = sql_anweisungen.entfernen(
                tabelle='tblMitglied',
                filtern='idStammlistennummer=%s' % (self.schlussel))
            self.datenbank_verbindung.execute(sql_anweisung, True)
        except:
            return
        self.overlay_meldungen_aufbau()
        self.kommandant_bereich_aufbau()

    def mitglied_aktualisieren(self):
        """ *Die Informationen eines Mitglieds werden abgefragt zum Speichern*

        **Beschreibung**

        Die **QMLUI** erhält die Anweisung die Informationen des momentan
        ausgewählten Mitglieds dem Python Unterbau zu übergeben.
        """
        self.mitglied_info_abspeichern_signal.emit()

    def mitglied_info_in_db_speichern(self):
        """ *Die Informationen eines Mitglieds werden in die DB gespeichert*

        **Beschreibung**

        Der Python Unterbau geht die Daten des zuletzt ausgewählten Mitglieds
        aus der **QMLUI** auslesen und speichert diese Daten in die Datenbank
        ab.
        """
        # Attribute aus der QMLUI werden abgefragt
        name = self.ui_objekt.property('edtMitgliedName')
        vorname = self.ui_objekt.property('edtMitgliedVorname')
        bday = self.ui_objekt.property('edtMitgliedBDay')

        # Falls ein Fehler während der Konvertierung auftritt
        try:
            bday = strptime(bday, "%d-%m-%Y")
            bday = strftime("%Y-%m-%d", bday)
        except ValueError:
            pass

        mobil = self.ui_objekt.property('edtMitgliedMobil')
        email = self.ui_objekt.property('edtMitgliedEmail')
        telefon = self.ui_objekt.property('edtMitgliedTelefon')
        ortschaft = self.ui_objekt.property('edtMitgliedOrtschaft')
        adresse = self.ui_objekt.property('edtMitgliedAdresse')
        postleitzahl = self.ui_objekt.property('edtMitgliedPostleitzahl')
        stunden = self.ui_objekt.property('edtMitgliedLehrgangstunden')
        medi = self.ui_objekt.property('edtMitgliedMedizinischeKontrolle')

        # Falls ein Fehler während der Konvertierung auftritt
        try:
            medi = strptime(medi, "%d-%m-%Y")
            medi = strftime("%Y-%m-%d", medi)
        except ValueError:
            pass

        sozial = self.ui_objekt.property('edtMitgliedSozialnummer')
        apteinsatz = self.ui_objekt.property('chkMitgliedAPTEinsatze')
        if apteinsatz:
            apteinsatz = 1
        else:
            apteinsatz = 0
        aptatemschutz = self.ui_objekt.property('chkMitgliedAPTAtemschutz')
        if aptatemschutz:
            aptatemschutz = 1
        else:
            aptatemschutz = 0

        # String für die SQL Anweisung wird zusammengesetzt
        feld_akt = 'dtName = "%s", ' % name
        feld_akt = feld_akt + 'dtVorname = "%s", ' % vorname
        feld_akt = feld_akt + 'dtGeburtsdatum = "%s", ' % str(bday)
        feld_akt = feld_akt + 'dtMobiltelefon = "%s", ' % mobil
        feld_akt = feld_akt + 'dtEmail = "%s", ' % email
        feld_akt = feld_akt + 'dtTelefonnummer = "%s", ' % telefon
        feld_akt = feld_akt + 'dtOrtschaft = "%s", ' % ortschaft
        feld_akt = feld_akt + 'dtAdresse = "%s", ' % adresse
        feld_akt = feld_akt + 'dtPostleitzahl = "%s", ' % postleitzahl
        feld_akt = feld_akt + 'dtLehrgangsstunden = "%s", ' % stunden
        feld_akt = feld_akt + 'dtAPTEinsätze = "%s", ' % apteinsatz
        feld_akt = feld_akt + 'dtAPTAtemschutz = "%s", ' % aptatemschutz
        feld_akt = feld_akt + 'dtMedizinischeKontrolle = "%s", ' % str(medi)
        feld_akt = feld_akt + 'dtSozialnummer = "%s"' % sozial

        # SQL Anweisung durchführen
        sql_anweisung = sql_anweisungen.aktualisieren(
            tabelle='tblMitglied',
            aktualisierung=feld_akt, filtern='idStammlistennummer=%s' %
            self.schlussel)
        self.datenbank_verbindung.execute(sql_anweisung, True)

        self.overlay_meldungen_aufbau()
        self.kommandant_bereich_aufbau(state_wechseln=False)

    def ausrustung_bearbeiten(self):
        """ *Das Menü zum bearbeiten der Ausrüstung eines Mitglieds*

        **Beschreibung**

        Das Menü zur Verwaltung der persönlichen Ausrüstung eines Mitglieds
        wird gesetzt und aufgerufen.
        Eine Liste mit allen nicht verwendeten Inventargegenständen wird
        angezeigt und die momentane Ausrüstung eines Mitglieds wird angezeigt.
        """
        try:
            # Liste von nicht verwendeten Inventargegenständen wird erzeugt
            inventar_liste = inventar.nicht_verwendete_inventargegenstaende(
                self.datenbank_verbindung)
            datensatz = []
            for eintrag in inventar_liste:
                datensatz.append(inventarliste_objekt.Inventarliste(
                    schlussel=eintrag[0], bezeichnung=eintrag[1],
                    kategorie=eintrag[4], knappheit=eintrag[6],
                    anzahl=eintrag[8]))
            inventarliste = verpacken_modelisieren.list_verpackung_modelisieren(
                datensatz)
            self.kontext.setContextProperty('inventar_liste', inventarliste)
        except:
            # Wenn eine Ausnahme eintritt, leere Liste setzen
            self.kontext.setContextProperty('inventar_liste', None)

        try:
            # Liste aller Ausrüstungsgegenstände eines Mitglieds wird erzeugt
            sql_anweisung = sql_anweisungen.sql(
                tabelle='tblInventar',
                filtern='fiStammlistennummerAusrustung=%s' % self.schlussel)
            sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
            datensatz = []
            for eintrag in sql_resultate:
                datensatz.append(inventarliste_objekt.Inventarliste(
                    schlussel=eintrag[0], bezeichnung=eintrag[1],
                    kategorie=eintrag[4], knappheit=eintrag[6]))
            ausrustung = verpacken_modelisieren.list_verpackung_modelisieren(
                datensatz)
            self.kontext.setContextProperty('ausrustung_liste', ausrustung)
        except:
            # Wenn eine Ausnahme eintritt, leere Liste setzen
            self.kontext.setContextProperty('ausrustung_liste', None)

        self.ausrustung_bearbeiten_signal.emit()

    def mitglied_info_anzeigen(self):
        """ *Die Mitglied Informationen wieder anzeigen* """
        self.mitglied_info_setzen(self.mitglied)

    def inventar_schlussel_setzen(self, packung):
        """ *Der Schlüssel des ausgewählten Inventargegenstandes wird gesetzt*

        **Parameter**

        - packung: Verpackter Datensatz eines Inventargegenstandes

        """
        self.inventar_schlussel = packung.datensatz.schlussel

    def ausrustung_schlussel_setzen(self, packung):
        """ *Der Schlüssel des ausgewählten Ausrüstungsgegenstandes setzen*

        **Parameter**

        - packung: Verpackter Datensatz eines Ausrüstungsgegenstandes

        """
        self.ausrustung_schlussel = packung.datensatz.schlussel

    def ausrustung_hinzu(self):
        """ *Ein Ausrüstungsgegenstand wird dem Mitglied hinzugefügt* """
        try:
            sql_anweisung = sql_anweisungen.aktualisieren(
                tabelle='tblInventar',
                aktualisierung='fiStammlistennummerAusrustung=%s' % self.schlussel,
                filtern='idInventarnummer=%s' % self.inventar_schlussel)
            self.datenbank_verbindung.execute(sql_anweisung, True)
        except:
            return
        self.ausrustung_bearbeiten()

    def ausrustung_entfernen(self):
        """ *Ein Ausrüstungsgegenstand wird von einem Mitglied entfernt* """
        try:
            sql_anweisung = sql_anweisungen.aktualisieren(
                tabelle='tblInventar',
                aktualisierung='fiStammlistennummerAusrustung=NULL',
                filtern='idInventarnummer=%s' % self.ausrustung_schlussel)
            self.datenbank_verbindung.execute(sql_anweisung, True)
        except:
            return
        self.ausrustung_bearbeiten()

    def overlay_bericht(self):
        """ *Ein Bericht wird aus der Meldungsliste erstellt* """
        heute = datetime.date.today()
        bericht = ""
        bericht = bericht + '<h2>Kommandant Bericht vom %s</h2>' % heute
        try:
            for text in self.meldungen_liste:
                bericht = bericht + '<p class="bericht">\n'
                bericht = bericht + text.meldung + "<br \>\n"
                bericht = bericht + "</p>\n"
        except:
            bericht = "<i>Keine Meldungen verfügbar</i>"
        bericht_erstellung.html_bericht(daten=bericht, benutzer='Kommandant')
