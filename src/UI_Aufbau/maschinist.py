# -*- coding: utf-8 -*-

# Ritchie Flick
# T3IFAN 2012/2013 Abschlussprojekt
# LN
# 'Feuerwehrverwaltungssoftware - FVS'
# 'src/UI_Aufbau/maschinist.py'

# Importieren von PySide Modulen
from PySide import QtCore
from PySide import QtDeclarative
from PySide.QtGui import QIcon

# Importieren von FVS Modulen
from Konfiguration import konfig
from Datenbank import db_verbindung
from Datenbank import sql_anweisungen
from Daten_Modelle import verpacken_modelisieren
from Daten_Objekte import fahrzeug_objekt
from Daten_Objekte import inventarliste_objekt
from Daten_Objekte import meldungen_objekt
from Logik import inventar
from Logik import fahrzeug
from Logik import bericht_erstellung

# Importieren der benötigten Kontroller
from Kontroller import listen
from Kontroller import button
from Kontroller import operationen


class QMLUI(QtDeclarative.QDeclarativeView):
    """ *Hauptklasse der grafischen Benutzeroberfläche des Maschinisten*

    **Beschreibung**

    Die **QMLUI** (grafische Benutzeroberfläche) des Maschinisten wird
    aufgebaut, die Verbindung zur Datenbank wird aufgebaut und alle nötigen
    Signale/Slots und Funktionen zum arbeiten mit der **QMLUI** werden
    implementiert.
    """

    # Initialisierung der Qt Signale
    fahrzeug_info_setzen_signal = QtCore.Signal()
    ausstattung_anzeigenVerstecken_signal = QtCore.Signal()
    ausstattung_verwaltung_starten_signal = QtCore.Signal()
    fahrzeug_infoFelder_freischaltenSperren_signal = QtCore.Signal()
    fahrzeug_info_abspeichern_signal = QtCore.Signal()
    neues_fahrzeug_signal = QtCore.Signal()
    kein_fahrzeug_state_signal = QtCore.Signal()
    bericht_erstellen_signal = QtCore.Signal()

    def __init__(self):
        super(QMLUI, self).__init__()
        # Verbindung zur Datenbank wird aufgebaut
        try:
            einstellungen = konfig.laden_konfig()
            self.datenbank_verbindung = db_verbindung.DB_Verbindung(
                einstellungen)
        except:
            print("Es konnte keine Datenbankverbindung hergestellt werden")

        # Festlegung welche Funktionen von welchen Listen aufgerufen werden
        listen_komponenten_funktionen = {
            "lstfahrzeugliste": [True, self.fahrzeug_info],
            "lstAusstattungliste": [True, self.ausstattung_schlussel_einlesen],
            "lstInventar": [True, self.inventargegenstand_schlussel_einlesen]
        }

        # Festlegung welche Funktionen von welchen Buttons aufgerufen werden
        button_komponenten_funktionen = {
            "btnAusstattungAuflistung": self.ausstattung_anzeigenVerstecken,
            "btnAusstattungVerwaltung": self.ausstattung_verwaltung_aufrufen,
            "btnAusstattungVerwaltungStop": self.ausstattung_verwaltung_stoppen,
            "btnAusstattungHinzufugen": self.zur_ausstattung_hinzufugen,
            "btnAusstattungEntfernen": self.aus_ausstattung_entfernen,
            "btnInfoBearbeiten": self.fahrzeug_infoFelder_freischaltenSperren,
            "btnAbspeichern": self.fahrzeug_info_abspeichern,
            "btnNeuFahrzeug": self.neues_fahrzeug_erstellen,
            "btnEntfernenFahrzeug": self.fahrzeug_entfernen,
            "btnBerichtErstellen": self.bericht_erstellen
        }

        # Festlegung welche Funktionen bei welchen Operation aufgerufen werden
        operationen_funktionen = {
            "fahrzeug_variablen_gesetzt": self.in_db_speichern
        }

        # Kontroller für die QMLUI werden eingerichtet
        listen_kontroller = listen.Kontroller(self,
                                              listen_komponenten_funktionen)
        button_kontroller = button.Kontroller(self,
                                              button_komponenten_funktionen)
        operationen_kontroller = operationen.Kontroller(self,
                                                        operationen_funktionen)

        # Attribute/Kontroller auf welche die QML UI zugreifen kann
        self.kontext = self.rootContext()
        self.kontext.setContextProperty('listen_kontroller', listen_kontroller)
        self.kontext.setContextProperty('button_kontroller', button_kontroller)
        self.kontext.setContextProperty('operationen_kontroller',
                                        operationen_kontroller)

        # Fahrzeugliste aufbauen
        self.fahrzeugliste_aufbauen()
        self.overlay_meldungen_aufbau()

        # QMLUI wird geladen
        self.setWindowTitle("FVS - Maschinist")
        self.setWindowIcon(QIcon("./icon.png"))
        self.setSource(QtCore.QUrl.fromLocalFile(
            './QMLUI/Maschinist.qml'))
        self.setResizeMode(QtDeclarative.QDeclarativeView.SizeRootObjectToView)

        # QMLUI Attribute werden zugänglich gemacht
        self.ui_objekt = self.rootObject()

        # Slots der QMLUI werden gesetzt
        self.fahrzeug_info_abspeichern_signal.connect(
            self.ui_objekt.fahrzeug_info_abspeichern_qml)

        self.fahrzeug_info_setzen_signal.connect(
            self.ui_objekt.fahrzeug_info_setzen_qml)

        self.ausstattung_anzeigenVerstecken_signal.connect(
            self.ui_objekt.ausstattung_anzeigen_setzen_qml)

        self.ausstattung_verwaltung_starten_signal.connect(
            self.ui_objekt.ausstattung_verwaltung_starten_qml)

        self.fahrzeug_infoFelder_freischaltenSperren_signal.connect(
            self.ui_objekt.fahrzeug_infoFelder_freischaltenSperren_qml)

        self.neues_fahrzeug_signal.connect(self.ui_objekt.neues_fahrzeug_qml)

        self.kein_fahrzeug_state_signal.connect(
            self.ui_objekt.kein_fahrzeug_state_qml)

        # Startinformationen werden gesetzt
        self.fahrzeugID = None
        self.inventarID = None
        self.ausstattungID = None

    def fahrzeugliste_aufbauen(self):
        """ *Fahrzeugliste wird aufgebaut und mit Daten gefüllt* """
        sql_anweisung = sql_anweisungen.sql(tabelle='tblFahrzeug')
        try:
            sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
        except:
            self.kontext.setContextProperty('fahrzeug_liste', None)
        datensatz = []
        # Instanz Variablen für Fahrzeug() und FahrzeugTyp() werden gesetzt
        if sql_resultate:
            for eintrag in sql_resultate:
                try:
                    datensatz.append(fahrzeug_objekt.Fahrzeug(
                        schlussel=eintrag[0],
                        kennzeichen=eintrag[1], bezeichnung=eintrag[2],
                        besatzung=eintrag[3], technisches_problem=eintrag[4],
                        kommentar=eintrag[5], tuv=eintrag[6],
                        werkstatt=eintrag[7], typ=eintrag[8]))
                except:
                    datensatz.append(fahrzeug_objekt.Fahrzeug(None))
        else:
            self.kontext.setContextProperty('fahrzeug_liste', None)

        # Liste aller Fahrzeug Typen wird erstellt
        fahrzeug_typ_liste = ['MTW', 'TLF', 'TSF', 'LF', 'HTLF', 'GW', 'RW1',
                              'DLA', 'TLK', 'DLK', 'KTW', 'RTW', 'NAW',
                              'S.A.M.U.']
        fahrzeug_typ_daten = []
        for eintrag in fahrzeug_typ_liste:
            fahrzeug_typ_daten.append(fahrzeug_objekt.FahrzeugTypListe(
                typ=eintrag))

        # Die Datensätze werden in ein 'QObject' verpackt
        # und ins Listen Modell modelisiert
        datenliste = verpacken_modelisieren.list_verpackung_modelisieren(
            datensatz)
        fahrzeug_typen = verpacken_modelisieren.list_verpackung_modelisieren(
            fahrzeug_typ_daten)

        # Die beiden Listen werden der QMLUI übergeben
        self.kontext.setContextProperty('fahrzeug_liste', datenliste)
        self.kontext.setContextProperty('fahrzeug_typ_liste', fahrzeug_typen)

    def overlay_meldungen_aufbau(self):
        """ *Die Meldungen für das Overlay werden aufgebaut*

        **Beschreibung**

        Methode welche alle benötigten/vorgesehenen Kontrollen/Tests durchführt
        und diese ins Overlay schreibt.
        """
        sql_anweisung = sql_anweisungen.sql(tabelle='tblFahrzeug')
        try:
            sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
        except:
            self.kontext.setContextProperty('meldungen_liste', None)
        datensatz = []
        # Instanz Variablen für Fahrzeug() und FahrzeugTyp() werden gesetzt
        if sql_resultate:
            for eintrag in sql_resultate:
                try:
                    if fahrzeug.tuvTest(eintrag[6]):
                        datensatz.append(meldungen_objekt.Meldungen(
                            'Der Tüv Test für das Fahrzeug \n%s %s ist abgelaufen' %
                            (eintrag[1], eintrag[2])))
                    if fahrzeug.werkTest(eintrag[7]):
                        datensatz.append(meldungen_objekt.Meldungen(
                            'Die Werkstattkontrolle für das Fahrzeug \n%s %s '
                            'ist abgelaufen' % (eintrag[1], eintrag[2])))
                    if fahrzeug.problemTest(eintrag[4]):
                        datensatz.append(meldungen_objekt.Meldungen(
                            'Für das Fahrzeug \n%s %s besteht ein Problem'
                            % (eintrag[1], eintrag[2])))
                except:
                    datensatz.append(meldungen_objekt.Meldungen(None))
            datenliste = verpacken_modelisieren.list_verpackung_modelisieren(
                datensatz)
            self.kontext.setContextProperty('meldungen_liste', datenliste)
        else:
            self.kontext.setContextProperty('meldungen_liste', None)
        self.meldungen_liste = datensatz

    def fahrzeug_info(self, fahrzeug_daten):
        """ *Fahrzeug Informationen werden gesetzt*

        **Parameter**

        - fahrzeug_daten: Verpackter Datensatz eines Fahrzeug Objektes

        """
        self.fahrzeugID = fahrzeug_daten.datensatz.schlussel
        self.kontext.setContextProperty('fahrzeug_kennzeichen',
                                        fahrzeug_daten.datensatz.kennzeichen)
        self.kontext.setContextProperty('fahrzeug_bezeichnung',
                                        fahrzeug_daten.datensatz.bezeichnung)
        self.kontext.setContextProperty(
            'fahrzeug_besatzung',
            fahrzeug_daten.datensatz.besatzung)
        self.kontext.setContextProperty(
            'fahrzeug_problem',
            fahrzeug_daten.datensatz.technisches_problem)
        self.kontext.setContextProperty('fahrzeug_kommentar',
                                        fahrzeug_daten.datensatz.kommentar)
        self.kontext.setContextProperty('fahrzeug_tuv',
                                        str(fahrzeug_daten.datensatz.tuv))
        self.kontext.setContextProperty(
            'fahrzeug_werkstatt',
            str(fahrzeug_daten.datensatz.werkstatt))
        self.kontext.setContextProperty('fahrzeug_typ',
                                        str(fahrzeug_daten.datensatz.typ))
        self.fahrzeug_info_setzen_signal.emit()

    def ausstattung_anzeigenVerstecken(self):
        """ *Die Ausstattung wird angezeigt oder versteckt* """
        sql_anweisung = sql_anweisungen.sql(
            tabelle='tblInventar',
            select='idInventarnummer, dtBezeichnung, dtKategorie, '
            'COUNT(dtBezeichnung) AS "Anzahl"',
            filtern='idInventarnummer IN (SELECT fiInventarnummer'
            ' FROM tblFahrzeugAusstattung WHERE fiFahrzeugNummer=%s)' % (
                self.fahrzeugID),
            gruppieren='dtBezeichnung')
        try:
            sql_resultate = self.datenbank_verbindung.execute(sql_anweisung)
        except:
            self.kontext.setContextProperty('ausstattung_auflistung', None)
        datensatz = []
        if sql_resultate:
            for eintrag in sql_resultate:
                try:
                    datensatz.append(inventarliste_objekt.Inventarliste(
                        schlussel=eintrag[0], bezeichnung=eintrag[1],
                        kategorie=eintrag[2], anzahl=eintrag[3]))
                except:
                    datensatz.append(inventarliste_objekt.Inventarliste(None))
            datenliste = verpacken_modelisieren.list_verpackung_modelisieren(
                datensatz)
            self.kontext.setContextProperty('ausstattung_auflistung',
                                            datenliste)
        else:
            self.kontext.setContextProperty('ausstattung_auflistung', None)
        self.ausstattung_anzeigenVerstecken_signal.emit()

    def ausstattung_schlussel_einlesen(self, ausstattungs_gegenstand):
        """ *Der Schlüssel des momentan ausgewählten Ausstattungegenstandes*

        **Parameter**

        - ausstattungs_gegenstand: Verpackter Datensatz eines Ausstattung
          Gegenstandes
        """
        self.ausstattungID = ausstattungs_gegenstand.datensatz.schlussel

    def ausstattung_verwaltung_aufrufen(self):
        """ *Die Verwaltung der Fahrzeugausstattung wird aufgerufen* """
        self.ausstattung_anzeigenVerstecken()
        datensatz = []
        # Wird benötigt damit die Datenbank eine neue Anfrage durchführt und
        # nicht aus dem Cache liest
        self.datenbank_verbindung.commit()
        liste = inventar.nicht_verwendete_inventargegenstaende(
            self.datenbank_verbindung)
        if liste:
            for eintrag in liste:
                try:
                    datensatz.append(inventarliste_objekt.Inventarliste(
                        schlussel=eintrag[0], bezeichnung=eintrag[1],
                        kategorie=eintrag[4], knappheit=eintrag[6],
                        anzahl=eintrag[8]))
                except:
                    datensatz.append(inventarliste_objekt.Inventarliste(None))
            inventarliste = verpacken_modelisieren.list_verpackung_modelisieren(
                datensatz)
            self.kontext.setContextProperty('inventarliste', inventarliste)
        else:
            self.kontext.setContextProperty('inventarliste', None)
        self.ausstattung_verwaltung_starten_signal.emit()

    def ausstattung_verwaltung_stoppen(self):
        """ *Die Verwaltung der Fahrzeugausstattung wird gestoppt* """
        self.ausstattung_anzeigenVerstecken_signal.emit()

    def inventargegenstand_schlussel_einlesen(self, inventar_eintrag):
        """ *Der momentan ausgewählte Inventargegenstand wird gesetzt*

        **Parameter**

        - inventar_eintrag: Verpackter Datensatz eines Inventargegenstandes

        """
        self.inventarID = inventar_eintrag.datensatz.schlussel

    def zur_ausstattung_hinzufugen(self):
        """ *Ein Inventargegenstand wird zur Fahrzeugausstattung hinzugefügt*
        """
        try:
            sql_anweisung = sql_anweisungen.hinzufugen(
                tabelle='tblFahrzeugAusstattung',
                felder='fiFahrzeugNummer, fiInventarnummer',
                werte='%s,%s' % (self.fahrzeugID, self.inventarID))
            self.datenbank_verbindung.execute(sql_anweisung, True)
        except:
            return
        self.ausstattung_verwaltung_aufrufen()

    def aus_ausstattung_entfernen(self):
        """ *Inventargegenstand wird aus der Fahrzeugausstattung entfernt* """
        try:
            sql_anweisung = sql_anweisungen.entfernen(
                tabelle='tblFahrzeugAusstattung',
                filtern='fiInventarnummer=%s' % self.ausstattungID)
            self.datenbank_verbindung.execute(sql_anweisung, True)
        except:
            return
        self.ausstattung_verwaltung_aufrufen()

    def fahrzeug_infoFelder_freischaltenSperren(self):
        """ *Felder der Fahrzeuginformationen werden freigeschaltet* """
        self.fahrzeug_infoFelder_freischaltenSperren_signal.emit()

    def fahrzeug_info_abspeichern(self):
        """ *Signal an die QMLUI dass Fahrzeuginformationen abgepseichern* """
        self.fahrzeug_info_abspeichern_signal.emit()
        self.overlay_meldungen_aufbau()

    def in_db_speichern(self):
        """ *Informationen aus der QMLUI in die DB abgespeichert* """
        kennzeichen = self.ui_objekt.property('edtKennzeichen')
        bezeichnung = self.ui_objekt.property('edtBezeichnung')
        besatzung = int(self.ui_objekt.property('edtBesatzung'))
        problem = self.ui_objekt.property('chkProblem')
        if problem:
            problem = 1
        else:
            problem = 0
        kommentar = self.ui_objekt.property('memProblemKommentar')
        tuv = self.ui_objekt.property('edtTuv')
        werkstatt = self.ui_objekt.property('edtWerkstatt')
        typ = self.ui_objekt.property('fahrzeugTyp')
        feld_akt = 'dtKennzeichen = "%s", ' % kennzeichen
        feld_akt = feld_akt + 'dtBezeichnungFahrzeug = "%s", ' % bezeichnung
        feld_akt = feld_akt + 'dtBesatzung = "%s", ' % besatzung
        feld_akt = feld_akt + 'dtTechnischesProblem = "%s", ' % problem
        feld_akt = feld_akt + 'dtTechnischerKommentar = "%s", ' % kommentar
        feld_akt = feld_akt + 'dtTUVKontrolle = "%s", ' % tuv
        feld_akt = feld_akt + 'dtWerkstattKontrolle = "%s", ' % werkstatt
        feld_akt = feld_akt + 'fiFahrzeugTypFahrzeug = "%s" ' % typ
        sql_anweisung = sql_anweisungen.aktualisieren(
            tabelle='tblFahrzeug',
            aktualisierung=feld_akt,
            filtern='idFahrzeugNummer=%s' % self.fahrzeugID)
        self.datenbank_verbindung.execute(sql_anweisung, True)
        self.fahrzeugliste_aufbauen()
        self.overlay_meldungen_aufbau()
        self.fahrzeug_infoFelder_freischaltenSperren()

    def fahrzeug_entfernen(self):
        """ *Ausgewähltes Fahrzeug wird aus der DB entfernt* """
        try:
            sql_anweisung = sql_anweisungen.entfernen(
                tabelle='tblFahrzeug',
                filtern='idFahrzeugNummer=%s' % self.fahrzeugID)
            self.datenbank_verbindung.execute(sql_anweisung, True)
        except:
            return
        self.fahrzeugliste_aufbauen()
        self.overlay_meldungen_aufbau()
        self.kein_fahrzeug_state_signal.emit()

    def neues_fahrzeug_erstellen(self):
        """ *Ein neues Fahrzeug wird erstellt* """
        sql_anweisung = sql_anweisungen.hinzufugen(tabelle='tblFahrzeug')
        self.datenbank_verbindung.execute(sql_anweisung, True)
        db = self.datenbank_verbindung
        self.fahrzeugID = db.letzter_erstellter_schlussel()
        self.neues_fahrzeug_signal.emit()

    def bericht_erstellen(self):
        """ *Ein Bericht wird aus der Meldungsliste erstellt* """
        bericht = ""
        try:
            for text in self.meldungen_liste:
                bericht = bericht + '<p class="bericht">'
                bericht = bericht + text.meldung + "<br \>"
                bericht = bericht + "</p>"
        except:
            bericht = "<i>Keine Meldungen</i>"
        bericht_erstellung.html_bericht(daten=bericht, benutzer="Maschinist")
