Logik
======

Sämtliche *Logik* Module für **FVS** welche verschiedene *Operationen* mit den
Daten aus der Datenbank durchführen.

Fahrzeug Kontrollen
----------------------

.. automodule:: Logik.fahrzeug
   :members:

Inventar Logik
----------------------

.. automodule:: Logik.inventar
   :members:

Bericht Erstellung
--------------------

.. automodule:: Logik.bericht_erstellung
   :members:

Protokoll Logik
------------------

.. automodule:: Logik.protokoll_logik
   :members:

Mitglied Logik
----------------

.. automodule:: Logik.mitglied_logik
   :members: