# -*- coding: utf-8 -*-

# Ritchie Flick
# T3IFAN 2012/2013 Abschlussprojekt
# LN
# 'Feuerwehrverwaltungssoftware - FVS'
# 'src/Logik/inventar.py'


def nicht_verwendete_inventargegenstaende(db_verbindung):
    """ *Liste aller nicht verwendeten Inventargegenständen*

    **Beschreibung**

    Führt *SQL* Anfragen in verschiedenen Tabellen durch um festzustellen
    welche *Inventargegenstände* noch nicht verwendet werden.

    **Parameter**

    - db_verbindung: Das Objekt der momentanen Verbindung zur Datenbank

    **Rückgabewerte**

    - datensatz: Liste aller bisher noch nicht verwendeten Inventargegenständen

    """
    datensatz = db_verbindung.execute("""
        SELECT *, COUNT(*) As 'Anzahl'
        FROM tblInventar
        WHERE fiStammlistennummerAusrustung IS NULL
            AND idInventarnummer NOT IN(
                SELECT fiInventarnummer
                FROM tblFahrzeugAusstattung)
        GROUP BY dtBezeichnung
        """)
    return datensatz
