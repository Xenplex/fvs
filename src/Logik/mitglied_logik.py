# -*- coding: utf-8 -*-

# Ritchie Flick
# T3IFAN 2012/2013 Abschlussprojekt
# LN
# 'Feuerwehrverwaltungssoftware - FVS'
# 'src/Logik/mitglied_logik.py'

"""
Hier sind Funktionen implementiert welche verwendet werden um Kontrollen und
Tests für verschiedene Mitglieder auszuführen.
"""

# Importieren von Python Modulen
import datetime


def medizinischeKontrolle(mediDatum):
    """ *Überprüfung der medizinischen Kontrolle*

    **Beschreibung**

    Funktion welche testet ob die *medizinische Kontrolle* eines Mitglieds
    bereits abgelaufen ist oder nicht.

    **Parameter**

    - mediDatum: Das Datum der letzten medizinischen Kontrolle

    **Rückgabewerte**

    - None: Die *medizinische Kontrolle* ist nicht abgelaufen
    - 1: Die *medizinische Kontrolle* ist abgelaufen

    """
    # Anzahl an Tagen wann eine medizinische Kontrolle
    # abgelaufen ist
    MEDIABLAUF = 10

    heute = datetime.date.today()
    try:
        if (heute - mediDatum) >= datetime.timedelta(days=MEDIABLAUF):
            return 1
        else:
            return None
    except:
        pass


def restliche_stunden(stunden):
    """ *Überprüft ob ein Mitglied noch freie Stunden hat*

    **Beschreibung**

    Funktion welche kontrolliert ob ein Mitglied noch genügend freie Stunden
    hat um an Lehrgängen teilzunehmen.

    **Parameter**

    - stunden: Die Anzahl an restlichen Stunden

    **Rückgabewerte**

    - None: Das Mitglied hat noch genügend Stunden
    - 1: Das Mitglied hat nicht mehr genügend Stunden
    """
    # Anzahl an Stunden welche ein Mitglied noch haben muss, bevor eine Warnung
    # ausgegeben wird
    MINDESTSTUNDEN = 10

    try:
        if MINDESTSTUNDEN > stunden:
            return 1
        else:
            return None
    except:
        pass
