# -*- coding: utf-8 *-*

# Ritchie Flick
# T3IFAN 2012/2013 Abschlussprojekt
# LN
# 'Feuerwehrverwaltungssoftware - FVS'
# 'src/Logik/bericht_erstellung.py'

# Importieren von Python Modulen
import webbrowser
import datetime
import os


def html_bericht(daten, benutzer, datei_name=None):
    """ *Erstellt einen HTML Bericht*

    **Beschreibung**

    Funktion welche die übergebenen Daten in einen *HTML* Datei als Bericht
    einbindet und anschließend diesen Bericht im *Standard Browser* des
    Benutzers anzeigt.

    **Parameter**

    - daten: Die Daten aus welchen der HTML Bericht erstellt werden soll
    - benutzer: Der Benutzer welcher den Bericht erstellt hat (wird verwendet
      umd die Dateien später voneinander zu unterscheiden)

    """
    heute = datetime.date.today()
    bericht = """
<!DOCTYPE html>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html">
            <meta  charset="utf-8">
            <link rel="stylesheet" href="./fvs.css" type="text/css" />
            <!--[if IE]>
            <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js">
            </script>
            <![endif]-->
            <title>Feuerwehrverwaltungssoftware - FVS</title>
        </head>
        <body>
            %s
            <br />
            <br />
            <br />
            <footer>
                <i>Erstellt mit FVS<i>
            </footer>
        </body>
    </html>""" % (daten)

    # Für Windows Systeme
    if os.name == "nt":
        # Sicherstellen dass das benötigte Verzeichnis existiert
        if not os.path.exists('.\Berichte'):
            os.makedirs('.\Berichte')
        if datei_name:
            html_datei = open('.\Berichte\%s_%s_%s.html' % (datei_name,
                                                             benutzer, heute), 'w')
            html_datei.write(bericht)
            html_datei.close()
            webbrowser.open('.\Berichte\%s_%s_%s.html' % (datei_name,
                                                           benutzer, heute))
        else:
            html_datei = open('.\Berichte\\bericht_%s_%s.html' % (
                benutzer, heute), 'w')
            html_datei.write(bericht)
            html_datei.close()
            webbrowser.open('.\Berichte\\bericht_%s_%s.html' % (benutzer, heute))
    # Für Linux Systeme
    else:
        # Sicherstellen dass das benötigte Verzeichnis existiert
        if not os.path.exists('../Berichte/'):
            os.makedirs('../Berichte/')
        if datei_name:
            html_datei = open('../Berichte/%s_%s_%s.html' % (datei_name,
                                                             benutzer, heute), 'w')
            html_datei.write(bericht)
            html_datei.close()
            webbrowser.open('../Berichte/%s_%s_%s.html' % (datei_name,
                                                           benutzer, heute))
        else:
            html_datei = open('../Berichte/bericht_%s_%s.html' % (
                benutzer, heute), 'w')
            html_datei.write(bericht)
            html_datei.close()
            webbrowser.open('../Berichte/bericht_%s_%s.html' % (benutzer, heute))
