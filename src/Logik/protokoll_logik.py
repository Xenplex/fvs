# -*- coding: utf-8 -*-

# Ritchie Flick
# T3IFAN 2012/2013 Abschlussprojekt
# LN
# 'Feuerwehrverwaltungssoftware - FVS'
# 'src/Logik/protokoll_logik.py'

from Daten_Objekte import referenz_objekt


def referenzenListe(db_verbindung, schlussel):
    """ *Zusammenstellung der Referenzen eines Protokolles*

    **Beschreibung**

    Alle Referenzen welche in einem Protokoll gesetzt wurden, werden ausgelesen
    und als Liste zurückgegeben.

    **Parameter**

    - db_verbindung: Das Objekt der momentanen Verbindung zur Datenbank
    - schlussel: Die ID des Protokolls für welche die Referenzen ausgelesen
      werden sollen

    **Rückgabewerte**

    - datensatz: Liste aller Referenzen eines Protokolls

    """
    try:
        # Inventar Referenzen
        sql_resultate = db_verbindung.execute("""
            SELECT idInventarnummer, dtBezeichnung
            FROM tblInventar, tblInventar_Protokoll_Referenzen Ref
            WHERE Ref.fiInventarnummerProtokoll = tblInventar.idInventarnummer
                AND Ref.fiProtokollInventar = %s
            """ % (schlussel))
        datensatz = []
        for eintrag in sql_resultate:
            datensatz.append(referenz_objekt.Referenz(schlussel=eintrag[0],
                                                      bezeichnung=eintrag[1],
                                                      referenzTabelle=1))
    except:
        pass

    try:
        # Ereignis Referenzen
        sql_resultate = db_verbindung.execute("""
            SELECT idEreignisnummer, dtBezeichnungEreignis
            FROM tblEreignis, tblEreignis_Protokoll_Referenzen Ref
            WHERE Ref.fiEreignisnummerProtokoll = tblEreignis.idEreignisnummer
                AND Ref.fiProtokoll = %s
            """ % (schlussel))
        for eintrag in sql_resultate:
            datensatz.append(referenz_objekt.Referenz(schlussel=eintrag[0],
                                                      bezeichnung=eintrag[1],
                                                      referenzTabelle=2))
    except:
        pass

    try:
        # Mitglied Referenzen
        sql_resultate = db_verbindung.execute("""
            SELECT idStammlistennummer, dtName
            FROM tblMitglied, tblMitglied_Protokoll_Referenzen Ref
            WHERE Ref.fiStammlistennummerProtokoll = tblMitglied.idStammlistennummer
                AND Ref.fiProtokollMitglied = %s
            """ % (schlussel))
        for eintrag in sql_resultate:
            datensatz.append(referenz_objekt.Referenz(schlussel=eintrag[0],
                                                      bezeichnung=eintrag[1],
                                                      referenzTabelle=3))
    except:
        pass

    try:
        # Fahrzeug Referenzen
        sql_resultate = db_verbindung.execute("""
            SELECT idFahrzeugNummer, dtBezeichnungFahrzeug
            FROM tblFahrzeug, tblFahrzeug_Protokoll_Referenzen Ref
            WHERE Ref.fiFahrzeugNummerProtokoll = tblFahrzeug.idFahrzeugNummer
                AND Ref.fiProtokollFahrzeug = %s
            """ % (schlussel))
        for eintrag in sql_resultate:
            datensatz.append(referenz_objekt.Referenz(schlussel=eintrag[0],
                                                      bezeichnung=eintrag[1],
                                                      referenzTabelle=4))
    except:
        pass

    return datensatz
