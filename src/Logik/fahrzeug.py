# -*- coding: utf-8 -*-

# Ritchie Flick
# T3IFAN 2012/2013 Abschlussprojekt
# LN
# 'Feuerwehrverwaltungssoftware - FVS'
# 'src/Logik/fahrzeug.py'

"""
Hier sind Funktionen implementiert, welche verwendet werden um Kontrollen und
Tests auf Fahrzeuge durchzuführen.
"""

# Importieren von Python Modulen
import datetime


def tuvTest(tuvdatum):
    """ *Testet TÜV Test*

    **Beschreibung**

    Funktion welche testet ob der *TÜV* eines Fahrzeuges bereits abgelaufen ist
    oder nicht.

    **Parameter**

    - tuvdatum: Das Datum des letzten *TÜV*

    **Rückgabewerte**

    - 0: Der *TÜV* ist nicht abgelaufen
    - 1: Der *TÜV* ist abgelaufen

    """
    TUVABLAUF = 365  # Anzahl an Tagen wann ein TÜV abgelaufen ist

    heute = datetime.date.today()
    try:
        if (heute - tuvdatum) >= datetime.timedelta(days=TUVABLAUF):
            return 1
        else:
            return None
    except:
        pass


def werkTest(werkdatum):
    """ *Testet Werkstattkontrolle Ablaufdatum*

    **Beschreibung**

    Funktion welche testet ob die *Werkstattkontrolle* bereits abgelaufen
    ist oder nicht.

    **Parameter**

    - werkdatum: Das Datum der letzten *Werkstattkontrolle*

    **Rückgabewerte**

    - 0: Die *Werkstattkontrolle* ist nicht abgelaufen
    - 1: Die *Werkstattkontrolle* ist abgelaufen

    """
    WERKABLAUF = 365  # Anzahl an Tagen wann ein TÜV abgelaufen ist

    heute = datetime.date.today()
    try:
        if (heute - werkdatum) >= datetime.timedelta(days=WERKABLAUF):
            return 1
        else:
            return None
    except:
        pass


def problemTest(problem_angabe):
    """ *Gibt an ob ein Problem besteht oder nicht*

    **Beschreibung**

    Kontrolliert ob für ein Fahrzeug ein technisches Problem besteht oder
    nicht.

    **Parameter**

    - problem_angabe: *Boolean* Wert ob ein technisches Problem besteht
      oder nicht

    **Rückgabewerte**

    - 0: Es besteht kein technisches Problem
    - 1: Es besteht ein technisches Problem

    """
    try:
        if problem_angabe == 0:
            return None
        else:
            return 1
    except:
        pass
