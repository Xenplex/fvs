# -*- coding: utf-8 -*-

# Ritchie Flick
# T3IFAN 2012/2013 Abschlussprojekt
# LN
# 'Feuerwehrverwaltungssoftware - FVS'
# 'src/Daten_Objekte/db_tabellen_objekt.py'


class DBTabelle():
    """ *Erstellung eines DBTabelle Objektes*

    **Parameter**

    - tabelle: Der Name der Tabelle

    """
    def __init__(self, tabelle):
        self.tabelle = tabelle

    def __str__(self):
        """ *String Representation des DBTabelle Objektes* """
        return "Tabellen Name: %s" % (self.tabelle)
