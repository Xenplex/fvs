# -*- coding: utf-8 -*-

# Ritchie Flick
# T3IFAN 2012/2013 Abschlussprojekt
# LN
# 'Feuerwehrverwaltungssoftware - FVS'
# 'src/Daten_Objekte/auszeichnung_objekt.py'


class Auszeichnung():
    """ *Erstellung eines Auszeichnung Objektes*

    **Parameter**

    - auszeichnung: Die ID/Name der Auszeichnung

    """
    def __init__(self, auszeichnung):
        super(Auszeichnung, self).__init__()
        self.auszeichnung = auszeichnung

    def __str__(self):
        """ *String Representation des Auszeichnung Objektes* """
        return self.auszeichnung
