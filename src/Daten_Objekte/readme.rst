Daten Objekte
==============

**Daten Objekte** enthält Python Module, welche zur Erstellung von
Daten Objekten verwendet werden.

Fahrzeug Objekt
-----------------

.. automodule:: Daten_Objekte.fahrzeug_objekt
   :members:

Inventarliste Objekt
----------------------

.. automodule:: Daten_Objekte.inventarliste_objekt
   :members:

Meldungen Objekt
------------------

.. automodule:: Daten_Objekte.meldungen_objekt
   :members:

Schlauch Objekt
----------------

.. automodule:: Daten_Objekte.schlauche_objekt
   :members:

Mitglied Objekt
-------------------

.. automodule:: Daten_Objekte.mitglied_objekt
   :members:

Ereignis Objekt
-----------------

.. automodule:: Daten_Objekte.ereignis_objekt
   :members:

Protokoll Objekt
------------------

.. automodule:: Daten_Objekte.protokoll_objekt
   :members:

Referenz Objekt
-----------------

.. automodule:: Daten_Objekte.referenz_objekt
   :members:

Lehrgang Objekt
-----------------

.. automodule:: Daten_Objekte.lehrgang_objekt
   :members:

Auszeichnung Objekt
---------------------

.. automodule:: Daten_Objekte.auszeichnung_objekt
   :members:

Führerschein Objekt
--------------------

.. automodule:: Daten_Objekte.fuhrerschein_objekt
   :members:

DB Tabellen Objekt
--------------------

.. automodule:: Daten_Objekte.db_tabellen_objekt
   :members:
