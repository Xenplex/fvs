# -*- coding: utf-8 -*-

# Ritchie Flick
# T3IFAN 2012/2013 Abschlussprojekt
# LN
# 'Feuerwehrverwaltungssoftware - FVS'
# 'src/Daten_Objekte/fuhrerschein_objekt.py'


class Fuhrerschein():
    """ *Erstellung eines Führerschein Objekt*

    **Parameter**

    - schlussel: Die ID/Name des Lehrgangs

    """
    def __init__(self, schlussel):
        super(Fuhrerschein, self).__init__()
        self.schlussel = schlussel

    def __str__(self):
        """ *String Representation des Fuhrerschein Objektes* """
        return self.schlussel
