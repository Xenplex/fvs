# -*- coding: utf-8 -*-

# Ritchie Flick
# T3IFAN 2012/2013 Abschlussprojekt
# LN
# 'Feuerwehrverwaltungssoftware - FVS'
# 'src/Daten_Objekte/meldungen_objekt.py'


class Meldungen():
    """ *Erstellung eines Meldungen Objektes*

    **Parameter**

    - meldung: Die Meldung welche gesetzt werden soll

    """
    def __init__(self, meldung):
        self.meldung = meldung

    def __str__(self):
        """ *String Representation des Meldungen Objektes* """
        return self.meldung
