# -*- coding: utf-8 -*-

# Ritchie Flick
# T3IFAN 2012/2013 Abschlussprojekt
# LN
# 'Feuerwehrverwaltungssoftware - FVS'
# 'src/Daten_Objekte/ereignis_objekt.py'

# Importieren von Python Modulen
import datetime


class Ereignis():
    """ *Erstellung eines Ereignis Objektes*

    **Parameter**

    - schlussel: Die ID des Ereignisses
    - typ: Der Typ des Ereignisses
    - startdatum: Datum an dem das Ereignis anfing
    - startzeit: Uhrzeit an dem das Ereignis anfing
    - enddatum: Datum an dem das Ereignis aufhörte
    - endzeit: Uhrzeut an dem das Ereignis aufhörte
    - bezeichnung: Die Bezeichnung für das Ereignis

    """
    def __init__(self, schlussel, typ=None, startdatum=None, startzeit=None,
                 enddatum=None, endzeit=None, bezeichnung=None):
        self.schlussel = schlussel
        if typ:
            self.typ = typ
        else:
            self.typ = 'Sonstiges'
        try:
            # Das Datum soll aus dem ISO 8601 Format in ein anderes Format
            # dd/mm/yyyy formatiert werden
            if startdatum:
                startdatum = startdatum.strftime("%d-%m-%Y")
                self.startdatum = startdatum
            else:
                startdatum = datetime.date.today()
                startdatum = startdatum.strftime("%d-%m-%Y")
                self.startdatum = startdatum
        except:
            pass

        self.startzeit = startzeit

        try:
            if enddatum:
                enddatum = enddatum.strftime("%d-%m-%Y")
                self.enddatum = enddatum
            else:
                enddatum = datetime.date.today()
                enddatum = enddatum.strftime("%d-%m-%Y")
                self.enddatum = enddatum
        except:
            pass

        self.endzeit = endzeit
        if bezeichnung:
            self.bezeichnung = bezeichnung
        else:
            self.bezeichnung = ''

    def __str__(self):
        """ *String Representation des Ereignis Objektes* """
        return "%s am %s" % (self.bezeichnung, self.startdatum)


class EreignisTyp():
    """ *Erstellung eines EreignisTyp Objektes*

    **Parameter**

    - typ: Der Typ des Ereignisses

    """
    def __init__(self, typ):
        self.typ = typ

    def __str__(self):
        """ *String Representation des EreignisTyp Objektes* """
        return "%s" % (self.typ)
