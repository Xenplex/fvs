# -*- coding: utf-8 -*-

# Ritchie Flick
# T3IFAN 2012/2013 Abschlussprojekt
# LN
# 'Feuerwehrverwaltungssoftware - FVS'
# 'src/Daten_Objekte/fahrzeug_objekt.py'


class Fahrzeug():
    """ *Erstellung eines Fahrzeug Objektes*

    **Parameter**

    - schlussel: Die ID des Fahrzeuges
    - kennzeichen: Das Kennzeichen des Fahrzeuges
    - bezeichnung: Die Bezeichnung des Fahrzeuges
    - besatzung: Die Besatzung des Fahrzeuges
    - technisches_problem: Boolean Wert ob ein technisches Problem vorliegt
      oder nicht
    - kommentar: Kommentar zu einem technischem Problem
    - tuv: Das Datum der letzten TÜV Kontrolle
    - werkstatt: Das Datum der letzten Werkstatt Kontrolle
    - typ: Der Typ des Fahrzeuges

    """
    def __init__(self, schlussel, kennzeichen, bezeichnung, besatzung,
                 technisches_problem, kommentar, tuv, werkstatt, typ):
        # Falls keine Daten vorhanden sind, muss ein anderer Wert als None
        # bzw NULL zugewiesen werden, damit die QMLUI keine Probleme verursacht
        self.schlussel = schlussel
        self.kennzeichen = kennzeichen
        self.bezeichnung = bezeichnung
        if besatzung:
            self.besatzung = besatzung
        else:
            self.besatzung = 1
        self.technisches_problem = technisches_problem
        if kommentar:
            self.kommentar = kommentar
        else:
            self.kommentar = ''
        if tuv:
            # Das Datum soll aus dem ISO 8601 Format in ein anderes Format
            # dd/mm/yyyy formatiert werden
            tuv = tuv.strftime("%d-%m-%Y")
            self.tuv = tuv
        else:
            self.tuv = ''
        if werkstatt:
            werkstatt = werkstatt.strftime("%d-%m-%Y")
            self.werkstatt = werkstatt
        else:
            self.werkstatt = ''
        if typ:
            self.typ = typ
        else:
            self.typ = ''

    def __str__(self):
        """ *String Representation des Fahrzeug Objektes* """
        return "Fahrzeug: %s %s" % (self.kennzeichen, self.bezeichnung)


class FahrzeugTypListe():
    """ *Erstellung eines Fahrzeug Typ Liste Objektes*

    **Parameter**

    - typ: Fahrzeug Typ

    """
    def __init__(self, typ):
        self.typ = typ

    def __str__(self):
        """ *String Representation des Fahrzeug Typ Liste Objektes* """
        return "%s" % (self.typ)
