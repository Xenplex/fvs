# -*- coding: utf-8 -*-

# Ritchie Flick
# T3IFAN 2012/2013 Abschlussprojekt
# LN
# 'Feuerwehrverwaltungssoftware - FVS'
# 'src/Daten_Objekte/referenz_objekt.py'


class Referenz():
    """ *Erstellung eines Referenz Objektes*

    **Parameter**

    - schlussel: Die ID der zu referenzierenden Eingabe
    - bezeichnung: Die Bezeichnung unter der die zu referenzierende Eingabe
      zu erkennen ist
    - referenzTabelle: Gibt an aus welcher Tabelle die Referenz stammt

    """
    def __init__(self, schlussel, bezeichnung, referenzTabelle):
        self.schlussel = schlussel
        self.bezeichnung = bezeichnung
        self.referenzTabelle = referenzTabelle

    def __str__(self):
        """ *Die String Representation des Referenz Objektes* """
        return "Bezeichnung: %s" % self.bezeichnung
