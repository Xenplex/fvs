# -*- coding: utf-8 -*-

# Ritchie Flick
# T3IFAN 2012/2013 Abschlussprojekt
# LN
# 'Feuerwehrverwaltungssoftware - FVS'
# 'src/Daten_Objekte/lehrgang_objekt.py'


class Lehrgang():
    """ *Erstellung eines Lehrgang Objekt*

    **Parameter**

    - schlussel: Die ID/Name des Lehrgangs

    """
    def __init__(self, schlussel):
        super(Lehrgang, self).__init__()
        self.schlussel = schlussel

    def __str__(self):
        """ *String Representation des Lehrgang Objektes* """
        return self.schlussel
