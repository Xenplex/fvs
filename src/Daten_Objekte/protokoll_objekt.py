# -*- coding: utf-8 -*-

# Ritchie Flick
# T3IFAN 2012/2013 Abschlussprojekt
# LN
# 'Feuerwehrverwaltungssoftware - FVS'
# 'src/Daten_Objekte/protokoll_objekt.py'

# Importieren von Python Modulen
import datetime


class Protokoll():
    """ *Erstellung eines Protokoll Objektes*

    **Parameter**

    - schlussel: Die ID des Protokolls
    - bezeichnung: Die Bezeichnung des Protokolls
    - datum: Das Erstellungsdatum des Protokolls
    - inhalt: Der Inhalt des Protokolls

    """
    def __init__(self, schlussel, bezeichnung, datum, inhalt):
        self.schlussel = schlussel
        if bezeichnung:
            self.bezeichnung = bezeichnung
        else:
            self.bezeichnung = ''
        if datum:
            # Das Datum soll aus dem ISO 8601 Format in ein anderes Format
            # dd/mm/yyyy formatiert werden
            datum = datum.strftime("%d-%m-%Y")
            self.datum = datum
        else:
            datum = datetime.date.today()
            datum = datum.strftime("%d-%m-%Y")
            self.datum = datum

        if inhalt:
            self.inhalt = inhalt
        else:
            self.inhalt = ''

    def __str__(self):
        """ *String Representation des Protokoll Objektes* """
        return "Nummer: %s \n %s" % (self.schlussel, self.bezeichnung)
