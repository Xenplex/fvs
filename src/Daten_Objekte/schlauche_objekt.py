# -*- coding: utf-8 -*-

# Ritchie Flick
# T3IFAN 2012/2013 Abschlussprojekt
# LN
# 'Feuerwehrverwaltungssoftware - FVS'
# 'src/Daten_Objekte/schlauche_objekt.py'


class SchlauchTypListe():
    """ *Erstellung einer SchlauchTypListe Objektes*

    **Parameter**

    - typ: Ein Schlauch Typ

    """
    def __init__(self, typ):
        self.typ = typ

    def __str__(self):
        """ *String Representation des Inventarliste Objektes* """
        return "%s" % (self.typ)
