# -*- coding: utf-8 -*-

# Ritchie Flick
# T3IFAN 2012/2013 Abschlussprojekt
# LN
# 'Feuerwehrverwaltungssoftware - FVS'
# '/src/Daten_Objekte/inventarliste_objekt.py'

# Importieren von Python Modulen
import datetime


class Inventarliste():
    """ *Erstellung eines Inventarliste Objektes*

    **Parameter**

    - schlussel: Die ID des Inventargegenstandes
    - bezeichnung: Die Bezeichnung des Inventargegenstandes
    - kategorie: Die Kategorie des Inventargegenstandes
    - knappheit: Ist vom jeweiligen Inventargegenstand noch genügend
      Vorhanden? (Bsp.: in einem Benzinkanister)
    - anzahl: Wie oft kommt ein bestimmter Gegenstand vorkommt
    - groesse: Die Größe des Gegenstandes
    - zustand: Der aktuelle Zustand des Inventargegenstandes
    - kontrolle: Das letzte Kontrolldatum des Inventargegenstandes
    - mitglied: Welchem Mitglied ist der Inventargegenstand zugewiesen
    - kontrolle_anzeigen: Soll das Datum der letzten Kontrolle in der String
      Representation angezeigt werden?

    """
    def __init__(self, schlussel, bezeichnung, kategorie, knappheit=None,
                 anzahl=None, groesse=None, zustand=None, kontrolle=None,
                 mitglied=None, kontrolle_anzeigen=False):
        self.schlussel = schlussel
        if bezeichnung:
            self.bezeichnung = bezeichnung
        else:
            self.bezeichnung = ''

        if kategorie:
            self.kategorie = kategorie
        else:
            self.kategorie = ''
        # Falls keine Anzahl ausgerechnet wurde, sollte dennoch ein Wert
        # existieren
        if anzahl:
            self.anzahl = anzahl
        else:
            self.anzahl = 1

        self.knappheit = knappheit

        if groesse:
            self.groesse = groesse
        else:
            self.groesse = ''
        if zustand:
            self.zustand = zustand
        else:
            self.zustand = ''

        if kontrolle:
            # Das Datum soll aus dem ISO 8601 Format in ein anderes Format
            # dd/mm/yyyy formatiert werden
            kontrolle = kontrolle.strftime("%d-%m-%Y")
            self.kontrolle = kontrolle
        else:
            kontrolle = datetime.date.today()
            kontrolle = kontrolle.strftime("%d-%m-%Y")
            self.kontrolle = kontrolle

        if mitglied:
            self.mitglied = mitglied
        else:
            self.mitglied = 'kein Mitglied'
        self.kontrolle_anzeigen = kontrolle_anzeigen

    def __str__(self):
        """ *String Representation des Inventarliste Objektes* """
        representation = """
Bezeichnung: %s
Kategorie: %s """ % (self.bezeichnung, self.kategorie)

        # Abhängig davon welche weiteren Parametern gesetzt sind und welche
        # nicht wird die String Representation erweitert
        if self.knappheit:
            if self.knappheit == 0:
                representation = representation + "\nKnappheit: Bestand OK"
            else:
                representation = representation + "\nKnappheit: Bestand KNAPP"
        if self.anzahl > 1:
            representation = representation + "\nAnzahl %d" % (
                self.anzahl)
        if self.zustand:
            representation = representation + "\nZustand: %s" % (self.zustand)
        if self.kontrolle_anzeigen:
            representation = representation + "\nKontrolldatum: %s" % (
                self.kontrolle)
        return representation


class Groesse():
    """ *Erstellung eines Größe Objektes*

    **Parameter**

    - groesse: Eine Größen Angabe

    """
    def __init__(self, groesse):
        super(Groesse, self).__init__()
        if groesse:
            self.groesse = groesse
        else:
            self.groesse = ''

    def __str__(self):
        """ *String Representation des Größe Objektes* """
        return "%s" % (self.groesse)


class Zustand():
    """ *Erstellung eines Zustand Objektes*

    **Parameter**

    - zustand: Eine Zustand Angabe

    """
    def __init__(self, zustand):
        super(Zustand, self).__init__()
        if zustand:
            self.zustand = zustand
        else:
            self.zustand = ''

    def __str__(self):
        """ *String Representation des Zustand Objektes* """
        return "%s" % (self.zustand)


class Kategorie():
    """ *Erstellung eines Kategorie Objektes*

    **Parameter**

    - kategorie: Name einer Kategorie

    """
    def __init__(self, kategorie):
        super(Kategorie, self).__init__()
        if kategorie:
            self.kategorie = kategorie
        else:
            self.kategorie = ''

    def __str__(self):
        """ *String Representation des Kategorie Objektes* """
        return "%s" % (self.kategorie)


class Kleidung():
    """ *Erstellung eines Kleidung Objektes*

    **Parameter**

    - art: Art der Kleidung (Jacke, Uniform,...)

    """
    def __init__(self, art):
        super(Kleidung, self).__init__()
        if art:
            self.art = art
        else:
            self.art = ''

    def __str__(self):
        """ *String Representation des Kleidung Objektes* """
        return "%s" % (self.art)
