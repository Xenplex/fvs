# -*- coding: utf-8 -*-

# Ritchie Flick
# T3IFAN 2012/2013 Abschlussprojekt
# LN
# 'Feuerwehrverwaltungssoftware - FVS'
# 'src/Daten_Objekte/mitglied.py'


class Mitglied():
    """ *Erstellung eines Mitglied Objektes*

    **Parameter**

    - schlussel: Stammlistennummer des Mitglieds
    - name: Der Familienname des Mitglieds
    - vorname: Der Vorname des Mitglieds
    - sozialnummer: Die Sozialversicherungsnummer des Mitglieds
    - bday: Der Geburtsdag des Mitglieds
    - mobiltelefon: Die Handy Nummer des Mitglieds
    - ermail: Die Email Adresse des Mitglieds
    - telefonnummer: Die Festnetz des Mitglieds
    - ortschaft: Die Ortschaft in der das Mitglied lebt
    - adresse: Die Adresse an der das Mitglied lebt
    - postleitzahl: Die Postleitzahl des Mitglieds
    - lehrgangsstunden: Die restlichen Lehrgangsstunden welche das Mitglied
      noch für weitere Lehrgänge zur Verfügung hat
    - mediKontrolle: Das Datum der letzten medizinischen Kontrolle
    - apteinsatz: Ja/Nein Angabe ob das Mitglied die medizinische Erlaubnis hat
      an Einsätzen teilzunehmen oder nicht
    - aptatemschutz: Ja/Nein Angabe ob das Mitglied die medizinische Erlaubnis
      hat den schweren Atemschutz zu tragen oder nicht

    """
    def __init__(self, schlussel, name, vorname, sozialnummer=None, bday=None,
                 mobiltelefon=None, email=None, telefonnummer=None,
                 ortschaft=None, adresse=None, postleitzahl=None,
                 lehrgangsstunden=None, mediKontrolle=None, apteinsatz=None,
                 aptatemschutz=None):
        # Falls keine Daten vorhanden sind, muss ein anderer Wert als None
        # bzw NULL zugewiesen werden, damit die QMLUI keine Probleme verursacht
        self.schlussel = schlussel
        self.name = name
        self.vorname = vorname
        self.sozialnummer = sozialnummer
        if bday:
            # Das Datum soll aus dem ISO 8601 Format in ein anderes Format
            # dd/mm/yyyy formatiert werden
            bday = bday.strftime("%d-%m-%Y")
            self.bday = bday
        else:
            self.bday = ''
        if mobiltelefon:
            self.mobiltelefon = mobiltelefon
        else:
            self.mobiltelefon = ''
        if email:
            self.email = email
        else:
            self.email = ''
        if telefonnummer:
            self.telefonnummer = telefonnummer
        else:
            self.telefonnummer = ''
        if ortschaft:
            self.ortschaft = ortschaft
        else:
            self.ortschaft = ''
        if adresse:
            self.adresse = adresse
        else:
            self.adresse = ''
        if postleitzahl:
            self.postleitzahl = postleitzahl
        else:
            self.postleitzahl = ''
        if lehrgangsstunden:
            self.lehrgangsstunden = lehrgangsstunden
        else:
            self.lehrgangsstunden = ''
        if mediKontrolle:
            mediKontrolle = mediKontrolle.strftime("%d-%m-%Y")
            self.mediKontrolle = mediKontrolle
        else:
            self.mediKontrolle = ''
        if apteinsatz:
            self.apteinsatz = apteinsatz
        else:
            # Ein Mitglied ist solange nicht APT, solange er sich nicht einer
            # medizinischen Kontrolle unterzogen hat
            self.apteinsatz = 0
        if aptatemschutz:
            self.aptatemschutz = aptatemschutz
        else:
            # Ein Mitglied ist solange nicht APT, solange er sich nicht einer
            # medizinischen Kontrolle unterzogen hat
            self.aptatemschutz = 0

    def __str__(self):
        """ *String Representation des Mitglied Objektes* """
        return "%s %s" % (self.name, self.vorname)
