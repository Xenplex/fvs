/*
    Ritchie Flick
    T3IFAN 2012/2013 Abschlussprojekt
    LN
    'Feuerwehrverwaltungssoftware - FVS'
    './QMLUI/Komponenten/Memo.qml'

    Implementation eines 'Memo' Feldes
*/

import QtQuick 1.1


Rectangle {
    id: umrandung

    // Aliase werden definiert auf welche zugegriffen werden kann wenn
    // eine neue Auswahl Liste erstellt wird
    property alias memoID: memo.objectName
    property alias breite: umrandung.width
    property alias hoehe: umrandung.height
    property alias text: memo.text

    // Eigenschaften der einzelnen Elemente werden festgelegt
    property int randBreite: 1
    property int randRadius: 1
    property int textGroesse: 13
    property color farbe: "silver"
    property color randFarbe: "black"
    property bool gesperrt: false

    // Memo Attribute
    border.width: randBreite
    border.color: randFarbe
    color: farbe
    radius: randRadius

    Flickable {
        id: flickArea
        anchors.fill: parent
        contentWidth: parent.width
        contentHeight: parent.height
        clip: true

        // Funktion kopiert von:
        // http://doc-snapshot.qt-project.org/4.8/qml-textedit.html
        function ensureVisible(r) {
            if (contentX >= r.x)
                contentX = r.x
            else if (contentX + width <= r.x + r.width)
                contentX = r.x + r.width - width
            if (contentY >= r.y)
                contentY = r.y
            else if (contentY + height <= r.y + r.height)
                contentY = r.y + r.height - height
        }

        TextEdit {
            id: memo
            wrapMode: TextEdit.Wrap
            anchors.fill: parent
            anchors.leftMargin: 5

            onCursorRectangleChanged: flickArea.ensureVisible(cursorRectangle)
        }
    }
}
