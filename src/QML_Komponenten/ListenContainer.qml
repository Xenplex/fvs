/*
    Ritchie Flick
    T3IFAN 2012/2013 Abschlussprojekt
    LN
    'Feuerwehrverwaltungssoftware - FVS'
    './QMLUI/Komponenten/ListenContainer.qml'

    --- Beschreibung ---
    Der ListenContainer wirdt verwendet damit man mit der ListenAnsicht auch
    sehen kann welchen ganzen Bereich die Liste ausfüllen kann. Komplett
    Design spezifisch.
*/

import QtQuick 1.1


Rectangle {
    id: listenContainer

    // Aliase werden definiert auf welche zugefriffen werden kann wenn
    // ein neuer Container erstellt wird
    property alias listeID: liste.objectName
    property alias daten_modell: liste.model

    // Eigenschaften der einzelnen Elemente werden festgelegt
    property int breite: 200
    property int hoehe: 20
    property int eintragHoehe: 30
    property color farbe: "grey"

    // ListenContainer Attribute
    width: breite
    height: hoehe
    color: farbe

    // Liste welche der Container umschließen soll
    ListenAnsicht {
        id: liste
        anchors.top: parent.top
        width: parent.width
        height: parent.height
        eintrag_hohe: eintragHoehe
    }
}
