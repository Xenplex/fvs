/*
    Ritchie Flick
    T3IFAN 2012/2013 Abschlussprojekt
    LN
    'Feuerwehrverwaltungssoftware - FVS'
    './QMLUI/Komponenten/Button.qml'

    Implementation eines 'Buttons'
*/

import QtQuick 1.1


Item {
    id: button
    // Aliase werden definiert auf welche zugegriffen werden kann wenn
    // ein neuer Button erstellt wird
    property alias buttonID: button.objectName
    property alias buttonFont: buttonText.font
    property alias buttonText: buttonText.text

    // Eigenschaften der einzelnen Elemente werden festgelegt
    property color randFarbe: "black"
    property color farbverlauf1: "white"
    property color farbverlauf2: "silver"
    property color farbverlauf3: "grey"

    // Button Attribute
    width: 130
    height: 55

    Rectangle {
        id: buttonForm
        anchors.fill: parent
        border.color: randFarbe
        // Farbverlauf
        gradient: Gradient {

            GradientStop {
                id: gradient1
                color: farbverlauf1
                position: 0.0
            }

            GradientStop {
                id: gradient2
                color: farbverlauf2
                position: 0.57
            }

            GradientStop {
                id: gradient3
                color: farbverlauf3
                position: 0.9
            }
        }

        Text {
            id: buttonText
            anchors.centerIn: parent
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                button_kontroller.button_gedruckt(buttonID)
            }
        }
    }
}
