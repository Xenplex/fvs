/*
    Ritchie Flick
    T3IFAN 2012/2013 Abschlussprojekt
    LN
    'Feuerwehrverwaltungssoftware - FVS'
    './QMLUI/Komponenten/Overlay.qml'

    --- Beschreibung ---
    Implementiert ein Overlay Element mit welchem eine Liste ins Fenster
    'rutscht' und wieder zurück. Es kann angegeben werden ob es sich um Overlay
    handelt was von 'oben nach unten' verläuft oder von 'rechts nach links'.
*/

import QtQuick 1.1


Rectangle {
    id: overlay

    // Kontrolliert welches Attribut des Overlay Elementes bei der Animation
    // geändert werden soll
    function richtung(gewunschte_richtung) {
        if (gewunschte_richtung === "height")
            return overlay.height = hoehe
        else
            return overlay.width = breite
    }

    // Aliase werden definiert auf welche zugefriffen werden kann wenn
    // ein neues Overlay erstellt wird
    property alias listeID: liste.objectName
    property alias daten_modell: liste.model
    property alias text: fenster_text.font

    // Eigenschaften der einzelnen Elemente werden festgelegt
    property int breite: 200
    property int hoehe: 20
    property int eintragHoehe: 30
    property int randBreite: 1
    property int randRadius: 1
    property string gewunschte_richtung: "height"
    property color farbe: "silver"
    property color randFarbe: "black"

    // Startstate
    state: "standard"

    // Overlay Attribute
    width: breite
    height: hoehe
    radius: randBreite
    color: farbe
    border.width: randBreite
    border.color: randFarbe

    // Enthält die Elemente welche das Overlay Fenster beinhalten soll
    ListenAnsicht {
        id: liste
        width: parent.width
        height: parent.height - 50
        anchors.top: parent.top
        visible: false
        eintrag_hohe: eintragHoehe
    }

    Text {
        id: fenster_text
        anchors.bottom: parent.bottom

        MouseArea {
            id: maus_feld
            anchors.fill: parent
            onClicked: {
                if (overlay.state == "standard")
                    overlay.state = "anzeigen"
                else
                    overlay.state = "standard"
            }
        }
    }

    // Verschiedene States welche angeben wie das Fenster sich verhalten soll
    states: [
        State {
            name: "standard"

            PropertyChanges {
                target: fenster_text
                anchors.centerIn: parent
                text: "Meldungliste öffnen"
            }

            PropertyChanges {
                target: liste
                visible: false
            }
        },

        State {
            name: "anzeigen"

            PropertyChanges {
                target: fenster_text
                text: "Meldungsliste schließen"
            }

            PropertyChanges {
                target: liste
                visible: true
            }
        }
    ]

    // Animationen welche sich beim Ändern der States aufgerufen werden
    transitions: [
        Transition {
            from: "standard"
            to: "anzeigen"

            SequentialAnimation {

                NumberAnimation {
                    target: overlay
                    property: gewunschte_richtung
                    duration: 200
                    to: (richtung(gewunschte_richtung) + 100) * 3
                }
            }
        },

        Transition {
            from: "anzeigen"
            to: "standard"

            SequentialAnimation {

                NumberAnimation {
                    target: overlay
                    property: gewunschte_richtung
                    duration: 200
                    to: richtung(gewunschte_richtung)
                }
            }
        }
    ]
}
