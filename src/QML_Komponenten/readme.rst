QML Komponenten
================

Verschiedene spezifische *QML Komponenten* welche von der **QMLUI** verwendet
werden um die *GUI* aufzubauen.

Die *QML Komponenten* mussten manuell designt/erstellt werden, da die verwendete
Version von *Qt* und *QML* (siehe :ref:`qtqml` ) keine Standard Komponenten 
mitlieferte.

Die zuständigen *Kontroller* werden außerdem in den einzelnen Komponenten definiert.

AuswahlListe
-------------

Die **AuswahlListe.qml** definiert ein *QML Komponent* welches eine Standard
*ComboBox* darstellen soll.

Button
-------

Ein Standard *Button* für die **QMLUI**.

InfoText
---------

Ein Standard *Label* für die **QMLUI**.

JaNeinAuswahl
--------------

Eine Standard *CheckBox* für die **QMLUI**.

ListenAnsicht
--------------

Eine *ListView* für die **QMLUI**.

ListenContainer
----------------

Eine Standard *ListBox* für die **QMLUI**.

Memo
-----

Ein Standard *Memo* Feld für die **QMLUI**.

Overlay
---------

Eine Komponente speziell für **FVS** entwickelt. Es stellt ein *Dropdown* Menü dar.
Innerhalb ist ein **ListenContainer** integriert, welche die einzelnen Meldungen
auflistet.
Das **Overlay** gleitet mit einer einfachen Animation ins Programmfenster hinein.

TextEingabe
------------

Eine Standard *EditBox* für die **QMLUI**.