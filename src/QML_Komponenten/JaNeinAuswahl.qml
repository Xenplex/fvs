/*
    Ritchie Flick
    T3IFAN 2012/2013 Abschlussprojekt
    LN
    'Feuerwehrverwaltungssoftware - FVS'
    './QMLUI/Komponenten/JaNeinAuswahl.qml'

    Implementation einer 'CheckBox'
*/

import QtQuick 1.1


Rectangle {
    id: auswahl

    // Startstate
    state: "unchecked"

    // Aliase werden definiert auf welche zugegriffen werden kann wenn
    // eine neue Auswahl erstellt wird
    property alias auswahlID: auswahl.objectName

    // Eigenschaften der einzelnen Elemente werden festgelegt
    property int breite: 20
    property int hoehe: 20
    property int randBreite: 1
    property int randRadius: 1
    property color farbe: "white"
    property color checkedFarbe: "silver"
    property color randFarbe: "black"
    property bool checkAuswahl: false

    // JaNeinAuswhal Attribute
    border.width: randBreite
    border.color: randFarbe
    radius: randRadius
    width: breite
    height: hoehe

    MouseArea {
        id: feld
        anchors.fill: parent
        onClicked: {
            if (auswahl.state === "unchecked") {
                checkAuswahl = true
                auswahl.state = "checked"
            }
            else {
                checkAuswahl = false
                auswahl.state = "unchecked"
            }
            jaNeinAuswahl_kontroller.status_geaendert(auswahlID, auswahl.state)
        }
    }

    states: [
        State {
            name: "unchecked"

            PropertyChanges {
                target: auswahl
                color: farbe
            }
        },

        State {
            name: "checked"
            
            PropertyChanges {
                target: auswahl
                color: checkedFarbe
            }
        }

    ]
}
