/*
    Ritchie Flick
    T3IFAN 2012/2013 Abschlussprojekt
    LN
    'Feuerwehrverwaltungssoftware - FVS'
    './QMLUI/Komponenten/ListenAnsicht.qml'

    Implementation einer 'List'
*/

import QtQuick 1.1


ListView {
    id: liste

    // Aliase werden definiert auf welche zugegriffen werden kann wenn
    // eine neue Listen Ansicht erstellt wird
    property alias daten_modell: liste.model
    property alias listeID: liste.objectName
    property alias breite: liste.width
    property alias hoehe: liste.height

    // Eigenschaften der einzelnen Elemente werden festgelegt
    property int eintrag_hohe
    property color eintrag_farbe1: "white"
    property color eintrag_farbe2: "silver"
    property color text_farbe: "black"

    property QtObject eintrag_bevor
    property int index_bevor

    clip: true

    delegate: Component {
        Rectangle {
            id: eintrag
            width: liste.width
            height: eintrag_hohe
            // Jeder zweite Eintrag hat eine andere Farbe
            color: ((index % 2 == 0) ? eintrag_farbe1 : eintrag_farbe2)

            Text {
                id: inhalt
                elide: Text.ElideRight
                // Abhängig vom übergebenem Daten Modell
                text: model.eintrag.verpackung
                color: text_farbe
                anchors.leftMargin: 10
                anchors.fill: parent
                verticalAlignment: Text.AlignVCenter
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    listen_kontroller.eintrag_ausgewaehlt(
                        model.eintrag, listeID)
                    // Hervorheben des momentan ausgewählten Eintrages
                    eintrag.color = "lightblue"
                    if (eintrag_bevor)
                        eintrag_bevor.color = (
                                    (index_bevor % 2 == 0) ?
                                        eintrag_farbe1 : eintrag_farbe2)
                    eintrag_bevor = eintrag
                    index_bevor = index
                }
            }
        }
    }
}
