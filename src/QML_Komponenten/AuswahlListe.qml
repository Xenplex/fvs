/*
    Ritchie Flick
    T3IFAN 2012/2013 Abschlussprojekt
    LN
    'Feuerwehrverwaltungssoftware - FVS'
    './QMLUI/Komponenten/AuswahlListe.qml'

    Implementation einer 'ComboBox'
*/

import QtQuick 1.1


Rectangle {
    id: combobox

    // Aliase werden definiert auf welche zugegriffen werden kann wenn
    // eine neue Auswahl Liste erstellt wird
    property alias comboID: liste.objectName
    property alias daten_modell: liste.model

    // Eigenschaften der einzelnen Elemente werden festgelegt
    property int comboboxBreite: 150
    property int comboboxHoehe: 20
    property int eintrag_hohe: 30
    property color eintrag_text_farbe: "black"
    property color eintrag_farbe1: "white"
    property color eintrag_farbe2: "grey"
    property color comboboxFarbe: "silver"
    property string auswahlText  // String des momentan ausgewählten Eintrages
    property bool schreibbar: true

    // Startstate
    state: "ausgewaehlt"

    // ComboBox Attribute
    width: comboboxBreite
    height: comboboxHoehe
    color: comboboxFarbe
    clip: true

    Text {
        id: ausgewaehltText
        anchors.fill: parent
        // Text wird in den State Definitionen festgelegt

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if (schreibbar === true ) {
                    combobox.state = "liste"
                }
            }
        }
    }

    ListView {
        id: liste
        height: 300
        width: parent.width

        delegate: Component {
            Rectangle {
                id: eintrag
                width: liste.width
                height: eintrag_hohe
                // Jeder zweite Eintrag hat eine andere Farbe
                color: ((index % 2 == 0) ? eintrag_farbe1 : eintrag_farbe2)

                Text {
                    id: inhalt
                    elide: Text.ElideRight
                    // Abhängig vom übergebenem Daten Modell
                    text: model.eintrag.verpackung
                    color: eintrag_text_farbe
                    anchors.leftMargin: 10
                    anchors.fill: parent
                    verticalAlignment: Text.AlignVCenter
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        combobox.state = "ausgewaehlt"
                        auswahlText = model.eintrag.verpackung
                        listen_kontroller.eintrag_ausgewaehlt(
                            model.eintrag, comboID)
                    }
                }
            }
        }
    }

    states: [
        State {
            name: "ausgewaehlt"

            PropertyChanges {
                target: liste
                visible: false
            }

            PropertyChanges {
                target: combobox
                height: comboboxHoehe
            }

            PropertyChanges {
                target: ausgewaehltText
                text: auswahlText
            }
        },
        
        State {
            name: "liste"

            PropertyChanges {
                target: liste
                visible: true
            }

            PropertyChanges {
                target: combobox
                height: comboboxHoehe + 250
            }
        }
    ]
}
