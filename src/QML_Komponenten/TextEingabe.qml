/*
    Ritchie Flick
    T3IFAN 2012/2013 Abschlussprojekt
    LN
    'Feuerwehrverwaltungssoftware - FVS'
    './QMLUI/Komponenten/TextEingabe.qml'

    Implementation eines 'Edit' Feldes
*/

import QtQuick 1.1


Rectangle {
    id: umrandung

    // Aliase werden definiert auf welche zugegriffen werden kann wenn
    // eine neue TextEingabe erstellt wird
    property alias inputID: input.objectName
    property alias text: input.text
    property alias blockiert: input.readOnly
    property alias fokus: input.focus
    property alias eingabeHilfe: input.inputMask

    // Eigenschaften der einzelnen Elemente werden festgelegt
    property int breite: 150
    property int hoehe: 20
    property int randBreite: 1
    property int randRadius: 1
    property int textGroesse: 13
    property color farbe: "silver"
    property color randFarbe: "black"
    property string passwordFeld: "Normal"
    property bool gesperrt: false

    // TextEingabe Attribute
    width: breite
    height: hoehe
    radius: randRadius
    border.width: randBreite
    border.color: randFarbe
    color: farbe

    TextInput {
        id: input
        anchors.centerIn: parent
        width: parent.width - 15
        font.pixelSize: textGroesse
        echoMode: passwordFeld
    }
}
