/*
    Ritchie Flick
    T3IFAN 2012/2013 Abschlussprojekt
    LN
    'Feuerwehrverwaltungssoftware - FVS'
    './QMLUI/Komponenten/InfoText.qml'

    Implementation eines 'Labels'
*/

import QtQuick 1.1


Text {
    id: infoText

    // Aliase werden definiert auf welche zugegriffen werden kann wenn
    // ein neuer Label erstellt wird
    property alias textID: infoText.objectName
    // text muss nicht definiert werden, da in beiden Sprachen (en, de)
    // text gleich geschrieben wird

    // Eigenschaften der einzelnen Elemente werden festgelegt
    property string schriftArt: "Helvetica"
    property int schriftGroesse: 15
    property color schriftFarbe: "grey"

    // InfoText Attribute
    font.family: schriftArt
    font.pixelSize: schriftGroesse
    color: schriftFarbe
}
