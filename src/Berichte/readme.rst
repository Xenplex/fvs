Berichte
=========

Erstellung von Berichten
-------------------------

In diesem Verzeichnis werden die erstellten HTML Berichte von **FVS** gespeichert. Die Berichte werden wie folgt abgespeichert:
**bericht<benutzer_name><datum_erstellung>**.
Dies gewährleistet, dass die einzelnen Berichte der verschiedenen Benutzer voneinander unterschieden werden können.

Die Berichte sind als *HTML5 & CSS3* [#]_ Dateien erstellt worden und sind unter allen Browser lauffähig, welche sich an die Vorgaben des w3c [#]_ halten.

fvs.css
---------

Hierbei handelt es sich um die *CSS* Datei, welche die Berichte verwenden. Es ist eine Standard *Vorlage* erstellt worden, welche jedoch von den Benutzern ganz nach ihren Bedürfnissen angepasst werden kann.


.. rubric:: Footnotes

.. [#] http://www.w3schools.com/html/html5_intro.asp
.. [#] http://www.w3.org/