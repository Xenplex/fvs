src/
================
Im *Source* Verzeichnis (*src/*) von **FVS** stehen die Hauptdateien
welche zur Ausführung von **FVS** benötigt werden und Verzeichnisse (für
*Python* *Packete*) zu den einzelnen Modulen für **FVS**.

fvs.py
-------
.. automodule:: fvs
   :members:

setup.py
---------

**setup.py** ist eine Konfigurationsdatei für cx-freeze durch welche die
benötigten Einstellungen und Meta-Daten zur Kompilation von **FVS**
konfiguriert werden.

Weitere Informationen
----------------------
Die einzelnen Klassen/Methoden zum Aufbau der **QMLUI** befinden sich in
*src/UI_Aufbau*, die Dateien zur Zusammenstellung der grafischen Oberfläche
befinden sich in *src/QMLUI* und die einzelnen Komponenten der **QMLUI**
befinden sich in *src/QML_Komponenten*.