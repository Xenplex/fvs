# -*- coding: utf-8 -*-

# Ritchie Flick
# T3IFAN 2012/2013 Abschlussprojekt
# LN
# 'Feuerwehrverwaltungssoftware - FVS'
# 'src/Datenbank/db_verbindung.py'

# Importieren von Python Modulen
import pymysql


class DB_Verbindung():
    """ *Datenbankverbindung*

    **Beschreibung**

    Die Klasse **DB_Verbindung** baut eine Verbindung zur Datenbank auf,
    dessen Verbindungs Parameter bei der Initialisation übergeben wurden.
    Instanzvariablen für die *Verbindung* und für den *Cursor* werden
    generiert.

    Alle weiteren Grundfunktionen, welche für die Arbeit mit einer Datenbank
    benötigt werden, sind als Methoden implementiert.

    **Parameter**

    - db_parameter: Enthält Liste mit den Verbindungsparametern welche
      benötigt werden um eine Verbindung zur Datenbank aufzubauen:

      - db_parameter[0]: IP Adresse des Servers
      - db_parameter[1]: Port Nummer auf die der Server hört
      - db_parameter[2]: Der Name der zu verwendeten Datenbank
      - db_parameter[3]: Der Benutzername der Datenbank
      - db_parameter[4]: Das Passwort des Benutzers

    """
    def __init__(self, db_parameter):
        super(DB_Verbindung, self).__init__()

        # Verbindungsaufbau
        try:
            self.verbindung = pymysql.connect(host=db_parameter[0],
                                              port=int(db_parameter[1]),
                                              db=db_parameter[2],
                                              user=db_parameter[3],
                                              passwd=db_parameter[4])
            self.cursor = self.verbindung.cursor()
        except pymysql.Error:
            return "err"

    def commit(self):
        """ *Ausstehende Transaktionen werden in die DB geschrieben* """
        self.verbindung.commit()

    def rollback(self):
        """ *Ausstehende Transaktionen werden verworfen* """
        self.verbindung.rollback()

    def execute(self, sql_anweisung, commit=False):
        """ *Ausführung einer SQL Anweisung*

        **Beschreibung**

        Die übergebene SQL Anweisung wird ausgeführt und falls der nötige
        Parameter gesetzt wurde, sofort *committed*.

        **Parameter**

        - sql_anweisung: Die auszuführende SQL Anweisung als String
        - commit: Soll die Anweisung sofort übernommen werden?

        **Rückgabewerte**

        - resultate: Enthält gegebenfalls die Resultate welche die SQL
          Anweisung zurückgibt

        """
        self.cursor.execute(sql_anweisung)
        resultate = self.cursor.fetchall()
        if resultate:
            return resultate
        if commit:
            self.commit()

    def letzter_erstellter_schlussel(self):
        """ *Gibt den letzten erstellten Schlüssel der DB zurück*

        **Beschreibung**

        Methode welche es erlaubt den letzen erstellen Schlüssel durch
        *AUTO INCREMENT* aus der Datenbank zu erhalten.

        **Rückgabewerte**

        -schlussel: Letzter erstellter *AUTO INCREMENT* Schlüssel der Datenbank

        """
        schlussel = self.cursor.lastrowid
        return schlussel
