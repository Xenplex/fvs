Datenbank
==========
Alle benötigten Dateien für die *MySQL* Datenbank befinden sich in diesem
Verzeichnis.

- BeispielDaten.sql: *SQL* Skript, welches bei Bedarf (zu
  Entwicklungszwecken oder zu Testzwecken) die Datenbank mit Beispieldaten
  füllt.
- FVSDatenbank.sql: *SQL* Skript welches die ganze Datenbank mit allen Tabellen und
  wichtigen Standarddaten.

Datenbank Verbindung
----------------------

.. automodule:: Datenbank.db_verbindung
   :members:

Admin Datenbank Verbindung
---------------------------

.. automodule:: Datenbank.admin_db_verbindung
   :members:
   :show-inheritance:

SQL Anweisungen
----------------

.. automodule:: Datenbank.sql_anweisungen
   :members:
