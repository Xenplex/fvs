USE `fvs_datenbank`;

SET foreign_key_checks=0;

/* 
Ritchie Flick
T3IFAN 2012/2013 Abschlussprojekt
LN
'Feuerwehrverwaltungssoftware - FVS'
'src/Datenbank/BeispielDaten.sql'

**Beschreibung**

Folgende Routinen werden aufgerufen/verwendet wenn der Administrator von FVS,
Beispieldaten für die Datenbank benötigt, um die Funktionalität von FVS zu
testen oder zu Entwicklungszwecken. Jede Tabelle der Datenbank wird mit
Beispielwerten gefüllt und Verbindungen zwischen den einzelnen Tabellen
werden hergestellt.

#### BEMERKUNG ####

Anfänglich war diese Datei mit SQL Routinen aufgebaut, um so die Übersich und
auch dem Entwickler die Möglichkeit zu geben einzelne Daten zu erstellen jedoch
musste die Datei durch technische Einschränkungen mit der Schulserver und
phymyadmin das SQL Skript umgeschrieben werden.

Begründung: *DELIMITER* welches benötigt wurde, konnte nicht vom Schulserver
beziehungsweise von phpmyadmin erkannt werden.

#############################################################################*/


/*
###############################################################################
# Mitglieder Verwaltung
###############################################################################
*/ 

TRUNCATE TABLE tblMitglied;
INSERT INTO tblMitglied (idStammlistennummer, dtName, dtVorname, dtSozialnummer, dtGeburtsdatum, dtMobiltelefon, dtEmail, dtTelefonnummer, dtOrtschaft, dtAdresse, dtPostleitzahl, dtLehrgangsstunden, dtAPTEinsätze, dtAPTAtemschutz, dtMedizinischeKontrolle)
VALUES (1, 'Flick', 'Ritchie', 342, '1990-12-19', 621423454, 'xenplex@gmail.com', 748392, 'Musterhausen', 'Straßenseite 13', 1337, 100, TRUE, TRUE, '2013-12-19');
INSERT INTO tblMitglied (idStammlistennummer, dtName, dtVorname, dtSozialnummer, dtGeburtsdatum, dtMobiltelefon, dtEmail, dtTelefonnummer, dtOrtschaft, dtAdresse, dtPostleitzahl, dtLehrgangsstunden, dtAPTEinsätze, dtAPTAtemschutz, dtMedizinischeKontrolle)
VALUES (2, 'Mustermann', 'Max', 534, '1995-09-22', 621345678, 'maxmustermann@yahoo.com', 743829, 'Musterhausen', 'Blaustraße 09', 1337, 90, TRUE, FALSE, '2012-09-13');
INSERT INTO tblMitglied (idStammlistennummer, dtName, dtVorname, dtSozialnummer, dtGeburtsdatum, dtMobiltelefon, dtEmail, dtTelefonnummer, dtOrtschaft, dtAdresse, dtPostleitzahl, dtLehrgangsstunden, dtAPTEinsätze, dtAPTAtemschutz, dtMedizinischeKontrolle)
VALUES (3, 'Aaron', 'Caroline', 432, '1987-07-30', 621783465, 'caroline.aaron@hotmail.com', 748213, 'Musterhausen', 'Bambusallee', 1337, 100, TRUE, TRUE, '2003-01-01');
INSERT INTO tblMitglied (idStammlistennummer, dtName, dtVorname, dtSozialnummer, dtGeburtsdatum, dtMobiltelefon, dtEmail, dtTelefonnummer, dtOrtschaft, dtLehrgangsstunden, dtAPTEinsätze, dtAPTAtemschutz, dtMedizinischeKontrolle)
VALUES (4, 'Bond', 'James', 007, '1953-03-07', 621007007, 'james.bond007@mi6.com', 007007, 'Musterhausen', 07, TRUE, TRUE, '2013-04-12');
INSERT INTO tblMitglied (idStammlistennummer, dtName, dtVorname, dtSozialnummer, dtGeburtsdatum, dtMobiltelefon, dtEmail, dtOrtschaft, dtAdresse, dtPostleitzahl, dtLehrgangsstunden, dtAPTEinsätze, dtAPTAtemschutz, dtMedizinischeKontrolle)
VALUES (5, 'Potter', 'Harry', 912, '1997-06-29', 621938175, 'harry.potter@hogwarts.co.uk', 'London', 'Winkelgasse', 9485, 75, TRUE, FALSE, '2012-12-12');
INSERT INTO tblMitglied (idStammlistennummer, dtName, dtVorname, dtSozialnummer, dtGeburtsdatum, dtMobiltelefon, dtEmail, dtOrtschaft, dtAdresse, dtPostleitzahl, dtLehrgangsstunden, dtAPTEinsätze, dtAPTAtemschutz, dtMedizinischeKontrolle)
VALUES (6, 'Deschain', 'Roland', 019, '1990-09-19', 621019019, 'roland.deschain@katet.com', 'Musterhausen', 'Rote Rose', 1919, 19, TRUE, TRUE, '1990-12-12');
INSERT INTO tblMitglied (idStammlistennummer, dtName, dtVorname, dtSozialnummer, dtGeburtsdatum, dtEmail, dtOrtschaft, dtAdresse, dtPostleitzahl, dtLehrgangsstunden, dtAPTEinsätze, dtAPTAtemschutz)
VALUES (7, 'Baggins', 'Frodo', 382, '1968-11-11', 'frodo.baggins@mittelerde.com', 'Musterhausen', 'Beutelsend', 9547, 100, TRUE, FALSE);

TRUNCATE TABLE tblEinAustritt;
INSERT INTO tblEinAustritt (dtEintrittDatum, fiStammlistennummerEinAustritt)
VALUES ('2003-12-19',
        1);
INSERT INTO tblEinAustritt (dtEintrittDatum, fiStammlistennummerEinAustritt)
VALUES ('1993-09-23',
        2);
INSERT INTO tblEinAustritt (dtEintrittDatum, fiStammlistennummerEinAustritt)
VALUES ('1997-03-09',
        3);
INSERT INTO tblEinAustritt (dtEintrittDatum, fiStammlistennummerEinAustritt)
VALUES ('1969-07-07',
        4);
INSERT INTO tblEinAustritt (dtEintrittDatum, fiStammlistennummerEinAustritt)
VALUES ('2007-07-30',
        5);
INSERT INTO tblEinAustritt (dtEintrittDatum, dtAustrittDatum, fiStammlistennummerEinAustritt)
VALUES ('2003-03-19',
        '2009-03-19',
        6);
INSERT INTO tblEinAustritt (dtEintrittDatum, fiStammlistennummerEinAustritt)
VALUES ('1980-07-25',
        7);

TRUNCATE TABLE tblMitgliedsAuszeichnungen;
INSERT INTO tblMitgliedsAuszeichnungen (fiAuszeichnung, fiStammlistennummerAuszeichnung, dtDatumAuszeichnung)
VALUES ('10JahreDienst',
        1,
        '2013-12-19');
INSERT INTO tblMitgliedsAuszeichnungen (fiAuszeichnung, fiStammlistennummerAuszeichnung, dtDatumAuszeichnung)
VALUES ('10JahreDienst',
        2,
        '2003-09-29');
INSERT INTO tblMitgliedsAuszeichnungen (fiAuszeichnung, fiStammlistennummerAuszeichnung, dtDatumAuszeichnung)
VALUES ('20JahreDienst',
        2,
        '2013-09-29');
INSERT INTO tblMitgliedsAuszeichnungen (fiAuszeichnung, fiStammlistennummerAuszeichnung, dtDatumAuszeichnung)
VALUES ('10JahreDienst',
        3,
        '1995-03-09');
INSERT INTO tblMitgliedsAuszeichnungen (fiAuszeichnung, fiStammlistennummerAuszeichnung, dtDatumAuszeichnung)
VALUES ('10JahreDienst',
        4,
        '1979-07-07');
INSERT INTO tblMitgliedsAuszeichnungen (fiAuszeichnung, fiStammlistennummerAuszeichnung, dtDatumAuszeichnung)
VALUES ('20JahreDienst',
        4,
        '1989-07-07');
INSERT INTO tblMitgliedsAuszeichnungen (fiAuszeichnung, fiStammlistennummerAuszeichnung, dtDatumAuszeichnung)
VALUES ('10JahreDienst',
        7,
        '1990-07-26');
INSERT INTO tblMitgliedsAuszeichnungen (fiAuszeichnung, fiStammlistennummerAuszeichnung, dtDatumAuszeichnung)
VALUES ('20JahreDienst',
        7,
        '2000-07-26');

TRUNCATE TABLE tblAbsolvieren;
INSERT INTO tblAbsolvieren (fiStammlistennummerAbsolvieren, fiLehrgangLehrgang, dtDatum)
VALUES (1,
        'BAT1',
        '2009-09-31');
INSERT INTO tblAbsolvieren (fiStammlistennummerAbsolvieren, fiLehrgangLehrgang, dtDatum)
VALUES (2,
        'BAT1',
        '2003-03-12');
INSERT INTO tblAbsolvieren (fiStammlistennummerAbsolvieren, fiLehrgangLehrgang, dtDatum)
VALUES (2,
        'BAT2',
        '2005-04-03');
INSERT INTO tblAbsolvieren (fiStammlistennummerAbsolvieren, fiLehrgangLehrgang, dtDatum)
VALUES (4,
        'BAT1',
        '1970-04-07');
INSERT INTO tblAbsolvieren (fiStammlistennummerAbsolvieren, fiLehrgangLehrgang, dtDatum)
VALUES (4,
        'BAT2',
        '1972-09-07');
INSERT INTO tblAbsolvieren (fiStammlistennummerAbsolvieren, fiLehrgangLehrgang, dtDatum)
VALUES (4,
        'BAT3',
        '1990-12-13');
INSERT INTO tblAbsolvieren (fiStammlistennummerAbsolvieren, fiLehrgangLehrgang, dtDatum)
VALUES (5,
        'BAT1',
        '2003-01-01');
INSERT INTO tblAbsolvieren (fiStammlistennummerAbsolvieren, fiLehrgangLehrgang, dtDatum)
VALUES (6,
        'BAT1',
        '2004-01-01');
INSERT INTO tblAbsolvieren (fiStammlistennummerAbsolvieren, fiLehrgangLehrgang, dtDatum)
VALUES (6,
        'BAT2',
        '2008-05-13');

TRUNCATE TABLE tblPositionierung;
INSERT INTO tblPositionierung (fiPositionsname, fiStammlistennummer, dtDatumPosition)
VALUES ('Mitglied',
        1,
        '2003-12-19');
INSERT INTO tblPositionierung (fiPositionsname, fiStammlistennummer, dtDatumPosition)
VALUES ('Inventarist',
        2,
        '2007-08-21');
INSERT INTO tblPositionierung (fiPositionsname, fiStammlistennummer, dtDatumPosition)
VALUES ('Mitglied',
        3,
        '1997-03-09');
INSERT INTO tblPositionierung (fiPositionsname, fiStammlistennummer, dtDatumPosition)
VALUES ('Kommandant',
        4,
        '2002-11-15');
INSERT INTO tblPositionierung (fiPositionsname, fiStammlistennummer, dtDatumPosition)
VALUES ('Mitglied',
        5,
        '2009-03-09');
INSERT INTO tblPositionierung (fiPositionsname, fiStammlistennummer, dtDatumPosition)
VALUES ('Kassierer',
        7,
        '1997-03-09');

TRUNCATE TABLE tblMitgliederFührerscheine;
INSERT INTO tblMitgliederFührerscheine (idFuehrerschein, fiStammlistennummerFührerschein)
VALUES ('B',
        1);
INSERT INTO tblMitgliederFührerscheine (idFuehrerschein, fiStammlistennummerFührerschein)
VALUES ('E',
        1);
INSERT INTO tblMitgliederFührerscheine (idFuehrerschein, fiStammlistennummerFührerschein)
VALUES ('B',
        2);
INSERT INTO tblMitgliederFührerscheine (idFuehrerschein, fiStammlistennummerFührerschein)
VALUES ('F',
        3);
INSERT INTO tblMitgliederFührerscheine (idFuehrerschein, fiStammlistennummerFührerschein)
VALUES ('A',
        4);
INSERT INTO tblMitgliederFührerscheine (idFuehrerschein, fiStammlistennummerFührerschein)
VALUES ('B',
        4);
INSERT INTO tblMitgliederFührerscheine (idFuehrerschein, fiStammlistennummerFührerschein)
VALUES ('C',
        4);
INSERT INTO tblMitgliederFührerscheine (idFuehrerschein, fiStammlistennummerFührerschein)
VALUES ('D',
        4);
INSERT INTO tblMitgliederFührerscheine (idFuehrerschein, fiStammlistennummerFührerschein)
VALUES ('E',
        4);
INSERT INTO tblMitgliederFührerscheine (idFuehrerschein, fiStammlistennummerFührerschein)
VALUES ('B',
        5);
INSERT INTO tblMitgliederFührerscheine (idFuehrerschein, fiStammlistennummerFührerschein)
VALUES ('E',
        5);
INSERT INTO tblMitgliederFührerscheine (idFuehrerschein, fiStammlistennummerFührerschein)
VALUES ('F',
        7);

/*###########################################################################*/ 

/*
###############################################################################
# Organisation
###############################################################################
*/

TRUNCATE TABLE tblEreignis;
INSERT INTO tblEreignis (dtEreignisTyp, dtStartdatum, dtStartzeit, dtEnddatum, dtEndzeit, dtBezeichnungEreignis)
VALUES ('Fest',
        '2013-04-23',
        '08:00',
        '2013-04-23',
        '23:00',
        'Pouletsfest');
INSERT INTO tblEreignis (dtEreignisTyp, dtStartdatum, dtStartzeit, dtEnddatum, dtEndzeit, dtBezeichnungEreignis)
VALUES ('Lehrgang',
        '2013-03-12',
        '06:00',
        '2013-03-13',
        '16:00',
        'BAT1 Lehrgang');
INSERT INTO tblEreignis (dtEreignisTyp, dtStartdatum, dtStartzeit, dtEnddatum, dtEndzeit, dtBezeichnungEreignis)
VALUES ('Medizinische Kontrolle',
        '2013-11-09',
        '11:00',
        '2013-11-09',
        '13:00',
        '');
INSERT INTO tblEreignis (dtEreignisTyp, dtStartdatum, dtStartzeit, dtEnddatum, dtEndzeit, dtBezeichnungEreignis)
VALUES ('Einsatz',
        '2012-11-12',
        '11:04',
        '2012-11-12',
        '16:29',
        'Groußbrand Scheier');
INSERT INTO tblEreignis (dtEreignisTyp, dtStartdatum, dtStartzeit, dtEnddatum, dtEndzeit, dtBezeichnungEreignis)
VALUES ('Einsatz',
        '2012-04-29',
        '22:09',
        '2012-04-30',
        '03:19',
        'Autosaccident');
INSERT INTO tblEreignis (dtEreignisTyp, dtStartdatum, dtStartzeit, dtEnddatum, dtEndzeit, dtBezeichnungEreignis)
VALUES ('Einsatz',
        '2012-04-29',
        '22:09',
        '2012-04-30',
        '05:55',
        '');
INSERT INTO tblEreignis (dtEreignisTyp, dtStartdatum, dtStartzeit, dtEnddatum, dtEndzeit, dtBezeichnungEreignis)
VALUES ('Generalversammlung',
        '2012-03-12',
        '20:00',
        '2012-03-13',
        '03:00',
        '1. Generalversammlung');
INSERT INTO tblEreignis (dtEreignisTyp, dtStartdatum, dtStartzeit, dtEnddatum, dtEndzeit, dtBezeichnungEreignis)
VALUES ('Einsatz',
        '2012-05-01',
        '00:00',
        '2012-05-01',
        '05:32',
        '');
INSERT INTO tblEreignis (dtEreignisTyp, dtStartdatum, dtStartzeit, dtEnddatum, dtEndzeit, dtBezeichnungEreignis)
VALUES ('Medizinische Kontrolle',
        '2012-05-05',
        '14:09',
        '2012-05-05',
        '18:34',
        '');
INSERT INTO tblEreignis (dtEreignisTyp, dtStartdatum, dtStartzeit, dtEnddatum, dtEndzeit, dtBezeichnungEreignis)
VALUES ('Lehrgang',
        '2012-06-06',
        '06:00',
        '2012-06-06',
        '21:00',
        '1. Hilfe');
INSERT INTO tblEreignis (dtEreignisTyp, dtStartdatum, dtStartzeit, dtEnddatum, dtEndzeit, dtBezeichnungEreignis)
VALUES ('Einsatz',
        '2012-07-05',
        '23:11',
        '2012-07-06',
        '06:43',
        '');
INSERT INTO tblEreignis (dtEreignisTyp, dtStartdatum, dtStartzeit, dtEnddatum, dtEndzeit, dtBezeichnungEreignis)
VALUES ('Einsatz',
        '2012-09-13',
        '22:09',
        '2012-09-13',
        '23:59',
        '');
INSERT INTO tblEreignis (dtEreignisTyp, dtStartdatum, dtStartzeit, dtEnddatum, dtEndzeit, dtBezeichnungEreignis)
VALUES ('Übung',
        '2012-12-09',
        '20:00',
        '2012-12-09',
        '22:00',
        'Gemeinschaftsübung');
INSERT INTO tblEreignis (dtEreignisTyp, dtStartdatum, dtStartzeit, dtEnddatum, dtEndzeit, dtBezeichnungEreignis)
VALUES ('Einsatz',
        '2013-01-01',
        '04:29',
        '2013-01-01',
        '05:21',
        '');
INSERT INTO tblEreignis (dtEreignisTyp, dtStartdatum, dtStartzeit, dtEnddatum, dtEndzeit, dtBezeichnungEreignis)
VALUES ('Einsatz',
        '2013-01-01',
        '03:13',
        '2013-01-01',
        '05:21',
        '');

TRUNCATE TABLE tblEinsatzbericht;
INSERT INTO tblEinsatzbericht (dtEinsatzTyp, dtOrtschaftEinsatz, dtAdresseEinsatz, dtPostleitzahlEinsatz, dtNameGeschädigter, dtVornameGeschädigter, dtKommentar, fiEreignisnummerEinsatz)
VALUES ('Autounfall',
        'Musterhausen',
        'am Herd 25',
        239182,
        'Duck',
        'Donald',
        'Irgendein Random Kommentar zum Einsatz.',
        4);
INSERT INTO tblEinsatzbericht (dtEinsatzTyp, dtOrtschaftEinsatz, dtAdresseEinsatz, dtPostleitzahlEinsatz, dtNameGeschädigter, dtVornameGeschädigter, dtKommentar, fiEreignisnummerEinsatz)
VALUES ('Feuer',
        'Musterhausen',
        'Blaustraße 19',
        1337,
        'Maus',
        'Mickey',
        'Weiterer Kommentar bei einem Einsatz.',
        5);
INSERT INTO tblEinsatzbericht (dtEinsatzTyp, dtOrtschaftEinsatz, dtAdresseEinsatz, dtPostleitzahlEinsatz, dtNameGeschädigter, dtVornameGeschädigter, dtKommentar, fiEreignisnummerEinsatz)
VALUES ('Feuer',
        'Musterhausen',
        'Blaustraße 19',
        1337,
        'Maus',
        'Mickey',
        'Das zweite Feuer innerhalb kürzester Zeit.',
        6);
INSERT INTO tblEinsatzbericht (dtEinsatzTyp, dtOrtschaftEinsatz, dtKommentar, fiEreignisnummerEinsatz)
VALUES ('Ölspur',
        'Musterhausen',
        'Die Ölspur hat sich durch das ganze Dorf gezogen.',
        8);
INSERT INTO tblEinsatzbericht (dtEinsatzTyp, dtOrtschaftEinsatz, dtAdresseEinsatz, dtPostleitzahlEinsatz, dtNameGeschädigter, dtVornameGeschädigter, dtKommentar, fiEreignisnummerEinsatz)
VALUES ('Technischer Einsatz', 'Musterhausen', 'Baumfällt', 9323, 'Fäller', 'Baum', 'Herr Fäller hat versucht einen Baum zu fällen,
                                                                                                                              welcher dannach auf sein eigenes Haus gefallen ist.',11);
INSERT INTO tblEinsatzbericht (dtEinsatzTyp, dtOrtschaftEinsatz, dtAdresseEinsatz, dtPostleitzahlEinsatz, dtNameGeschädigter, dtVornameGeschädigter, dtKommentar, fiEreignisnummerEinsatz)
VALUES ('Autounfall', 'Musterhausen', 'Baumgarten 03', 3813, 'Theater', 'Affen', 'Herr Theater ist wegen einer Bananenschale ins Schleudern geraten und mit dem Wagen IN einen Baum geknallt.', 12);
INSERT INTO tblEinsatzbericht (dtEinsatzTyp, dtOrtschaftEinsatz, dtKommentar, fiEreignisnummerEinsatz)
VALUES ('Technischer Einsatz',
        'Musterhausen',
        'Nach einem großen Sturm musste das Dorf von Trümmern befreit werden.',
        14);
INSERT INTO tblEinsatzbericht (dtEinsatzTyp, dtOrtschaftEinsatz, dtAdresseEinsatz, dtPostleitzahlEinsatz, dtNameGeschädigter, dtVornameGeschädigter, dtKommentar, fiEreignisnummerEinsatz)
VALUES ('Feuer', 'Musterhausen', 'Blaustraße 19', 1337, 'Maus', 'Mickey', 'Wir sind zum Schluss gekommen dass Herr Maus die Feuer der letzten Monate gelegt hatte um die Versicherung auszutricksen. Hier ist was wird heute beobachtet haben: Lorem ipsum dolor sit amet,
                                                                                                                                                                                                                                                                     consectetur adipiscing elit. Suspendisse ac quam lectus. Nullam suscipit elit id turpis fringilla suscipit. Duis eget est ac eros iaculis fringilla. Ut viverra lorem quis orci aliquam ut dignissim elit ultricies. Donec mollis,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                nisl AT interdum congue,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        sapien est pulvinar mi,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   sollicitudin fermentum purus massa nec purus. Maecenas id nulla eget nunc venenatis elementum. Mauris eu tellus eros. Aliquam AT diam nec mi consequat sodales nec ac orci. Curabitur consequat sem sit amet nulla sollicitudin IN pulvinar purus tincidunt.', 15);

TRUNCATE TABLE tblTeilgenommen;
INSERT INTO tblTeilgenommen (fiStammlistennummerTeilnahme, fiEreignisnummer)
VALUES (1,
        4);
INSERT INTO tblTeilgenommen (fiStammlistennummerTeilnahme, fiEreignisnummer)
VALUES (1,
        5);
INSERT INTO tblTeilgenommen (fiStammlistennummerTeilnahme, fiEreignisnummer)
VALUES (1,
        6);
INSERT INTO tblTeilgenommen (fiStammlistennummerTeilnahme, fiEreignisnummer)
VALUES (1,
        7);
INSERT INTO tblTeilgenommen (fiStammlistennummerTeilnahme, fiEreignisnummer)
VALUES (1,
        9);
INSERT INTO tblTeilgenommen (fiStammlistennummerTeilnahme, fiEreignisnummer)
VALUES (1,
        11);
INSERT INTO tblTeilgenommen (fiStammlistennummerTeilnahme, fiEreignisnummer)
VALUES (1,
        13);
INSERT INTO tblTeilgenommen (fiStammlistennummerTeilnahme, fiEreignisnummer)
VALUES (2,
        15);
INSERT INTO tblTeilgenommen (fiStammlistennummerTeilnahme, fiEreignisnummer)
VALUES (2,
        6);
INSERT INTO tblTeilgenommen (fiStammlistennummerTeilnahme, fiEreignisnummer)
VALUES (2,
        7);
INSERT INTO tblTeilgenommen (fiStammlistennummerTeilnahme, fiEreignisnummer)
VALUES (2,
        8);
INSERT INTO tblTeilgenommen (fiStammlistennummerTeilnahme, fiEreignisnummer)
VALUES (2,
        9);
INSERT INTO tblTeilgenommen (fiStammlistennummerTeilnahme, fiEreignisnummer)
VALUES (2,
        10);
INSERT INTO tblTeilgenommen (fiStammlistennummerTeilnahme, fiEreignisnummer)
VALUES (2,
        11);
INSERT INTO tblTeilgenommen (fiStammlistennummerTeilnahme, fiEreignisnummer)
VALUES (2,
        12);
INSERT INTO tblTeilgenommen (fiStammlistennummerTeilnahme, fiEreignisnummer)
VALUES (2,
        13);
INSERT INTO tblTeilgenommen (fiStammlistennummerTeilnahme, fiEreignisnummer)
VALUES (2,
        4);
INSERT INTO tblTeilgenommen (fiStammlistennummerTeilnahme, fiEreignisnummer)
VALUES (2,
        3);
INSERT INTO tblTeilgenommen (fiStammlistennummerTeilnahme, fiEreignisnummer)
VALUES (4,
        1);
INSERT INTO tblTeilgenommen (fiStammlistennummerTeilnahme, fiEreignisnummer)
VALUES (4,
        2);
INSERT INTO tblTeilgenommen (fiStammlistennummerTeilnahme, fiEreignisnummer)
VALUES (4,
        3);
INSERT INTO tblTeilgenommen (fiStammlistennummerTeilnahme, fiEreignisnummer)
VALUES (4,
        4);
INSERT INTO tblTeilgenommen (fiStammlistennummerTeilnahme, fiEreignisnummer)
VALUES (4,
        5);
INSERT INTO tblTeilgenommen (fiStammlistennummerTeilnahme, fiEreignisnummer)
VALUES (4,
        7);
INSERT INTO tblTeilgenommen (fiStammlistennummerTeilnahme, fiEreignisnummer)
VALUES (4,
        8);
INSERT INTO tblTeilgenommen (fiStammlistennummerTeilnahme, fiEreignisnummer)
VALUES (4,
        9);
INSERT INTO tblTeilgenommen (fiStammlistennummerTeilnahme, fiEreignisnummer)
VALUES (4,
        10);
INSERT INTO tblTeilgenommen (fiStammlistennummerTeilnahme, fiEreignisnummer)
VALUES (4,
        11);
INSERT INTO tblTeilgenommen (fiStammlistennummerTeilnahme, fiEreignisnummer)
VALUES (4,
        12);
INSERT INTO tblTeilgenommen (fiStammlistennummerTeilnahme, fiEreignisnummer)
VALUES (4,
        13);
INSERT INTO tblTeilgenommen (fiStammlistennummerTeilnahme, fiEreignisnummer)
VALUES (4,
        14);
INSERT INTO tblTeilgenommen (fiStammlistennummerTeilnahme, fiEreignisnummer)
VALUES (7,
        5);
INSERT INTO tblTeilgenommen (fiStammlistennummerTeilnahme, fiEreignisnummer)
VALUES (7,
        7);
INSERT INTO tblTeilgenommen (fiStammlistennummerTeilnahme, fiEreignisnummer)
VALUES (7,
        11);
INSERT INTO tblTeilgenommen (fiStammlistennummerTeilnahme, fiEreignisnummer)
VALUES (7,
        12);
INSERT INTO tblTeilgenommen (fiStammlistennummerTeilnahme, fiEreignisnummer)
VALUES (7,
        13);
INSERT INTO tblTeilgenommen (fiStammlistennummerTeilnahme, fiEreignisnummer)
VALUES (7,
        15);

TRUNCATE TABLE tblProtokolle;
INSERT INTO tblProtokolle (dtBezeichnungProtokoll, dtErstellungsdatum, dtProtokollInhalt)
VALUES ('Beispiel Protokoll', '2002-12-13', 'Lorem ipsum dolor sit amet,
                                                                   consectetur adipiscing elit. Nam ac tortor tortor. Nulla eget augue et ipsum semper rutrum sed a justo. Vestibulum convallis,
                                                                                                                                                                                      nisl ut vehicula ultrices,
                                                                                                                                                                                              odio quam tincidunt dolor,
                                                                                                                                                                                                        id dignissim turpis turpis eu magna. Suspendisse eleifend urna IN quam mattis nec bibendum mauris consequat. Sed convallis justo eget magna dictum AT facilisis nisi laoreet. Ut sit amet ante IN velit ultrices convallis a sed augue. Nulla facilisi.');
INSERT INTO tblProtokolle (dtBezeichnungProtokoll, dtErstellungsdatum, dtProtokollInhalt)
VALUES ('Generalversammlung', '2003-01-05', 'Lorem ipsum dolor sit amet,
                                                                   consectetur adipiscing elit. Nam ac tortor tortor. Nulla eget augue et ipsum semper rutrum sed a justo. Vestibulum convallis,
                                                                                                                                                                                      nisl ut vehicula ultrices,
                                                                                                                                                                                              odio quam tincidunt dolor,
                                                                                                                                                                                                        id dignissim turpis turpis eu magna.');
INSERT INTO tblProtokolle (dtBezeichnungProtokoll, dtErstellungsdatum, dtProtokollInhalt)
VALUES ('Versammlungs Protokoll', '2003-11-29', 'Lorem ipsum dolor sit amet,
                                                                       consectetur adipiscing elit. Nam ac tortor tortor. Nulla eget augue et ipsum semper rutrum sed a justo. Vestibulum convallis,
                                                                                                                                                                                          nisl ut vehicula ultrices,
                                                                                                                                                                                                  odio quam tincidunt dolor,
                                                                                                                                                                                                            id dignissim turpis turpis eu magna. Suspendisse eleifend urna IN quam mattis nec bibendum mauris consequat. Sed convallis justo eget magna dictum AT facilisis nisi laoreet. Ut sit amet ante IN velit ultrices convallis a sed augue. Nulla facilisi.');
INSERT INTO tblProtokolle (dtBezeichnungProtokoll, dtErstellungsdatum, dtProtokollInhalt)
VALUES ('Bericht über Fest', '2004-06-25', 'Lorem ipsum dolor sit amet,
                                                                  consectetur adipiscing elit. Nam ac tortor tortor. Nulla eget augue et ipsum semper rutrum sed a justo. Vestibulum convallis,
                                                                                                                                                                                     nisl ut vehicula ultrices,
                                                                                                                                                                                             odio quam tincidunt dolor,
                                                                                                                                                                                                       id dignissim turpis turpis eu magna. Suspendisse eleifend urna IN quam mattis nec bibendum mauris consequat. Sed convallis justo eget magna dictum AT facilisis nisi laoreet. Ut sit amet ante IN velit ultrices convallis a sed augue. Nulla facilisi. Suspendisse eleifend urna IN quam mattis nec bibendum mauris consequat. Sed convallis justo eget magna dictum AT facilisis nisi laoreet. Ut sit amet ante IN velit ultrices convallis a sed augue. Nulla facilisi. Suspendisse eleifend urna IN quam mattis nec bibendum mauris consequat. Sed convallis justo eget magna dictum AT facilisis nisi laoreet. Ut sit amet ante IN velit ultrices convallis a sed augue. Nulla facilisi.');
INSERT INTO tblProtokolle (dtBezeichnungProtokoll, dtErstellungsdatum, dtProtokollInhalt)
VALUES ('Einsatzbesprechung', '2007-03-07', 'Lorem ipsum dolor sit amet,
                                                                   consectetur adipiscing elit. Nam ac tortor tortor. Nulla eget augue et ipsum semper rutrum sed a justo. Vestibulum convallis,
                                                                                                                                                                                      nisl ut vehicula ultrices,
                                                                                                                                                                                              odio quam tincidunt dolor,
                                                                                                                                                                                                        id dignissim turpis turpis eu magna. Suspendisse eleifend urna IN quam mattis nec bibendum mauris consequat. Sed convallis justo eget magna dictum AT facilisis nisi laoreet. Ut sit amet ante IN velit ultrices convallis a sed augue. Nulla facilisi.');
INSERT INTO tblProtokolle (dtBezeichnungProtokoll, dtErstellungsdatum, dtProtokollInhalt)
VALUES ('Übungsplanung', '2007-07-07', 'Lorem ipsum dolor sit amet,
                                                              consectetur adipiscing elit. Nam ac tortor tortor. Nulla eget augue et ipsum semper rutrum sed a justo. Vestibulum convallis,
                                                                                                                                                                                 nisl ut vehicula ultrices,
                                                                                                                                                                                         odio quam tincidunt dolor,
                                                                                                                    id dignissim turpis turpis eu magna. Suspendisse eleifend urna IN quam mattis nec bibendum mauris consequat. Sed convallis justo eget magna dictum AT facilisis nisi laoreet. Ut sit amet ante IN velit ultrices convallis a sed augue. Nulla facilisi.');

TRUNCATE TABLE tblInventar_Protokoll_Referenzen;
INSERT INTO tblInventar_Protokoll_Referenzen (fiProtokollInventar, fiInventarnummerProtokoll)
VALUES (1, 1);

INSERT INTO tblInventar_Protokoll_Referenzen (fiProtokollInventar, fiInventarnummerProtokoll)
VALUES (1, 2);

INSERT INTO tblInventar_Protokoll_Referenzen (fiProtokollInventar, fiInventarnummerProtokoll)
VALUES (1, 3);

INSERT INTO tblInventar_Protokoll_Referenzen (fiProtokollInventar, fiInventarnummerProtokoll)
VALUES (2, 2);


TRUNCATE TABLE tblEreignis_Protokoll_Referenzen;
INSERT INTO tblEreignis_Protokoll_Referenzen (fiProtokoll, fiEreignisnummerProtokoll)
VALUES (1, 1);

INSERT INTO tblEreignis_Protokoll_Referenzen (fiProtokoll, fiEreignisnummerProtokoll)
VALUES (1, 2);

INSERT INTO tblEreignis_Protokoll_Referenzen (fiProtokoll, fiEreignisnummerProtokoll)
VALUES (1, 3);

INSERT INTO tblEreignis_Protokoll_Referenzen (fiProtokoll, fiEreignisnummerProtokoll)
VALUES (2, 1);


TRUNCATE TABLE tblMitglied_Protokoll_Referenzen;
INSERT INTO tblMitglied_Protokoll_Referenzen (fiProtokollMitglied, fiStammlistennummerProtokoll)
VALUES (1, 1);

INSERT INTO tblMitglied_Protokoll_Referenzen (fiProtokollMitglied, fiStammlistennummerProtokoll)
VALUES (1, 3);

INSERT INTO tblMitglied_Protokoll_Referenzen (fiProtokollMitglied, fiStammlistennummerProtokoll)
VALUES (1, 2);

INSERT INTO tblMitglied_Protokoll_Referenzen (fiProtokollMitglied, fiStammlistennummerProtokoll)
VALUES (2, 1);


TRUNCATE TABLE tblFahrzeug_Protokoll_Referenzen;
INSERT INTO tblFahrzeug_Protokoll_Referenzen (fiProtokollFahrzeug, fiFahrzeugNummerProtokoll)
VALUES (1, 2);

INSERT INTO tblFahrzeug_Protokoll_Referenzen (fiProtokollFahrzeug, fiFahrzeugNummerProtokoll)
VALUES (1, 1);

INSERT INTO tblFahrzeug_Protokoll_Referenzen (fiProtokollFahrzeug, fiFahrzeugNummerProtokoll)
VALUES (1, 3);

INSERT INTO tblFahrzeug_Protokoll_Referenzen (fiProtokollFahrzeug, fiFahrzeugNummerProtokoll)
VALUES (2, 2);

/*###########################################################################*/

/*
###############################################################################
# Maschinist
###############################################################################
*/

TRUNCATE TABLE tblFahrzeug;
INSERT INTO tblFahrzeug (dtKennzeichen, dtBezeichnungFahrzeug, dtBesatzung, dtTechnischesProblem, dtTechnischerKommentar, dtTUVKontrolle, dtWerkstattKontrolle, fiFahrzeugTypFahrzeug)
VALUES ('BM 1912',
        'MTW Toyota',
        9,
        0,
        '',
        '2012-03-23',
        '2012-03-10',
        'MTW');
INSERT INTO tblFahrzeug (dtKennzeichen, dtBezeichnungFahrzeug, dtBesatzung, dtTechnischesProblem, dtTechnischerKommentar, dtTUVKontrolle, dtWerkstattKontrolle, fiFahrzeugTypFahrzeug)
VALUES ('CR 9318',
        'TSF Nissan',
        6,
        0,
        '',
        '2011-09-22',
        '2011-07-03',
        'TSF');
INSERT INTO tblFahrzeug (dtKennzeichen, dtBezeichnungFahrzeug, dtBesatzung, dtTechnischesProblem, dtTechnischerKommentar, dtTUVKontrolle, dtWerkstattKontrolle, fiFahrzeugTypFahrzeug)
VALUES ('ZT 9372',
        'TLF 2000',
        3,
        1,
        'Die Dichtungen sind undischt',
        '2013-03-05',
        '2013-02-12',
        'TLF');
INSERT INTO tblFahrzeug (dtKennzeichen, dtBezeichnungFahrzeug, dtBesatzung, dtTechnischesProblem, dtTechnischerKommentar, dtTUVKontrolle, dtWerkstattKontrolle, fiFahrzeugTypFahrzeug)
VALUES ('XF 3224',
        'TLF 3500',
        3,
        0,
        '',
        '2010-05-19',
        '2010-08-03',
        'TLF');
INSERT INTO tblFahrzeug (dtKennzeichen, dtBezeichnungFahrzeug, dtBesatzung, dtTechnischesProblem, dtTechnischerKommentar, dtTUVKontrolle, dtWerkstattKontrolle, fiFahrzeugTypFahrzeug)
VALUES ('JG 8393',
        'Krankenwagen #1',
        4,
        0,
        '',
        '2012-01-01',
        '2012-01-25',
        'RTW');
INSERT INTO tblFahrzeug (dtKennzeichen, dtBezeichnungFahrzeug, dtBesatzung, dtTechnischesProblem, dtTechnischerKommentar, dtTUVKontrolle, dtWerkstattKontrolle, fiFahrzeugTypFahrzeug)
VALUES ('PD 5482',
        'Rettungsleiter',
        2,
        1,
        'Die Leiter fährt nicht mehr aus',
        '2007-03-05',
        '2007-11-29',
        'DLK');
INSERT INTO tblFahrzeug (dtKennzeichen, dtBezeichnungFahrzeug, dtBesatzung, dtTechnischesProblem, dtTechnischerKommentar, dtTUVKontrolle, dtWerkstattKontrolle, fiFahrzeugTypFahrzeug)
VALUES ('KD 8937',
        'HTLF 15000',
        9,
        0,
        '',
        '2013-03-05',
        '2013-02-12',
        'HTLF');
INSERT INTO tblFahrzeug (dtKennzeichen, dtBezeichnungFahrzeug, dtBesatzung, dtTechnischesProblem, dtTechnischerKommentar, dtTUVKontrolle, dtWerkstattKontrolle, fiFahrzeugTypFahrzeug)
VALUES ('XY3829',
        'TSF Mercedes',
        3,
        0,
        '',
        '2012-03-05',
        '2012-02-12',
        'TSF');

/*###################################################################################################################################*/

/*
######################################################################################################################################
# Inventar
######################################################################################################################################
*/

TRUNCATE TABLE tblInventar;
INSERT INTO tblInventar (dtBezeichnung, dtGrosse, dtZustand, dtKategorie, fiStammlistennummerAusrustung)
VALUES ('Jacke',
        'XL',
        'Gut',
        'Kleider',
        1);
INSERT INTO tblInventar (dtBezeichnung, dtGrosse, dtZustand, dtKategorie, fiStammlistennummerAusrustung)
VALUES ('HAX Schuhe',
        'L',
        'Gut',
        'Kleider',
        1);
INSERT INTO tblInventar (dtBezeichnung, dtZustand, dtKategorie, dtKontrollDatum)
VALUES ('Lüfter',
        'Gut',
        'Maschine',
        '2001-03-12');
INSERT INTO tblInventar (dtBezeichnung, dtZustand, dtKategorie, dtKontrollDatum)
VALUES ('Feuerlöscher AC',
        'mittel',
        'Feuerlöscher',
        '1998-07-29');
INSERT INTO tblInventar (dtBezeichnung, dtKategorie, dtZustand)
VALUES ('LX1',
        'Kommunikation',
        'Beschädigt');
INSERT INTO tblInventar (dtBezeichnung, dtKategorie, dtZustand, fiStammlistennummerAusrustung)
VALUES ('LX3',
        'Kommunikation',
        'Gut',
        1);
INSERT INTO tblInventar (dtBezeichnung, dtKategorie, dtZustand)
VALUES ('LX3',
        'Kommunikation',
        'Gut');
INSERT INTO tblInventar (dtBezeichnung, dtKategorie, dtKnappheit)
VALUES ('Schaummittel 1',
        'Schaummittel',
        0);
INSERT INTO tblInventar (dtBezeichnung, dtKategorie, dtKnappheit)
VALUES ('Schaummittel 2',
        'Schaummittel',
        0);
INSERT INTO tblInventar (dtBezeichnung, dtKategorie, dtKnappheit)
VALUES ('Schaummittel 3',
        'Schaummittel',
        1);
INSERT INTO tblInventar (dtBezeichnung, dtGrosse, dtZustand, dtKategorie, fiStammlistennummerAusrustung)
VALUES ('Helm',
        'XL',
        'Gut',
        'Kleider',
        1);
INSERT INTO tblInventar (dtBezeichnung, dtGrosse, dtZustand, dtKategorie, fiStammlistennummerAusrustung)
VALUES ('Uniform',
        'XL',
        'Gut',
        'Kleider',
        1);
INSERT INTO tblInventar (dtBezeichnung, dtGrosse, dtZustand, dtKategorie, fiStammlistennummerAusrustung)
VALUES ('Helm',
        'L',
        'Gut',
        'Kleider',
        2);
INSERT INTO tblInventar (dtBezeichnung, dtGrosse, dtZustand, dtKategorie, fiStammlistennummerAusrustung)
VALUES ('Uniform',
        'L',
        'Gut',
        'Kleider',
        2);
INSERT INTO tblInventar (dtBezeichnung, dtGrosse, dtZustand, dtKategorie, fiStammlistennummerAusrustung)
VALUES ('Fire Tec',
        'XL',
        'Gut',
        'Kleider',
        2);
INSERT INTO tblInventar (dtBezeichnung, dtKategorie, dtZustand, fiStammlistennummerAusrustung)
VALUES ('LX2',
        'Kommunikation',
        'Mittel',
        2);
INSERT INTO tblInventar (dtBezeichnung, dtKategorie, dtZustand, fiStammlistennummerAusrustung)
VALUES ('LX3',
        'Kommunikation',
        'Gut',
        4);
INSERT INTO tblInventar (dtBezeichnung, dtKategorie, dtZustand, fiStammlistennummerAusrustung)
VALUES ('LX1',
        'Kommunikation',
        'Schlecht',
        7);
INSERT INTO tblInventar (dtBezeichnung, dtKategorie, dtZustand)
VALUES ('LX2',
        'Kommunikation',
        'Gut');
INSERT INTO tblInventar (dtBezeichnung, dtKategorie, dtZustand)
VALUES ('LX3',
        'Kommunikation',
        'Beschädigt');
INSERT INTO tblInventar (dtBezeichnung, dtKategorie, dtZustand)
VALUES ('LX1',
        'Kommunikation',
        'Schlecht');
INSERT INTO tblInventar (dtBezeichnung, dtKategorie, dtZustand)
VALUES ('D Schlauch',
        'Schlauche',
        'Gut');
INSERT INTO tblInventar (dtBezeichnung, dtKategorie, dtZustand)
VALUES ('C Schlauch',
        'Schlauche',
        'Beschädigt');
INSERT INTO tblInventar (dtBezeichnung, dtKategorie, dtZustand)
VALUES ('C Schlauch',
        'Schlauche',
        'Beschädigt');
INSERT INTO tblInventar (dtBezeichnung, dtKategorie, dtZustand)
VALUES ('C Schlauch',
        'Schlauche',
        'Mittel');
INSERT INTO tblInventar (dtBezeichnung, dtKategorie, dtZustand)
VALUES ('C Schlauch',
        'Schlauche',
        'Mittel');
INSERT INTO tblInventar (dtBezeichnung, dtKategorie, dtZustand)
VALUES ('C Schlauch',
        'Schlauche',
        'Gut');
INSERT INTO tblInventar (dtBezeichnung, dtKategorie, dtZustand)
VALUES ('C Schlauch',
        'Schlauche',
        'Gut');
INSERT INTO tblInventar (dtBezeichnung, dtKategorie, dtZustand)
VALUES ('B Schlauch',
        'Schlauche',
        'Beschädigt');
INSERT INTO tblInventar (dtBezeichnung, dtKategorie, dtZustand)
VALUES ('B Schlauch',
        'Schlauche',
        'Mittel');
INSERT INTO tblInventar (dtBezeichnung, dtKategorie, dtZustand)
VALUES ('B Schlauch',
        'Schlauche',
        'Mittel');
INSERT INTO tblInventar (dtBezeichnung, dtKategorie, dtZustand)
VALUES ('B Schlauch',
        'Schlauche',
        'Gut');
INSERT INTO tblInventar (dtBezeichnung, dtKategorie, dtZustand)
VALUES ('B Schlauch',
        'Schlauche',
        'Gut');
INSERT INTO tblInventar (dtBezeichnung, dtKategorie, dtZustand)
VALUES ('A Schlauch',
        'Schlauche',
        'Mittel');
INSERT INTO tblInventar (dtBezeichnung, dtKategorie, dtZustand)
VALUES ('A Schlauch',
        'Schlauche',
        'Gut');
INSERT INTO tblInventar (dtBezeichnung, dtKategorie, dtZustand)
VALUES ('A Schlauch',
        'Schlauche',
        'Schlecht');
INSERT INTO tblInventar (dtBezeichnung, dtKategorie, dtZustand)
VALUES ('A Schlauch',
        'Schlauche',
        'Schlecht');
INSERT INTO tblInventar (dtBezeichnung, dtZustand, dtKategorie, dtKontrollDatum)
VALUES ('Atemschutzgerät #1',
        'Mittel',
        'Atemschutz',
        '1998-07-29');
INSERT INTO tblInventar (dtBezeichnung, dtZustand, dtKategorie, dtKontrollDatum)
VALUES ('Atemschutzgerät #2',
        'Gut',
        'Atemschutz',
        '2002-03-13');
INSERT INTO tblInventar (dtBezeichnung, dtZustand, dtKategorie, dtKontrollDatum)
VALUES ('Atemschutzgerät #3',
        'Beschädigt',
        'Atemschutz',
        '2009-12-19');
INSERT INTO tblInventar (dtBezeichnung, dtZustand, dtKategorie, dtKontrollDatum)
VALUES ('3m Atemschutzgerät',
        'Mittel',
        'Atemschutz',
        '1995-07-29');
INSERT INTO tblInventar (dtBezeichnung, dtZustand, dtKategorie, dtKontrollDatum)
VALUES ('Sahlberg Atemschutzgerät',
        'Gut',
        'Atemschutz',
        '2013-01-01');
INSERT INTO tblInventar (dtBezeichnung, dtZustand, dtKategorie, dtKontrollDatum)
VALUES ('Hormuth Atemschutzgerät',
        'Schlecht',
        'Atemschutz',
        '2012-12-30');
INSERT INTO tblInventar (dtBezeichnung, dtKategorie, dtKontrollDatum)
VALUES ('Kleiner Einsatzkoffer',
        '1. Hilfe',
        '1990-12-14');
INSERT INTO tblInventar (dtBezeichnung, dtKategorie, dtKontrollDatum)
VALUES ('Großer Einsatzkoffer',
        '1. Hilfe',
        '2012-12-14');


TRUNCATE TABLE tblFahrzeugAusstattung;
INSERT INTO tblFahrzeugAusstattung (fiFahrzeugnummer, fiInventarnummer)
VALUES (3,
        3);
INSERT INTO tblFahrzeugAusstattung (fiFahrzeugnummer, fiInventarnummer)
VALUES (3,
        4);
INSERT INTO tblFahrzeugAusstattung (fiFahrzeugnummer, fiInventarnummer)
VALUES (3,
        5);
INSERT INTO tblFahrzeugAusstattung (fiFahrzeugnummer, fiInventarnummer)
VALUES (3,
        10);
INSERT INTO tblFahrzeugAusstattung (fiFahrzeugnummer, fiInventarnummer)
VALUES (2,
        6);
INSERT INTO tblFahrzeugAusstattung (fiFahrzeugnummer, fiInventarnummer)
VALUES (2,
        11);
INSERT INTO tblFahrzeugAusstattung (fiFahrzeugnummer, fiInventarnummer)
VALUES (3,
        12);
INSERT INTO tblFahrzeugAusstattung (fiFahrzeugnummer, fiInventarnummer)
VALUES (3,
        13);
INSERT INTO tblFahrzeugAusstattung (fiFahrzeugnummer, fiInventarnummer)
VALUES (2,
        14);
INSERT INTO tblFahrzeugAusstattung (fiFahrzeugnummer, fiInventarnummer)
VALUES (1,
        15);
INSERT INTO tblFahrzeugAusstattung (fiFahrzeugnummer, fiInventarnummer)
VALUES (8,
        23);
INSERT INTO tblFahrzeugAusstattung (fiFahrzeugnummer, fiInventarnummer)
VALUES (8,
        24);
INSERT INTO tblFahrzeugAusstattung (fiFahrzeugnummer, fiInventarnummer)
VALUES (8,
        25);
INSERT INTO tblFahrzeugAusstattung (fiFahrzeugnummer, fiInventarnummer)
VALUES (7,
        43);
INSERT INTO tblFahrzeugAusstattung (fiFahrzeugnummer, fiInventarnummer)
VALUES (7,
        44);
INSERT INTO tblFahrzeugAusstattung (fiFahrzeugnummer, fiInventarnummer)
VALUES (6,
        45);
INSERT INTO tblFahrzeugAusstattung (fiFahrzeugnummer, fiInventarnummer)
VALUES (6,
        46);
INSERT INTO tblFahrzeugAusstattung (fiFahrzeugnummer, fiInventarnummer)
VALUES (6,
        47);
INSERT INTO tblFahrzeugAusstattung (fiFahrzeugnummer, fiInventarnummer)
VALUES (6,
        48);
INSERT INTO tblFahrzeugAusstattung (fiFahrzeugnummer, fiInventarnummer)
VALUES (6,
        50);
INSERT INTO tblFahrzeugAusstattung (fiFahrzeugnummer, fiInventarnummer)
VALUES (3,
        54);
INSERT INTO tblFahrzeugAusstattung (fiFahrzeugnummer, fiInventarnummer)
VALUES (3,
        55);
INSERT INTO tblFahrzeugAusstattung (fiFahrzeugnummer, fiInventarnummer)
VALUES (3,
        56);

/*###########################################################################*/

SET foreign_key_checks=1;

/*###########################################################################*/