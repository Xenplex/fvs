SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP SCHEMA IF EXISTS `fvs_datenbank` ;
CREATE SCHEMA IF NOT EXISTS `fvs_datenbank` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci ;
USE `fvs_datenbank` ;

-- -----------------------------------------------------
-- Table `fvs_datenbank`.`tblMitglied`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fvs_datenbank`.`tblMitglied` ;

CREATE  TABLE IF NOT EXISTS `fvs_datenbank`.`tblMitglied` (
  `idStammlistennummer` INT NOT NULL AUTO_INCREMENT COMMENT 'Stammlistennummer\neines Mitgliedes.\nEinzigartig für jedes\nMitglied landesweit.' ,
  `dtName` VARCHAR(45) NULL COMMENT 'Familienname des\nMitgliedes.' ,
  `dtVorname` VARCHAR(45) NULL COMMENT 'Vorname des\nMitgliedes.' ,
  `dtSozialnummer` SMALLINT NULL COMMENT 'Die 3 stellige\nVersicherungs-\nnummer.\n(ohne Geburts-\ndatum)' ,
  `dtGeburtsdatum` DATE NULL COMMENT 'Der Geburtstag\ndes Mitgliedes.' ,
  `dtMobiltelefon` INT NULL COMMENT 'Handynummer des\nMitgliedes.' ,
  `dtEmail` VARCHAR(45) NULL COMMENT 'Die Email Adresse\ndes Mitgliedes.' ,
  `dtTelefonnummer` INT NULL COMMENT 'Festnetznummer\ndes Mitgliedes.' ,
  `dtOrtschaft` VARCHAR(45) NULL COMMENT 'Der Dorfname/\nStadtname in\nwelcher das Mitglied\nangemeldet ist.' ,
  `dtAdresse` TEXT NULL COMMENT 'Die Hausnummer\ndes Hauses eines\nMitgliedes.' ,
  `dtPostleitzahl` MEDIUMINT NULL COMMENT 'Die Postleitzahl des\nHauses eines\nMitgliedes.' ,
  `dtLehrgangsstunden` MEDIUMINT NULL COMMENT 'Stundenanzahl\nwelche ein Mitglied\nnoch zur Verfügung\nhat, um an Lehr-\ngängen teilzunehmen,\nDiese Zahl wird\njählrich zurück-\ngesetzt.' ,
  `dtAPTEinsätze` TINYINT(1) NULL COMMENT 'Gibt an ob ein\nMitglied APT für\nregulare Einsätze\nist oder nicht.' ,
  `dtAPTAtemschutz` TINYINT(1) NULL COMMENT 'Gibt an ob ein\nMitglied APT für\nden Gebrauch des\nAtemschutzes ist\noder nicht.' ,
  `dtMedizinischeKontrolle` DATE NULL ,
  PRIMARY KEY (`idStammlistennummer`) ,
  UNIQUE INDEX `idStammlistennummer_UNIQUE` (`idStammlistennummer` ASC) )
ENGINE = InnoDB
COMMENT = 'Tabelle welche alle benötigten Informationen eines Mitgliede' /* comment truncated */;


-- -----------------------------------------------------
-- Table `fvs_datenbank`.`tblEinAustritt`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fvs_datenbank`.`tblEinAustritt` ;

CREATE  TABLE IF NOT EXISTS `fvs_datenbank`.`tblEinAustritt` (
  `idEinAustritt` INT NOT NULL AUTO_INCREMENT COMMENT 'Automatisches\nSchlüssel' ,
  `dtEintrittDatum` DATE NULL COMMENT 'Datum an dem ein\nMitglied einer\nEinsatzstelle\nbeigetreten ist.' ,
  `dtAustrittDatum` DATE NULL COMMENT 'Datum an dem ein\nMitglied bei einer\nEinsatzstelle aus-\ngetreten ist.' ,
  `fiStammlistennummerEinAustritt` INT NOT NULL ,
  PRIMARY KEY (`idEinAustritt`, `fiStammlistennummerEinAustritt`) ,
  INDEX `fk_tblEinAustritt_tblMitglied` (`fiStammlistennummerEinAustritt` ASC) ,
  CONSTRAINT `fk_tblEinAustritt_tblMitglied`
    FOREIGN KEY (`fiStammlistennummerEinAustritt` )
    REFERENCES `fvs_datenbank`.`tblMitglied` (`idStammlistennummer` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Tabelle welche alle Ein & Austritte aus der Feuerwehr eines ' /* comment truncated */;


-- -----------------------------------------------------
-- Table `fvs_datenbank`.`tblAuszeichnung`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fvs_datenbank`.`tblAuszeichnung` ;

CREATE  TABLE IF NOT EXISTS `fvs_datenbank`.`tblAuszeichnung` (
  `idAuszeichnung` VARCHAR(100) NOT NULL COMMENT 'Name/Bezeichnung\ndes Auszeichnung.' ,
  PRIMARY KEY (`idAuszeichnung`) )
ENGINE = InnoDB
COMMENT = 'Tabelle welche alle möglichen Auszeichnungen, welche ein Mit' /* comment truncated */;


-- -----------------------------------------------------
-- Table `fvs_datenbank`.`tblMitgliedsAuszeichnungen`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fvs_datenbank`.`tblMitgliedsAuszeichnungen` ;

CREATE  TABLE IF NOT EXISTS `fvs_datenbank`.`tblMitgliedsAuszeichnungen` (
  `fiAuszeichnung` VARCHAR(100) NOT NULL ,
  `fiStammlistennummerAuszeichnung` INT NOT NULL ,
  `dtDatumAuszeichnung` DATE NULL ,
  PRIMARY KEY (`fiAuszeichnung`, `fiStammlistennummerAuszeichnung`) ,
  INDEX `fk_tblAuszeichnung_has_tblMitglied_tblMitglied1` (`fiStammlistennummerAuszeichnung` ASC) ,
  INDEX `fk_tblAuszeichnung_has_tblMitglied_tblAuszeichnung1` (`fiAuszeichnung` ASC) ,
  CONSTRAINT `fk_tblAuszeichnung_has_tblMitglied_tblAuszeichnung1`
    FOREIGN KEY (`fiAuszeichnung` )
    REFERENCES `fvs_datenbank`.`tblAuszeichnung` (`idAuszeichnung` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tblAuszeichnung_has_tblMitglied_tblMitglied1`
    FOREIGN KEY (`fiStammlistennummerAuszeichnung` )
    REFERENCES `fvs_datenbank`.`tblMitglied` (`idStammlistennummer` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Tabelle welche alle Auszeichnungen auflistet, welche ein Mit' /* comment truncated */;


-- -----------------------------------------------------
-- Table `fvs_datenbank`.`tblLehrgang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fvs_datenbank`.`tblLehrgang` ;

CREATE  TABLE IF NOT EXISTS `fvs_datenbank`.`tblLehrgang` (
  `idLehrgang` VARCHAR(50) NOT NULL COMMENT 'Bezeichnung des\nLehrgangs.' ,
  `dtDauer` SMALLINT NULL COMMENT 'Anzahl an Stunden,\nwelche ein\nLehrgang benötigt.' ,
  PRIMARY KEY (`idLehrgang`) )
ENGINE = InnoDB
COMMENT = 'Tabelle welche alle möglichen Lehrgänge auflistet und ihre D' /* comment truncated */;


-- -----------------------------------------------------
-- Table `fvs_datenbank`.`tblAbsolvieren`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fvs_datenbank`.`tblAbsolvieren` ;

CREATE  TABLE IF NOT EXISTS `fvs_datenbank`.`tblAbsolvieren` (
  `fiStammlistennummerAbsolvieren` INT NOT NULL ,
  `fiLehrgangLehrgang` VARCHAR(50) NOT NULL ,
  `dtDatum` DATE NULL COMMENT 'Datum an dem ein\nMitglied den Lehr-\ngang abgeschlossen\nhat.' ,
  PRIMARY KEY (`fiStammlistennummerAbsolvieren`, `fiLehrgangLehrgang`) ,
  INDEX `fk_tblMitglied_has_tblLehrgang_tblLehrgang1` (`fiLehrgangLehrgang` ASC) ,
  INDEX `fk_tblMitglied_has_tblLehrgang_tblMitglied1` (`fiStammlistennummerAbsolvieren` ASC) ,
  CONSTRAINT `fk_tblMitglied_has_tblLehrgang_tblMitglied1`
    FOREIGN KEY (`fiStammlistennummerAbsolvieren` )
    REFERENCES `fvs_datenbank`.`tblMitglied` (`idStammlistennummer` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tblMitglied_has_tblLehrgang_tblLehrgang1`
    FOREIGN KEY (`fiLehrgangLehrgang` )
    REFERENCES `fvs_datenbank`.`tblLehrgang` (`idLehrgang` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Tabelle welche alle Lehrgägne, welche ein Mitglied absolvier' /* comment truncated */;


-- -----------------------------------------------------
-- Table `fvs_datenbank`.`tblEreignis`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fvs_datenbank`.`tblEreignis` ;

CREATE  TABLE IF NOT EXISTS `fvs_datenbank`.`tblEreignis` (
  `idEreignisnummer` INT NOT NULL AUTO_INCREMENT ,
  `dtEreignisTyp` VARCHAR(45) NULL COMMENT 'Gibt an um welchen\nTyp das Ereignis ist.\n(Übung, Einsatz,...)' ,
  `dtStartdatum` DATE NULL ,
  `dtStartzeit` TIME NULL ,
  `dtEnddatum` DATE NULL ,
  `dtEndzeit` TIME NULL ,
  `dtBezeichnungEreignis` VARCHAR(45) NULL ,
  PRIMARY KEY (`idEreignisnummer`) )
ENGINE = InnoDB
COMMENT = 'Tabelle in der alle Ereignisse (Übungen, medizinischer Kontr' /* comment truncated */;


-- -----------------------------------------------------
-- Table `fvs_datenbank`.`tblTeilgenommen`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fvs_datenbank`.`tblTeilgenommen` ;

CREATE  TABLE IF NOT EXISTS `fvs_datenbank`.`tblTeilgenommen` (
  `fiStammlistennummerTeilnahme` INT NOT NULL ,
  `fiEreignisnummer` INT NOT NULL ,
  PRIMARY KEY (`fiStammlistennummerTeilnahme`, `fiEreignisnummer`) ,
  INDEX `fk_tblMitglied_has_tblEreignis_tblEreignis1` (`fiEreignisnummer` ASC) ,
  INDEX `fk_tblMitglied_has_tblEreignis_tblMitglied1` (`fiStammlistennummerTeilnahme` ASC) ,
  CONSTRAINT `fk_tblMitglied_has_tblEreignis_tblMitglied1`
    FOREIGN KEY (`fiStammlistennummerTeilnahme` )
    REFERENCES `fvs_datenbank`.`tblMitglied` (`idStammlistennummer` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tblMitglied_has_tblEreignis_tblEreignis1`
    FOREIGN KEY (`fiEreignisnummer` )
    REFERENCES `fvs_datenbank`.`tblEreignis` (`idEreignisnummer` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Tabelle welche angibt welches Mitglied an welchem Ereignis t' /* comment truncated */;


-- -----------------------------------------------------
-- Table `fvs_datenbank`.`tblInventar`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fvs_datenbank`.`tblInventar` ;

CREATE  TABLE IF NOT EXISTS `fvs_datenbank`.`tblInventar` (
  `idInventarnummer` INT NOT NULL AUTO_INCREMENT ,
  `dtBezeichnung` VARCHAR(45) NOT NULL COMMENT 'Name/Bezeichnung\ndes Gegenstandes.\nKann Markennamen,\nModellname\noder allgemeiner\nName sein.' ,
  `dtGrosse` VARCHAR(5) NULL COMMENT 'Falls es sich um\nKleidung handelt,\nwird hier die Größe\nangegeben.' ,
  `dtZustand` VARCHAR(45) NULL COMMENT 'Jeder Gegenstand\nbekommt einen\nZustand zugewiesen.\n(brandneu, veraltet,...)' ,
  `dtKategorie` VARCHAR(45) NOT NULL COMMENT 'Kategorie in der das\nInventargegenstand\neingeteilt wird.' ,
  `dtKontrollDatum` DATE NULL COMMENT 'Datum der letzten\nKontrolle des Inventar-\ngegenstandes, falls\nbenötigt.' ,
  `dtKnappheit` TINYINT(1) NULL DEFAULT 0 COMMENT 'Falls der Inventar-\ngegenstand nicht\nzählbar ist.' ,
  `fiStammlistennummerAusrustung` INT NULL COMMENT 'Gibt an ob der\njeweilige Inventar-\ngegenstand einem\neinzelnen Mitglied\n(Ausrüstung)\nzugewiesen ist.' ,
  PRIMARY KEY (`idInventarnummer`) ,
  INDEX `fk_tblInventar_tblMitglied1` (`fiStammlistennummerAusrustung` ASC) ,
  CONSTRAINT `fk_tblInventar_tblMitglied1`
    FOREIGN KEY (`fiStammlistennummerAusrustung` )
    REFERENCES `fvs_datenbank`.`tblMitglied` (`idStammlistennummer` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Tabelle welche alle Inventargegenstände auflistet, \nunabhäng' /* comment truncated */;


-- -----------------------------------------------------
-- Table `fvs_datenbank`.`tblMitgliederFührerscheine`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fvs_datenbank`.`tblMitgliederFührerscheine` ;

CREATE  TABLE IF NOT EXISTS `fvs_datenbank`.`tblMitgliederFührerscheine` (
  `fiStammlistennummerFührerschein` INT NOT NULL ,
  `idFuehrerschein` CHAR NOT NULL ,
  PRIMARY KEY (`fiStammlistennummerFührerschein`, `idFuehrerschein`) ,
  INDEX `fk_tblFührerscheine_has_tblMitglied_tblMitglied1` (`fiStammlistennummerFührerschein` ASC) ,
  CONSTRAINT `fk_tblFührerscheine_has_tblMitglied_tblMitglied1`
    FOREIGN KEY (`fiStammlistennummerFührerschein` )
    REFERENCES `fvs_datenbank`.`tblMitglied` (`idStammlistennummer` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Tabelle welche alle Führerscheine ,welche ein Mitglied besit' /* comment truncated */;


-- -----------------------------------------------------
-- Table `fvs_datenbank`.`tblFahrzeug`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fvs_datenbank`.`tblFahrzeug` ;

CREATE  TABLE IF NOT EXISTS `fvs_datenbank`.`tblFahrzeug` (
  `idFahrzeugNummer` INT NOT NULL AUTO_INCREMENT COMMENT 'Identifizierungs-\nnummer des\nFahrzeuges.' ,
  `dtKennzeichen` VARCHAR(45) NOT NULL COMMENT 'Das aktuelle Auto-\nkennzeichen des \nFahrzeuges.' ,
  `dtBezeichnungFahrzeug` VARCHAR(45) NOT NULL COMMENT 'Der Name/Modell/Bez-\neichnung des Fahr-\nzeuges.' ,
  `dtBesatzung` TINYINT NULL COMMENT 'Anzahl an Mitgliedern\nwelche im Fahrzeug\nPlatz haben.' ,
  `dtTechnischesProblem` TINYINT(1) NULL DEFAULT 0 COMMENT 'Gibt an ob ein\ntechnisches\nProblem vorliegt.' ,
  `dtTechnischerKommentar` MEDIUMTEXT NULL COMMENT 'Freier Kommentar\ndes Mechanikers.' ,
  `dtTUVKontrolle` DATE NULL COMMENT 'Gibt an wann das\nFahrzeug zuletzt\nim TÜV Test war.' ,
  `dtWerkstattKontrolle` DATE NULL COMMENT 'Gibt an wann das\nFahrzeug zuletzt\nin der Wekrstatt war.' ,
  `fiFahrzeugTypFahrzeug` VARCHAR(50) NOT NULL DEFAULT 'TLF' ,
  PRIMARY KEY (`idFahrzeugNummer`) )
ENGINE = InnoDB
COMMENT = 'Tabelle welche alle Fahrzeuge auflistet.';


-- -----------------------------------------------------
-- Table `fvs_datenbank`.`tblFahrzeugAusstattung`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fvs_datenbank`.`tblFahrzeugAusstattung` ;

CREATE  TABLE IF NOT EXISTS `fvs_datenbank`.`tblFahrzeugAusstattung` (
  `fiFahrzeugNummer` INT NOT NULL ,
  `fiInventarnummer` INT NOT NULL ,
  PRIMARY KEY (`fiFahrzeugNummer`, `fiInventarnummer`) ,
  INDEX `fk_tblFahrzeug_has_tblInventar_tblInventar1` (`fiInventarnummer` ASC) ,
  INDEX `fk_tblFahrzeug_has_tblInventar_tblFahrzeug1` (`fiFahrzeugNummer` ASC) ,
  CONSTRAINT `fk_tblFahrzeug_has_tblInventar_tblFahrzeug1`
    FOREIGN KEY (`fiFahrzeugNummer` )
    REFERENCES `fvs_datenbank`.`tblFahrzeug` (`idFahrzeugNummer` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tblFahrzeug_has_tblInventar_tblInventar1`
    FOREIGN KEY (`fiInventarnummer` )
    REFERENCES `fvs_datenbank`.`tblInventar` (`idInventarnummer` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Tabelle welche die Ausstattung eines Fahrzeuges auflistet.';


-- -----------------------------------------------------
-- Table `fvs_datenbank`.`tblBenutzer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fvs_datenbank`.`tblBenutzer` ;

CREATE  TABLE IF NOT EXISTS `fvs_datenbank`.`tblBenutzer` (
  `idBenutzerNummer` INT NOT NULL AUTO_INCREMENT ,
  `dtLogin` VARCHAR(45) NULL ,
  `dtPasswort` VARCHAR(45) NULL ,
  `dtBenutzergruppe` VARCHAR(45) NULL COMMENT 'Die Benutzergruppe in \nwelcher der jeweilige\nBenutzer eingeteilt ist\nund welche festlegt\nwelche Rechte der\nBenutzer hat.' ,
  PRIMARY KEY (`idBenutzerNummer`) )
ENGINE = InnoDB
COMMENT = 'Enthält die Benutzeraccounts der Benutzer von FVS.';


-- -----------------------------------------------------
-- Table `fvs_datenbank`.`tblPosition`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fvs_datenbank`.`tblPosition` ;

CREATE  TABLE IF NOT EXISTS `fvs_datenbank`.`tblPosition` (
  `idPositionsName` VARCHAR(20) NOT NULL COMMENT 'Name/Bezeichnung der\nPosition.' ,
  PRIMARY KEY (`idPositionsName`) )
ENGINE = InnoDB
COMMENT = 'Tabelle welche alle möglichen Positionen innerhalb einer Feu' /* comment truncated */;


-- -----------------------------------------------------
-- Table `fvs_datenbank`.`tblPositionierung`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fvs_datenbank`.`tblPositionierung` ;

CREATE  TABLE IF NOT EXISTS `fvs_datenbank`.`tblPositionierung` (
  `fiPositionsname` VARCHAR(20) NOT NULL DEFAULT 'Mitglied' ,
  `fiStammlistennummer` INT NOT NULL ,
  `dtDatumPosition` DATE NULL COMMENT 'Gibt den Datum an an\nwelchem das Mitglied\ndie angegebene\nPosition eingenommen\nhat.' ,
  PRIMARY KEY (`fiPositionsname`, `fiStammlistennummer`) ,
  INDEX `fk_Position_has_tblMitglied_tblMitglied1` (`fiStammlistennummer` ASC) ,
  INDEX `fk_Position_has_tblMitglied_Position1` (`fiPositionsname` ASC) ,
  CONSTRAINT `fk_Position_has_tblMitglied_Position1`
    FOREIGN KEY (`fiPositionsname` )
    REFERENCES `fvs_datenbank`.`tblPosition` (`idPositionsName` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Position_has_tblMitglied_tblMitglied1`
    FOREIGN KEY (`fiStammlistennummer` )
    REFERENCES `fvs_datenbank`.`tblMitglied` (`idStammlistennummer` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Tabelle welche angibt, an welchem Datum, welches Mitglied, w' /* comment truncated */;


-- -----------------------------------------------------
-- Table `fvs_datenbank`.`tblEinsatzbericht`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fvs_datenbank`.`tblEinsatzbericht` ;

CREATE  TABLE IF NOT EXISTS `fvs_datenbank`.`tblEinsatzbericht` (
  `idBerichtnummer` INT NOT NULL AUTO_INCREMENT ,
  `dtEinsatzTyp` VARCHAR(45) NULL COMMENT 'Gibt die Kategorie des\nEinsatzes an.' ,
  `dtOrtschaftEinsatz` VARCHAR(45) NULL ,
  `dtAdresseEinsatz` TINYTEXT NULL ,
  `dtPostleitzahlEinsatz` INT NULL ,
  `dtNameGeschädigter` VARCHAR(45) NULL ,
  `dtVornameGeschädigter` VARCHAR(45) NULL ,
  `dtKommentar` TEXT NULL COMMENT 'Freier Kommentar\nzum Einsatz.' ,
  `fiEreignisnummerEinsatz` INT NOT NULL ,
  PRIMARY KEY (`idBerichtnummer`, `fiEreignisnummerEinsatz`) ,
  INDEX `fk_tblEinsatzbericht_tblEreignis1` (`fiEreignisnummerEinsatz` ASC) ,
  CONSTRAINT `fk_tblEinsatzbericht_tblEreignis1`
    FOREIGN KEY (`fiEreignisnummerEinsatz` )
    REFERENCES `fvs_datenbank`.`tblEreignis` (`idEreignisnummer` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Tabelle welche zusätzliche Informationen über Ereignisse wel' /* comment truncated */;


-- -----------------------------------------------------
-- Table `fvs_datenbank`.`tblProtokolle`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fvs_datenbank`.`tblProtokolle` ;

CREATE  TABLE IF NOT EXISTS `fvs_datenbank`.`tblProtokolle` (
  `idProtokoll` INT NOT NULL AUTO_INCREMENT ,
  `dtBezeichnungProtokoll` TEXT NULL ,
  `dtErstellungsdatum` DATE NULL ,
  `dtProtokollInhalt` LONGTEXT NULL ,
  PRIMARY KEY (`idProtokoll`) )
ENGINE = InnoDB
COMMENT = 'Enthält alle Informationen welche für ein Versammlungsprotok' /* comment truncated */;


-- -----------------------------------------------------
-- Table `fvs_datenbank`.`tblEreignis_Protokoll_Referenzen`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fvs_datenbank`.`tblEreignis_Protokoll_Referenzen` ;

CREATE  TABLE IF NOT EXISTS `fvs_datenbank`.`tblEreignis_Protokoll_Referenzen` (
  `fiProtokoll` INT NOT NULL ,
  `fiEreignisnummerProtokoll` INT NOT NULL ,
  PRIMARY KEY (`fiProtokoll`, `fiEreignisnummerProtokoll`) ,
  INDEX `fk_tblProtokolle_has_tblEreignis_tblEreignis1` (`fiEreignisnummerProtokoll` ASC) ,
  INDEX `fk_tblProtokolle_has_tblEreignis_tblProtokolle1` (`fiProtokoll` ASC) ,
  CONSTRAINT `fk_tblProtokolle_has_tblEreignis_tblProtokolle1`
    FOREIGN KEY (`fiProtokoll` )
    REFERENCES `fvs_datenbank`.`tblProtokolle` (`idProtokoll` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tblProtokolle_has_tblEreignis_tblEreignis1`
    FOREIGN KEY (`fiEreignisnummerProtokoll` )
    REFERENCES `fvs_datenbank`.`tblEreignis` (`idEreignisnummer` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Enthält die Referenzen zu Ereignissen in einem Protokoll.';


-- -----------------------------------------------------
-- Table `fvs_datenbank`.`tblMitglied_Protokoll_Referenzen`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fvs_datenbank`.`tblMitglied_Protokoll_Referenzen` ;

CREATE  TABLE IF NOT EXISTS `fvs_datenbank`.`tblMitglied_Protokoll_Referenzen` (
  `fiProtokollMitglied` INT NOT NULL ,
  `fiStammlistennummerProtokoll` INT NOT NULL ,
  PRIMARY KEY (`fiProtokollMitglied`, `fiStammlistennummerProtokoll`) ,
  INDEX `fk_tblProtokolle_has_tblMitglied_tblMitglied1` (`fiStammlistennummerProtokoll` ASC) ,
  INDEX `fk_tblProtokolle_has_tblMitglied_tblProtokolle1` (`fiProtokollMitglied` ASC) ,
  CONSTRAINT `fk_tblProtokolle_has_tblMitglied_tblProtokolle1`
    FOREIGN KEY (`fiProtokollMitglied` )
    REFERENCES `fvs_datenbank`.`tblProtokolle` (`idProtokoll` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tblProtokolle_has_tblMitglied_tblMitglied1`
    FOREIGN KEY (`fiStammlistennummerProtokoll` )
    REFERENCES `fvs_datenbank`.`tblMitglied` (`idStammlistennummer` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Enthält alle Protokoll Referenzen zu Mitgliedern.';


-- -----------------------------------------------------
-- Table `fvs_datenbank`.`tblInventar_Protokoll_Referenzen`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fvs_datenbank`.`tblInventar_Protokoll_Referenzen` ;

CREATE  TABLE IF NOT EXISTS `fvs_datenbank`.`tblInventar_Protokoll_Referenzen` (
  `fiProtokollInventar` INT NOT NULL ,
  `fiInventarnummerProtokoll` INT NOT NULL ,
  PRIMARY KEY (`fiProtokollInventar`, `fiInventarnummerProtokoll`) ,
  INDEX `fk_tblProtokolle_has_tblInventar_tblInventar1` (`fiInventarnummerProtokoll` ASC) ,
  INDEX `fk_tblProtokolle_has_tblInventar_tblProtokolle1` (`fiProtokollInventar` ASC) ,
  CONSTRAINT `fk_tblProtokolle_has_tblInventar_tblProtokolle1`
    FOREIGN KEY (`fiProtokollInventar` )
    REFERENCES `fvs_datenbank`.`tblProtokolle` (`idProtokoll` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tblProtokolle_has_tblInventar_tblInventar1`
    FOREIGN KEY (`fiInventarnummerProtokoll` )
    REFERENCES `fvs_datenbank`.`tblInventar` (`idInventarnummer` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Enthält die Referenzen zu Inventargegenständen in einem Prot' /* comment truncated */;


-- -----------------------------------------------------
-- Table `fvs_datenbank`.`tblFahrzeug_Protokoll_Referenzen`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fvs_datenbank`.`tblFahrzeug_Protokoll_Referenzen` ;

CREATE  TABLE IF NOT EXISTS `fvs_datenbank`.`tblFahrzeug_Protokoll_Referenzen` (
  `fiProtokollFahrzeug` INT NOT NULL ,
  `fiFahrzeugNummerProtokoll` INT NOT NULL ,
  PRIMARY KEY (`fiProtokollFahrzeug`, `fiFahrzeugNummerProtokoll`) ,
  INDEX `fk_tblProtokolle_has_tblFahrzeug_tblFahrzeug1` (`fiFahrzeugNummerProtokoll` ASC) ,
  INDEX `fk_tblProtokolle_has_tblFahrzeug_tblProtokolle1` (`fiProtokollFahrzeug` ASC) ,
  CONSTRAINT `fk_tblProtokolle_has_tblFahrzeug_tblProtokolle1`
    FOREIGN KEY (`fiProtokollFahrzeug` )
    REFERENCES `fvs_datenbank`.`tblProtokolle` (`idProtokoll` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tblProtokolle_has_tblFahrzeug_tblFahrzeug1`
    FOREIGN KEY (`fiFahrzeugNummerProtokoll` )
    REFERENCES `fvs_datenbank`.`tblFahrzeug` (`idFahrzeugNummer` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Enthält die Referenzen zu Fahrzeugen in einem Protokoll.';



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `fvs_datenbank`.`tblAuszeichnung`
-- -----------------------------------------------------
START TRANSACTION;
USE `fvs_datenbank`;
INSERT INTO `fvs_datenbank`.`tblAuszeichnung` (`idAuszeichnung`) VALUES ('10JahreDienst');
INSERT INTO `fvs_datenbank`.`tblAuszeichnung` (`idAuszeichnung`) VALUES ('20JahreDienst');
INSERT INTO `fvs_datenbank`.`tblAuszeichnung` (`idAuszeichnung`) VALUES ('Tapferkeitsmedaille');

COMMIT;

-- -----------------------------------------------------
-- Data for table `fvs_datenbank`.`tblLehrgang`
-- -----------------------------------------------------
START TRANSACTION;
USE `fvs_datenbank`;
INSERT INTO `fvs_datenbank`.`tblLehrgang` (`idLehrgang`, `dtDauer`) VALUES ('BAT1', 30);
INSERT INTO `fvs_datenbank`.`tblLehrgang` (`idLehrgang`, `dtDauer`) VALUES ('BAT2', 45);
INSERT INTO `fvs_datenbank`.`tblLehrgang` (`idLehrgang`, `dtDauer`) VALUES ('BAT3', 75);

COMMIT;

-- -----------------------------------------------------
-- Data for table `fvs_datenbank`.`tblBenutzer`
-- -----------------------------------------------------
START TRANSACTION;
USE `fvs_datenbank`;
INSERT INTO `fvs_datenbank`.`tblBenutzer` (`idBenutzerNummer`, `dtLogin`, `dtPasswort`, `dtBenutzergruppe`) VALUES (NULL, 'admin', 'admin', 'admin');
INSERT INTO `fvs_datenbank`.`tblBenutzer` (`idBenutzerNummer`, `dtLogin`, `dtPasswort`, `dtBenutzergruppe`) VALUES (NULL, 'kommandant', 'kommandant', 'kommandant');
INSERT INTO `fvs_datenbank`.`tblBenutzer` (`idBenutzerNummer`, `dtLogin`, `dtPasswort`, `dtBenutzergruppe`) VALUES (NULL, 'unterkommandant', 'unterkommandant', 'unterkommandant');
INSERT INTO `fvs_datenbank`.`tblBenutzer` (`idBenutzerNummer`, `dtLogin`, `dtPasswort`, `dtBenutzergruppe`) VALUES (NULL, 'sekretaer', 'sekretaer', 'sekretaer');
INSERT INTO `fvs_datenbank`.`tblBenutzer` (`idBenutzerNummer`, `dtLogin`, `dtPasswort`, `dtBenutzergruppe`) VALUES (NULL, 'inventarist', 'inventarist', 'inventarist');
INSERT INTO `fvs_datenbank`.`tblBenutzer` (`idBenutzerNummer`, `dtLogin`, `dtPasswort`, `dtBenutzergruppe`) VALUES (NULL, 'maschinist', 'maschinist', 'maschinist');

COMMIT;

-- -----------------------------------------------------
-- Data for table `fvs_datenbank`.`tblPosition`
-- -----------------------------------------------------
START TRANSACTION;
USE `fvs_datenbank`;
INSERT INTO `fvs_datenbank`.`tblPosition` (`idPositionsName`) VALUES ('Kommandant');
INSERT INTO `fvs_datenbank`.`tblPosition` (`idPositionsName`) VALUES ('Unter-Kommandant');
INSERT INTO `fvs_datenbank`.`tblPosition` (`idPositionsName`) VALUES ('Kassierer');
INSERT INTO `fvs_datenbank`.`tblPosition` (`idPositionsName`) VALUES ('Sekretär');
INSERT INTO `fvs_datenbank`.`tblPosition` (`idPositionsName`) VALUES ('Inventarist');
INSERT INTO `fvs_datenbank`.`tblPosition` (`idPositionsName`) VALUES ('Fahrzeugmaschinist');
INSERT INTO `fvs_datenbank`.`tblPosition` (`idPositionsName`) VALUES ('Mitglied');

COMMIT;
