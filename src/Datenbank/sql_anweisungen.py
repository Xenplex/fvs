# -*- coding: utf-8 -*-

# Ritchie Flick
# T3IFAN 2012/2013 Abschlussprojekt
# LN
# 'Feuerwehrverwaltungssoftware - FVS'
# 'src/Datenbank/sql_anweisungen.py'

""" **Beschreibung**

*sql_anweisungen.py* enthält Funktionen mit welchen beliebige SQL Anweisungen
generiert werden können.
"""


def sql(tabelle, select="*", filtern=None, gruppieren=None, sortieren=None):
    """ *Funktion zur Zusammenstellung einer SQL SELECT Anweisung*

    **Beschreibung**

    Standard *SQL* Anfrage, welche über Parameterübergaben ausgebaut werden
    kann.

    Wenn keine Parameter übergeben wurden, wird eine *Default*
    **sql_anweisung** erstellt und als Rückgabewert zurückgegeben.

    Wenn Parameter übergeben wurden, wird abgefragt welche Parameter gesetzt
    wurden und die **sql_anweisung** wird gegebenfalls ausgebaut.

    **Parameter**

    - select: Parameter zum setzen der eigentlichen SELECT Anweisung.
    - tabelle: Die gewünschte Tabelle
    - filtern: Wenn gesetzt, wird eine *WHERE* Anweisung mit dem übergebenem
      Inhalt erstellt.
    - gruppieren: Wenn gesetzt, wird eine *GROUP BY* Anweisung mit dem
      übergebenem Inhalt erstellt.
    - sortieren: Wenn gesetzt, wird eine *ORDER BY* Anweisung mit dem
      übergebenem Inhalt erstellt.

    """
    sql_anweisung = """ SELECT %s
                        FROM %s
                    """ % (select, tabelle)

    # Hier wird die "Standard" SQL Anfrage, gemäß der gesetzten Parameter nach
    # Bedürfnis ausgebaut
    if filtern:
        sql_anweisung = sql_anweisung + """ WHERE %s
                                        """ % (filtern)

    if gruppieren:
        sql_anweisung = sql_anweisung + """ GROUP BY %s
                                        """ % (gruppieren)
    if sortieren:
        sql_anweisung = sql_anweisung + """ ORDER BY %s
                                        """ % (sortieren)
    return sql_anweisung


def null_test(tabelle, null_feld, felder):
    """ *Sucht alle Einträge in welchen eine bestimmte Zelle* **NULL** *ist*

    **Beschreibung**

    Funktion mit welcher erkannt werden kann, welche Felder in einer Tabelle
    keinen Inhalt (also *NULL*) enthalten.

    **Parameter**

    - tabelle: Tabelle welche kontrolliert werden soll
    - null_feld: Kolonne welches auf *NULL* Feld kontrolliert werden soll
    - felder: Kolonnen welche man erhalten will (z.Bsp.: Schlüsselkolonne)

    """
    sql_anweisung = """ SELECT %s
                        FROM %s
                        WHERE %s IS NULL """ % (felder, tabelle, null_feld)
    return sql_anweisung


def hinzufugen(tabelle, felder='', werte=''):
    """ *Ein neuer Eintrag wird in die angegebene Tabelle eingefügt*

    **Beschreibung**

    Ein neuer Eintrag wird in der angegebenen Tabelle mit den übergebenen
    Daten (falls gegeben) erstellt. Wurden keine Werte übergeben, wird ein
    leerer neuer Eintrag erstellt.

    **Parameter**

    - tabelle: Tabelle in welcher der neue Eintrag erstellt werden soll
    - felder: Die gewünschten Kolonnen welche Werte erhalten sollen
    - werte: Die gewünschten Werten mit welchen der neue Eintrag erstellt
      werden soll

    """
    sql_anweisung = """ INSERT INTO %s(%s)
                        VALUES (%s)
                    """ % (tabelle, felder, werte)
    return sql_anweisung


def entfernen(tabelle, filtern=None):
    """ *Angegebener Eintrag wird aus der entsprechenden Tabelle gelöscht*

    **Beschreibung**

    Löscht den angegebenen Eintrag aus der jeweiligen Tabelle.

    **Parameter**

    - tabelle: Die Tabelle aus welcher ein Eintrag gelöscht werden soll
    - filtern: Wird gesetzt falls nur ein bestimmter oder mehrer Einträge
      nach einer gewissen Angabe gelöscht werden sollen.

    """
    sql_anweisung = """ DELETE FROM %s
                    """ % (tabelle)
    if filtern:
        sql_anweisung = sql_anweisung + """ WHERE %s
                                        """ % (filtern)
    return sql_anweisung


def aktualisieren(tabelle, aktualisierung, filtern):
    """ *Der angegebene Eintrag wird mit den jeweiligen Werten aktualisiert*

    **Beschreibung**

    Funktion mit welcher der angegebene Eintrag oder Einträge aktualisiert
    werden.

    **Parameter**

    - tabelle: Tabelle in welcher ein Eintrag aktualisiert werden soll
    - aktualisierung: Die gewünschte Aktualisierung/ der gewünschte neue Wert
    - filtern: Der gewünschte Filter.

    """
    sql_anweisung = """ UPDATE %s
                        SET %s
                        WHERE %s
                    """ % (tabelle, aktualisierung, filtern)
    return sql_anweisung
