# -*- coding: utf-8 -*-

# Ritchie Flick
# T3IFAN 2012/2013 Abschlussprojekt
# LN
# 'Feuerwehrverwaltungssoftware - FVS'
# 'src/Datenbank/admin_db_verbindung.py'

# Importieren von FVS Modulen
from Datenbank import db_verbindung


class Admin_DB_Verbindung(db_verbindung.DB_Verbindung):
    """ *Datenbankverbindung*

    **Beschreibung**

    Die Klasse **Admin_DB_Verbindung** erbt von der Klasse **DB_Verbindung**.

    Weitere Methoden speziell für den *Administrator* werden hier
    implementiert.

    **Parameter**

    - db_parameter: Enthält Liste mit den Verbindungsparametern welche
      benötigt werden um eine Verbindung zur Datenbank aufzubauen

      - db_parameter[0]: IP Adresse des Servers
      - db_parameter[1]: Port Nummer auf die der Server hört
      - db_parameter[2]: Der Name der zu verwendeten Datenbank
      - db_parameter[3]: Der Benutzername der Datenbank
      - db_parameter[4]: Das Passwort des Benutzers

    """
    def __init__(self, db_parameter):
        db_verbindung.DB_Verbindung.__init__(self, db_parameter)
        super(Admin_DB_Verbindung, self).__init__(db_parameter)

    def tabelle_kontrolle(self, tabelle):
        """ *Fehler in der angegebenen Tabelle werden zurückgegeben*

        **Beschreibung**

        Die angegebene Tabelle wird durch den eingebauten SQL Befehl
        *CHECKT TABLE* auf Fehler untersucht.

        **Parameter**

        - tabelle: Die Tabelle welche mit dem SQL Statement *CHECK TABLE*
          kontrolliert weden soll.

        **Rückgabewerte**

        - kontroll_resultat: Das Resultat der SQL Anfrage

        """
        sql_anweisung = "CHECK TABLE %s EXTENDED" % (tabelle)
        kontroll_resultat = self.execute(sql_anweisung)
        return kontroll_resultat

    def tabelle_reparieren(self, tabelle):
        """ *Die angegebene Tabelle wird repariert*

        **Beschreibung**

        Die angegebene Tabelle wird mit dem eingebautenem SQL Befehl
        *OPTIMIZE TABLE* repariert.

        **Parameter**

        - tabelle: Die Tabelle welche mit dem SQL Statement repariert werden
          soll.

        """
        sql_anweisung = "REPAIR TABLE %s" % (tabelle)
        self.execute(sql_anweisung)

    def tabelle_optimieren(self, tabelle):
        """ *Die angegebene Tabelle wird optimiert*

        **Beschreibung**

        Die angegebene Tabelle wird mit einem eingebautenem SQL Befehl
        *OPTIMIZE TABLE* optimiert.

        **Parameter**

        - tabelle: Die Tabelle welche optimiert werden soll.

        """
        sql_anweisung = "OPTIMIZE TABLE %s" % (tabelle)
        self.execute(sql_anweisung)

    def liste_aller_tabellen(self):
        """ *Eine Liste aller Tabellen in der Datenbank erzeugen*

        **Beschreibung**

        Es wird eine Liste mit allen Tabellen welche in der Datenbank
        existieren erzeugt mit dem eingebautenem SQL Befehl *SHOW TABLES*.

        **Rückgabewerte**

        - tabellen_liste: Eine Liste aller Tabellen in der Datenbank

        """
        sql_anweisung = "SHOW TABLES"
        tabellen_liste = self.execute(sql_anweisung)
        return tabellen_liste
