Kontroller
===========

Die *Kontroller* dienen dazu die verschiedenen *Signale* der **QMLUI** zu
empfangen (*Slots*) und die gewünschten Funktionen aufzurufen.

Operationen
-------------

.. automodule:: Kontroller.operationen
  :members:

Button
-------

.. automodule:: Kontroller.button
   :members:

Listen
-------

.. automodule:: Kontroller.listen
   :members:

JaNeinAuswahl
--------------

.. automodule:: Kontroller.jaNeinAuswahl
   :members:
