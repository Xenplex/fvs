# -*- coding: utf-8 -*-

# Ritchie Flick
# T3IFAN 2012/2013 Abschlussprojekt
# LN
# 'Feuerwehrverwaltungssoftware - FVS'
# 'src/Kontroller/listen.py'

# Importieren von PySide Modulen
from PySide import QtCore


class Kontroller(QtCore.QObject):
    """ *Kontroller für eine ListenAnsicht Komponente der QMLUI* """
    def __init__(self, basisklasse, basisklasse_funktionen):
        super(Kontroller, self).__init__()
        self.basisklasse = basisklasse
        self.basisklasse_funktionen = basisklasse_funktionen

    @QtCore.Slot(QtCore.QObject, str)
    def eintrag_ausgewaehlt(self, packung, listeID):
        """ *Aufgerufen wenn Eintrag in der Fahrzeugliste ausgewählt wurde*

        **Beschreibung**

        Die gewünschte Funktion der Basisklasse wird aufgerufen, abhängig davon
        von welcher Liste das Signal ausgegangen ist.
        *packung* wird von der **QMLUI** übergeben und stellt den Inhalt
        (als **QObject** verpackt) des momentan ausgewählten Eintrages dar.
        """
        # [0] :: Boolean Wert ob die Funktion 'packung' als Parameter benötigt
        # [1] :: Die eigentliche Funktion
        for schlussel, funktion in self.basisklasse_funktionen.items():
            if listeID == schlussel:
                if funktion[0]:
                    funktion[1](packung)
                else:
                    funktion[1]()
