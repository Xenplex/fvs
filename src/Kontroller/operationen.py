# -*- coding: utf-8 -*-

# Ritchie Flick
# T3IFAN 2012/2013 Abschlussprojekt
# LN
# 'Feuerwehrverwaltungssoftware - FVS'
# 'src/Kontroller/operationen.py'

# Importieren von PySide Modulen
from PySide import QtCore


class Kontroller(QtCore.QObject):
    """ *Kontroller für Programminterne Signale*

    **Beschreibung**

    **Operationen** ist ein Kontroller welche die Signale innerhalb
    von **FVS** verwaltet. Die Signale werden vom Programm für das Programm
    gesendet und sind unabhängig des Benutzers.
    Dieser *Kontroller* wird verwendet um möglichen Problemen zwischen der
    **QMLUI** und des *Python* Unterbaus zu umgehen (Bsp.: *Python* soll
    Variablen von der **QMLUI** einlesen, jedoch ist die **QMLUI** langsamer
    als *Python* und hat die Variablen noch nicht gesetzt)
    """
    def __init__(self, basisklasse, basisklasse_funktionen):
        super(Kontroller, self).__init__()
        self.basisklasse = basisklasse
        self.basisklasse_funktionen = basisklasse_funktionen

    @QtCore.Slot(str)
    def operations_signal(self, operationID):
        """ *Aufgerufen wenn ein Programminternes Signal gesendet wurde*

        **Beschreibung**

        Die gewünschte Funktion der Basisklasse wird aufgerufen, abhängig davon
        von welches Signal gesendet wurde.
        """
        for schlussel, funktion in self.basisklasse_funktionen.items():
            if operationID == schlussel:
                funktion()
