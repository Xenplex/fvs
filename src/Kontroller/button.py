# -*- coding: utf-8 -*-

# Ritchie Flick
# T3IFAN 2012/2013 Abschlussprojekt
# LN
# 'Feuerwehrverwaltungssoftware - FVS'
# 'src/Kontroller/button.py'

# Importieren von PySide Modulen
from PySide import QtCore


class Kontroller(QtCore.QObject):
    """ *Kontroller für die Button Komponente der QMLUI* """
    def __init__(self, basisklasse, basisklasse_funktionen):
        super(Kontroller, self).__init__()
        self.basisklasse = basisklasse
        self.basisklasse_funktionen = basisklasse_funktionen

    @QtCore.Slot(str)
    def button_gedruckt(self, buttonID):
        """ *Aufgerufen wenn ein Button gedrückt wurde*

        **Beschreibung**

        Die gewünschte *Funktion* der *Basisklasse* wird aufgerufen, abhängig
        davon von welchem **Button** das *Signal* ausgegangen ist.
        """
        for schlussel, funktion in self.basisklasse_funktionen.items():
            if buttonID == schlussel:
                funktion()
