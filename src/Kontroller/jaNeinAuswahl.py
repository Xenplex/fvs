# -*- coding: utf-8 -*-

# Ritchie Flick
# T3IFAN 2012/2013 Abschlussprojekt
# LN
# 'Feuerwehrverwaltungssoftware - FVS'
# 'src/Kontroller/jaNeinAuswahl.py'

# Importieren von PySide Modulen
from PySide import QtCore


class Kontroller(QtCore.QObject):
    """ *Kontroller für die JaNeinAuswahl Komponente der QMLUI* """
    def __init__(self, basisklasse, basisklasse_funktionen):
        super(Kontroller, self).__init__()
        self.basisklasse = basisklasse
        self.basisklasse_funktionen = basisklasse_funktionen

    @QtCore.Slot(str, str)
    def status_geaendert(self, auswahlID, status):
        """ *Aufgerufen wenn der Status der JaNeinAuswahl getoggled wurde*

        **Beschreibung**
        Die gewünschte *Funktion* der *Basisklasse* wird aufgerufen, abhängig
        davon von welcher **JaNeinAuswahl** das *Signal* ausgegangen ist.

        **Parameter**

        - auswahlID: Die *ID* der **JaNeinAuswahl**
        - status: Der momentane *Status* der **JaNeinAuswahl**

        """
        for schlussel, funktion in self.basisklasse_funktionen.items():
            if auswahlID == schlussel:
                funktion(status)
