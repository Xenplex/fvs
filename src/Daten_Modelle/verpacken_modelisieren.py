# -*- coding: utf-8 -*-

# Ritchie Flick
# T3IFAN 2012/2013 Abschlussprojekt
# LN
# 'Feuerwehrverwaltungssoftware - FVS'
# 'src/Daten_Modelle/verpacken_modelisieren.py'

""" **Beschreibung**

Enthält *Funktionen* welche zum "verpacken" von Datensätzen in ein **QObject**
benötigt werden und das anschließende einbinden in die entsprechende *Modell*
Klasse.
"""

# Importieren von FVS Modulen
from Daten_Modelle import list_modell
from Daten_Modelle import verpackung


def verpacken(datensatz):
    """ *Der übergebene Parameter wird verpackt*

    **Beschreibung**

    Für die Daten Modelle müssen die Daten Objekte in ein **QObject** verpackt
    werden damit *Qt* und die **QMLUI** damit arbeiten können.

    **Parameter**

    - datensatz: Daten Objekt welches in ein **QObject** verpackt werden soll

    **Rückgabewerte**

    - verpackte_daten: Datensatz verpackt als ein **QObject**

    """
    verpackte_daten = [verpackung.DatenVerpackung(
        daten) for daten in datensatz]
    return verpackte_daten


def list_verpackung_modelisieren(datensatz):
    """ *Verpackung & Modelisierung eines Datensatzes*

    **Beschreibung**

    Das übergebene Daten Objekt wird als erstes durch die Funktion
    **verpacken** (*datensatz*) in ein **QObject** verpackt und anschließend
    ins Daten Modell für eine **ListenAnsicht** (*ListView*) modelisiert.

    **Parameter**

    - datensatz: Daten Objekt welches verpackt und modelisiert werden soll

    **Rückgabewerte**

    - datenliste: Verpacktes Daten Objekt in ein **ListenAnsicht** Modell
      modelisiert.

    """
    verpackte_daten = verpacken(datensatz)
    datenliste = list_modell.ListenModell(verpackte_daten)
    return datenliste
