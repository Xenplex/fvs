# -*- coding: utf-8 -*-

# Ritchie Flick
# T3IFAN 2012/2013 Abschlussprojekt
# LN
# 'Feuerwehrverwaltungssoftware - FVS'
# 'src/Daten_Modelle/list_modell.py'

# Importieren von PySide Modulen
from PySide.QtCore import QAbstractListModel
from PySide.QtCore import QModelIndex
from PySide.QtCore import Qt


class ListenModell(QAbstractListModel):
    """ *Modellklasse für einen Datensatz mit Listenstruktur*

    **Beschreibung**

    *ListenModell* ist die Modellklasse für Datensätze, welche als Liste
    dargestellt werden sollen.

    **Parameter**

    - datensatz: Der Datensatz welcher ins **ListenModell** modelisiert
      werden soll.

    """
    KOLONNEN = ('eintrag',)

    def __init__(self, datensatz):
        QAbstractListModel.__init__(self)
        self.datensatz = datensatz

        # Erstellt ein Attribut durch welchen die einzelnen 'verpackten'
        # Datensätze in QML angesprochen werden können
        self.setRoleNames(dict(enumerate(ListenModell.KOLONNEN)))

    # Folgende Methoden müssen implementiert sein, damit PySide und Qt
    # richtig funktionieren
    # (Aufbau und Funktionalität sind vorgeschrieben)
    def rowCount(self, parent=QModelIndex()):
        """ *Muss in jeder Modellklasse für die *View* Klasse implementiert
        sein*
        """
        return len(self.datensatz)

    def data(self, index, role=Qt.DisplayRole):
        """ *Muss in jeder Modellklasse für die View Klasse implementiert
        sein*
        """
        return self.datensatz[index.row()]
