Daten_Modelle
==============

**Daten_Modelle** enthält Dateien zur Modelisierung von Daten Objekten,
nach dem *MVC* Prinzip.

list_modell
--------------

.. automodule:: Daten_Modelle.list_modell
   :members:

verpacken_modelisieren
-----------------------

.. automodule:: Daten_Modelle.verpacken_modelisieren
   :members:

verpackung
------------

.. automodule:: Daten_Modelle.verpackung
   :members: