# -*- coding: utf-8 -*-

# Ritchie Flick
# T3IFAN 2012/2013 Abschlussprojekt
# LN
# 'Feuerwehrverwaltungssoftware - FVS'
# 'src/Daten_Modelle/verpackung.py'

# Importieren von PySide Modulen
from PySide import QtCore


class DatenVerpackung(QtCore.QObject):
    """ *Klasse zum Verpacken eines Python Objektes in ein* **QObject**

    **Beschreibung**

    Klasse welche als *Verpackung* für ein **QObject** verwendet wird. Der
    Aufbau der Klasse ist vorgegeben und wird von *Qt* so benötigt.

    **Parameter**

    - datensatz: Datensatz welcher *verpackt* werden soll

    """
    def __init__(self, datensatz):
        QtCore.QObject.__init__(self)
        self.datensatz = datensatz

    def _name(self):
        return str(self.datensatz)

    # Neue Instanz eines Signals
    veraendert = QtCore.Signal()

    # Erstellung eines Attributes, mit welchem in QML (GUI) auf dieses Objekt
    # zugreifen werden kann
    verpackung = QtCore.Property(str, _name, notify=veraendert)
