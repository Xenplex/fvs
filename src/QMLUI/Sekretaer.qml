/*
    Ritchie Flick
    T3IFAN 2012/2013 Abschlussprojekt
    LN
    'Feuerwehrverwaltungssoftware - FVS'
    'src/QMLUI/Sekretaer.qml'

     Erstellung des Hauptschirms des Sekretärs
*/

import QtQuick 1.1
import "../QML_Komponenten"

Rectangle {
    id: sekretaer_schirm

    // Folgende Funtkion zum Kontrollieren von Datumen wurde kopiert von:
    // http://www.qodo.co.uk/blog/javascript-checking-if-a-date-is-valid/

    // Checks a string to see if it in a valid date format
    // of (D)D/(M)M/(YY)YY and returns true/false
    function isValidDate(s) {
        // format D(D)/M(M)/(YY)YY
        var dateFormat = /^\d{1,4}[\.|\/|-]\d{1,2}[\.|\/|-]\d{1,4}$/;

        if (dateFormat.test(s)) {
            // remove any leading zeros from date values
            s = s.replace(/0*(\d*)/gi,"$1");
            var dateArray = s.split(/[\.|\/|-]/);

            // correct month value
            dateArray[1] = dateArray[1]-1;

            // correct year value
            if (dateArray[2].length<4) {
                // correct year value
                dateArray[2] = (parseInt(dateArray[2]) < 50) ? 2000 + parseInt(dateArray[2]) : 1900 + parseInt(dateArray[2]);
            }

            var testDate = new Date(dateArray[2], dateArray[1], dateArray[0]);
            if (testDate.getDate()!=dateArray[0] || testDate.getMonth()!=dateArray[1] || testDate.getFullYear()!=dateArray[2]) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    // Attribute auf welche der Python Unterbau zugreifen kann
    property string edtProtokollDatum
    property string edtProtokollBezeichnung
    property string edtProtokollInhalt
    property bool einsatzVerwaltung
    property string edtEreignisBezeichnung
    property string edtEreignisTyp
    property string edtEreignisStartdatum
    property string edtEreignisStartzeit
    property string edtEreignisEnddatum
    property string edtEreignisEndzeit
    property string edtEinsatzTyp
    property string edtOrtschaft
    property string edtAdresse
    property string edtPostleitzahl
    property string edtBetreffenderName
    property string edtBetreffenderVorname

    // Attribute auf welche der Python Unterbau zugreift werden gesetzt
    function protokoll_info_abspeichern_qml() {
        edtProtokollDatum = datum.text
        if (!(isValidDate(edtProtokollDatum))) {
            return
        }
        edtProtokollBezeichnung = protokoll_bezeichnung.text
        edtProtokollInhalt = protokoll.text
        operationen_kontroller.operations_signal("protokoll_variablen_gesetzt")
    }

    function abspeichern_ereignis_qml() {
        edtEreignisBezeichnung = bezeichnung.text
        edtEreignisTyp = ereignis_typ.auswahlText
        edtEreignisStartdatum = startdatum.text
        if (!(isValidDate(edtEreignisStartdatum)))
            return
        edtEreignisStartzeit = startzeit.text
        edtEreignisEnddatum = enddatum.text
        if (!(isValidDate(edtEreignisEnddatum)))
            return
        edtEreignisEndzeit = endzeit.text
        edtEinsatzTyp = einsatztyp_auswahl.auswahlText
        edtOrtschaft = ortschaft.text
        edtAdresse = adresse.text
        edtPostleitzahl = postleitzahl.text
        edtBetreffenderName = betreffender_name.text
        edtBetreffenderVorname = betreffender_vorname.text
        operationen_kontroller.operations_signal("ereignis_daten")
    }

    // Funktionen zum ändern der States
    function protokoll_verwaltung_qml() {
        sekretaer_schirm.state = "protokoll"
    }

    function zum_hauptmenu_qml() {
        sekretaer_schirm.state = "start"
    }

    function ereignis_verwaltung_qml() {
        sekretaer_schirm.state = "ereignis"
        einsatzVerwaltung = false
    }

    function einsatz_verwaltung_qml() {
        sekretaer_schirm.state = "einsatz"
        einsatzVerwaltung = true
    }

    // Attribute aus dem Python Unterbau werden gelesen und gesetzt
    function protokoll_info_setzen_qml() {
        datum.text = protokoll_datum
        protokoll_bezeichnung.text = protokoll_bezeichnung_info
        referenzen.daten_modell = protokoll_referenzen
        protokoll.text = protokoll_inhalt
    }

    function ereignisInfo_qml() {
        bezeichnung.text = ereignis_bezeichnung
        ereignis_typ.auswahlText = ereignis_typ_t
        startdatum.text = ereignis_startdatum
        startzeit.text = ereignis_startzeit
        enddatum.text = ereignis_enddatum
        endzeit.text = ereignis_endzeit
        teilnehmer_liste.daten_modell = teilnahme_liste
    }

    function einsatzInfo_qml() {
        einsatztyp_auswahl.auswahlText = einsatz_einsatztyp
        ortschaft.text = einsatz_ortschaft
        adresse.text = einsatz_adresse
        postleitzahl.text = einsatz_postleitzahl
        betreffender_name.text = betreff_name
        betreffender_vorname.text = betreff_vorname
    }

    // Startstate
    state: "start"

    // Hauptfenster Attribute - werden überschrieben da FVS im FullScreen Modus
    // aufgerufen wird
    width: 1024
    height: 768

    // Hauptkomponenten
    ListenContainer {
        id: auswahl_liste
        anchors.left: parent.left
        hoehe: parent.height
        breite: parent.width / 5
        eintragHoehe: 50
    }

    InfoText {
        id: uberschrift
        visible: false
        schriftFarbe: "black"
        schriftGroesse: 50
        text: "Sekretär"
        x: parent.width / 2.5
        anchors.top: parent.top
    }

    Button {
        id: neu_eintrag
        visible: false
        buttonText: "Neuer Eintrag"
        anchors {
            bottom: auswahl_liste.bottom
            bottomMargin: 10
            left: auswahl_liste.left
        }
    }

    Button {
        id: entfernen_eintrag
        visible: false
        buttonText: "Entfernen Eintrag"
        anchors {
            top: neu_eintrag.top
            left: neu_eintrag.right
            leftMargin: 10
            right: auswahl_liste.right
        }
    }

    // Hauptmenü
    Item {
        id: hauptmenu
        visible: true
        anchors {
            top: parent.top
            left: auswahl_liste.right
            right: parent.right
            topMargin: 70
        }
        height: parent.height / 13

        Button {
            id: protokoll_verwaltung
            buttonID: "btnProtokollVerwaltung"
            buttonText: "Protokoll\nVerwaltung"
            anchors {
                left: parent.left
                leftMargin: 10
            }
        }

        Button {
            id: ereignis_verwaltung
            buttonID: "btnEreignisVerwaltung"
            buttonText: "Ereignis\nVerwaltung"
            anchors {
                left: protokoll_verwaltung.right
                top: protokoll_verwaltung.top
                leftMargin: 10
            }
        }

        Button {
            id: einsatzbericht_verwaltung
            buttonID: "btnEinsatzberichtVerwaltung"
            buttonText: "Einsatzbericht\nVerwaltung"
            anchors {
                left: ereignis_verwaltung.right
                top: ereignis_verwaltung.top
                leftMargin: 10
            }
        }
    }

    Button {
        id: zum_hauptmenu
        buttonID: "btnZumHauptmenu"
        buttonText: "Hauptmenü"
        visible: false
        anchors {
            top: parent.top
            left: auswahl_liste.left
            topMargin: 30
            leftMargin: 300
        }
        width: 200
    }

    InfoText {
        id: uberschrift_protokoll
        visible: false
        text: "Protokoll Verwaltung"
        schriftGroesse: 30
        schriftFarbe: "black"
        anchors {
            left: zum_hauptmenu.right
            top: zum_hauptmenu.top
            leftMargin: 30
        }
    }

    InfoText {
        id: uberschrift_ereignis
        visible: false
        text: "Ereignis Verwaltung"
        schriftGroesse: 30
        schriftFarbe: "black"
        anchors {
            left: zum_hauptmenu.right
            top: zum_hauptmenu.top
            leftMargin: 30
        }
    }

    InfoText {
        id: uberschrift_einsatz
        visible: false
        text: "Einsatz Verwaltung"
        schriftGroesse: 30
        schriftFarbe: "black"
        anchors {
            left: zum_hauptmenu.right
            top: zum_hauptmenu.top
            leftMargin: 30
        }
    }

    Item {
        id: protokoll_menu
        visible: false
        anchors {
            top: hauptmenu.bottom
            bottom: parent.bottom
            left: auswahl_liste.right
            right: parent.right
        }

        InfoText {
            id: referenzen_text
            text: "Referenzen"
            anchors {
                left: referenzen.left
                bottom: referenzen.top
                bottomMargin: 10
            }
        }

        ListenContainer {
            id: referenzen
            listeID: "lstReferenzen"
            anchors {
                left: entfernen.right
                top: entfernen.top
                leftMargin: 30
            }
            hoehe: 300
            breite: 300
        }

        Button {
            id: protokoll_abspeichern
            buttonID: "btnProtokollAbspeichern"
            buttonText: "Abspeichern"
            anchors {
                left: referenzen.right
                top: referenzen.top
                leftMargin: 10
                topMargin: 30
            }
        }

        Button {
            id: protokoll_bericht
            buttonID: "btnProtokollBericht"
            buttonText: "Bericht\nerstellen"
            anchors {
                left: protokoll_abspeichern.left
                top: protokoll_abspeichern.bottom
                topMargin: 10
            }
        }

        InfoText {
            id: ereignis_auswahl_text
            text: "Ereignisse"
            anchors {
                left: ereignis_auswahl.left
                bottom: ereignis_auswahl.top
                bottomMargin: 10
            }
        }

        AuswahlListe {
            id: ereignis_auswahl
            comboID: "cmbEreignisAuswahl"
            anchors {
                top: parent.top
                left: parent.left
                leftMargin: 20
                rightMargin: 20
            }
        }

        Button {
            id: ereignis_hinzu
            buttonID: "btnEreignisHinzu"
            buttonText: "Ereignis\nHinzufügen"
            anchors {
                top: ereignis_auswahl.bottom
                left: ereignis_auswahl.left
                topMargin: 10
            }
        }

        InfoText {
            id: inventar_auswahl_text
            text: "Inventar"
            anchors {
                bottom: inventar_auswahl.top
                left: inventar_auswahl.left
                bottomMargin: 10
            }
        }

        AuswahlListe {
            id: inventar_auswahl
            comboID: "cmbInventarAuswahl"
            anchors {
                top: ereignis_auswahl.top
                left: ereignis_auswahl.right
                leftMargin: 10
            }
            eintrag_hohe: 50
        }

        Button {
            id: inventar_hinzu
            buttonID: "btnInventarHinzu"
            buttonText: "Inventar\nHinzufügen"
            anchors {
                top: inventar_auswahl.bottom
                left: inventar_auswahl.left
                topMargin: 10
            }
        }

        InfoText {
            id: mitglied_auswahl_text
            text: "Mitglieder"
            anchors {
                left: mitglied_auswahl.left
                bottom: mitglied_auswahl.top
                bottomMargin: 10
            }
        }

        AuswahlListe {
            id: mitglied_auswahl
            comboID: "cmbMitgliedAuswahl"
            anchors {
                top: inventar_auswahl.top
                left: inventar_auswahl.right
                leftMargin: 10
            }
        }

        Button {
            id: mitglied_hinzu
            buttonID: "btnMitgliedHinzu"
            buttonText: "Mitglied\nHinzufügen"
            anchors {
                top: mitglied_auswahl.bottom
                left: mitglied_auswahl.left
                topMargin: 10
            }
        }

        InfoText {
            id: fahrzeug_auswahl_text
            text: "Fahrzeuge"
            anchors {
                left: fahrzeug_auswahl.left
                bottom: fahrzeug_auswahl.top
                bottomMargin: 10
            }
        }

        AuswahlListe {
            id: fahrzeug_auswahl
            comboID: "cmbFahrzeugAuswahl"
            anchors {
                top: mitglied_auswahl.top
                left: mitglied_auswahl.right
                leftMargin: 10
            }
        }

        Button {
            id: fahrzeug_hinzu
            buttonID: "btnFahrzeugHinzu"
            buttonText: "Fahrzeug\nHinzufügen"
            anchors {
                top: fahrzeug_auswahl.bottom
                left: fahrzeug_auswahl.left
                topMargin: 10
            }
        }

        Button {
            id: entfernen
            buttonID: "btnEntfernen"
            buttonText: "Referenz\nEntfernen"
            anchors {
                top: ereignis_hinzu.bottom
                left: ereignis_hinzu.left
                topMargin: 50
            }
        }

        InfoText {
            id: datum_text
            text: "Protokoll Datum"
            anchors {
                left: datum.left
                bottom: datum.top
                bottomMargin: 10
            }
        }

        TextEingabe {
            id: datum
            inputID: "edtDatum"
            eingabeHilfe: "00-00-0000"
            anchors {
                top: entfernen.bottom
                left: entfernen.left
                topMargin: 30
            }
        }

        InfoText {
            id: protokoll_bezeichnung_text
            text: "Protokoll Bezeichnung"
            anchors {
                left: datum.left
                top: datum.bottom
                topMargin: 10
            }
        }

        TextEingabe {
            id: protokoll_bezeichnung
            inputID: "edtBezeichnung"
            anchors {
                left: protokoll_bezeichnung_text.left
                top: protokoll_bezeichnung_text.bottom
                topMargin: 10
            }
        }

        InfoText {
            id: protokoll_text
            text: "Protokoll Inhalt"
            anchors {
                left: protokoll.left
                bottom: protokoll.top
                margins: 10
            }
        }

        Memo {
            id: protokoll
            memoID: "memProtokoll"
            anchors {
                left: parent.left
                right: parent.right
                top: referenzen.bottom
                bottom: parent.bottom
                topMargin: 30
            }
        }
    }

    Item {
        id: ereignis_menu
        visible: false
        anchors {
            top: hauptmenu.bottom
            bottom: parent.bottom
            left: auswahl_liste.right
        }
        width: parent.width / 2

        InfoText {
            id: bezeichnung_text
            text: "Bezeichnung"
            anchors {
                top: parent.top
                left: parent.left
                leftMargin: 10
            }
        }

        TextEingabe {
            id: bezeichnung
            inputID: "edtBezeichnung"
            anchors {
                top: bezeichnung_text.top
                left: bezeichnung_text.right
                leftMargin: 10
            }
        }

        InfoText {
            id: ereignis_typ_text
            text: "Ereignis Typ"
            anchors {
                left: bezeichnung_text.left
                top: bezeichnung_text.bottom
                topMargin: 10
            }
        }

        AuswahlListe {
            id: ereignis_typ
            anchors {
                top: bezeichnung.bottom
                left: bezeichnung.left
                topMargin: 10
            }
        }

        InfoText {
            id: startdatum_text
            text: "Startdatum"
            anchors {
                left: ereignis_typ_text.left
                top: ereignis_typ_text.bottom
                topMargin: 10
            }
        }

        TextEingabe {
            id: startdatum
            inputID: "edtStartdatum"
            eingabeHilfe: "00-00-0000"
            anchors {
                top: ereignis_typ.bottom
                left: ereignis_typ.left
                topMargin: 10
            }
        }

        InfoText {
            id: startzeit_text
            text: "Startzeit"
            anchors {
                left: startzeit.right
                top: startzeit.top
                leftMargin: 10
            }
        }

        TextEingabe {
            id: startzeit
            inputID: "edtStartzeit"
            anchors {
                top: startdatum.top
                left: startdatum.right
                leftMargin: 10
            }
        }

        InfoText {
            id: enddatum_text
            text: "Enddatum"
            anchors {
                left: startdatum_text.left
                top: startdatum_text.bottom
                topMargin: 10
            }
        }

        TextEingabe {
            id: enddatum
            inputID: "edtEnddatum"
            eingabeHilfe: "00-00-0000"
            anchors {
                top: startdatum.bottom
                left: startdatum.left
                topMargin: 10
            }
        }

        InfoText {
            id: endzeit_text
            text: "Endzeit"
            anchors {
                left: endzeit.right
                top: endzeit.top
                leftMargin: 10
            }
        }

        TextEingabe {
            id: endzeit
            inputID: "edtEndzeit"
            anchors {
                top: enddatum.top
                left: enddatum.right
                leftMargin: 10
            }
        }

        InfoText {
            id: teilnehmer_liste_text
            text: "Teilnehmerliste"
            anchors {
                left: startdatum.left
                top: enddatum.bottom
                right: endzeit.right
                topMargin: 50
            }
        }

        ListenContainer {
            id: teilnehmer_liste
            listeID: "lstTeilnehmer"
            anchors {
                left: teilnehmer_liste_text.left
                right: teilnehmer_liste_text.right
                top: teilnehmer_liste_text.bottom
                topMargin: 10
            }
            hoehe: 300
        }

        Button {
            id: teilnehmer_hinzu
            buttonID: "btnTeilnehmerHinzu"
            buttonText: "Teilnehmer\nHinzufügen"
            anchors {
                left: teilnehmer_liste.left
                top: teilnehmer_liste.bottom
                topMargin: 10
            }
        }

        Button {
            id: teilnehmer_entfernen
            buttonID: "btnTeilnehmerEntfernen"
            buttonText: "Teilnehmer\nEntfernen"
            anchors {
                right: teilnehmer_liste.right
                top: teilnehmer_hinzu.top
                leftMargin: 10
            }
        }

        InfoText {
            id: teilnehmer_auswahl_text
            text: "Teilnehmer Auswahl"
            anchors {
                left: teilnehmer_auswahl.left
                bottom: teilnehmer_auswahl.top
                bottomMargin: 10
            }
        }

        AuswahlListe {
            id: teilnehmer_auswahl
            comboID: "cmbTeilnehmerAuswahl"
            anchors {
                left: teilnehmer_entfernen.right
                top: teilnehmer_entfernen.top
                leftMargin: 10
            }
        }

        Button {
            id: ereignis_abspeichern
            buttonID: "btnEreignisAbspeichern"
            buttonText: "Abspeichern"
            anchors {
                left: teilnehmer_auswahl_text.left
                right: teilnehmer_auswahl_text.right
                bottom: teilnehmer_auswahl_text.top
                bottomMargin: 10
            }
        }
    }

    Item {
        id: einsatz_menu
        visible: false
        anchors {
            right: parent.right
            top: hauptmenu.bottom
            bottom: parent.bottom
            left: ereignis_menu.right
        }

        InfoText {
            id: einsatztyp_auswahl_text
            text: "Einsatztyp Auswahl"
            anchors {
                right: einsatztyp_auswahl.left
                top: einsatztyp_auswahl.top
                rightMargin: 20
            }
        }

        AuswahlListe {
            id: einsatztyp_auswahl
            anchors {
                left: parent.left
                top: parent.top
                leftMargin: 10
                topMargin: 10
            }
        }

        InfoText {
            id: ortschaft_text
            text:"Ortschaft"
            anchors {
                left: einsatztyp_auswahl_text.left
                top: einsatztyp_auswahl_text.bottom
                topMargin: 10
            }
        }

        TextEingabe {
            id: ortschaft
            inputID: "edtOrtschaft"
            anchors {
                top: einsatztyp_auswahl.bottom
                left: einsatztyp_auswahl.left
                topMargin: 10
            }
        }

        InfoText {
            id: adresse_text
            text: "Adresse"
            anchors {
                left: ortschaft_text.left
                top: ortschaft_text.bottom
                topMargin: 10
            }
        }

        TextEingabe {
            id: adresse
            inputID: "edtAdresse"
            anchors {
                top: ortschaft.bottom
                left: ortschaft.left
                topMargin: 10
            }
        }

        InfoText {
            id: postleitzahl_text
            text: "Postleitzahl"
            anchors {
                left: adresse_text.left
                top: adresse_text.bottom
                topMargin: 10
            }
        }

        TextEingabe {
            id: postleitzahl
            inputID: "edtPostleitzahl"
            anchors {
                top: adresse.bottom
                left: adresse.left
                topMargin: 10
            }
        }

        InfoText {
            id: betreffender_name_text
            text: "Betreffender Name"
            anchors {
                left: postleitzahl_text.left
                top: postleitzahl_text.bottom
                topMargin: 10
            }
        }

        TextEingabe {
            id: betreffender_name
            inputID: "edtBetreffenderName"
            anchors {
                top: postleitzahl.bottom
                left: postleitzahl.left
                topMargin: 10
            }
        }

        InfoText {
            id: betreffender_vorname_text
            text: "Betreffender Vorname"
            anchors {
                left: betreffender_name_text.left
                top: betreffender_name_text.bottom
                topMargin: 10
            }
        }

        TextEingabe {
            id: betreffender_vorname
            inputID: "edtBetreffenderVorname"
            anchors {
                top: betreffender_name.bottom
                left: betreffender_name.left
                topMargin: 10
            }
        }
    }

    states: [
    State {
            name: "protokoll"

            PropertyChanges {
                target: hauptmenu
                visible: false
            }

            PropertyChanges {
                target: neu_eintrag
                buttonID: "btnNeuProtokoll"
                visible: true
            }

            PropertyChanges {
                target: entfernen_eintrag
                buttonID: "btnEntfernenProtokoll"
                visible: true

            }

            PropertyChanges {
                target: uberschrift_protokoll
                visible: true
            }

            PropertyChanges {
                target: zum_hauptmenu
                visible: true
            }

            PropertyChanges {
                target: protokoll_menu
                visible: true
            }

            PropertyChanges {
                target: auswahl_liste
                daten_modell: protokoll_liste
                listeID: "lstProtokolle"
            }

            PropertyChanges {
                target: ereignis_auswahl
                daten_modell: ereignis_liste
            }

            PropertyChanges {
                target: inventar_auswahl
                daten_modell: inventar_liste
            }

            PropertyChanges {
                target: mitglied_auswahl
                daten_modell: mitglied_liste
            }

            PropertyChanges {
                target: fahrzeug_auswahl
                daten_modell: fahrzeug_liste
            }
        },

        State {
            name: "ereignis"

            PropertyChanges {
                target: zum_hauptmenu
                visible: true
            }
            PropertyChanges {
                target: uberschrift_ereignis
                visible: true
            }

            PropertyChanges {
                target: hauptmenu
                visible: false
            }

            PropertyChanges {
                target: ereignis_menu
                visible: true
            }

            PropertyChanges {
                target: ereignis_typ
                daten_modell: ereignis_typen
            }

            PropertyChanges {
                target: teilnehmer_auswahl
                daten_modell: mitglied_liste
            }

            PropertyChanges {
                target: auswahl_liste
                daten_modell: ereignis_liste
                listeID: "lstEreignisse"
            }

            PropertyChanges {
                target: neu_eintrag
                visible: true
                buttonID: "btnNeuEreignis"
            }

            PropertyChanges {
                target: entfernen_eintrag
                visible: true
                buttonID: "btnEntfernenEreignis"
            }

            PropertyChanges {
                target: ereignis_typ
                daten_modell: einsatz_typen
            }
        },

        State {
            name: "einsatz"

            PropertyChanges {
                target: zum_hauptmenu
                visible: true
            }

            PropertyChanges {
                target: uberschrift_einsatz
                visible: true
            }

            PropertyChanges {
                target: hauptmenu
                visible: false
            }

            PropertyChanges {
                target: ereignis_menu
                visible: true
            }

            PropertyChanges {
                target: ereignis_typ
                auswahlText: "Einsatz"
                schreibbar: false
                daten_modell: einsatz_typen
            }

            PropertyChanges {
                target: teilnehmer_auswahl
                daten_modell: mitglied_liste
            }

            PropertyChanges {
                target: auswahl_liste
                daten_modell: ereignis_liste
                listeID: "lstEinsatz"
            }

            PropertyChanges {
                target: einsatz_menu
                visible: true
            }

            PropertyChanges {
                target: neu_eintrag
                visible: true
                buttonID: "btnNeuEreignis"
            }

            PropertyChanges {
                target: entfernen_eintrag
                visible: true
                buttonID: "btnEntfernenEreignis"
            }
        },

        State {
            name: "start"

            PropertyChanges {
                target: uberschrift
                visible: true
            }
        }

    ]
}
