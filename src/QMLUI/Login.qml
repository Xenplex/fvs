/*
    Ritchie Flick
    T3IFAN 2012/2013 Abschlussprojekt
    LN
    'Feuerwehrverwaltungssoftware - FVS'
    'src/QMLUI/Admin.qml'

     Erstellung des Login
*/

import QtQuick 1.1
import "../QML_Komponenten"

Rectangle {
    id: login

    // Attribute auf welche der Python Unterbau zugreifen kann
    property string edtLogin
    property string edtPasswort

    property string edtAdresse
    property string edtPort
    property string edtName
    property string edtBenutzer
    property string edtKonfigPasswort

    // Funktionen zum Ändern der States
    function konfigurieren_fenster_qml() {
        login.state = "konfiguration"
    }

    function zum_login_qml() {
        login.state = "start"
    }

    // Funktionen zum Setzen/Auslesen von Attributen
    function anmelde_daten_setzen_qml() {
        edtLogin = login_name.text
        edtPasswort = login_passwort.text
        operationen_kontroller.operations_signal("anmelde_daten")
    }

    function konfig_speichern_qml() {
        edtAdresse = datenbank_adresse.text
        edtPort = datenbank_port.text
        edtName = datenbank_name.text
        edtBenutzer = datenbank_benutzer.text
        edtKonfigPasswort = datenbank_passwort.text
        operationen_kontroller.operations_signal("konfig_daten")
    }

    // Meldungen
    function fehler_anmeldung_qml() {
        fehler_anmeldung.visible = true
    }

    function fehler_verbindung_qml() {
        fehler_verbindung.text = "Es konnte keine Verbindung\nzur Datenbank" +
        "hergestellt werden,\nbitte kontrollieren Sie ihre\nVerbindungsdaten!"
        fehler_verbindung.schriftFarbe = "red"
    }

    function verbindung_ok_qml() {
        fehler_verbindung.text = "Es konnte eine Datenbank\nVerbindung\n" +
        "hergestellt werden"
        fehler_verbindung.schriftFarbe = "darkgreen"
    }

    //Startstate
    state: "start"

    // Hauptfenster Attribute - werden überschrieben da FVS im FullScreen Modus
    // aufgerufen wird
    width: 570
    height: 270

    Item {
        id: login_fenster
        anchors.fill: parent.fill
        visible: false

        InfoText {
            id: fehler_anmeldung
            text: "Es ist ein Fehler bei der Anmeldung
entstanden. Bitte kontrollieren Sie
ihre Anmeldedaten!"
            visible: false
            schriftFarbe: "red"
            anchors {
                left: login_name.right
                top: login_name.top
                leftMargin: 10
            }
        }

        InfoText {
            id: login_name_text
            text: "Benutzername"
            anchors {
                left: parent.left
                top: parent.top
                margins: 30
            }
        }

        TextEingabe {
            id: login_name
            inputID: "edtLoginName"
            anchors {
                left: login_name_text.left
                top: login_name_text.bottom
                topMargin: 10
            }
        }

        InfoText {
            id: login_passwort_text
            text: "Passwort"
            anchors {
                left: login_name.left
                top: login_name.bottom
                topMargin: 10
            }
        }

        TextEingabe {
            id: login_passwort
            inputID: "edtLoginPasswort"
            passwordFeld: "Password"
            anchors {
                left: login_passwort_text.left
                top: login_passwort_text.bottom
                topMargin: 10
            }
        }

        Button {
            id: anmelden
            buttonID: "btnAnmelden"
            buttonText: "Anmelden"
            anchors {
                left: login_passwort.left
                top: login_passwort.bottom
                topMargin: 10
            }
        }

        Button {
            id: abbrechen
            buttonID: "btnAbbrechen"
            buttonText: "Abbrechen"
            anchors {
                left: anmelden.right
                top: anmelden.top
                leftMargin: 10
            }
        }

        Button {
            id: konfig
            buttonID: "btnKonfig"
            buttonText: "Konfiguration"
            anchors {
                left: abbrechen.right
                top: abbrechen.top
                leftMargin: 10
            }
        }
    }

    Item {
        id: konfiguration_fenster
        anchors.fill: parent.fill
        visible: false

        InfoText {
            id: fehler_verbindung
            anchors {
                left: datenbank_adresse.right
                top: datenbank_adresse.top
                leftMargin: 10
            }
        }

        InfoText {
            id: datenbank_adresse_text
            text: "Datenbank Adresse"
            anchors {
                left: parent.left
                top: parent.top
                margins: 30
            }
        }

        TextEingabe {
            id: datenbank_adresse
            inputID: "edtDatenbankAdresse"
            text: db_adresse
            anchors {
                left: datenbank_adresse_text.right
                top: datenbank_adresse_text.top
                leftMargin: 10
            }
        }

        InfoText {
            id: datenbank_port_text
            text: "Datenbank Port"
            anchors {
                left: datenbank_adresse_text.left
                top: datenbank_adresse_text.bottom
                topMargin: 10
            }
        }

        TextEingabe {
            id: datenbank_port
            inputID: "edtDatenbankPort"
            text: db_port
            anchors {
                left: datenbank_adresse.left
                top: datenbank_port_text.top
                right: datenbank_adresse.right
                leftMargin: 10
            }
        }

        InfoText {
            id: datenbank_name_text
            text: "Datenbank Name"
            anchors {
                left: datenbank_port_text.left
                top: datenbank_port_text.bottom
                topMargin: 10
            }
        }

        TextEingabe {
            id: datenbank_name
            inputID: "edtDatenbankName"
            text: db_name
            anchors {
                left: datenbank_adresse.left
                top: datenbank_name_text.top
                right: datenbank_port.right
                leftMargin: 10
            }
        }

        InfoText {
            id: datenbank_benutzer_text
            text: "Datenbank Benutzer"
            anchors {
                left: datenbank_name_text.left
                top: datenbank_name_text.bottom
                topMargin: 10
            }
        }

        TextEingabe {
            id: datenbank_benutzer
            inputID: "edtDatenbankBenutzer"
            text: db_benutzer
            anchors {
                left: datenbank_adresse.left
                top: datenbank_benutzer_text.top
                right: datenbank_name.right
                leftMargin: 10
            }
        }

        InfoText {
            id: datenbank_passwort_text
            text: "Datenbank Passwort"
            anchors {
                left: datenbank_benutzer_text.left
                top: datenbank_benutzer_text.bottom
                topMargin: 10
            }
        }

        TextEingabe {
            id: datenbank_passwort
            inputID: "edtDatenbankPasswort"
            text: db_passwort
            passwordFeld: "Password"
            anchors {
                left: datenbank_adresse.left
                top: datenbank_passwort_text.top
                right: datenbank_benutzer.right
                leftMargin: 10
            }
        }

        Button {
            id: abspeichern
            buttonID: "btnAbspeichern"
            buttonText: "Abspeichern"
            anchors {
                left: datenbank_passwort_text.left
                top: datenbank_passwort_text.bottom
                topMargin: 30
            }
        }

        Button {
            id: konfig_abbrechen
            buttonID: "btnKonfigAbbrechen"
            buttonText: "Abbrechen"
            anchors {
                left: abspeichern.right
                top: abspeichern.top
                leftMargin: 10
            }
        }
    }

    states: [
    State {
            name: "start"

            PropertyChanges {
                target: login_fenster
                visible: true
            }

            PropertyChanges {
                target: login_name
                fokus: true
            }
        },
        State {
            name: "konfiguration"

            PropertyChanges {
                target: konfiguration_fenster
                visible: true
            }

            PropertyChanges {
                target: datenbank_adresse
                fokus: true
            }
        }
    ]
}
