QMLUI
======

Dateien welche die *grafische Benutzeroberfläche* darstellen für **FVS**.
Die **QMLUI** Dateien bauen ihre Oberflächen aus den einzelnen Komponenten
aus **QML_Komponenten** auf.

Admin
------

Die grafische Benutzeroberfläche für den Administrator.

Inventarist
------------

Die grafische Benutzeroberfläche für den Inventaristen.

Kommandant
-----------

Die grafische Benutzeroberfläche für den Kommandanten.

Login
------

Die grafische Benutzeroberfläche für die Benutzeranmeldung.

Maschinist
-----------

Die grafische Benutzeroberfläche für den Maschinisten.

Sekretaer
----------

Die grafische Benutzeroberfläche für den Sekretär.