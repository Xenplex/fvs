/*
    Ritchie Flick
    T3IFAN 2012/2013 Abschlussprojekt
    LN
    'Feuerwehrverwaltungssoftware - FVS'
    './src/QMLUI/Inventarist.qml'

    Erstellung des Hauptschirms des Inventaristen
*/

import QtQuick 1.1
import "../QML_Komponenten"


Rectangle {
    id: inventarist_schirm

    // Folgende Funtkion zum Kontrollieren von Datumen wurde kopiert von:
    // http://www.qodo.co.uk/blog/javascript-checking-if-a-date-is-valid/

    // Checks a string to see if it in a valid date format
    // of (D)D/(M)M/(YY)YY and returns true/false
    function isValidDate(s) {
        // format D(D)/M(M)/(YY)YY
        var dateFormat = /^\d{1,4}[\.|\/|-]\d{1,2}[\.|\/|-]\d{1,4}$/;

        if (dateFormat.test(s)) {
            // remove any leading zeros from date values
            s = s.replace(/0*(\d*)/gi,"$1");
            var dateArray = s.split(/[\.|\/|-]/);

            // correct month value
            dateArray[1] = dateArray[1]-1;

            // correct year value
            if (dateArray[2].length<4) {
                // correct year value
                dateArray[2] = (parseInt(dateArray[2]) < 50) ? 2000 + parseInt(dateArray[2]) : 1900 + parseInt(dateArray[2]);
            }

            var testDate = new Date(dateArray[2], dateArray[1], dateArray[0]);
            if (testDate.getDate()!=dateArray[0] || testDate.getMonth()!=dateArray[1] || testDate.getFullYear()!=dateArray[2]) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    property int edtSchlauchHinzuAnzahl
    property string cmbSchlauchTypAuswahl
    property string edtAtemschutzBezeichnung
    property string edtAtemschutzKontrolle
    property string edtAtemschutzZustand

    property string edtKleidungBezeichnung
    property string edtKleidungGroesse
    property string edtKleidungZustand
    property string edtKleidungMitglied

    property string edtKommunikationBezeichnung
    property string edtKommunikationZustand
    property string edtKommunikationMitglied

    property string edtSonstigeBezeichnung
    property string edtSonstigeGroesse
    property string edtSonstigeZustand
    property string edtSonstigeKategorie
    property string edtSonstigeKontrolle

    function sonstige_informationen_setzen_qml() {
        bezeichnung.text = inventar_bezeichnung
        groesse.auswahlText = inventar_groesse
        zustand.auswahlText = inventar_zustand
        kategorie.auswahlText = inventar_kategorie
        kontroll_datum.text = inventar_kontrolle
    }

    function sonstige_liste_setzen_qml() {
        inventarist_schirm.state = "sonstige_gegenstaende"
    }

    function zum_hauptmenu_qml() {
        inventarist_schirm.state = "start"
    }

    function schlauch_liste_setzen_qml() {
        inventarist_schirm.state = "schlauch_gegenstaende"
        schlauch_a_anzahl.text = a_schlauch_anzahl
        schlauch_b_anzahl.text = b_schlauch_anzahl
        schlauch_c_anzahl.text = c_schlauch_anzahl
        schlauch_d_anzahl.text = d_schlauch_anzahl
        schlauch_beschaedigt_anzahl.text = beschaedigt_schlauch_anzahl
    }

    function aktueller_schlauch_info_setzen_qml() {
        aktueller_schlauch.text = schlauch_bezeichnung
        zustand_liste.auswahlText = schlauch_zustand
    }

    function neu_schlauch_anzahl_abfragen_qml() {
        edtSchlauchHinzuAnzahl = schlauch_hinzufugen_anzahl.text
        cmbSchlauchTypAuswahl = schlauch_typ_auswahl.auswahlText
        operationen_kontroller.operations_signal("schlauch_anzahl_gesetzt")
    }

    function atemschutz_liste_setzen_qml() {
        inventarist_schirm.state = "atemschutz_gegenstaende"
    }

    function atemschutz_info_setzen_qml() {
        atemschutz_bezeichnung.text = atemschutz_bezeichnung_wert
        atemschutz_kontroll_datum.text = atemschutz_kontrolle_wert
        atemschutz_zustand_auswahl.auswahlText = atemschutz_zustand_wert
    }

    function atemschutz_info_abfragen_qml() {
        edtAtemschutzBezeichnung = atemschutz_bezeichnung.text
        edtAtemschutzKontrolle = atemschutz_kontroll_datum.text
        edtAtemschutzZustand = atemschutz_zustand_auswahl.auswahlText
        operationen_kontroller.operations_signal("atemschutz_info_abgefragt")
    }

    function kleider_gegenstaende_setzen_qml() {
        inventarist_schirm.state = "kleider_gegenstaende"
    }

    function kleidung_info_setzen_qml() {
        kleider_bezeichnung.text = kleidung_bezeichnung_wert
        kleider_groesse.auswahlText = kleidung_groesse_wert
        kleider_zustand.auswahlText = kleidung_zustand_wert
        kleider_mitglied.auswahlText = kleidung_mitglied_wert
    }

    function kleidung_info_abfragen_qml() {
        edtKleidungBezeichnung = kleider_bezeichnung.text
        edtKleidungGroesse = kleider_groesse.auswahlText
        edtKleidungZustand = kleider_zustand.auswahlText
        edtKleidungMitglied = kleider_mitglied.auswahlText
        operationen_kontroller.operations_signal("kleidung_info_abgefragt")
    }

    function kommunikation_gegenstaende_setzen_qml() {
        inventarist_schirm.state = "kommunikation_gegenstaende"
    }

    function kommunikation_info_setzen_qml() {
        kommunikation_bezeichnung.text = kommunikation_bezeichnung_wert
        kommunikation_zustand.auswahlText = kommunikation_zustand_wert
        kommunikation_mitglied.auswahlText = kommunikation_mitglied_wert
    }

    function kommunikation_info_abfragen_qml() {
        edtKommunikationBezeichnung = kommunikation_bezeichnung.text
        edtKommunikationZustand = kommunikation_zustand.auswahlText
        edtKommunikationMitglied = kommunikation_mitglied.auswahlText
        operationen_kontroller.operations_signal("kommuni_info_abgefragt")
    }

    function sonstiges_abspeichern_qml() {
        edtSonstigeBezeichnung = bezeichnung.text
        edtSonstigeGroesse = groesse.auswahlText
        edtSonstigeZustand = zustand.auswahlText
        edtSonstigeKategorie = kategorie.auswahlText
        edtSonstigeKontrolle = kontroll_datum.text
        if (!(isValidDate(edtSonstigeKontrolle)))
            return
        operationen_kontroller.operations_signal("sonstige_info_abgefragt")
    }

    // Startstate
    state: "start"

    // Hauptfenster Attribute - werden überschrieben da FVS im FullScreen Modus
    // aufgerufen wird
    width: 1024
    height: 768

    // Hauptkomponenten

    ListenContainer {
        id: auswahl_liste
        anchors.left: parent.left
        hoehe: parent.height
        breite: parent.width / 5
        eintragHoehe: 50
    }

    InfoText {
        id: uberschrift
        visible: false
        schriftFarbe: "black"
        schriftGroesse: 50
        text: "Inventarist"
        x: parent.width / 2.5
        anchors.top: parent.top
    }

    // Hauptmenü

    Item {
        id: hauptmenu
        visible: false
        anchors {
            top: parent.top
            left: auswahl_liste.right
            right: parent.right
            topMargin: 70
        }
        height: parent.height / 2

        Button {
            id: schlauch_verwaltung
            buttonID: "btnSchlauchVerwaltung"
            buttonText: "Schlauch\nVerwaltung"
            anchors {
                left: parent.left
                leftMargin: 10
            }
        }

        Button {
            id: atemschutz_verwaltung
            buttonID: "btnAtemschutzVerwaltung"
            buttonText: "Atemschutz\nVerwaltung"
            anchors {
                left: schlauch_verwaltung.right
                top: schlauch_verwaltung.top
                leftMargin: 10
            }
        }

        Button {
            id: kleider_verwaltung
            buttonID: "btnKleiderVerwaltung"
            buttonText: "Kleider\nVerwaltung"
            anchors {
                left: atemschutz_verwaltung.right
                top: atemschutz_verwaltung.top
                leftMargin: 10
            }
        }

        Button {
            id: kommunikation_verwaltung
            buttonID: "btnKommunikationVerwaltung"
            buttonText: "Kommunikation\nVerwaltung"
            anchors {
                left: kleider_verwaltung.right
                top: kleider_verwaltung.top
                leftMargin: 10
            }
        }

        Button {
            id: sonstige_verwaltung
            buttonID: "btnSonstigeVerwaltung"
            buttonText: "Sonstige\nVerwaltung"
            anchors {
                left: kommunikation_verwaltung.right
                top: kommunikation_verwaltung.top
                leftMargin: 10
            }
        }
    }

    Button {
        id: zum_hauptmenu
        buttonID: "btnZumHauptmenu"
        buttonText: "Hauptmenü"
        visible: false
        anchors {
            top: parent.top
            left: auswahl_liste.left
            topMargin: 30
            leftMargin: 300
        }
        width: 200
    }

    InfoText {
        id: schlauch_verwaltung_uberschrift
        visible: false
        text: "Schläuche Verwaltung"
        schriftFarbe: "black"
        schriftGroesse: 30
        anchors {
            left: zum_hauptmenu.right
            top: zum_hauptmenu.top
            leftMargin: 30
        }
    }

    InfoText {
        id: atemschutz_verwaltung_uberschrift
        visible: false
        text: "Atemschutzgeräte Verwaltung"
        schriftFarbe: "black"
        schriftGroesse: 30
        anchors {
            left: zum_hauptmenu.right
            top: zum_hauptmenu.top
            leftMargin: 30
        }
    }

    InfoText {
        id: kleider_verwaltung_uberschrift
        visible: false
        text: "Kleider Verwaltung"
        schriftFarbe: "black"
        schriftGroesse: 30
        anchors {
            left: zum_hauptmenu.right
            top: zum_hauptmenu.top
            leftMargin: 30
        }
    }

    InfoText {
        id: kommunikation_verwaltung_uberschrift
        visible: false
        text: "Kommunikation Verwaltung"
        schriftFarbe: "black"
        schriftGroesse: 30
        anchors {
            left: zum_hauptmenu.right
            top: zum_hauptmenu.top
            leftMargin: 30
        }
    }

    InfoText {
        id: sonstige_verwaltung_uberschrift
        visible: false
        text: "Sonstige Verwaltung"
        schriftFarbe: "black"
        schriftGroesse: 30
        anchors {
            left: zum_hauptmenu.right
            top: zum_hauptmenu.top
            leftMargin: 30
        }
    }

    // Informationen bearbeiten für Sonstige Inventargegenstände

    Item {
        id: sonstige_info_bearbeiten
        visible: false
        anchors {
            top: parent.top
            bottom: parent.bottom
            left: auswahl_liste.right
            right: parent.right
            topMargin: 135
        }

        InfoText {
            id: bezeichnung_text
            text: "Bezeichnung"
            anchors {
                top: parent.top
                left: parent.left
                leftMargin: 20
                rightMargin: 20
            }
        }

        TextEingabe {
            id: bezeichnung
            inputID: "edtBezeichnung"
            anchors {
                left: bezeichnung_text.right
                top: bezeichnung_text.top
                leftMargin: 10
            }
        }

        InfoText {
            id: groesse_text
            text: "Größe"
            anchors {
                left: bezeichnung_text.left
                top: bezeichnung_text.bottom
                topMargin: 10
            }
        }

        AuswahlListe {
            id: groesse
            daten_modell: groessen_auswahl
            anchors {
                top: bezeichnung.bottom
                left: bezeichnung.left
                topMargin: 10
            }
        }

        InfoText {
            id: zustand_zustand_text
            text: "Zustand"
            anchors {
                left: groesse_text.left
                top: groesse_text.bottom
                topMargin: 10
            }
        }

        AuswahlListe {
            id: zustand
            daten_modell: zustaende_auswahl
            anchors {
                top: groesse.bottom
                left: groesse.left
                topMargin: 10
            }
        }

        InfoText {
            id: kategorie_text
            text: "Kategorie"
            anchors {
                left: kategorie.right
                top: kategorie.top
                leftMargin: 10
            }
        }

        AuswahlListe {
            id: kategorie
            daten_modell: kategorien_auswahl
            anchors {
                left: bezeichnung.right
                top: bezeichnung.top
                leftMargin: 10
            }
        }

        InfoText {
            id: kontroll_datum_text
            text: "Letzte Kontrolle"
            anchors {
                left: kontroll_datum.right
                top: kontroll_datum.top
                leftMargin: 10
            }
        }

        TextEingabe {
            id: kontroll_datum
            inputID: "edtKontrollDatum"
            eingabeHilfe: "00-00-0000"
            anchors {
                top: kategorie.bottom
                left: kategorie.left
                topMargin: 10
            }
        }

        Button {
            id: sonstiges_neu
            buttonID: "btnNeuSonstiges"
            buttonText: "Neu"
            anchors {
                left: zustand_zustand_text.left
                top: zustand_zustand_text.bottom
                topMargin: 10
            }
        }

        Button {
            id: sonstiges_entfernen
            buttonID: "btnEntfernenSonstiges"
            buttonText: "Entfernen"
            anchors {
                left: sonstiges_neu.right
                top: sonstiges_neu.top
                leftMargin: 10
            }
        }

        Button {
            id: sonstiges_abspeichern
            buttonID: "btnAbspeichernSonstiges"
            buttonText: "Abspeichern"
            anchors {
                left: sonstiges_entfernen.right
                top: sonstiges_entfernen.top
                leftMargin: 10
            }
        }
    }

    Item {
        id: schlauch_bearbeiten
        visible: false
        anchors {
            top: parent.top
            bottom: parent.bottom
            left: auswahl_liste.right
            right: parent.right
            topMargin: 135
        }

        Button {
            id: schlauch_bericht
            buttonID: "btnSchlauchBericht"
            buttonText: "Bericht"
            anchors {
                left: parent.left
                top: parent.top
                leftMargin: 10
            }
        }

        InfoText {
            id: typ_a
            text: "Typ A"
            anchors {
                left: parent.left
                top: schlauch_bericht.bottom
                leftMargin: 30
                topMargin: 30
            }
        }

        InfoText {
            id: typ_b
            text: "Typ B"
            anchors {
                left: typ_a.left
                top: typ_a.bottom
                topMargin: 10
            }
        }

        InfoText {
            id: typ_c
            text: "Typ C"
            anchors {
                left: typ_b.left
                top: typ_b.bottom
                topMargin: 10
            }
        }

        InfoText {
            id: typ_d
            text: "Typ D"
            anchors {
                left: typ_c.left
                top: typ_c.bottom
                topMargin: 10
            }
        }

        JaNeinAuswahl {
            id: typ_a_auswahl
            auswahlID: "chkTypA"
            anchors {
                left: typ_a.right
                top: typ_a.top
                leftMargin: 10
            }
        }

        JaNeinAuswahl {
            id: typ_b_auswahl
            auswahlID: "chkTypB"
            anchors {
                left: typ_b.right
                top: typ_b.top
                leftMargin: 10
            }
        }

        JaNeinAuswahl {
            id: typ_c_auswahl
            auswahlID: "chkTypC"
            anchors {
                left: typ_c.right
                top: typ_c.top
                leftMargin: 10
            }
        }

        JaNeinAuswahl {
            id: typ_d_auswahl
            auswahlID: "chkTypD"
            anchors {
                left: typ_d.right
                top: typ_d.top
                leftMargin: 10
            }
        }

        InfoText {
            id: zustand_text
            text: "Beschädigt"
            anchors {
                left: typ_a_auswahl.right
                top: typ_a_auswahl.top
                leftMargin: 100
            }
        }

        JaNeinAuswahl {
            id: zustand_auswahl
            auswahlID: "chkZustand"
            anchors {
                left: zustand_text.right
                top: zustand_text.top
                leftMargin: 10
            }
        }

        InfoText {
            id: schlauch_a_anzahl_text
            text: "A Schlauch Anzahl"
            anchors {
                left: zustand_auswahl.right
                top: zustand_auswahl.top
                leftMargin: 30
            }
        }

        InfoText {
            id: schlauch_b_anzahl_text
            text: "B Schlauch Anzahl"
            anchors {
                left: schlauch_a_anzahl_text.left
                top: schlauch_a_anzahl_text.bottom
                topMargin: 10
            }
        }

        InfoText {
            id: schlauch_c_anzahl_text
            text: "C Schlauch Anzahl"
            anchors {
                left: schlauch_b_anzahl_text.left
                top: schlauch_b_anzahl_text.bottom
                topMargin: 10
            }
        }

        InfoText {
            id: schlauch_d_anzahl_text
            text: "D Schlauch Anzahl"
            anchors {
                left: schlauch_c_anzahl_text.left
                top: schlauch_c_anzahl_text.bottom
                topMargin: 10
            }
        }

        InfoText {
            id: schlauch_beschaedigt_anzahl_text
            text: "Beschädigte Schläuche"
            anchors {
                left: schlauch_d_anzahl_text.left
                top: schlauch_d_anzahl_text.bottom
                topMargin: 10
            }
        }

        InfoText {
            id: schlauch_a_anzahl
            anchors {
                left: schlauch_a_anzahl_text.right
                top: schlauch_a_anzahl_text.top
                leftMargin: 10
            }
        }

        InfoText {
            id: schlauch_b_anzahl
            anchors {
                left: schlauch_b_anzahl_text.right
                top: schlauch_b_anzahl_text.top
                leftMargin: 10
            }
        }

        InfoText {
            id: schlauch_c_anzahl
            anchors {
                left: schlauch_c_anzahl_text.right
                top: schlauch_c_anzahl_text.top
                leftMargin: 10
            }
        }

        InfoText {
            id: schlauch_d_anzahl
            anchors {
                left: schlauch_d_anzahl_text.right
                top: schlauch_d_anzahl_text.top
                leftMargin: 10
            }
        }

        InfoText {
            id: schlauch_beschaedigt_anzahl
            anchors {
                left: schlauch_beschaedigt_anzahl_text.right
                top: schlauch_beschaedigt_anzahl_text.top
                leftMargin: 10
            }
        }

        Button {
            id: einzelner_schlauch_entfernen
            buttonID: "btnEinzelnSchlauchEntfernen"
            buttonText: "Einzelnen Schlauch\nentfernen"
            anchors {
                top: typ_d.bottom
                left: parent.left
                leftMargin: 10
                topMargin: 100
            }
        }

        Button {
            id: aktuelle_schlauch_liste_entfernen
            buttonID: "btnAktuelleSchlauchListeEntfernen"
            buttonText: "Aktuelle Listeneinträge\nentfernen"
            anchors {
                top: einzelner_schlauch_entfernen.bottom
                left: einzelner_schlauch_entfernen.left
                topMargin: 10
            }
        }

        InfoText {
            id: aktueller_schlauch
            anchors {
                left: aktuelle_schlauch_liste_entfernen.right
                top: aktuelle_schlauch_liste_entfernen.top
                leftMargin: 10
            }
        }

        InfoText {
            id: zustand_liste_text
            text: "Zustand"
            anchors {
                left: zustand_liste.left
                top: zustand_liste.bottom
                bottomMargin: 10
            }
        }

        AuswahlListe {
            id: zustand_liste
            comboID: "cmbZustandAuswahl"
            daten_modell: zustaende_auswahl
            anchors {
                left: aktueller_schlauch.left
                top: aktueller_schlauch.bottom
                topMargin: 10
            }
        }

        Button {
            id: schlauche_hinzufugen
            buttonID: "btnSchlaucheHinzufugen"
            buttonText: "Schläuche hinzufügen"
            anchors {
                left: schlauch_hinzufugen_anzahl.left
                bottom: schlauch_hinzufugen_anzahl.top
                bottomMargin: 10
            }
        }

        InfoText {
            id: schlauch_hinzufugen_anzahl_text
            text: "Anzahl"
            anchors {
                left: schlauch_hinzufugen_anzahl.right
                top: schlauch_hinzufugen_anzahl.top
                leftMargin: 10
            }
        }

        TextEingabe {
            id: schlauch_hinzufugen_anzahl
            inputID: "edtSchlauchHinzuAnzahl"
            anchors {
                left: zustand_liste.right
                top: zustand_liste.top
                leftMargin: 50
            }
        }

        InfoText {
            id: schlauch_typ_auswahl_text
            text: "Schlauch Typ Auswahl"
            anchors {
                left: schlauch_typ_auswahl.right
                top: schlauch_typ_auswahl.top
                leftMargin: 10
            }
        }

        AuswahlListe {
            id: schlauch_typ_auswahl
            comboID: "cmbSchlauchTypAuswahl"
            daten_modell: schlauch_typ_liste
            anchors {
                left: schlauch_hinzufugen_anzahl.left
                top: schlauch_hinzufugen_anzahl.bottom
                topMargin: 10
            }
        }
    }

    Item {
        id: atemschutz_bearbeiten
        visible: false
        anchors {
            top: parent.top
            bottom: parent.bottom
            left: auswahl_liste.right
            right: parent.right
            topMargin: 135
        }

        Button {
            id: atemschutz_kontrollen_bericht
            buttonText: "Kontrollen Bericht"
            buttonID: "btnAtemschutzKontrollenBericht"
            anchors {
                left: parent.left
                top: parent.top
                leftMargin: 250
            }
        }

        Button {
            id: atemschutz_beschaedigt_bericht
            buttonText: "Beschädigt Bericht"
            buttonID: "btnAtemschutzBeschaedigtBericht"
            anchors {
                left: atemschutz_kontrollen_bericht.right
                top: atemschutz_kontrollen_bericht.top
                leftMargin: 100
            }
        }

        InfoText {
            id: atemschutz_kontrolle_liste_text
            text: "Nächstes Atemschutzgerät zur Kontrolle (Aufsteigend)"
            anchors {
                left: atemschutz_kontrolle_liste.left
                bottom: atemschutz_kontrolle_liste.top
                bottomMargin: 10
            }
        }

        ListenContainer {
            id: atemschutz_kontrolle_liste
            listeID: "lstAtemschutzKontrolle"
            anchors {
                left: atemschutz_beschaedigt_bericht.left
                top: atemschutz_beschaedigt_bericht.bottom
                bottom: parent.bottom
                topMargin: 70
            }
            breite: 300
            eintragHoehe: 70
        }

        InfoText {
            id: atemschutz_beschaedigt_anzeigen_text
            text: "Filter beschädigte Geräte"
            anchors {
                right: atemschutz_beschaedigt_anzeigen.left
                top: atemschutz_beschaedigt_anzeigen.top
                rightMargin: 10
            }
        }

        JaNeinAuswahl {
            id: atemschutz_beschaedigt_anzeigen
            auswahlID: "chkAtemschutzBeschaedigtAnzeigen"
            anchors {
                right: atemschutz_kontrollen_bericht.left
                top: atemschutz_kontrollen_bericht.top
                rightMargin: 10
            }
        }

        InfoText {
            id: atemschutz_bezeichnung_text
            text: "Bezeichnung"
            anchors {
                right: atemschutz_bezeichnung.left
                top: atemschutz_bezeichnung.top
                rightMargin: 10
            }
        }

        TextEingabe {
            id: atemschutz_bezeichnung
            inputID: "edtAtemschutzBezeichnung"
            anchors {
                left: atemschutz_kontrollen_bericht.left
                top: atemschutz_kontrollen_bericht.bottom
                topMargin: 100
            }
        }

        InfoText {
            id: atemschutz_kontroll_datum_text
            text: "Letzte Kontrolle"
            anchors {
                right: atemschutz_bezeichnung_text.right
                top: atemschutz_bezeichnung_text.bottom
                topMargin: 10
            }
        }

        TextEingabe {
            id: atemschutz_kontroll_datum
            inputID: "edtAtemschutzKontrollDatum"
            eingabeHilfe: "00-00-0000"
            anchors {
                left: atemschutz_bezeichnung.left
                top: atemschutz_bezeichnung.bottom
                topMargin: 10
            }
        }

        InfoText {
            id: atemschutz_zustand_auswahl_text
            text: "Zustand"
            anchors {
                right: atemschutz_kontroll_datum_text.right
                top: atemschutz_kontroll_datum_text.bottom
                topMargin: 10
            }
        }

        AuswahlListe {
            id: atemschutz_zustand_auswahl
            comboID: "cmbAtemschutzZustand"
            daten_modell: zustaende_auswahl
            anchors {
                left: atemschutz_kontroll_datum.left
                top: atemschutz_kontroll_datum.bottom
                topMargin: 10
            }
        }

        Button {
            id: neuer_atemschutz
            buttonID: "btnNeuAtemschutz"
            buttonText: "Neuer Atemschutz"
            anchors {
                left: atemschutz_bezeichnung.left
                bottom: atemschutz_bezeichnung.top
                bottomMargin: 10
            }
        }

        Button {
            id: entfernen_atemschutz
            buttonID: "btnEntfernenAtemschutz"
            buttonText: "Atemschutz entfernen"
            anchors {
                left: atemschutz_zustand_auswahl.left
                top: atemschutz_zustand_auswahl.bottom
                topMargin: 10
            }
        }

        Button {
            id: aktualisieren_atemschutz
            buttonID: "btnAktualisierenAtemschutz"
            buttonText: "Aktualisieren"
            anchors {
                top: entfernen_atemschutz.bottom
                left: entfernen_atemschutz.left
                topMargin: 10
            }
        }
    }

    Item {
        id: kleider_bearbeiten
        visible: false
        anchors {
            top: parent.top
            bottom: parent.bottom
            left: auswahl_liste.right
            right: parent.right
            topMargin: 135
        }

        InfoText {
            id: kleider_bezeichnung_text
            text: "Bezeichnung"
            anchors {
                left: parent.left
                top: parent.top
                topMargin: 50
                leftMargin: 50
            }
        }

        TextEingabe {
            id: kleider_bezeichnung
            inputID: "edtKleiderBezeichnung"
            anchors {
                left: kleider_bezeichnung_text.right
                top: kleider_bezeichnung_text.top
                leftMargin: 10
            }
        }

        InfoText {
            id: kleider_groesse_text
            text: "Größe"
            anchors {
                left: kleider_bezeichnung_text.left
                top: kleider_bezeichnung_text.bottom
                topMargin: 10
            }
        }

        AuswahlListe {
            id: kleider_groesse
            comboID: "cmbKleiderGroesse"
            daten_modell: groessen_auswahl
            anchors {
                left: kleider_bezeichnung.left
                top: kleider_bezeichnung.bottom
                topMargin: 10
            }
        }

        InfoText {
            id: kleider_zustand_text
            text: "Zustand"
            anchors {
                left: kleider_groesse_text.left
                top: kleider_groesse_text.bottom
                topMargin: 10
            }
        }

        AuswahlListe {
            id: kleider_zustand
            comboID: "cmbKleiderZustand"
            daten_modell: zustaende_auswahl
            anchors {
                left: kleider_groesse.left
                top: kleider_groesse.bottom
                topMargin: 10
            }
        }

        InfoText {
            id: kleider_mitglied_text
            text: "Mitglied"
            anchors {
                left: kleider_zustand_text.left
                top: kleider_zustand_text.bottom
                topMargin: 10
            }
        }

        AuswahlListe {
            id: kleider_mitglied
            comboID: "cmbKleiderMitglied"
            anchors {
                left: kleider_zustand.left
                top: kleider_zustand.bottom
                topMargin: 10
            }
        }

        Button {
            id: kleider_neu
            buttonID: "btnKleiderNeu"
            buttonText: "Neu"
            anchors {
                left: kleider_mitglied.left
                top: kleider_mitglied.bottom
                topMargin: 10
            }
        }

        Button {
            id: kleider_entfernen
            buttonID: "btnKleiderEntfernen"
            buttonText: "Entfernen"
            anchors {
                left: kleider_neu.right
                top: kleider_neu.top
                leftMargin: 10
            }
        }

        Button {
            id: kleider_aktualisieren
            buttonID: "btnKleiderAktualisieren"
            buttonText: "Aktualisieren"
            anchors {
                left: kleider_entfernen.right
                top: kleider_entfernen.top
                leftMargin: 10
            }
        }
    }

    Item {
        id: kommunikation_bearbeitung
        visible: false
        anchors {
            top: parent.top
            bottom: parent.bottom
            left: auswahl_liste.right
            right: parent.right
            topMargin: 135
        }

        InfoText {
            id: kommunikation_bezeichnung_text
            text: "Bezeichnung"
            anchors {
                top: parent.top
                left: parent.left
                leftMargin: 20
                rightMargin: 20
            }
        }

        TextEingabe {
            id: kommunikation_bezeichnung
            inputID: "edtKommunikationBezeichnung"
            anchors {
                left: kommunikation_bezeichnung_text.right
                top: kommunikation_bezeichnung_text.top
                leftMargin: 10
            }
        }

        InfoText {
            id: kommunikation_zustand_text
            text: "Zustand"
            anchors {
                left: kommunikation_bezeichnung_text.left
                top: kommunikation_bezeichnung_text.bottom
                topMargin: 10
            }
        }

        AuswahlListe {
            id: kommunikation_zustand
            daten_modell: zustaende_auswahl
            anchors {
                left: kommunikation_bezeichnung.left
                top: kommunikation_bezeichnung.bottom
                topMargin: 10
            }
        }

        InfoText {
            id: kommunikation_mitglied_text
            text: "Mitglied"
            anchors {
                left: kommunikation_zustand_text.left
                top: kommunikation_zustand_text.bottom
                topMargin: 10
            }
        }

        AuswahlListe {
            id: kommunikation_mitglied
            anchors {
                left: kommunikation_zustand.left
                top: kommunikation_zustand.bottom
                topMargin: 10
            }
        }

        Button {
            id: kommunikation_neu
            buttonID: "btnKommunikationNeu"
            buttonText: "Neu"
            anchors {
                left: kommunikation_mitglied.left
                top: kommunikation_mitglied.bottom
                topMargin: 10
            }
        }

        Button {
            id: kommunikation_entfernen
            buttonID: "btnKommunikationEntfernen"
            buttonText: "Entfernen"
            anchors {
                left: kommunikation_neu.right
                top: kommunikation_neu.top
                leftMargin: 10
            }
        }

        Button {
            id: kommunikation_aktualisieren
            buttonID: "btnKommunikationAktualisieren"
            buttonText: "Aktualisieren"
            anchors {
                left: kommunikation_entfernen.right
                top: kommunikation_entfernen.top
                leftMargin: 10
            }
        }
    }

    states: [
        State {
            name: "start"

            PropertyChanges {
                target: hauptmenu
                visible: true
            }

            PropertyChanges {
                target: uberschrift
                visible: true
            }
        },

        State {
            name: "sonstige_gegenstaende"

            PropertyChanges {
                target: auswahl_liste
                daten_modell: sonstige_gegenstaende
                listeID: "lstSonstigeGegenstaende"
                eintragHoehe: 70
            }

            PropertyChanges {
                target: sonstige_info_bearbeiten
                visible: true
            }

            PropertyChanges {
                target: zum_hauptmenu
                visible: true
            }

            PropertyChanges {
                target: sonstige_verwaltung_uberschrift
                visible: true
            }
        },

        State {
            name: "schlauch_gegenstaende"

            PropertyChanges {
                target: schlauch_bearbeiten
                visible: true
            }

            PropertyChanges {
                target: auswahl_liste
                daten_modell: schlauch_gegenstaende
                listeID: "lstSchlauchGegenstaende"
                eintragHoehe: 70
            }

            PropertyChanges {
                target: zum_hauptmenu
                visible: true
            }

            PropertyChanges {
                target: schlauch_verwaltung_uberschrift
                visible: true
            }
        },

        State {
            name: "atemschutz_gegenstaende"

            PropertyChanges {
                target: atemschutz_bearbeiten
                visible: true
            }

            PropertyChanges {
                target: auswahl_liste
                daten_modell: atemschutz_auflistung
                listeID: "lstAtemschutzAuflistung"
                eintragHoehe: 70
            }

            PropertyChanges {
                target: zum_hauptmenu
                visible: true
            }

            PropertyChanges {
                target: atemschutz_kontrolle_liste
                daten_modell: atemschutz_kontrolle_liste_model
            }

            PropertyChanges {
                target: atemschutz_verwaltung_uberschrift
                visible: true
            }
        },

        State {
            name: "kleider_gegenstaende"

            PropertyChanges {
                target: kleider_bearbeiten
                visible: true
            }

            PropertyChanges {
                target: auswahl_liste
                daten_modell: kleider_auflistung
                listeID: "lstKleiderAuflistung"
                eintragHoehe: 70
            }

            PropertyChanges {
                target: kleider_mitglied
                daten_modell: kleider_mitglieder
            }

            PropertyChanges {
                target: zum_hauptmenu
                visible: true
            }

            PropertyChanges {
                target: kleider_verwaltung_uberschrift
                visible: true
            }
        },

        State {
            name: "kommunikation_gegenstaende"

            PropertyChanges {
                target: kommunikation_bearbeitung
                visible: true
            }

            PropertyChanges {
                target: zum_hauptmenu
                visible: true
            }

            PropertyChanges {
                target: kommunikation_mitglied
                daten_modell: kommunikation_mitglieder
            }

            PropertyChanges {
                target: auswahl_liste
                daten_modell: kommunikation_auflistung
                listeID: "lstKommunikationAuflistung"
                eintragHoehe: 70
            }

            PropertyChanges {
                target: kommunikation_verwaltung_uberschrift
                visible: true
            }
        }

    ]
}
