/*
    Ritchie Flick
    T3IFAN 2012/2013 Abschlussprojekt
    LN
    'Feuerwehrverwaltungssoftware - FVS'
    './src/QMLUI/Maschinist.qml'

    Erstellung des Hauptschirms des Maschinisten
*/

import QtQuick 1.1
import "../QML_Komponenten"

Rectangle {
    id: masch_schirm

    // Attribute auf welche der Python Unterbau zugreifen kann
    property string edtKennzeichen
    property string edtBezeichnung
    property string edtBesatzung
    property bool chkProblem
    property string memProblemKommentar
    property string edtTuv
    property string edtWerkstatt
    property string fahrzeugTyp

    // Attribute auf welche der Python Unterbau zugreift werden gesetzt
    function fahrzeug_info_abspeichern_qml() {
        edtKennzeichen = kennzeichen.text
        edtBezeichnung = bezeichnung.text
        edtBesatzung = besatzung.text
        chkProblem = problemAuswahl.checkAuswahl
        memProblemKommentar = problemKommentar.text
        edtTuv = tuv.text
        edtWerkstatt = werkstatt.text
        fahrzeugTyp = fahrzeug_typ_auswahl.auswahlText
        operationen_kontroller.operations_signal("fahrzeug_variablen_gesetzt")
    }

    // Slot Funktionen zum wechseln der States

    // Alle Informationen des jeweiligen Fahrzeugs werden ausgelesen
    // und in die entsprechenden Felder gesetzt
    function fahrzeug_info_setzen_qml() {
        masch_schirm.state = "fahrzeug_info"
        kennzeichen.text = fahrzeug_kennzeichen
        bezeichnung.text = fahrzeug_bezeichnung
        besatzung.text = fahrzeug_besatzung
        if (fahrzeug_problem == 1) {
            problemAuswahl.state = "checked"
            problemAuswahl.checkAuswahl = true
        }
        else {
            problemAuswahl.state = "unchecked"
            problemAuswahl.checkAuswahl = false
        }
        problemKommentar.text = fahrzeug_kommentar
        tuv.text = fahrzeug_tuv
        werkstatt.text = fahrzeug_werkstatt
        fahrzeug_typ_auswahl.auswahlText = fahrzeug_typ
    }

    // Wechselt State zur Anzeige der Inventarliste
    function ausstattung_anzeigen_setzen_qml() {
        if (masch_schirm.state === "fahrzeug_info")
            masch_schirm.state = "ausstattung_auflisten"
        else
            masch_schirm.state = "fahrzeug_info"
    }

    // Wechselt State zur Verwaltung der Fahrzeugausstattung
    function ausstattung_verwaltung_starten_qml() {
        if (masch_schirm.state === ("fahrzeug_info" || "ausstattung_auflisten"))
            masch_schirm.state = "ausstattung_verwaltung"
        else
            masch_schirm.state = "ausstattung_auflisten"
    }

    // Die Datenfelder werden schreibbar gemacht oder gesperrt
    function fahrzeug_infoFelder_freischaltenSperren_qml() {
        if (masch_schirm.state === "fahr_info_bearbeiten")
            masch_schirm.state = "fahrzeug_info"
        else
            masch_schirm.state = "fahr_info_bearbeiten"
    }

    // Wechselt State zum Hinzufügen eines neuen Fahrzeuges
    function neues_fahrzeug_qml() {
        if (masch_schirm.state === "neues_fahrzeug")
            masch_schirm.state = "kein_fahrzeug"
        else
            masch_schirm.state = "neues_fahrzeug"
    }

    // Wechselt State zum Startstate
    function kein_fahrzeug_state_qml() {
        masch_schirm.state = "kein_fahrzeug"
    }

    // Startstate
    state: "kein_fahrzeug"

    // Hauptfenster Attribute - werden überschrieben da FVS im FullScreen Modus
    // aufgerufen wird
    width: 1024
    height: 768

    // Hauptkomponenten von masch_schirm

    // Liste welche alle Fahrzeuge beinhaltet
    ListenContainer {
        id: auswahl_liste
        listeID: "lstfahrzeugliste"
        daten_modell: fahrzeug_liste
        anchors.left: parent.left
        hoehe: parent.height
        breite: parent.width / 5
    }

    // Button zum Hinzufügen eines neuen Fahrzeuges
    Button {
        id: neu_fahrzeug
        buttonID: "btnNeuFahrzeug"
        anchors {
            bottom: parent.bottom
            left: auswahl_liste.left
        }
        buttonText: "Neues Fahrzeug"
    }

    // Button zum Entfernen eines Fahrzeuges
    Button {
        id: entfernen_fahrzeug
        buttonID: "btnEntfernenFahrzeug"
        anchors {
            bottom: parent.bottom
            right: auswahl_liste.right
        }
        buttonText: "Fahrzeug entfernen"
    }

    // Beinhaltet alle Elemente zum Anzeigen der Fahrzeug Informationen
    Item {
        id: fahrzeug_info
        anchors.fill: parent

        InfoText {
            id: kennzeichen_text
            text: "Kennzeichen"
            anchors {
                left: kennzeichen.right
                top: kennzeichen.top
                leftMargin: 10
            }
        }

        // Kennzeichen des Fahrzeuges
        TextEingabe {
            id: kennzeichen
            inputID: "edtKennzeichen"
            x: parent.width / 4
            y: parent.height / 4
            blockiert: true
        }

        InfoText {
            id: bezeichnung_text
            text: "Bezeichnung"
            anchors {
                left: bezeichnung.right
                top: bezeichnung.top
                leftMargin: 10
            }
        }

        // Bezeichnung des Fahrzeuges
        TextEingabe {
            id: bezeichnung
            inputID: "edtBezeichnung"
            anchors {
                top: kennzeichen.bottom
                left: kennzeichen.left
                topMargin: 10
            }
            blockiert: true
        }

        InfoText {
            id: besatzung_text
            text: "Maximale Besatzung"
            anchors {
                left: besatzung.right
                top: besatzung.top
                leftMargin: 10
            }
        }

        // Maximale Besatzungsanzahl des Fahrzeuges
        TextEingabe {
            id: besatzung
            inputID: "edtBesatzung"
            anchors {
                top: bezeichnung.bottom
                left: bezeichnung.left
                topMargin: 10
            }
            blockiert: true
        }

        InfoText {
            id: problemAuswahlText
            anchors {
                top: besatzung.bottom
                left: besatzung.left
                topMargin: 10
            }
            text: "Besteht ein technisches Problem?"
        }

        // Checkbox ob ein Problem vorliegt oder nicht
        JaNeinAuswahl {
            id: problemAuswahl
            auswahlID: "chkProblem"
            anchors {
                top: problemAuswahlText.top
                left: problemAuswahlText.right
                leftMargin: 10
            }
        }

        InfoText {
            id: problemKommentar_text
            text: "Freier Kommentar"
            anchors {
                top: problemAuswahlText.bottom
                left: problemAuswahlText.left
                right: problemAuswahl.right
                topMargin: 30
            }
        }

        // Freier Kommentar zum Problem
        Memo {
            id: problemKommentar
            memoID: "memProblemKommentar"
            anchors {
                left: problemKommentar_text.left
                right: fahrzeug_typ_auswahl.right
                bottom: parent.bottom
                bottomMargin: 10
                top: problemKommentar_text.bottom
                topMargin: 10
            }
            hoehe: 200
        }

        // Button um die momentane Fahrzeugausstattung anzuzeigen oder nicht
        Button {
            id: ausstattung_anzeigen
            buttonID: "btnAusstattungAuflistung"
            anchors {
                bottom: tuv.top
                left: tuv.left
                bottomMargin: 10
            }
            buttonText: "Fahrzeug Ausstattung"
        }

        InfoText {
            id: tuv_text
            text: "Letzter TUV Test"
            anchors {
                right: tuv.left
                top: tuv.top
                rightMargin: 10
            }
        }

        // Datum des letzten TUV Test
        TextEingabe {
            id: tuv
            inputID: "edtTuv"
            eingabeHilfe: "00-00-0000"
            x: parent.width / 1.5
            anchors.top: kennzeichen.top
            blockiert: true
        }

        InfoText {
            id: werkstatt_text
            text: "Letzte Wekrstatt Kontrolle"
            anchors {
                right: werkstatt.left
                top: werkstatt.top
                rightMargin: 10
            }
        }

        // Datum der letzten Werkstatt Kontrolle
        TextEingabe {
            id: werkstatt
            inputID: "edtWerkstatt"
            eingabeHilfe: "00-00-0000"
            anchors {
                top: tuv.bottom
                left: tuv.left
                topMargin: 10
            }
            blockiert: true
        }

        InfoText {
            id: fahrzeug_typ_auswahl_text
            text: "Fahrzeug Typ"
            anchors {
                right: fahrzeug_typ_auswahl.left
                top: fahrzeug_typ_auswahl.top
                rightMargin: 10
            }
        }

        // Combobox zur Auswahl des Fahrzeugtyps
        AuswahlListe {
            id: fahrzeug_typ_auswahl
            daten_modell: fahrzeug_typ_liste
            anchors {
                top: werkstatt.bottom
                left: werkstatt.left
                topMargin: 10
            }
            schreibbar: false
        }

        // Button zum Aufrufen der Fahrzeug Verwaltung
        Button {
            id: ausstattung_verwaltung
            buttonID: "btnAusstattungVerwaltung"
            anchors {
                right: ausstattung_anzeigen.left
                top: ausstattung_anzeigen.top
                leftMargin: 30
            }
            buttonText: "Ausstattung verwalten"
        }

        // Button zum freischalten der Datenfelder
        Button {
            id: info_bearbeiten
            buttonID: "btnInfoBearbeiten"
            anchors {
                right: ausstattung_verwaltung.left
                top: ausstattung_verwaltung.top
                leftMargin: 30
            }
            buttonText: "Fahrzeug Info Bearbeiten"
        }
    }

    // Komponenten zur Verwaltung der Fahrzeugausstattung
    Item {
        id: ausstattung_verwaltung_felder
        anchors.fill: parent
        visible: false

        Button {
            id: fahrzeug_ausstattung_abbrechen
            buttonID: "btnAusstattungVerwaltungStop"
            anchors {
                bottom: text_hinzufugen.top
                left: text_hinzufugen.left
                bottomMargin: 10
            }
            buttonText: "Verwaltung beenden"
        }

        // Hinzufügen eines Inventargegenstandes zur Ausstattung
        Button {
            id: ausstattung_hinzufugen
            buttonID: "btnAusstattungHinzufugen"
            x: parent.width / 2 - 75
            y: parent.height / 3
            buttonText: ">>>"
        }

        InfoText {
            id: text_hinzufugen
            text: "Neuer Gegenstand zur Ausstattung hinzufügen"
            anchors {
                bottom: ausstattung_hinzufugen.top
                left: ausstattung_hinzufugen.left
                bottomMargin: 10
            }
        }

        // Entfernt ein Ausstattungsgegenstand
        Button {
            id: ausstattung_entfernen
            buttonID: "btnAusstattungEntfernen"
            anchors {
                top: ausstattung_hinzufugen.bottom
                left: ausstattung_hinzufugen.right
                topMargin: 10
            }
            buttonText: "<<<"
        }

        InfoText {
            id: text_entfernen
            text: "Gegenstand aus der Ausstattung entfernen"
            anchors {
                top: ausstattung_entfernen.bottom
                right: ausstattung_entfernen.right
                topMargin: 10
            }
        }
    }

    ListenContainer {
        id: ausstattungliste
        listeID: "lstAusstattungliste"
        anchors.right: parent.right
        hoehe: parent.height
        breite: parent.width / 5
        eintragHoehe: 50
        visible: false
    }

    // Overlay

    Overlay {
        id: meldungen_overlay
        listeID: "lstMeldungen"
        daten_modell: meldungen_liste
        x: parent.width / 2.5
        anchors.top: parent.top
        breite: 300
        hoehe: 40
        eintragHoehe: 50
        text.pixelSize: 17
        }

    Button {
        id: bericht_erstellen
        buttonID: "btnBerichtErstellen"
        buttonText: "Meldungen Bericht"
        anchors {
            right: meldungen_overlay.left
            rightMargin: 20
        }
    }

    InfoText {
        id: uberschrift
        visible: false
        text: "Maschinist"
        schriftFarbe: "black"
        schriftGroesse: 50
        anchors {
            left: meldungen_overlay.right
            top: parent.top
            leftMargin: 10
        }
    }

    states: [
        State {
            name: "kein_fahrzeug"

            PropertyChanges {
                target: fahrzeug_info
                visible: false
            }

            PropertyChanges {
                target: uberschrift
                visible: true
            }
        },

        State {
            name: "fahrzeug_info"

            PropertyChanges {
                target: fahrzeug_info
                visible: true
            }
        },

        State {
            name: "ausstattung_auflisten"

            PropertyChanges {
                target: ausstattungliste
                daten_modell: ausstattung_auflistung
                visible: true
                eintragHoehe: 70
            }
        },

        State {
            name: "ausstattung_verwaltung"

            PropertyChanges {
                target: auswahl_liste
                listeID: "lstInventar"
                daten_modell: inventarliste
                eintragHoehe: 70
            }

            PropertyChanges {
                target: ausstattungliste
                daten_modell: ausstattung_auflistung
                visible: true
                eintragHoehe: 70
            }

            PropertyChanges {
                target: fahrzeug_info
                visible: false
            }

            PropertyChanges {
                target: ausstattung_verwaltung_felder
                visible: true
            }

            PropertyChanges {
                target: neu_fahrzeug
                visible: false
            }

            PropertyChanges {
                target: entfernen_fahrzeug
                visible: false
            }
        },

        State {
            name: "fahr_info_bearbeiten"

            PropertyChanges {
                target: kennzeichen
                blockiert: false
            }

            PropertyChanges {
                target: bezeichnung
                blockiert: false
            }

            PropertyChanges {
                target: besatzung
                blockiert: false
            }

            PropertyChanges {
                target: tuv
                blockiert: false
            }

            PropertyChanges {
                target: werkstatt
                blockiert: false
            }

            PropertyChanges {
                target: fahrzeug_typ_auswahl
                schreibbar: true
            }

            PropertyChanges {
                target: info_bearbeiten
                buttonID: "btnAbspeichern"
                buttonText: "Informationen Abspeichern"
            }
        },

        State {
            name: "neues_fahrzeug"

            PropertyChanges {
                target: kennzeichen
                visible: true
                blockiert: false
            }

            PropertyChanges {
                target: bezeichnung
                visible: true
                blockiert: false
            }

            PropertyChanges {
                target: besatzung
                visible: true
                blockiert: false
            }

            PropertyChanges {
                target: problemAuswahlText
                visible: true
            }

            PropertyChanges {
                target: problemAuswahl
                visible: true
            }

            PropertyChanges {
                target: problemKommentar
                visible: true
            }

            PropertyChanges {
                target: tuv
                visible: true
                blockiert: false
            }

            PropertyChanges {
                target: werkstatt
                visible: true
                blockiert: false
            }

            PropertyChanges {
                target: fahrzeug_typ_auswahl
                visible: true
                schreibbar: true
            }

            PropertyChanges {
                target: info_bearbeiten
                visible: true
                buttonID: "btnNeuFahrzeug"
                buttonText: "Neues Fahrzeug Abspeichern"
            }
        }
    ]
}
