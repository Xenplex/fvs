/*
    Ritchie Flick
    T3IFAN 2012/2013 Abschlussprojekt
    LN
    'Feuerwehrverwaltungssoftware - FVS'
    'src/QMLUI/Kommandant.qml'

     Erstellung des Hauptschirms des Kommandanten
*/

import QtQuick 1.1
import "../QML_Komponenten"

Rectangle {
    id: kommandant_schirm

    // Folgende Funtkion zum Kontrollieren von Datumen wurde kopiert von:
    // http://www.qodo.co.uk/blog/javascript-checking-if-a-date-is-valid/

    // Checks a string to see if it in a valid date format
    // of (D)D/(M)M/(YY)YY and returns true/false
    function isValidDate(s) {
        // format D(D)/M(M)/(YY)YY
        var dateFormat = /^\d{1,4}[\.|\/|-]\d{1,2}[\.|\/|-]\d{1,4}$/;

        if (dateFormat.test(s)) {
            // remove any leading zeros from date values
            s = s.replace(/0*(\d*)/gi,"$1");
            var dateArray = s.split(/[\.|\/|-]/);

            // correct month value
            dateArray[1] = dateArray[1]-1;

            // correct year value
            if (dateArray[2].length<4) {
                // correct year value
                dateArray[2] = (parseInt(dateArray[2]) < 50) ? 2000 + parseInt(dateArray[2]) : 1900 + parseInt(dateArray[2]);
            }

            var testDate = new Date(dateArray[2], dateArray[1], dateArray[0]);
            if (testDate.getDate()!=dateArray[0] || testDate.getMonth()!=dateArray[1] || testDate.getFullYear()!=dateArray[2]) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    // Attribute auf welche der Python Unterbau zugreifen kann
    property string edtMitgliedName
    property string edtMitgliedVorname
    property string edtMitgliedBDay
    property string edtMitgliedMobil
    property string edtMitgliedEmail
    property string edtMitgliedTelefon
    property string edtMitgliedOrtschaft
    property string edtMitgliedAdresse
    property string edtMitgliedPostleitzahl
    property string edtMitgliedLehrgangstunden
    property string edtMitgliedMedizinischeKontrolle
    property bool chkMitgliedAPTEinsatze
    property bool chkMitgliedAPTAtemschutz
    property string edtMitgliedSozialnummer

    // Attribute auf welche der Python Unterbau zugreift werden gesetzt
    function mitglied_info_abspeichern_qml() {
        edtMitgliedName = mitglied_name.text
        edtMitgliedVorname = mitglied_vorname.text
        edtMitgliedBDay = mitglied_geburtsdatum.text
        if (isValidDate(edtMitgliedBDay))
            fehler_datum.visible = false
        else {
            fehler_datum.visible = true
            return
        }
        edtMitgliedMobil = mitglied_mobiltelefon.text
        edtMitgliedEmail = mitglied_email.text
        edtMitgliedTelefon = mitglied_telefon.text
        edtMitgliedOrtschaft = mitglied_ortschaft.text
        edtMitgliedAdresse = mitglied_adresse.text
        edtMitgliedPostleitzahl = mitglied_postleitzahl.text
        edtMitgliedLehrgangstunden = mitglied_lehrgangstunden.text
        edtMitgliedMedizinischeKontrolle = mitglied_medizinische_kontrolle.text
        chkMitgliedAPTEinsatze = mitglied_apt_einsatze.checkAuswahl
        chkMitgliedAPTAtemschutz = mitglied_apt_atemschutz.checkAuswahl
        edtMitgliedSozialnummer = mitglied_sozialnummer.text
        operationen_kontroller.operations_signal("mitglied_variablen_gesetzt")
    }

    // Funktionen zum ändern der States
    function kommandant_bereich_aufbau_qml() {
        kommandant_schirm.state = "start"
    }

    function ausrustung_bearbeiten_qml() {
        kommandant_schirm.state = "ausrustung"
    }

    // Attribute aus dem Python Unterbau werden gelesen und gesetzt
    function mitglied_info_setzen_qml() {
        mitglied_name.text = mitglied_info_name
        mitglied_vorname.text = mitglied_info_vorname
        mitglied_sozialnummer.text = mitglied_info_sozialnummer
        mitglied_geburtsdatum.text = mitglied_info_bday
        mitglied_mobiltelefon.text = mitglied_info_mobiltelefon
        mitglied_email.text = mitglied_info_email
        mitglied_telefon.text = mitglied_info_telefonnummer
        mitglied_ortschaft.text = mitglied_info_ortschaft
        mitglied_adresse.text = (mitglied_info_adresse)
        mitglied_postleitzahl.text = mitglied_info_postleitzahl
        mitglied_lehrgangstunden.text = mitglied_info_lehrgangsstunden
        mitglied_medizinische_kontrolle.text = mitglied_info_medi
        if (mitglied_info_apteinsatz == 1) {
            mitglied_apt_einsatze.state = "checked"
            mitglied_apt_einsatze.checkAuswahl = true
        }
        else {
            mitglied_apt_einsatze.state = "unchecked"
            mitglied_apt_einsatze.checkAuswahl = false
        }
        if (mitglied_info_aptatemschutz == 1) {
            mitglied_apt_atemschutz.state = "checked"
            mitglied_apt_atemschutz.checkAuswahl = true
        }
        else {
            mitglied_apt_atemschutz.state = "unchecked"
            mitglied_apt_atemschutz.checkAuswahl = false
        }
        kommandant_schirm.state = "benutzer"
    }

    //Startstate
    state: "start"

    // Hauptfenster Attribute - werden überschrieben da FVS im FullScreen Modus
    // aufgerufen wird
    width: 1024
    height: 768

    // Hauptkomponenten
    ListenContainer {
        id: auswahl_liste
        anchors {
            left: neu_mitglied.left
            right: entfernen_mitglied.right
        }
        hoehe: parent.height
        eintragHoehe: 50
    }

    InfoText {
        id: uberschrift
        visible: false
        text: "Kommandant"
        schriftFarbe: "black"
        schriftGroesse: 40
        anchors {
            left: auswahl_liste.right
            top: parent.top
        }
    }

    Button {
        id: neu_mitglied
        buttonID: "btnNeuMitglied"
        buttonText: "Neues Mitglied"
        anchors {
            bottom: auswahl_liste.bottom
            bottomMargin: 10
        }
    }

    Button {
        id: entfernen_mitglied
        buttonID: "btnEntfernenMitglied"
        buttonText: "Entfernen Mitglied"
        anchors {
            top: neu_mitglied.top
            left: neu_mitglied.right
            leftMargin: 10
        }
    }

    // Hauptmenü
    Item {
        id: hauptmenu
        visible: true
        anchors {
            top: parent.top
            left: auswahl_liste.right
            right: parent.right
            topMargin: 70
        }
        height: parent.height / 13

        Button {
            id: sekretaer_bereich
            buttonID: "btnSekretaerBereich"
            buttonText: "Sekretär\nBereich"
            anchors {
                left: parent.left
                leftMargin: 10
            }
        }

        Button {
            id: inventarist_bereich
            buttonID: "btnInventarBereich"
            buttonText: "Inventarist\nBereich"
            anchors {
                left: sekretaer_bereich.right
                top: sekretaer_bereich.top
                leftMargin: 10
            }
        }

        Button {
            id: maschinist_bereich
            buttonID: "btnMaschinistBereich"
            buttonText: "Maschinist\nBereich"
            anchors {
                left: inventarist_bereich.right
                top: inventarist_bereich.top
                leftMargin: 10
            }
        }
    }

    Item {
        id: benutzer
        visible: false
        anchors {
            left: auswahl_liste.right
            top: hauptmenu.bottom
            bottom: parent.bottom
            right: parent.right
        }

        InfoText {
            id: mitglied_name_text
            text: "Name"
            anchors {
                top: parent.top
                left: parent.left
                margins: 10
            }
        }

        TextEingabe {
            id: mitglied_name
            inputID: "edtMitgliedName"
            anchors {
                top: mitglied_name_text.top
                left: mitglied_name_text.right
                leftMargin: 10
            }
        }

        InfoText {
            id: mitglied_vorname_text
            text: "Vorname"
            anchors {
                left: mitglied_name_text.left
                top: mitglied_name_text.bottom
                topMargin: 10
            }
        }

        TextEingabe {
            id: mitglied_vorname
            inputID: "edtMitgliedVorname"
            anchors {
                top: mitglied_vorname_text.top
                left: mitglied_vorname_text.right
                right: mitglied_name.right
                leftMargin: 10
            }
        }

        InfoText {
            id: mitglied_sozialnummer_text
            text: "Sozialnummer"
            anchors {
                top: mitglied_vorname_text.bottom
                left: mitglied_vorname_text.left
                topMargin: 10
            }
        }

        TextEingabe {
            id: mitglied_sozialnummer
            inputID: "edtMitgliedSozialnummer"
            anchors {
                top: mitglied_sozialnummer_text.top
                left: mitglied_sozialnummer_text.right
                right: mitglied_name.right
                leftMargin: 10
            }
        }

        InfoText {
            id: mitglied_geburtsdatum_text
            text: "Geburtsdatum"
            anchors {
                top: mitglied_sozialnummer_text.bottom
                left: mitglied_sozialnummer_text.left
                topMargin: 10
            }
        }

        TextEingabe {
            id: mitglied_geburtsdatum
            inputID: "edtMitgliedBDay"
            eingabeHilfe: "00-00-0000"
            anchors {
                top: mitglied_geburtsdatum_text.top
                left: mitglied_geburtsdatum_text.right
                right: mitglied_name.right
                leftMargin: 10
            }
        }

        InfoText {
            id: mitglied_mobiltelefon_text
            text: "Mobiltelefon"
            anchors {
                top: mitglied_geburtsdatum_text.bottom
                left: mitglied_geburtsdatum_text.left
                topMargin: 10
            }
        }

        TextEingabe {
            id: mitglied_mobiltelefon
            inputID: "edtMitgliedMobil"
            anchors {
                left: mitglied_mobiltelefon_text.right
                top: mitglied_mobiltelefon_text.top
                right: mitglied_name.right
                leftMargin: 10
            }
        }

        InfoText {
            id: mitglied_email_text
            text: "Email"
            anchors {
                top: mitglied_mobiltelefon_text.bottom
                left: mitglied_mobiltelefon_text.left
                topMargin: 10
            }
        }

        TextEingabe {
            id: mitglied_email
            inputID: "edtMitgliedEmail"
            anchors {
                left: mitglied_email_text.right
                top: mitglied_email_text.top
                right: mitglied_name.right
                leftMargin: 10
            }
        }

        InfoText {
            id: mitglied_telefon_text
            text: "Festnetz"
            anchors {
                top: mitglied_email_text.bottom
                left: mitglied_email_text.left
                topMargin: 10
            }
        }

        TextEingabe {
            id: mitglied_telefon
            inputID: "edtMitgliedTelefon"
            anchors {
                left: mitglied_telefon_text.right
                top: mitglied_telefon_text.top
                right: mitglied_name.right
                leftMargin: 10
            }
        }

        InfoText {
            id: mitglied_ortschaft_text
            text: "Ortschaft"
            anchors {
                top: mitglied_telefon_text.bottom
                left: mitglied_telefon_text.left
                topMargin: 10
            }
        }

        TextEingabe {
            id: mitglied_ortschaft
            inputID: "edtMitgliedOrtschaft"
            anchors {
                left: mitglied_ortschaft_text.right
                top: mitglied_ortschaft_text.top
                right: mitglied_name.right
                leftMargin: 10
            }
        }

        Button {
            id: aktualisieren
            buttonID: "btnAktualisieren"
            buttonText: "Änderungen speichern"
            anchors {
                top: mitglied_name.top
                left: mitglied_name.right
                leftMargin: 50
            }
        }

        Button {
            id: ausrustung_bearbeiten
            buttonID: "btnAusrustungBearbeiten"
            buttonText: "Ausrüstung\nbearbeiten"
            anchors {
                top: aktualisieren.top
                left: aktualisieren.right
                leftMargin: 10
            }
        }

        InfoText {
            id: mitglied_adresse_text
            text: "Adresse"
            anchors {
                left: mitglied_ortschaft_text.left
                top: mitglied_ortschaft_text.bottom
                topMargin: 10
            }
        }

        TextEingabe {
            id: mitglied_adresse
            inputID: "edtMitgliedAdresse"
            text: "teetdjfdjsafhsb"
            anchors {
                left: mitglied_adresse_text.right
                top: mitglied_adresse_text.top
                right: mitglied_name.right
                leftMargin: 10
            }
        }

        InfoText {
            id: mitglied_postleitzahl_text
            text: "Postleitzahl"
            anchors {
                left: mitglied_adresse_text.left
                top: mitglied_adresse_text.bottom
                topMargin: 10
            }
        }

        TextEingabe {
            id: mitglied_postleitzahl
            inputID: "edtMitgliedPostleitzahl"
            anchors {
                left: mitglied_postleitzahl_text.right
                top: mitglied_postleitzahl_text.top
                right: mitglied_name.right
                leftMargin: 10
            }
        }

        InfoText {
            id: mitglied_lehrgangstunden_text
            text: "Lehrgangstunden"
            anchors {
                left: mitglied_postleitzahl_text.left
                top: mitglied_postleitzahl_text.bottom
                topMargin: 10
            }
        }

        TextEingabe {
            id: mitglied_lehrgangstunden
            inputID: "edtMitgliedLehrgangstunden"
            anchors {
                left: mitglied_lehrgangstunden_text.right
                top: mitglied_lehrgangstunden_text.top
                right: mitglied_name.right
                leftMargin: 10
            }
        }

        InfoText {
            id: mitglied_medizinisch_text
            text: "Mediz. Kontrolle"
            anchors {
                left: mitglied_lehrgangstunden_text.left
                top: mitglied_lehrgangstunden_text.bottom
                topMargin: 10
            }
        }

        TextEingabe {
            id: mitglied_medizinische_kontrolle
            inputID: "edtMediKontrolle"
            eingabeHilfe: "00-00-0000"
            anchors {
                left: mitglied_medizinisch_text.right
                top: mitglied_medizinisch_text.top
                right: mitglied_name.right
                leftMargin: 10
            }
        }

        InfoText {
            id: apt_einsatze_text
            text: "APT Einsatz?"
            anchors {
                left: mitglied_medizinisch_text.left
                top: mitglied_medizinisch_text.bottom
                topMargin: 10
            }
        }

        InfoText {
            id: fehler_datum
            visible: false
            schriftFarbe: "red"
            text: "Es ist ein Fehler mit\neinem Datum augetreten, bitte
kontrollieren\nSie ihre Angaben!"
            anchors {
                left: apt_einsatze_text.left
                top: apt_einsatze_text.bottom
                topMargin: 75
            }
        }

        JaNeinAuswahl {
            id: mitglied_apt_einsatze
            auswahlID: "chkMitgliedAPTEinsatze"
            anchors {
                top: apt_einsatze_text.top
                right: mitglied_name.right
                leftMargin: 10
            }
        }

        InfoText {
            id: apt_atemschutz_text
            text: "APT Atemschutz"
            anchors {
                left: apt_einsatze_text.left
                top: apt_einsatze_text.bottom
                topMargin: 10
            }
        }

        JaNeinAuswahl {
            id: mitglied_apt_atemschutz
            auswahlID: "chkMitgliedAPTAtemschutz"
            anchors {
                top: apt_atemschutz_text.top
                right: mitglied_name.right
                leftMargin: 10
            }
        }

        InfoText {
            id: lehrgange_liste_text
            text: "Lehrgänge"
            anchors {
                left: lehrgaenge_liste.left
                right: lehrgaenge_liste.right
                bottom: lehrgaenge_liste.top
                bottomMargin: 10
            }
        }

        ListenContainer {
            id: lehrgaenge_liste
            listeID: "lstLehrgaenge"
            anchors {
                left: aktualisieren.left
                top: aktualisieren.bottom
                topMargin: 50
            }
            hoehe: 250
        }

        Button {
            id: lehrgang_hinzufugen
            buttonID: "btnLehrgangHinzu"
            buttonText: "Hinzufügen"
            anchors {
                top: lehrgaenge_liste.bottom
                left: lehrgaenge_liste.left
                topMargin: 10
            }
        }

        AuswahlListe {
            id: lehrgaenge_auswahl
            comboID: "cmbLehrgaengeAuswahl"
            anchors {
                top: lehrgang_hinzufugen.bottom
                left: lehrgang_hinzufugen.left
                topMargin: 10
            }
        }

        Button {
            id: lehrgang_entfernen
            buttonID: "btnLehrgangEntfernen"
            buttonText: "Entfernen"
            anchors {
                top: lehrgaenge_auswahl.bottom
                left: lehrgaenge_auswahl.left
                topMargin: 10
            }
        }

        InfoText {
            id: auszeichnung_liste_text
            text: "Auszeichnungen"
            anchors {
                left: auszeichnung_liste.left
                right: auszeichnung_liste.right
                bottom: auszeichnung_liste.top
                bottomMargin: 10
            }
        }

        ListenContainer {
            id: auszeichnung_liste
            listeID: "lstAuszeichnungen"
            anchors {
                top: lehrgaenge_liste.top
                left: lehrgaenge_liste.right
                leftMargin: 30
            }
            hoehe: 250
        }

        Button {
            id: auszeichnung_hinzufugen
            buttonID: "btnAuszeichnungHinzu"
            buttonText: "Hinzufügen"
            anchors {
                top: auszeichnung_liste.bottom
                left: auszeichnung_liste.left
                topMargin: 10
            }
        }

        AuswahlListe {
            id: auszeichnung_auswahl
            comboID: "cmbAuszeichnungAuswahl"
            anchors {
                top: auszeichnung_hinzufugen.bottom
                left: auszeichnung_hinzufugen.left
                topMargin: 10
            }
        }

        Button {
            id: auszeichnung_entfernen
            buttonID: "btnAuszeichnungEntfernen"
            buttonText: "Entfernen"
            anchors {
                top: auszeichnung_auswahl.bottom
                left: auszeichnung_auswahl.left
                topMargin: 10
            }
        }

        InfoText {
            id: fuhrerscheine_liste_text
            text: "Führerscheine"
            anchors {
                left: fuhrerscheine_liste.left
                right: fuhrerscheine_liste.right
                bottom: fuhrerscheine_liste.top
                bottomMargin: 10
            }
        }

        ListenContainer {
            id: fuhrerscheine_liste
            listeID: "lstFuhrerscheine"
            anchors {
                left: auszeichnung_liste.right
                top: auszeichnung_liste.top
                leftMargin: 30
            }
            hoehe: 250
        }

        Button {
            id: fuhrerschein_hinzufugen
            buttonID: "btnFuhrerscheinHinzu"
            buttonText: "Hinzufügen"
            anchors {
                top: fuhrerscheine_liste.bottom
                left: fuhrerscheine_liste.left
                topMargin: 10
            }
        }

        AuswahlListe {
            id: fuhrerschein_auswahl
            comboID: "cmbFuhrerscheinAuswahl"
            anchors {
                top: fuhrerschein_hinzufugen.bottom
                left: fuhrerschein_hinzufugen.left
                topMargin: 10
            }
        }

        Button {
            id: fuhrerschein_entfernen
            buttonID: "btnFuhrerscheinEntfernen"
            buttonText: "Entfernen"
            anchors {
                top: fuhrerschein_auswahl.bottom
                left: fuhrerschein_auswahl.left
                topMargin: 10
            }
        }
    }

    Item {
        id: ausrustung_verwaltung
        visible: false
        anchors {
            left: auswahl_liste.right
            top: hauptmenu.bottom
            bottom: parent.bottom
            right: parent.right
        }

        ListenContainer {
            id: ausrustung
            listeID: "lstAusrustung"
            anchors {
                right: parent.right
                top: parent.top
                bottom: parent.bottom
            }
            breite: 300
        }

        Button {
            id: ausrustung_hinzu
            buttonID: "btnAusrustungHinzu"
            buttonText: "Hinzufügen"
            anchors {
                left: parent.left
                top: parent.top
                margins: 100
            }
        }

        Button {
            id: ausrustung_entfernen
            buttonID: "btnAusrustungEntfernen"
            buttonText: "Entfernen"
            anchors {
                left: ausrustung_hinzu.right
                top: ausrustung_hinzu.top
                leftMargin: 10
            }
        }

        Button {
            id: zuruck_mitglied
            buttonID: "btnZuruckMitglied"
            buttonText: "Zurück zu den\nMitglied Infos"
            anchors {
                left: ausrustung_hinzu.left
                right: ausrustung_entfernen.right
                top: ausrustung_hinzu.bottom
                topMargin: 10
            }
        }
    }

    // Overlay
    Overlay {
        id: meldungen_overlay
        listeID: "lstMeldungen"
        daten_modell: meldungen_liste
        x: parent.width / 2.5
        anchors.top: parent.top
        breite: 300
        hoehe: 40
        eintragHoehe: 50
        text.pixelSize: 17
    }

    Button {
        id: overlay_bericht
        buttonID: "btnOverlayBericht"
        buttonText: "Bericht erstellen"
        anchors {
            left: meldungen_overlay.right
            top: meldungen_overlay.top
            leftMargin: 10
        }
    }

    states: [
        State {
            name: "start"

            PropertyChanges {
                target: auswahl_liste
                listeID: "lstMitglieder"
                daten_modell: mitglieder_liste
            }

            PropertyChanges {
                target: uberschrift
                visible: true
            }
        },

        State {
            name: "benutzer"

            PropertyChanges {
                target: auswahl_liste
                listeID: "lstMitglieder"
                daten_modell: mitglieder_liste
            }

            PropertyChanges {
                target: benutzer
                visible: true
            }

            PropertyChanges {
                target: lehrgaenge_liste
                daten_modell: mitglied_info_lehrgaenge
            }

            PropertyChanges {
                target: lehrgaenge_auswahl
                daten_modell: alle_lehrgaenge
            }

            PropertyChanges {
                target: auszeichnung_liste
                daten_modell: mitglied_info_auszeichnungen
            }

            PropertyChanges {
                target: auszeichnung_auswahl
                daten_modell: alle_auszeichnungen
            }

            PropertyChanges {
                target: fuhrerscheine_liste
                daten_modell: mitglied_info_fuhrerscheine
            }

            PropertyChanges {
                target: fuhrerschein_auswahl
                daten_modell: alle_fuhrerscheine
            }
        },

        State {
            name: "ausrustung"

            PropertyChanges {
                target: auswahl_liste
                listeID: "lstInventar"
                daten_modell: inventar_liste
                eintragHoehe: 70
            }

            PropertyChanges {
                target: ausrustung_verwaltung
                visible: true
            }

            PropertyChanges {
                target: ausrustung
                listeID: "lstAusrustung"
                daten_modell: ausrustung_liste
                eintragHoehe: 70
            }

            PropertyChanges {
                target: neu_mitglied
                visible: false
            }

            PropertyChanges {
                target: entfernen_mitglied
                visible: false
            }
        }

    ]
}
