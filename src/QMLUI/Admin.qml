/*
    Ritchie Flick
    T3IFAN 2012/2013 Abschlussprojekt
    LN
    'Feuerwehrverwaltungssoftware - FVS'
    'src/QMLUI/Admin.qml'

     Erstellung des Hauptschirms des Administrators
*/

import QtQuick 1.1
import "../QML_Komponenten"

Rectangle {
    id: admin_schirm

    function tabelle_kontrollieren_qml() {
        tabelle_kontrollieren_resultat.text = kontrolle
        if (kontrolle === "OK")
            tabelle_kontrollieren_resultat.color = "green"
    }

    //Startstate
    state: "start"

    // Hauptfenster Attribute - werden überschrieben da FVS im FullScreen Modus
    // aufgerufen wird
    width: 1024
    height: 768

    ListenContainer {
        id: tabellen
        listeID: "lstTabellen"
        daten_modell: tabellen_liste
        anchors {
            left: parent.left
            top: parent.top
            margins: 50
        }
        breite: 300
        hoehe: 300
    }

    Button {
        id: tabelle_kontrollieren
        buttonID: "btnTabelleKontrolle"
        buttonText: "Tabelle Kontrolle"
        anchors {
            left: tabellen.right
            top: tabellen.top
            leftMargin: 10
        }
    }

    Button {
        id: tabelle_reparieren
        buttonID: "btnTabelleReparieren"
        buttonText: "Reparieren"
        anchors {
            left: tabelle_kontrollieren.left
            top: tabelle_kontrollieren.bottom
            topMargin: 10
        }
    }

    InfoText {
        text: "Funktion mit der verwendeten Storage Engine\n
                InnoDB nicht verfügbar"
        color: "red"
        font.bold: true
        anchors {
            left: tabelle_reparieren.right
            top: tabelle_reparieren.top
            leftMargin: 30
        }
    }

    Button {
        id: tabelle_optimieren
        buttonID: "btnTabelleOptimieren"
        buttonText: "Optimieren"
        anchors {
            left: tabelle_reparieren.left
            top: tabelle_reparieren.bottom
            topMargin: 10
        }
    }

    InfoText {
        id: tabelle_kontrollieren_resultat
        color: "red"
        anchors {
            left: tabelle_kontrollieren.right
            top: tabelle_kontrollieren.top
            leftMargin: 10
        }
    }
}
