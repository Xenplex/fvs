# -*- coding: utf-8 *-*
#!/usr/bin/env python3.2

# Ritchie Flick
# T3IFAN 2012/2013 Abschlussprojekt
# LN
# 'Feuerwehrverwaltungssoftware - FVS'
# 'src/fvs.py'

"""
**fvs.py** ist die Ausführungsdatei von **FVS** und dient zum Starten des
Programmes. Das Loginfenster wird geladen und aufgerufen.
"""

# Importieren von Python Modulen
import sys

# Importieren von PySide Modulen
from PySide.QtGui import QApplication

# Importieren von FVS Modulen
from UI_Aufbau import login


# Falls diese Datei direkt ausgeführt wird dann:
if __name__ == '__main__':
    fvs = QApplication(sys.argv)
    Login = login.QMLUI()
    Login.show()
    sys.exit(fvs.exec_())
