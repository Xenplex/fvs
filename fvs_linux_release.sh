#!/bin/bash

# Ritchie Flick
# T3IFAN 2012/2013 Abschlussprojekt
# LN
# 'Feuerwehrverwaltungssoftware - FVS'
# 'fvs_linux_release.sh'

# **Beschreibung**

# 'fvs_linux_release.sh' ist ein *Bash* Skript zur Kompilation von
# **FVS - Feuerwehrverwaltungssoftware**, der Generierung seiner
# Dokumentation mit *Sphinx* und weiteren Funktionen.

# Das Skript lässt den Entwickler eine *CFG* Datei erstellen als
# Konfigurationsdatei, in dem die aktuelle Versionsnummer von **FVS**
# angegeben wird, der Name des Entwicklers und gegebenfalls in welches
# Verzeichnis das Projekt *exportiert* werden soll.

# **FVS** wird mit der angegebenen Versionsnummer und Entwickler Name
# kompiliert und ins entsprechende **Release** Verzeichnis
# kopiert. Die Bibliotheken werden in ein verstecktes Verzeichnis kopiert,
# damit der Benutzer keine für ihn unnötigen Dateien sieht, um die Benutzer-
# freundlichkeit zu erhöhen.

# Die Dokumentation von **FVS** wird mit der angegebenen Versionsnummer
# und dem Namen des Entwicklers neu generiert und ins aktuelle
# **Release** Verzeichnis von **FVS** kopiert.

# Die benötigten *SQL* Skripte werden in das aktuelle **Release** Verzeichnis
# kopiert um dem Entwickler/Benutzer die Einrchtung der Datenbank und/oder das
# Entwickeln/Testen von **FVS** zu erleichtern.

# Das **Release** Verzeichnis kann optional *exportiert* werden in ein
# gewünschte Verzeichnis. (z.Bsp.: *Dropbox*)

# Des weiteren kann **FVS** mit *git* und der angegebenen Versionsnummer
# *getagt* und anschließend automatisch ins eingerichtete
# *git repository* *gepusht* werden. (z.Bsp.: *Github*, *Bitbucket*)


# Kontrolliert ob die Konfigurationsdatei von **FVS** existiert oder nicht.

# **Beschreibung**

# Wenn die Konfigurationsdatei nicht existiert wird der Entwickler
# aufgefordert anzugeben ob er eine Konfigurationsdatei erstellen möchte
# oder nicht.

# Soll eine Konfigurationsdatei erstellt werden, wird die benötigte Funktion
# aufgerufen.
# Wenn keine Konfigurationsdatei erstellt werden soll, so wird das Skript
# beendet.
function Kontrolliere-fvs_konfig()
{
    if [ ! -f ./fvs_release_konfig.cfg ]; then
        until [ "$antwort" == "y" ] || [ "$antwort" == "n" ];
        do
            echo "Es ist noch keine Konfigurationsdatei vorhanden!"
            echo "Soll Konfigurationsdatei erstellt werden? [y/n]"
            read antwort
        done
        if [ "$antwort" == "n" ]; then
            echo "Es wird eine Konfigurationsdatei benötigt!"
            echo "Das Skript wird beendet..."
            exit
        else
            ErstelleKonfig
        fi
    else
        echo "Konfigurationsdatei vorhanden!"
    fi
}


# Erstellung der Konfigurationsdatei

# **Beschreibung**

# Der Entwickler wird aufgefordert verschiedene Parameter anzugeben, mit
# welchen die *XML* Konfigurationsdatei ins *root* Verzeichnis von **FVS**
# erstellt wird.
function ErstelleKonfig()
{
    echo "Bitte geben sie den Entwickler Namen ein:"
    read entwicklerName
    echo "Bitte geben sie aktuelle Versionsnummer an:"
    read versionNummer
    echo "Bitte geben sie ein Verzeichnis zum Export an:"
    read exportVerzeichnis
    echo "name=\"$entwicklerName\"" > ./fvs_release_konfig.cfg
    echo "version=\"$versionNummer\"" >> ./fvs_release_konfig.cfg
    echo "export=\"$exportVerzeichnis\"" >> ./fvs_release_konfig.cfg
}


# Auslesen der Konfigurationsdatei

# **Beschreibung**

# Die abgespeicherten Parameter werden aus der *XML* Konfigurationsdatei
# ausgelesen.

# **Rückgabewerte**

# * $KonfDaten: Enthält die Einstellungen für das Release Skript im XML Format
function AuslesenKonfig()
{
    source ./fvs_release_konfig.cfg
    KonfDaten[0]=$name
    KonfDaten[1]=$version
    KonfDaten[2]=$export
}


# Erstellen der Release Verzeichnisse

# **Beschreibung**

# Alle benötigten Verzeichnisse zum Release werden, wenn noch nicht
# vorhanden, erstellt.

# Die Namen der Verzeichnisse hängen von der aktuellen Versionsnummer ab.
function ReleaseVerzeichnisse()
{
    AuslesenKonfig

    if [ ! -d ./Release ]; then
        mkdir ./Release
    fi

    if [ ! -d ./Release/Linux${KonfDaten[1]} ]; then
        mkdir ./Release/Linux${KonfDaten[1]}
    fi

    if [ ! -d ./Release/Linux${KonfDaten[1]}/.fvs ]; then
        mkdir ./Release/Linux${KonfDaten[1]}/.fvs
    fi

    if [ ! -d ./Release/Linux${KonfDaten[1]}/SQL ]; then
        mkdir ./Release/Linux${KonfDaten[1]}/SQL
    fi

    if [ ! -d ./Release/Linux${KonfDaten[1]}/Berichte ]; then
        mkdir ./Release/Linux${KonfDaten[1]}/Berichte
    fi
}


# Kompilation von **FVS**

# **Beschreibung**

# **FVS** wird mit Hilfe von *cx_freeze* zu einer EXE-Datei kompiliert und
# anschließend ins Release Verzeichnis seiner aktuellen Version
# verschoben.

# Alle Dateien welche zum Aufbau der *GUI* benötigt werden, als auch die
# Konfigurationsdatei zur Verbindungsherstellung zur Datenbank, werden
# außerdem ins Release Verzeichnis der aktuellen Version kopiert/erstellt.
function Kompilieren()
{
    AuslesenKonfig

    cd src
    python3.2 setup.py build > /dev/null
    cd ..

    cp -rf ./src/build/exe.linux-x86_64-3.2/* ./Release/Linux${KonfDaten[1]}/.fvs
    rm -rf ./src/build

    cp -rf ./src/QML_Komponenten ./Release/Linux${KonfDaten[1]}/.fvs
    cp -rf ./src/QMLUI ./Release/Linux${KonfDaten[1]}/.fvs

    touch ./Release/Linux${KonfDaten[1]}/.fvs/konfig.cfg
    echo " [Einstellungen]
    datenbankname = fvs_datenbank
    datenbankpasswort = fvspw
    datenbankadresse = 127.0.0.1
    datenbankport = 3306
    datenbankbenutzer = fvs
    " > "./Release/Linux${KonfDaten[1]}/.fvs/konfig.cfg"

    echo "#!/bin/bash
    cd .fvs
    ./fvs" > ./Release/Linux${KonfDaten[1]}/FVS
    chmod a+x ./Release/Linux${KonfDaten[1]}/FVS
}


# SQL Skripte werden kopiert

# **Beschreibung**

# Die SQL Skripte werden ins jeweilige **Release** Verzeichnis kopiert.
function KopierenSQL()
{
    AuslesenKonfig

    cp ./src/Datenbank/*.sql ./Release/Linux${KonfDaten[1]}/SQL
}


# CSS Datei wird kopiert

# **Beschreibung**

# Die CSS Datei für die Berichte wird ins jeweilige **Release** Verzeichnis
# kopiert
function KopierenCSS()
{
    AuslesenKonfig

    cp ./src/Berichte/fvs.css ./Release/Linux${KonfDaten[1]}/Berichte
}


# Dokumentation erstellen

# **Beschreibung**

# Die Dokumentation von **FVS** wird mit *Sphinx* erstellt/generiert.

# Alle *readme.rst* Dateien der einzelnen Verzeichnisse werden ins *Source*
# Verzeichnis von *Sphinx* kopiert und gemäß des Namens des Verzeichnisses
# umbenannt.

# Die Dokumentation wird als *HTML* Webseite generiert und als *TXT* Datei
# generiert.

# Anschließend werden alle kopierten *.rst Dateien wieder aus dem Verzeichnis
# gelöscht und die Dokumentation wird ins aktuelle Release Verzeichnis
# kopiert.
function ErstellenDokumentation()
{
    ReleaseVerzeichnisse

    AuslesenKonfig

    ReadmeDateien=$(find ./src/ -type f -name "*.rst")
    for f in $ReadmeDateien
    do
        verzeichnis=${f%*/*}
        verzeichnis_name=$(basename $verzeichnis)
        cp $f ./Sphinx/source/$verzeichnis_name.rst
    done

    # Readme Dateien aus anderen Verzeichnissen werden kopiert
    cp readme.rst ./Sphinx/source/Einleitung.rst
    cp ./Sphinx/readme.rst ./Sphinx/source/Sphinx.rst
    cp ./Aufbau/readme.rst ./Sphinx/source/Aufbau.rst
    
    # Die Bilder müssen über diesen Weg importiert werden, damit im Online Repository
    # die Bilder in den readme.rst Dateien angezeigt werden und in der Erstellten
    # Dokumentation zu sehen sind
    mkdir ./Sphinx/source/DatenbankMCD
    mkdir ./Sphinx/source/DatenbankMCD/datenbankdesign/
    mkdir ./Sphinx/source/MySQLWorkbench
    mkdir ./Sphinx/source/Struktur
    mkdir ./Sphinx/source/Kontroller
    cp ./Aufbau/DatenbankMCD/datenbankdesign/fvs_datenbank_MCD.png ./Sphinx/source/DatenbankMCD/datenbankdesign/fvs_datenbank_MCD.png
    cp ./Aufbau/MySQLWorkbench/fvs_datenbank_eer.png ./Sphinx/source/MySQLWorkbench/fvs_datenbank_eer.png
    cp ./Aufbau/Struktur/struktur.png ./Sphinx/source/Struktur/struktur.png
    cp ./Aufbau/Kontroller/einfacher_kontroller.png ./Sphinx/source/Kontroller/einfacher_kontroller.png
    cp ./Aufbau/Kontroller/operationen_kontroller.png ./Sphinx/source/Kontroller/operationen_kontroller.png

    cd ./Sphinx
    # Wegen einer technischen Einschränkung ist es nicht möglich den Author
    # Namen in der *.pdf Datei zu ändern
    make html latexpdf name="${KonfDaten[0]}" version=${KonfDaten[1]} > /dev/null
    cd ..

    for f in $ReadmeDateien
    do
        verzeichnis=${f%*/*}
        verzeichnis_name=$(basename $verzeichnis)
        rm ./Sphinx/source/$verzeichnis_name.rst
    done

    rm ./Sphinx/source/Einleitung.rst
    rm ./Sphinx/source/Sphinx.rst
    rm ./Sphinx/source/Aufbau.rst
    rm -rf ./Sphinx/source/DatenbankMCD
    rm -rf ./Sphinx/source/MySQLWorkbench
    rm -rf ./Sphinx/source/Struktur
    rm -rf ./Sphinx/source/Kontroller

    cp -r ./Dokumentation/html ./Release/Linux${KonfDaten[1]}/Dokumentation/
    cp -r ./Dokumentation/latex/FVS.pdf ./Release/Linux${KonfDaten[1]}/Dokumentation/FVS.pdf
}


# Exportieren des Release Verzeichnisses

# **Beschreibung**

# Das aktuelle **Release** Verzeichnis wird in das gewünschte Verzeichnis auf
# dem selben PC exportiert.
function Exportieren()
{
    ReleaseVerzeichnisse

    AuslesenKonfig

    cp -rf ./Release/Linux${KonfDaten[1]} $export
}


# Exportieren der Git Repository

# **Beschreibung**

# Das Git Repository von **FVS** wird mit der aktuellen Versionsnummer
# *git tag* und anschließend mit *git push* veröffentlicht.
function GitExportieren()
{
    AuslesenKonfig

    git tag ${KonfDaten[1]}
    git push --tags
    git push
}


# FVS aufrufen

# **Beschreibung**

# Die kompilierte Version von **FVS** wird aufgerufen.
function FVSStarten()
{
    AuslesenKonfig

    cd ./Release/Linux${KonfDaten[1]}
    ./FVS
}


# Hauptausführung
KonfDaten[0]=''
KonfDaten[1]=''
KonfDaten[2]=''

if [ "$1" == "Komplett" ]; then
    Kontrolliere-fvs_konfig
    ReleaseVerzeichnisse
    Kompilieren
    KopierenSQL
    KopierenCSS
    ErstellenDokumentation
    Exportieren
    GitExportieren
    FVSStarten
elif [[ $1 == 'Dokumentation' ]]; then
    ErstellenDokumentation
else
    echo "Keine Parameter übergeben"
fi