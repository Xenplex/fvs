Projekt Aufbau
=================

Datenbank Aufbau
-----------------

MCD
^^^^

*MCD* Diagramm der kompletten Datenbank:

.. image:: DatenbankMCD/datenbankdesign/fvs_datenbank_MCD.png

MySQL Workbench EER Modell
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Die komplette Datenbank als *EER Modell* [#]_ generiert mit *MySQL Workbench*.

.. image:: MySQLWorkbench/fvs_datenbank_eer.png

Erklärungen zu der einzelnen Tabellen
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

tblMitglied
************

**tblMitglied** enthält alle Informationen über ein Mitglied.

tblEinAustritt
***************

**tblEinAustritt** enthält alle Informationen über einen Ein- bzw Austritt aus der Feuerwehr eines Mitglieds. Jedes Mitglied kann mehrmals aus der Feuerwehr Ein- bzw Austreten.

tblAuszeichnung
****************

**tblAuszeichnung** enthält alle möglichen Auszeichnungen welche ein Mitglied während seinen Dienstjahren in der Feuerwehr erhalten kann.

tblMitgliederFührerscheine
***************************

**tblMitgliederFührerscheine** enthält alle möglichen Führerscheine welche ein Mitglied besitzen kann.

tblPosition
************

**tblPosition** enthält alle Positionen welche von den Mitglieder eingenommen werden können.

tblEreignis
************

**tblEreignis** enthält alle Ereignisse welche für eine Feuerwehr auftreten können. Zu Eregnissen zählen:

- Übungen
- Medizinische Kontrollen
- Fest
- Einsätze

tblLehrgang
************

**tblLehrgang** enthält alle möglichen Lehrgänge welche ein Mitglied absolvieren kann und wie lange ein Lehrgang dauert.

tblEinsatzbericht
******************

**tblEinsatzbericht** wird als Erweiterung der Tabelle **tblEreignis** verwendet und enthält alle weiteren Informationen welche für einen Einsatz aufgezeichnet werden müssen.

tblInventar
************

**tblInventar** enthält alle Inventargegenstände einer Feuerwehr. Dazu zählen:

- Kleider
- Kommunikationsgeräte
- Erste Hilfe Ausrüstung
- Wasserführende Armaturen
- Schläuche
- Atemschutzgeräte
- Maschinen (Lüfter, Motorsäge, ...)

tblFahrzeug
************

**tblFahrzeug** enthält alle Informationen welche über ein Fahrzeug benötigt werden.

tblProtokolle
**************

Enthält alle möglichen Protokolle welche erstellt worden sind. Ein Protokoll hat Verbindungen zu den Tabellen:

- **tblMitglied**
- **tblEreignis**
- **tblFahrzeug**
- **tblInventar**

Da innerhalb eines Protokoll *Referenzen* auf einzelne Mitglieder, Ereignisse, Fahrzeuge, Inventargegenstände gesetzt werden können.

Programm Aufbau
----------------

Struktur
^^^^^^^^^^

.. image:: Struktur/struktur.png

Die **fvs.py** ist die Hauptdatei und von ihr aus wird **FVS** gestartet. Im Verzeichnis **UI_Aufbau** sind die Hauptdateien der einzelnen Bereiche. Sie importieren die für sie benötigten *Python* *Module* und sie bauen die grafische Benutzeroberfläche aus **QMLUI** auf.

**QML_Komponenten** enthält einzelne Komponenten der **QMLUI** (Buttons, Listen, Labels, ...), aus welchen die Oberfläche aufgebaut wird.

Weitere Informationen zu den einzelnen Dateien/Verzeichnisse ist zu finden im Teil der :ref:`programmcode-dokumentation`

Kontroller
^^^^^^^^^^^

Die Kontroller kümmern sich um das *empfangen* der Signale aus der **QMLUI** und dem Aufrufen der jeweiligen Methode, welche zur ID des Senders passen.

*Legende*

- **QMLUI**: *XY* QML Komponent aus der grafischen Oberfläche
- **Kontroller**: *XY* Kontroller zuständig für *XY* QML Komponent
- **UI_Aufbau**: *XY* Modul welches die **QMLUI** aufgebaut hat

Einfacher Kontroller
**********************

.. image:: Kontroller/einfacher_kontroller.png

Operationen Kontroller
***********************

Der Operationen Kontroller wird benötigt, da die **QMLUI** ein *QtCore.Signal()* an den Kontroller schickt, wenn jedoch zum Beispiel die Daten aus der grafischen Benutzeroberfläche ausgelesen werden soll (Beispiel aus Eingabefeldern), so muss diese Operation erst durch den *Python* Unterbau gestartet werden.

Nach dem Ablauf *2: Methode_aufrufen* geht ein Signal zurück an die **QMLUI**, welches diese veranlasst die Informationen in Variablen zu schreiben. Bei diesem Vorgang tritt folgendes Problem auf: Wenn der *Python* Unterbau nicht auf ein Signal der **QMLUI** warten würde, würde es die Variablen auslesen, bevor die **QMLUI** diese neue gesetzt hat.

.. image:: Kontroller/operationen_kontroller.png

Bemerkung
**********

Die Lösung der Kontroller ist suboptimal, jedoch konnte bislang keine bessere Lösung gefunden werden, welche mit dem momentanen Aufbau & Struktur von **FVS** kompatibel ist.

Benutzerbereiche
-----------------

**FVS** ist in 5 Benutzerbereiche aufgeteilt:

- Admin
- Inventarist
- Kommandant
- Maschinist
- Sekretär

Admin
^^^^^^

Der *Administrator* hat keinen direkten Zugang zu den anderen Benutzerbereichen, jedoch hat er die Möglichkeit die Tabellen der Datenbank zu kontrollieren/optimieren/reparieren. [#prob]_

Inventarist
^^^^^^^^^^^^

Der *Inventarist* kümmert sich um die Verwaltung des Inventars der Feuerwehr. Der Inventarist kontrolliert den Bestand der Gegenstände und deren Zustand.

Kommandant
^^^^^^^^^^^

Der *Kommandant* kümmert sich um die Verwaltung der Mitglieder. Außerdem hat der *Kommandant* als Leiter der Feuerwehr zugang zu allen anderen Benutzerbereichen mit Ausnahme des *Admin* Bereichs.

Maschinist
^^^^^^^^^^^

Der *Maschinist* kümmert sich um die Fahrzeuge der Feuerwehr und kontrolliert regelmäßig deren Ausstattung und die Datumen derer Kontrollen.

Sekretär
^^^^^^^^^

Der *Sekretär* kümmert sich um die Verwaltung der Übungen, Einsatzberichten und die Protokollführung, in welchem Referenzen über Fahrzeuge, Mitglieder, Ereignisse oder Inventargegenstände gesetzt werden können, um so besser die Übersicht zu behalten, von was in einem Protokoll die Rede ist.

Login Informationen
--------------------

+------------------------+----------------+-------------------------+
| Benutzername           | Passwort       | Benutzerbereich         |
+========================+================+=========================+
| admin                  | admin          | Administrator           |
+------------------------+----------------+-------------------------+
| inventarist            | inventarist    | Inventarist             |
+------------------------+----------------+-------------------------+
| kommandant             | kommandant     | Inventarist, Kommandant |
|                        |                | Maschinist, Sekretär    |
+------------------------+----------------+-------------------------+
| maschinist             | maschinist     | Maschinist              |
+------------------------+----------------+-------------------------+
| sekretaer              | sekretaer      | Sekretär                |
+------------------------+----------------+-------------------------+


.. rubric:: Footnotes

.. [#] http://en.wikipedia.org/wiki/Enhanced_entity%E2%80%93relationship_model
.. [#prob] Die verwendete Version der Datenbank und Storage Engines sind nicht kompatibel mit diesen Funktionen