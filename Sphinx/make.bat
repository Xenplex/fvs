@ECHO OFF

REM Ritchie Flick
REM T3IFAN 2012/2013 Abschlussprojekt
REM LN
REM 'Feuerwehrverwaltungssoftware - FVS'
REM './Sphinx/make.bat'

REM **Beschreibung**

REM Batch Datei zur Erstellung der Dokumentation mit *Sphinx*
REM auf Windows Systemen.


if "%SPHINXBUILD%" == "" (
	set SPHINXBUILD=sphinx-build -D copyright=%2 -D release=%3
)
set BUILDDIR=..\Dokumentation
set ALLSPHINXOPTS=-d %BUILDDIR%/doctrees %SPHINXOPTS% source
set I18NSPHINXOPTS=%SPHINXOPTS% source

if "%1" == "html" (
	%SPHINXBUILD% -b html %ALLSPHINXOPTS% %BUILDDIR%/html
	if errorlevel 1 exit /b 1
	echo.
	echo.Erstellung abgeschlossen. HTML Seiten zu finden in %BUILDDIR%/html
	goto end
)

if "%1" == "text" (
	%SPHINXBUILD% -b text %ALLSPHINXOPTS% %BUILDDIR%/text
	if errorlevel 1 exit /b 1
	echo.
	echo.Erstellung abgeschlossen. Text Dateien zu finden in %BUILDDIR%/text
	goto end
)

:end
