# -*- coding: utf-8 -*-

""" Ritchie Flick
T3IFAN 2012/2013 Abschlussprojekt
LN
'Feuerwehrverwaltungssoftware - FVS'
'Sphinx/source/conf.py'

**Beschreibung**

Grundeinstellungen für *Sphinx*
"""

import sys
import os

# src Verzeichnis zum System Pfad hinzufügen
sys.path.insert(0, os.path.abspath('../../src'))

# Sphinx Erweiterungen
extensions = ['sphinx.ext.autodoc']

# Suffix der Dokumentdateien
source_suffix = '.rst'

# Enkodierung der Dokumentdateien
source_encoding = 'utf-8-sig'

# Dateiname der "master" Datei
master_doc = 'index'

# Allgemeine Projetinformationen
project = u'FVS'
copyright = u'2013, Ritchie Flick'

# Kurze Version
version = '0.1'
# Komplette Version
release = '0.1.1'

# Sprachen
language = 'de'

# Zu verwendeten Pygments
pygments_style = 'sphinx'


# HTML Ausgabe Einstellungen

# 'Default theme' verwenden
html_theme = 'default'

# Pfad für zum Beispiell *.css Dateien
html_static_path = ['_static']

# HTML Help
htmlhelp_basename = 'FVSdoc'


# LaTex Ausgabe Einstellungen

latex_elements = {
# The paper size ('letterpaper' or 'a4paper').
#'papersize': 'letterpaper',

# The font size ('10pt', '11pt' or '12pt').
#'pointsize': '10pt',

# Additional stuff for the LaTeX preamble.
#'preamble': '',
}

latex_documents = [
  ('index', 'FVS.tex', u'FVS Dokumentation',
   u'Ritchie Flick', 'manual'),
]
