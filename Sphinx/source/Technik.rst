Technik
=========

Verwendete Technologien
-------------------------

**FVS** verwendet folgende Technologien:

- Python [#]_
- Qt & QML [#]_
- PySide [#]_
- MySQL [#]_

Python
^^^^^^^

*Python* ist eine höhere Programmier- bzw. Skriptsprache und *Open Source*. Es ist eine multiparadimatische und dynamische Programmiersprache und ist 1991 erstmals erschienen.
Obwohl *Python* mehrere Programmierparadigma unterstützt, wird in **FVS** vorrangig die objektorientierte Programmierparadigma verwendet.

*Python* wurde als Programmiersprache gewählt da sie einfach zu lernen, eine klare Syntax und viele Standard Bibliotheken von Haus liefert. Desweiteren wird *Python* ständig weiter entwickelt und verbessert und besitzt eine aktive Community. *Python* ist außerdem platformunabhängig, was die Verwendung von **FVS** auf den meisten Betriebssystemen möglich macht.

Qt & QML
^^^^^^^^^

*Qt* (aussgesprochen eng. **cute**) ist eine plattformunabhängige Bibliothek/Framework für grafische Benutzeroberflächen. *Qt* wird meistens in Verbindung mit der Programmiersprache *C++* verwendet, jedoch kann es auch mit anderen Programmiersprachen, über sogenannte Sprachanbindungen [#]_ [#]_, verwendet werden.

Wenn das Produkt welches mit *Qt* entwickelt wird, nicht unter einen freien Lizenz steht, kann wahlweise eine kommerzielle Lizenz erworben werden. Wird jedoch eine freie Lizenz für das Projekt verwendet, kann *Qt* außerdem unter einen freien Lizenz verwendet werden.

*QML* ist eine Entwicklung von *Qt* zur Entwicklung von grafischen Benutzeroberflächen, welche vor allem *Touch-Screen* freundlich sind. Weiterhin ermöglich *QML* sehr viele Möglichkeiten zum Aufbau der grafischen Benutzeroberfläche. Die GUI lässt sich beinahe wie eine HTML&CSS Seite frei gestalten und kann somit genau so viele visuelle Effekte enthalten.

*Qt* und *QML* wurden ausgewählt, da es stark weiterentwickelt wird, es mit einer freien Lizenz verwendet werden kann und für spätere Versionen offizieller Support für *Android*, *iPhone* geplant ist (es gibt auch inoffizielle Ports [#]_), was die Benutzer **FVS** noch effektiver verwenden lässt. *QML* wird außerdem bei *Ubuntu Touch* [#]_ von Haus aus unterstützt.

PySide
^^^^^^^

*PySide* ist eine Sprachanbindung für *Python* und *Qt*. *PySide* wurde im Gegenstatz zu *PyQt* [#]_ verwendet, da *PySide* das verwenden einer freien Lizenz erlaubt.

Der Grund wieso *PySide* überhaupt gewählt wurde, ist in den ersten beiden Punkten für *Python* und *Qt* erklärt.

MySQL
^^^^^^

**FVS** benötigt für seine Arbeit eine Datenbank und *MySQL* ist eine der am weitesten verbreiteten *Open Source* Datenbanken.

Verwendete Bibliotheken
-------------------------

Im folgenden sollen Bibliotheken, welche in **FVS** verwendet wurden, jedoch nicht Teil der Standardbibliothek einer Sprache sind, aufgeslitet werden:

- PyMySQL: Ein purer *Python* *MySQL* Klient. Wird in **FVS** verwendet für die Verbindung mit der *MySQL* Datenbank. [#]_

Verwendete Code Ausschnitte
-----------------------------

Liste mit allen Code Ausschnitten welche über *copy/paste* in **FVS** eingebunden wurden:

- Javascript Funktion zum Kontrollieren eines Datums: http://www.qodo.co.uk/blog/javascript-checking-if-a-date-is-valid/
- Funktion für die **Memo** **QML_Komponente**, kopiert von http://doc-snapshot.qt-project.org/4.8/qml-textedit.html

Verwendete Software/Programme
------------------------------

Während der Arbeit an **FVS** wurden verschiedene Programme verwendet:


- *Sublime Text 2* ist ein moderner, einfacher Text Editor für Programmierer. [#]_ Wurde zur Erstellung der *Python* Dateien verwendet.
- *Qt Creator* ist eine *IDE*, speziell für das *Qt Framework*. [#]_ Wurde zur Erstellung der *QML* Dateien verwendet.
- Zur Verwaltung des Programmcodes von **FVS** wurde *git* [#]_ als *Versionsverwaltung* [#]_ und *bitbucket* [#]_ als Host des Programmcodes verwendet.
- *MySQL Workbench* [#]_ zur Erstellung/Bearbeitung/Verwaltung der *MySQL* Datenbank
- *Umbrello UML Modeller* [#]_ ist ein *KDE* [#]_ zur Erstellung von *UML* Diagrammen für den Aufbau der Datenbank und dem allgemeinen Aufbau von **FVS**.
- *ReText* [#]_ zum Schreiben der *ReStructuredText* [#]_ Datein für die Dokumentation von **FVS**.
- *cxFreeze* [#]_ ist eine Zusammenstellung von Skripten zum *Einfrieren* von Python Anwendungen in ausführbare Dateien.
- *Shutter 0.89* [#]_ zum Erstellen und Bearbeiten der Screenshots für das Benutzerhandbuch

.. rubric:: Footnotes

.. [#] http://python.org/
.. [#] http://qt-project.org/
.. [#] http://qt-project.org/wiki/Category:LanguageBindings::PySide
.. [#] http://www.mysql.com/
.. [#] http://de.wikipedia.org/wiki/Sprachanbindung
.. [#] http://en.wikipedia.org/wiki/Wrapper_library
.. [#] http://thp.io/2011/pyside-android/
.. [#] http://developer.ubuntu.com/get-started/gomobile/
.. [#] http://www.riverbankcomputing.com/software/pyqt/intro
.. [#] https://github.com/petehunt/PyMySQL
.. [#] http://www.sublimetext.com/
.. [#] http://qt-project.org/wiki/Category:Tools::QtCreator
.. [#] http://git-scm.com/
.. [#] http://de.wikipedia.org/wiki/Versionsverwaltung
.. [#] https://bitbucket.org/
.. [#] http://www.mysql.com/products/workbench/
.. [#] http://uml.sourceforge.net/
.. [#] http://www.kde.org/
.. [#] http://sourceforge.net/p/retext/home/ReText/
.. [#] http://docutils.sourceforge.net/rst.html
.. [#] http://cx-freeze.sourceforge.net/
.. [#] http://shutter-project.org/
