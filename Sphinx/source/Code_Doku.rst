.. _programmcode-dokumentation:

Programmcode Dokumentation
===========================

.. toctree::
   :maxdepth: 10

   src
   Daten_Modelle
   Daten_Objekte
   Datenbank
   Konfiguration
   Kontroller
   Logik
   Berichte
   QMLUI
   QML_Komponenten
   UI_Aufbau