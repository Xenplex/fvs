Allgemeines
============

Grundlegende Informationen
----------------------------

**FVS** ist als Teil einer Abschlussarbeit *(Projet de fin d’étude – Formation du Technicien en Informatique)* der Klasse T3IFAN 2012/2013 in Wiltz, Luxemburg entstanden.

- **Author** Ritchie FLICK
- **Klasse** T3IFAN 2012/2013
- **Schule** LN - Lycée du Nord
- **Projektname** *Feuerwehrverwaltungssoftware - FVS*

Begriffserklärungen
---------------------

- **FVS**: **Feuerwehrverwaltungssoftware** = Der Name dieses Projektes
- *Modul*: Ein Modul in *Python* ist im allgemeinen eine Datei welche nicht als eigenständiges Programm aufgerufen wird, sondern in ein Programm eingebunden wird. Es kann ausführbare Anweisungen, als auch Klassen- oder Funktiondefinitionen enthalten. [#]_
- *Pakete*: Pakete werden dazu verwendet, den Namensraum um *Python* *Module* zu strukturieren, damit muss der Autor sich keine Sorgen um mögliche gleiche Modulnamen oder globale Variablennamen zu machen. [#]_

Anforderungen
--------------

Betriebssysteme
^^^^^^^^^^^^^^^^

**FVS** wurde getestet auf *Windows 7* [#]_ von *Microsoft* [#]_ und auf *Kubuntu 12.10* [#]_ von *Canonical* [#]_. **FVS** ist somit kompatibel und lauffähig auf *Windows* als auch auf den meisten *Linux* Distributionen.

Bermerkung
***********

Obwohl **FVS** theoretisch auch auf *Win 7* oder *Ubuntu* Touch Tablets/Laptops lauffähig ist, wurde dies jedoch noch nicht getestet und es kann keine Garantie auf eine problem freie Benutzung auf solche Geräte gegeben werden.

Datenbank
^^^^^^^^^^

**FVS** ist nur kompatibel mit einer *MySQL* [#]_ Datenbank von *Oracle* [#]_. Es wurden keine weiteren Datenbanken getestet, weder noch im Programm selbst eingebaut.

Python & PySide
^^^^^^^^^^^^^^^^

**FVS** wurde mit *Python 3.2* und *PySide 1.1.1* programmiert. Es kann keine Garantie für die Komptabilität mit anderen Versionen gegeben werden. [#komp1]_

.. _qtqml:

Qt & QML
^^^^^^^^^

**FVS** wurde mit *Qt 4.8* und *QML 1.1* programmiert. Es kann keine Garantie für die Komptabilität mit anderen Versionen gegeben werden. [#komp2]_


.. rubric:: Footnotes

.. [#] Weitere Informationen zu *Modulen* zu finden auf: http://tutorial.pocoo.org/modules.html
.. [#] Weitere Informationen zu *Paketen* zu finden auf: http://tutorial.pocoo.org/modules.html#pakete
.. [#] http://windows.microsoft.com/en-us/windows7/products/home
.. [#] http://www.microsoft.com/
.. [#] http://www.ubuntu.com/
.. [#] http://www.canonical.com/
.. [#] http://www.mysql.com/
.. [#] http://www.oracle.com/index.html
.. [#komp1] Einschränkungen sind nur für die unkompilierte Version von **FVS** zu erwarten. Die kompilierte Version von **FVS** hat keine externen Abhängigkeiten.
.. [#komp2] Einschränkungen sind nur für die unkompilierte Version von **FVS** zu erwarten. Die kompilierte Version von **FVS** hat keine externen Abhängigkeiten.