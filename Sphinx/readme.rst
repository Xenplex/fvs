Sphinx/
========

Das Verzeichnis *Sphinx/* enthält alle Dateien welche Sphinx [#]_ benötigt um die Dokumentation für **FVS** generieren zu können.

Makefile
---------

Die *Makefile* Datei dient zum erstellen der **FVS** Dokumentation auf *Linux* Systemen::

    SPHINXOPTS    =
    SPHINXBUILD   = sphinx-build -D copyright="$(name)" -D release="$(version)"
    PAPER         =
    BUILDDIR      = ../Dokumentation

    PAPEROPT_a4     = -D latex_paper_size=a4
    PAPEROPT_letter = -D latex_paper_size=letter
    ALLSPHINXOPTS   = -d $(BUILDDIR)/doctrees $(PAPEROPT_$(PAPER)) $(SPHINXOPTS) source
    I18NSPHINXOPTS  = $(PAPEROPT_$(PAPER)) $(SPHINXOPTS) source

    .PHONY: html latex latexpdf

    html:
        $(SPHINXBUILD) -b html $(ALLSPHINXOPTS) $(BUILDDIR)/html
        @echo
        @echo "Erstellung abgeschlossen. HTML Seiten zu finden in $(BUILDDIR)/html"

    latex:
        $(SPHINXBUILD) -b latex $(ALLSPHINXOPTS) $(BUILDDIR)/latex
        @echo
        @echo "Erstellung abgeschlossen; LaTeX Dateien in $(BUILDDIR)/latex."

    latexpdf:
        $(SPHINXBUILD) -b latex $(ALLSPHINXOPTS) $(BUILDDIR)/latex
        @echo "LaTeX Dateien werden gesendet an pdflatex..."
        $(MAKE) -C $(BUILDDIR)/latex all-pdf
        @echo "pdflatex abgeschlossen; PDF Datei in $(BUILDDIR)/latex."

make.bat
---------

Die *Batch* Datei zum erstellen der **FVS** Dokumentation auf *Windows* Systemen::

    if "%SPHINXBUILD%" == "" (
        set SPHINXBUILD=sphinx-build -D copyright=%2 -D release=%3
    )
    set BUILDDIR=..\Dokumentation
    set ALLSPHINXOPTS=-d %BUILDDIR%/doctrees %SPHINXOPTS% source
    set I18NSPHINXOPTS=%SPHINXOPTS% source

    if "%1" == "html" (
        %SPHINXBUILD% -b html %ALLSPHINXOPTS% %BUILDDIR%/html
        if errorlevel 1 exit /b 1
        echo.
        echo.Erstellung abgeschlossen. HTML Seiten zu finden in %BUILDDIR%/html
        goto end
    )

    if "%1" == "text" (
        %SPHINXBUILD% -b text %ALLSPHINXOPTS% %BUILDDIR%/text
        if errorlevel 1 exit /b 1
        echo.
        echo.Erstellung abgeschlossen. Text Dateien zu finden in %BUILDDIR%/text
        goto end
    )

    :end

Sphinx/source/
---------------

Enthält alle Dateien welche den Aufbau der Dokumentation festlegen und weitere Dokumentation zu verschiedenen Aspekten von **FVS**.


.. rubric:: Footnotes

.. [#] Sphinx ist ein Software-Dokumentationswerkzeug welche aus reStructuredText Dateien Dokumentationen als HTML Webseiten, PDF Dokumente und weitere mögliche Formate umwandelt. Weitere Informationen zu finden auf: http://sphinx-doc.org/