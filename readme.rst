Einleitung
===========

Aktuellste Programmversionen
-----------------------------

Die aktuellsten Programmversionen für *Windows* und *Linux* sind zu finden auf:

https://bitbucket.org/Xenplex/fvs/downloads

Technische Dokumentation
---------------------------------

Die aktuellste Version dieser Dokumentation ist zu finden auf:

http://158.64.96.130/~reserve/

Oder als Download unter:
https://bitbucket.org/Xenplex/fvs/downloads

Benutzerhandbuch
-----------------

Die aktuellste Version des Benutzerhandbuches ist zu finden auf:

https://bitbucket.org/Xenplex/fvs/downloads

Programmcode Repository
------------------------

Die aktuellste Version des Projektes ist zu finden auf:

http://bitbucket.org/Xenplex/fvs

Aufgabestellung von FVS
------------------------

Die Feuerwehrverwaltungssoftware oder kurz **FVS** ist ein Desktopprogramm, geschrieben in *QML* [#]_ und *Python PySide* [#]_, zur Verwaltung einer Feuerwehreinsatzstelle.
**FVS** ist aus folgenden *Benutzerbereichen* aufgebaut:

- Administrator
- Kommandant
- Sekretär
- Inventarist
- Maschinist

**FVS** hat sich zum Ziel gesetzt die Aufgaben dieser Bereiche für die jeweiligen Mitglieder zu vereinfachen und somit die Übersicht und Zusammenarbeit zu verbessern.

Die Benutzer von **FVS** erhalten die Möglichkeit automatisch Meldungen zu erhalten, wann zum Beispiel die *TÜV* Kontrolle eines Fahrzeugs abgelaufen ist, Inventargegenstände zu verwalten und Berichte erstellen zu können.

Kurzinformationen über eine Feuerwehreinsatzstelle
---------------------------------------------------

Um die Aufgabestellung von **FVS** besser zu verstehen, soll hier eine kurze Übersicht über den allgemeinen Aufbau einer Feuerwehreinsatzstelle gegeben werden. [#]_

Eine Feuerwehreinsatzstelle wird von Mitgliedern in unterschiedlichen Positionen und Aufgaben verwaltet.

Der *Kommandant* als Leiter der Einsatzstelle kümmert sich vor allem um die Verwaltung der einzelnen Mitglieder und soll eine Übersicht über alle Bereiche haben.

Der *Sekretär* kümmert sich um organisatorische Aufgaben, wie zum Beispiel der Aufbau des Übungsplans, der Führung von Protokollen während einer Versammlung und dem Ausfüllen der Einsatzberichte.

Der *Inventarist* kümmert sich um den gesamten Inventar einer Feuerwehreinsatzstelle und muss sich um mögliche beschädigte Inventargegenstände kümmern und den Überblick behalten wo die einzelnen Inventargegenstände verwendet werden, damit bei Bedarf die jeweiligen Gegenstände wieder gefunden werden können.

Der *Maschinist* kümmert sich um die Fahrzeuge einer Feuerwehreinsatzstelle und stellt sicher dass alle Fahrzeuge die benötigte Ausstattung besitzen und die einzelnen Kontrollen (z.Bsp: *TÜV*) durchgeführt wurden.

Release Skripte
----------------

Linux Release Skript
^^^^^^^^^^^^^^^^^^^^^

Das **Linux Release Skript** dient zur Automatisierung verschiedener Aufgaben auf *Linux* Systemen.
Ein Ausschnitt aus der Dokumentation von **fvs_linux_release.sh** [#]_:

    **fvs_linux_release.sh** ist ein *Bash* Skript zur Kompilation von
    **FVS - Feuerwehrverwaltungssoftware**, der Generierung seiner
    Dokumentation mit *Sphinx* und weiteren Funktionen.

    Das Skript lässt den Entwickler eine *fvs_release_konfig.cfg* Datei erstellen
    als Konfigurationsdatei, in dem die aktuelle Versionsnummer von **FVS**
    angegeben wird, der Name des Entwicklers und gegebenfalls in welches
    Verzeichnis das Projekt *exportiert* werden soll.

    **FVS** wird mit der angegebenen Versionsnummer und Entwickler Name
    kompiliert und ins entsprechende **Release** Verzeichnis
    kopiert. Die Bibliotheken werden in ein verstecktes Verzeichnis kopiert,
    damit der Benutzer keine für ihn unnötigen Dateien sieht, um die Benutzer-
    freundlichkeit zu erhöhen.

    Die Dokumentation von **FVS** wird mit der angegebenen Versionsnummer
    und dem Namen des Entwicklers neu generiert und ins aktuelle
    **Release** Verzeichnis von **FVS** kopiert.

    Die benötigten *SQL* Skripte werden in das aktuelle **Release** Verzeichnis
    kopiert um dem Entwickler/Benutzer die Einrchtung der Datenbank und/oder das
    Entwickeln/Testen von **FVS** zu erleichtern.

    Das **Release** Verzeichnis kann optional *exportiert* werden in ein
    gewünschte Verzeichnis. (z.Bsp.: *Dropbox*)

    Des weiteren kann **FVS** mit *git* und der angegebenen Versionsnummer
    *getagt* und anschließend automatisch ins eingerichtete
    *git repository* *gepusht* werden. (z.Bsp.: *Github*, *Bitbucket*)

Windows Release Skript
^^^^^^^^^^^^^^^^^^^^^^^

Das **Windows Release Skript** dient zur Automatisierung verschiedener Aufgaben auf *Windows* Systemen. Ein Ausschnitt aus der Dokumentation von **fvs_win_release.ps1** [#]_:

    **fvs_win_release.ps1** ist ein *Powershell* Skript zur Kompilation von
    **FVS - Feuerwehrverwaltungssoftware**, der Generierung seiner
    Dokumentation mit *Sphinx* und weiteren Funktionen.

    Das Skript lässt den Entwickler eine *XML* Datei erstellen als
    Konfigurationsdatei, in dem die aktuelle Versionsnummer von **FVS**
    angegeben wird, der Name des Entwicklers und gegebenfalls in welches
    Verzeichnis das Projekt *exportiert* werden soll.

    **FVS** wird mit der angegebenen Versionsnummer und Entwickler Name
    kompiliert und ins entsprechende **Release** Verzeichnis
    kopiert. Die Bibliotheken erhalten das Attribut *hidden*, damit der
    Endbenutzer nur die für ihn wichtigen Dateien sieht, zur Erhöhung
    Benutzerfreundlichkeit.

    Die Dokumentation von **FVS** wird mit der angegebenen Versionsnummer
    und dem Namen des Entwicklers neu generiert und ins aktuelle
    **Release** Verzeichnis von **FVS** kopiert.

    Die benötigten *SQL* Skripte werden in das aktuelle **Release** kopiert
    um dem Entwickler/Benutzer die Einrchtung der Datenbank und/oder das
    Entwickeln/Testen von **FVS** zu erleichtern.

    Das **Release** Verzeichnis kann optional *exportiert* werden in ein
    gewünschte Verzeichnis. (z.Bsp.: *Dropbox*)

    Des weiteren kann **FVS** mit *git* und der angegebenen Versionsnummer
    *getagt* und anschließend automatisch ins eingerichtete
    *git repository* *gepusht* werden. (z.Bsp.: *Github*, *Bitbucket*)

Erstellen von Berichten
------------------------

**FVS** generiert seine Berichte als *HTML & CSS* Seiten. Dies ermöglicht dem Benutzer von **FVS** seine Berichte mit *CSS* Kenntnissen seinen Bedürfnissen anzupassen und wenn gewünscht auf seiner Webseite zu präsentieren.

Die Berichte können dann über die Print Funktion des Browsers auf Wunsch ausgedruckt werden.

Die Berichte werden in das Verzeichnis *Berichte/* abgelegt, in der auch die *fvs.css* Datei liegt, welche ein erfahrener Benutzer verwenden kann um die Berichte anzupassen.

Die Berichte werden im folgendem Format abgespeichert:
**bericht<benutzer_name><datum_erstellung>**


.. rubric:: Footnotes

.. [#] *Qt Meta Language*: Bestandteil von *Qt* und dient zum Aufbau von grafischen Benutzeroberflächen
.. [#] *Qt* Anbindungen für *Python*: http://qt-project.org/wiki/PySide
.. [#] Weitere Informationen zu finden auf: http://de.wikipedia.org/wiki/Feuerwehr
.. [#] http://en.wikipedia.org/wiki/Bash_(Unix_shell)
.. [#] http://en.wikipedia.org/wiki/Windows_PowerShell