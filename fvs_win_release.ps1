﻿<#
    .SYNOPSIS
        Release Skript für **FVS** auf Windows System

    .DESCRIPTION
        'fvs_win_release.ps1' ist ein *Powershell* Skript zur Kompilation von
        **FVS - Feuerwehrverwaltungssoftware**, der Generierung seiner
        Dokumentation mit *Sphinx* und weiteren Funktionen.

        Das Skript lässt den Entwickler eine *XML* Datei erstellen als
        Konfigurationsdatei, in dem die aktuelle Versionsnummer von **FVS**
        angegeben wird, der Name des Entwicklers und gegebenfalls in welches
        Verzeichnis das Projekt *exportiert* werden soll.

        **FVS** wird mit der angegebenen Versionsnummer und Entwickler Name
        kompiliert und ins entsprechende **Release** Verzeichnis
        kopiert. Die Bibliotheken erhalten das Attribut *hidden*, damit der
        Endbenutzer nur die für ihn wichtigen Dateien sieht, zur Erhöhung
        Benutzerfreundlichkeit.

        Die Dokumentation von **FVS** wird mit der angegebenen Versionsnummer
        und dem Namen des Entwicklers neu generiert und ins aktuelle
        **Release** Verzeichnis von **FVS** kopiert.

        Die benötigten *SQL* Skripte werden in das aktuelle **Release** kopiert
        um dem Entwickler/Benutzer die Einrchtung der Datenbank und/oder das
        Entwickeln/Testen von **FVS** zu erleichtern.

        Das **Release** Verzeichnis kann optional *exportiert* werden in ein
        gewünschte Verzeichnis. (z.Bsp.: *Dropbox*)

        Des weiteren kann **FVS** mit *git* und der angegebenen Versionsnummer
        *getagt* und anschließend automatisch ins eingerichtete
        *git repository* *gepusht* werden. (z.Bsp.: *Github*, *Bitbucket*)

    .PARAMETER Komplett
        Alle Funktionen von 'fvs_win_release.ps1' werden aufgerufen.

    .PARAMETER KonfigErstellung
        Ist der Parameter **KonfigErstellung** gesetzt, so wird nur die
        Funktion zur Erstellung der Konfigurationsdatei ausgeführt.

    .PARAMETER Dokumentation
        Ist der Parameter **Dokumentation** gesetzt, so wird nur die Funktion
        zur Erstellung der Dokumentation ausgeführt.

    .PARAMETER Kompilieren
        Ist der Parameter **Kompilieren** gesetzt, so wird **FVS** ins Release
        Verzeichnis kompiliert.

    .PARAMETER SQLSkripte
        Ist der Parameter **SQLSkripte** gesetzt, wird die Funktion zum
        kopieren der SQL Skripte ins aktuelle **Release** Verzeichnis
        aufgerufen.

    .PARAMETER Export
        Ist der Parameter **Export** gesetzt, wird das Release Verzeichnis ins
        gewünschte Verzeichnis verschoben.

    .PARAMETER GitExport
        Ist der Parameter **GitExport** gesetzt, wird **FVS** mit Hilfe von
        *git* exportiert.

    .PARAMETER Aufrufen
        **FVS** wird am Ende aufgerufen. Das Programm wird gestartet.

    .EXAMPLE
        fvs_win_release.ps1 -Komplett

        Alle Funktionen werden aufgerufen.

    .EXAMPLE
        fvs_win_release.ps1 -<param1> -<param2> -<param3>

        Gewünschte einzel Funktionen als Parameter übergeben

    .NOTES
        Ritchie Flick
        T3IFAN 2012/2013 Abschlussprojekt
        LN
        'Feuerwehrverwaltungssoftware - FVS'
        'fvs_win_release.ps1'

    .LINK
        Projekt Repository: https://bitbucket.org/Xenplex/fvs
#>

[CmdletBinding(HelpUri = 'http://www.bitbucket.org/Xenplex/fvs')]

# Kommandozeilenparameter
Param(
    [Parameter(Mandatory=$false,
               ValueFromPipeline=$false,
               ValueFromPipelineByPropertyName=$false, 
               ValueFromRemainingArguments=$false)]
    [switch]
    $Komplett,

    [Parameter(Mandatory=$false,
               ValueFromPipeline=$false,
               ValueFromPipelineByPropertyName=$false, 
               ValueFromRemainingArguments=$false)]
    [switch]
    $KonfigErstellung,

    [Parameter(Mandatory=$false,
               ValueFromPipeline=$false,
               ValueFromPipelineByPropertyName=$false, 
               ValueFromRemainingArguments=$false)]
    [switch]
    $Dokumentation,

    [Parameter(Mandatory=$false,
               ValueFromPipeline=$false,
               ValueFromPipelineByPropertyName=$false, 
               ValueFromRemainingArguments=$false)]
    [switch]
    $Kompilieren,

    [Parameter(Mandatory=$false,
               ValueFromPipeline=$false,
               ValueFromPipelineByPropertyName=$false, 
               ValueFromRemainingArguments=$false)]
    [switch]
    $SQLSkripte,

    [Parameter(Mandatory=$false,
               ValueFromPipeline=$false,
               ValueFromPipelineByPropertyName=$false, 
               ValueFromRemainingArguments=$false)]
    [switch]
    $Export,

    [Parameter(Mandatory=$false,
               ValueFromPipeline=$false,
               ValueFromPipelineByPropertyName=$false, 
               ValueFromRemainingArguments=$false)]
    [switch]
    $GitExport,

    [Parameter(Mandatory=$false,
               ValueFromPipeline=$false,
               ValueFromPipelineByPropertyName=$false, 
               ValueFromRemainingArguments=$false)]
    [switch]
    $Aufrufen
)


<#
    Kontrolliert ob die Konfigurationsdatei von **FVS** existiert oder nicht.

    **Beschreibung**

    Wenn die Konfigurationsdatei nicht existiert wird der Entwickler
    aufgefordert anzugeben ob er eine Konfigurationsdatei erstellen möchte
    oder nicht.

    Soll eine Konfigurationsdatei erstellt werden, wird die benötigte Funktion
    aufgerufen.
    Wenn keine Konfigurationsdatei erstellt werden soll, so wird das Skript
    beendet.
#>
function Kontrolliere-FVS_Konfig()
{
    if (-not (Test-Path .\fvs_release_konfig.xml)) {
    do
    {
        Write-Host "Es ist noch keine " -NoNewline
        Write-Host "fvs_release_konfig.xml " -ForegroundColor Cyan -NoNewline
        Write-Host "Datei enthalten"
        Write-Host "Konfigurationsdatei erstellen?[" -NoNewline
        Write-Host "y" -ForegroundColor DarkGreen -NoNewline
        Write-Host "/" -NoNewLine
        Write-Host "n" -ForegroundColor DarkRed -NoNewline
        Write-Host "]: " -NoNewline
        $antwort = Read-Host
        if (($antwort -notmatch "y") -and ($antwort -notmatch "n")) {
            Write-Host "Bitte nur y oder n angeben!" -ForegroundColor Red
        }
    }
    while (($antwort -notmatch "y") -and ($antwort -notmatch "n"))
    if ($antwort -match "n") {
        Clear-Host
        Write-Host "Achtung, es wird eine
        Konfig Datei benötigt!
        Das Skript wird beendet..." -ForegroundColor Red -NoNewline
        Exit
    }
    else {
        ErstelleKonfig
    }
    } 
}


<#
    Erstellung der Konfigurationsdatei

    **Beschreibung**

    Der Entwickler wird aufgefordert verschiedene Parameter anzugeben, mit
    welchen die *XML* Konfigurationsdatei ins *root* Verzeichnis von **FVS**
    erstellt wird.
#>
function ErstelleKonfig()
{
    $name = Read-Host -Prompt "Bitte geben sie den Entwickler Namen ein"
    $version = Read-Host -Prompt "Bitte geben sie aktuelle Versionsnummer an"
    $export = Read-Host -Prompt "Bitte geben sie ein Verzeichnis zum Export an"
    $XMLDaten = 
@"
<FVSKonfig version="1.0">
    <Einstellungen>
        <EntwicklerName>"$name"</EntwicklerName>
        <FVSVersion>$version</FVSVersion>
        <Export>$export</Export>
    </Einstellungen>
</FVSKonfig>
"@

    try
    {
        $XMLDaten | Out-File -FilePath "fvs_release_konfig.xml" -Force
    }
    catch
    {
        [system.exception]
        Write-Host "Ein Fehler ist aufgetreten!" -ForegroundColor Red

        Write-Debug "XMLDaten Inhalt:"
        Write-Debug $XMLDaten
    }
}


<#
    Auslesen der Konfigurationsdatei

    **Beschreibung**

    Die abgespeicherten Parameter werden aus der *XML* Konfigurationsdatei
    ausgelesen.

    **Rückgabewerte**

    * $XMLDaten: Enthält die Einstellungen für das Release Skript im XML Format
#>
function AuslesenKonfig()
{
    try
    {
        [xml] $XMLDaten = Get-Content ".\fvs_release_konfig.xml"
    }
    catch
    {
        [system.exception]
        Write-Host "Ein Fehler ist aufgetreten!" -ForegroundColor Red
        $XMLDaten = "Auslesen der Konfig Datei fehlerhaft!"

        if ( -not (Test-Path ".\fvs_win_release.ps1"))
        {
            Write-Debug "Die Konfigurationsdatei exisitert nicht!"
        }
    }
    finally
    {
        $XMLDaten
    }
}


<#
    Erstellen der Release Verzeichnisse

    **Beschreibung**

    Alle benötigten Verzeichnisse zum Release werden, wenn noch nicht
    vorhanden, erstellt.

    Die Namen der Verzeichnisse hängen von der aktuellen Versionsnummer ab.
#>
function ReleaseVerzeichnisse()
{
    $einstellungen = AuslesenKonfig
    $version = $einstellungen.FVSKonfig.Einstellungen.FVSVersion

    if ( -not (Test-Path ".\Release")) {
        try
        {
            mkdir "Release" | Write-Verbose
        }
        catch
        {
            [system.exception]
            Write-Host "Ein Fehler ist aufgetreten!" -ForegroundColor Red
        }
    }

    if ( -not (Test-Path ".\Release\Win$version")) {
        try
        {
            mkdir ".\Release\Win$version" | Write-Verbose
        }
        catch
        {
            [system.exception]
            Write-Host "Ein Fehler ist aufgetreten!" -ForegroundColor Red
        }
    }

    if ( -not (Test-Path ".\Release\Win$version\SQL")) {
        try
        {
            mkdir ".\Release\Win$version\SQL" | Write-Verbose
        }
        catch
        {
            [system.exception]
            Write-Host "Ein Fehler ist aufgetreten!" -ForegroundColor Red
        }
    }

    if ( -not (Test-Path ".\Release\Win$version\Dokumentation")) {
        try
        {
            mkdir ".\Release\Win$version\Dokumentation" | Write-Verbose
        }
        catch
        {
            [system.exception]
            Write-Host "Ein Fehler ist aufgetreten!" -ForegroundColor Red
        }
    }

    if ( -not (Test-Path ".\Release\Win$version\Berichte")) {
        try
        {
            mkdir ".\Release\Win$version\Berichte" | Write-Verbose
        }
        catch
        {
            [system.exception]
            Write-Host "Ein Fehler ist aufgetreten!" -ForegroundColor Red
        }
    }
}


<#
    Kompilation von **FVS**

    **Beschreibung**

    **FVS** wird mit Hilfe von *cx_freeze* zu einer EXE-Datei kompiliert und
    anschließend ins Release Verzeichnis seiner aktuellen Version
    verschoben.

    Alle Dateien welche zum Aufbau der *GUI* benötigt werden, als auch die
    Konfigurationsdatei zur Verbindungsherstellung zur Datenbank, werden
    außerdem ins Release Verzeichnis der aktuellen Version kopiert/erstellt.
#>
function Kompilieren()
{
    $einstellungen = AuslesenKonfig
    $version = $einstellungen.FVSKonfig.Einstellungen.FVSVersion
    
    # Kompilierung starten
    try
    {
        cd src
        python.exe setup.py build | Write-Verbose
        cd ..
    }
    catch
    {
        [system.exception]
        Write-Host "Ein Fehler ist aufgetreten!" -ForegroundColor Red
        Break
    }
    
    ReleaseVerzeichnisse
    
    try
    {
        Move-Item -Force ".\src\build\exe.win32-3.2\*" ".\Release\Win$version"
        Remove-Item -Recurse ".\src\build"
    }
    catch
    {
        [system.exception]
        Write-Host "Ein Fehler ist aufgetreten!" -ForegroundColor Red
    }

    try
    {
        # Integration der QML Komponenten
        Copy-Item -Recurse -Force ".\src\QML*" ".\Release\Win$version"
    }
    catch
    {
        [system.exception]
        Write-Host "Ein Fehler ist aufgetreten!" -ForegroundColor Red
    }

    try
    {
        # Erstellung der Standardkonfigurationsdatei
        if (Test-Path ".\Release\Win$version\konfig.cfg")
        {
            Remove-Item -Force ".\Release\Win$version\konfig.cfg"
        }

        New-Item -Force ".\Release\Win$version\konfig.cfg" -ItemType file |
        Write-Verbose

        "[Einstellungen]
        datenbankname = fvs_datenbank
        datenbankpasswort = fvspw
        datenbankadresse = 127.0.0.1
        datenbankport = 3307
        datenbankbenutzer = fvs
        " |
        Out-File ".\Release\Win$version\konfig.cfg" -Encoding ascii
    }
    catch
    {
        [system.exception]
        Write-Host "Ein Fehler ist aufgetreten!" -ForegroundColor Red
    }

    # Alle Bibliotheke und kompilierte Dateien Attribut setzen auf "hidden"
    Set-ItemProperty ".\Release\Win$version\*.pyd" -Name Attributes -Value "Hidden"
    Set-ItemProperty ".\Release\Win$version\*.dll" -Name Attributes -Value "Hidden"
    Set-ItemProperty ".\Release\Win$version\*.zip" -Name Attributes -Value "Hidden"
    Set-ItemProperty ".\Release\Win$version\*.cfg" -Name Attributes -Value "Hidden"
    Set-ItemProperty ".\Release\Win$version\QML*" -Name Attributes -Value "Hidden"
}


<#
    SQL Skripte werden kopiert

    **Beschreibung**

    Die SQL Skripte werden ins jeweilige **Release** Verzeichnis kopiert.
#>
function KopierenSQL()
{
    $einstellungen = AuslesenKonfig
    $version = $einstellungen.FVSKonfig.Einstellungen.FVSVersion

    ReleaseVerzeichnisse
    
    try
    {
        Copy-Item -Force ".\src\Datenbank\*.sql" ".\Release\Win$version\SQL"
    }
    catch
    {
        [system.exception]
        Write-Host "Ein Fehler ist aufgetreten!" -ForegroundColor Red
    }
}


# CSS Datei wird kopiert

# **Beschreibung**

# Die CSS Datei für die Berichte wird ins jeweilige **Release** Verzeichnis
# kopiert
function KopierenCSS()
{
    $einstellungen = AuslesenKonfig
    $version = $einstellungen.FVSKonfig.Einstellungen.FVSVersion

    ReleaseVerzeichnisse
    
    try
    {
        Copy-Item -Force ".\src\Berichte\fvs.css" ".\Release\Win$version\Berichte"
    }
    catch
    {
        [system.exception]
        Write-Host "Ein Fehler ist aufgetreten!" -ForegroundColor Red
    }   
}


<#
    Dokumentation erstellen

    **Beschreibung**

    Die Dokumentation von **FVS** wird mit *Sphinx* erstellt/generiert.

    Alle *readme.rst* Dateien der einzelnen Verzeichnisse werden ins *Source*
    Verzeichnis von *Sphinx* kopiert und gemäß des Namens des Verzeichnisses
    umbenannt.

    Die Dokumentation wird als *HTML* Webseite generiert und als *TXT* Datei
    generiert.

    Anschließend werden alle kopierten *.rst Dateien wieder aus dem Verzeichnis
    gelöscht und die Dokumentation wird ins aktuelle Release Verzeichnis
    kopiert.
#>
function ErstellenDokumentation()
{
    ReleaseVerzeichnisse
    
    $einstellungen = AuslesenKonfig
    $name = $einstellungen.FVSKonfig.Einstellungen.EntwicklerName
    $version = $einstellungen.FVSKonfig.Einstellungen.FVSVersion
    
    $dokumentation_dateien=(Get-ChildItem -Recurse ".\src\*.rst")
    $dokumentation_dateien | Write-Debug
    foreach ($datei in $dokumentation_dateien)
    {
        $neuer_name=$datei.DirectoryName | Split-Path -Leaf
        Copy-Item -Force $datei ".\Sphinx\source\$neuer_name.rst"
        Write-Debug "Datei Name $datei"
        Write-Debug "Neuer Datei Name $neuer_name"
    }

    Copy-Item ".\readme.rst" ".\Sphinx\source\Einleitung.rst"
    Copy-Item ".\Sphinx\readme.rst" ".\Sphinx\source\Sphinx.rst"
    Copy-Item ".\AUfbau\readme.rst" ".\Sphinx\source\Aufbau.rst"

    # Die Bilder müssen über diesen Weg importiert werden, damit im Online Repository
    # die Bilder in den readme.rst Dateien angezeigt werden und in der Erstellten
    # Dokumentation zu sehen sind
    mkdir ".\Sphinx\source\DatenbankMCD"
    mkdir ".\Sphinx\source\DatenbankMCD\datenbankdesign"
    mkdir ".\Sphinx\source\MySQLWorkbench"
    mkdir ".\Sphinx\source\Struktur"
    mkdir ".\Sphinx\source\Kontroller"
    Copy-Item ".\Aufbau\DatenbankMCD\datenbankdesign\fvs_datenbank_MCD.png" ".\Sphinx\source\DatenbankMCD\datenbankdesign\fvs_datenbank_MCD.png"
    Copy-Item ".\Aufbau\MySQLWorkbench\fvs_datenbank_eer.png" ".\Sphinx\source\MySQLWorkbench\fvs_datenbank_eer.png"
    Copy-Item ".\Aufbau\Struktur\struktur.png" ".\Sphinx\source\Struktur\struktur.png"
    Copy-Item ".\Aufbau\Kontroller\einfacher_kontroller.png" ".\Sphinx\source\Kontroller\einfacher_kontroller.png"
    Copy-Item ".\Aufbau\Kontroller\operationen_kontroller.png" ".\Sphinx\source\Kontroller\operationen_kontroller.png"

    cd ".\Sphinx"
    .\make.bat html $name $version | Write-Verbose
    .\make.bat text $name $version | Write-Verbose
    cd ..

    foreach ($datei in $dokumentation_dateien)
    {
        $neuer_name=$datei.DirectoryName | Split-Path -Leaf
        Remove-Item -Force ".\Sphinx\source\$neuer_name.rst"
    }

    Remove-Item -Force ".\Sphinx\source\Einleitung.rst"
    Remove-Item -Force ".\Sphinx\source\Sphinx.rst"
    Remove-Item -Force ".\Sphinx\source\Aufbau.rst"
    Remove-Item -Force -Recurse ".\Sphinx\source\DatenbankMCD"
    Remove-Item -Force -Recurse ".\Sphinx\source\MySQLWorkbench"
    Remove-Item -Force -Recurse ".\Sphinx\source\Struktur"
    Remove-Item -Force -Recurse ".\Sphinx\source\Kontroller"

    try
    {
        Copy-Item -Force -Recurse ".\Dokumentation\*" ".\Release\Win$version\Dokumentation"
    }
    catch
    {
        [system.exception]
        Write-Host "Ein Fehler ist aufgetreten!" -ForegroundColor Red
    }
}


<#
    Exportieren des Release Verzeichnisses

    **Beschreibung**

    Das aktuelle **Release** Verzeichnis wird in das gewünschte Verzeichnis auf
    dem selben PC exportiert.
#>
function Exportieren()
{
    ReleaseVerzeichnisse

    $einstellungen = AuslesenKonfig
    $version = $einstellungen.FVSKonfig.Einstellungen.FVSVersion
    $export = $einstellungen.FVSKonfig.Einstellungen.Export

    try
    {
        Copy-Item -Force -Recurse ".\Release\Win$version" $export
    }
    catch
    {
        [system.exception]
        Write-Host "Ein Fehler ist aufgetreten!" -ForegroundColor Red
    }
}


<#
    Exportieren der Git Repository

    **Beschreibung**

    Das Git Repository von **FVS** wird mit der aktuellen Versionsnummer
    *git tag* und anschließend mit *git push* veröffentlicht.
#>
function GitExportieren()
{
    $einstellungen = AuslesenKonfig
    $version = $einstellungen.FVSKonfig.Einstellungen.FVSVersion

    git tag $version | Write-Verbose
    git push --tags | Write-Verbose
    git push
}


<#
    FVS aufrufen

    **Beschreibung**

    Die kompilierte Version von **FVS** wird aufgerufen.
#>
function FVSStarten()
{
    $einstellungen = AuslesenKonfig
    $version = $einstellungen.FVSKonfig.Einstellungen.FVSVersion

    cd ".\Release\Win$version"
    .\fvs.exe
    cd ..\..\
}


<#
    Hauptausführung des Skriptes
#>
if ($Komplett) {
    Kontrolliere-FVS_Konfig
    ReleaseVerzeichnisse
    Kompilieren
    KopierenSQL
    KopierenCSS
    ErstellenDokumentation
    Exportieren
    GitExportieren
    FVSStarten
    Write-Host "Das Skript wurde komplett ausgeführt" -ForegroundColor Green
    Exit
}

if ($KonfigErstellung) {ErstelleKonfig}

if ($Dokumentation) {ErstellenDokumentation}

if ($Kompilieren) {Kompilieren}

if ($SQLSkripte) {KopierenSQL}

if ($Export) {Exportieren}

if ($GitExport) {GitExportieren}

if ($Aufrufen) {FVSStarten}

Exit
